<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Companies Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\CategoriesTable|\Cake\ORM\Association\BelongsTo $Categories
 *
 * @method \App\Model\Entity\Company get($primaryKey, $options = [])
 * @method \App\Model\Entity\Company newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Company[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Company|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Company patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Company[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Company findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CompaniesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('companies');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER'
        ]);

		$this->hasMany('Employees', [
            'foreignKey' => 'company_id',
            'dependent'  => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('CompanyImages', [
            'foreignKey' => 'company_id',
            'dependent'  => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('PaymentDetails', [
            'foreignKey' => 'company_id',
            'dependent'  => true,
            'cascadeCallbacks' => true
        ]);
		
		$this->hasMany('Booking', [
            'foreignKey' => 'company_id',
            'dependent'  => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('Schedules', [
            'foreignKey' => 'company_id',
            'dependent'  => true,
            'cascadeCallbacks' => true
        ]);
		
		$this->hasMany('CompanyCategory', [
            'foreignKey' => 'company_id',
            'dependent'  => true,
            'cascadeCallbacks' => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('company_name')
            ->maxLength('company_name', 45)
            ->allowEmpty('company_name');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 45)
            ->allowEmpty('first_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 45)
            ->allowEmpty('last_name');

        $validator
            ->scalar('about')
            ->allowEmpty('about');

        $validator
            ->scalar('address')
            ->maxLength('address', 125)
            ->allowEmpty('address');

        $validator
            ->scalar('city')
            ->maxLength('city', 45)
            ->allowEmpty('city');
			
		$validator
            ->scalar('state')
            ->maxLength('state', 50)
            ->allowEmpty('state');
			
		$validator
            ->scalar('country')
            ->maxLength('country', 50)
            ->allowEmpty('country');

        $validator
            ->scalar('zipcode')
            ->maxLength('zipcode', 45)
            ->allowEmpty('zipcode');

        $validator
            ->decimal('latitude')
            ->allowEmpty('latitude');

        $validator
            ->decimal('longitude')
            ->allowEmpty('longitude');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 16)
            ->allowEmpty('phone');

        $validator
            ->scalar('offer')
            ->allowEmpty('offer');
            
        $validator
            ->scalar('status')
            ->allowEmpty('status');

        $validator
            ->scalar('type')
            ->allowEmpty('type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {   
        //$rules->add($rules->isUnique(['phone']));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['category_id'], 'Categories'));

        return $rules;
    }
}
