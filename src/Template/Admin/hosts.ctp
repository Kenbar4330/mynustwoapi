<!-- Content Header (Page header) -->
<?php
$type = $company_name = $city = $zipcode = $cat = '';

if(isset($_REQUEST['type'])) { 
    $type = trim($_REQUEST['type']); 
}

if(isset($_REQUEST['company_name'])) { 
    $company_name = $_REQUEST['company_name']; 
}

if(isset($_REQUEST['city'])) { 
    $city = $_REQUEST['city']; 
}

if(isset($_REQUEST['zipcode'])) { 
    $zipcode = $_REQUEST['zipcode']; 
}

if(isset($_REQUEST['category']) && !empty($_REQUEST['category'])) { 
    $cat = $_REQUEST['category']; 
}
?>
<section class="content-header">
 <?php echo $this->Flash->render(); ?>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Hosts(Business) Listing</h3>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
	<div class="row well">
	    <div class="col-md-12 col-lg-12 margin-tb searchBoxCls">
	        <!-- Search form -->
	        <div class="row">
	            <form role="form" method="GET" action="">

	                <input type="hidden" name="type" value="<?php if(isset($type)){echo "$type"; } ?>">

	                <div class="col-md-2">
	                    <input type="text" class="form-control" name="company_name" placeholder="Business Name" value="<?php if(isset($company_name)){echo "$company_name"; } ?>">
	                </div>
					
					<div class="col-md-2">
	                    <input type="text" class="form-control" name="city" placeholder="City" value="<?php if(isset($city)){echo "$city"; } ?>">
	                </div>

	                <div class="col-md-2">
	                    <input type="text" class="form-control" name="zipcode" placeholder="Zip Code" value="<?php if(isset($zipcode)){echo "$zipcode"; } ?>">
	                </div>

	                <div class="col-md-2">
						<select class="form-control" name="category">
							<option value="">Select Category</option>
							<?php
							foreach($categories as $category):
							?>
								<option value="<?= $this->Number->format($category['id']) ?>" <?php if(isset($cat) && !empty($cat) && $cat==$category['id']){echo "selected";} else {echo "";} ?>><?= h($category['name']) ?></option>
							<?php
	                        endforeach
	                        ?>
	                    </select>
	                </div>            

	                <div class="col-md-1">
	                    <button class="btn btn-default btn-sm form-control" type="submit">
	                        <i class="fa fa-search"></i>
	                    </button>
	                </div>

	                <div class="col-md-1">
	                    <a class="btn btn-default btn-sm form-control pull-left" href="hosts"><strong>Reset</strong></a>
	                </div>
	                
	            </form>
	        </div>
	        <!-- End search form -->
	    </div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-body">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><?= $this->Paginator->sort('id', 'Id') ?></th>
								<th>Business Name</th>
								<th>Phone</th>
								<th>Category</th>
								<th>Type</th>
								<th>City</th>
								<th>Zipcode</th>
								<th>Empl.</th>
								<th>Actions</th>
								<th>Subscription</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($hosts as $host): ?>
							<tr>
								<td><?= $this->Number->format($host['id']) ?></td>
								<td><?= h($host['company']['company_name']) ?></td>
								<td><?= h($host['phone']) ?></td>
								<td><?= h($host['company']['category']['name']) ?></td>
								<td><?= h($host['company']['type']) ?></td>
								<td><?= h($host['company']['city']) ?></td>
								<td><?= h($host['company']['zipcode']) ?></td>
								<td><?= h(count($host['company']['employees'])) ?></td>
								<td class="actions">
									<?= $this->Html->link(
									     __('View'),
									    ['action' => 'viewHostDetails', $host['id']],
									    ['escape' => false, 'title' => __('View'), 'class' => 'btn btn-primary btn-xs']
									) ?>

									<?= $this->Html->link(
									     __('Edit'),
									    ['action' => 'updateHostDetails', $host['id']],
									    ['escape' => false, 'title' => __('Edit'), 'class' => 'btn btn-primary btn-xs']
									) ?>

									<?= $this->Html->link(
									     __('Empl.'),
									    ['action' => 'viewHostEmployees', $host['id']],
									    ['escape' => false, 'title' => __('Empl.'), 'class' => 'btn btn-primary btn-xs']
									) ?>

									<?= $this->Html->link(
									     __('Delete'),
									    ['action' => 'deleteHosts', $host['id']],
									    ['escape' => false, 'title' => __('Delete'), 'class' => 'btn btn-danger btn-xs',
									    'confirm' => __('Are you sure you want to Delete ?')]
									) ?>
                            	</td> 
                            	<td>
                            	<?= $this->Form->create('add', ['id'=>'AddForm', 'url' => '/admin/update-subscription']) ?>
                            	<div class="form-group">
				                   <div class="col-sm-6 userType">
				                      <!--   <?php if($host['payment_status'] == 'free') {
				                			echo "You have given Free Subscription";
						                }
						                else {
						                	echo "Not Subscribe";
						                }
						                ?> -->
						                <?php  if($host['payment_status'] != 'yes'): ?>
					                        <select name="payment_status" class="user-filter" id="mainBox">
					                            <option value="select_<?php echo $host['id'] ?>">Select</option>
					                            <option value="free_<?php echo $host['id'] ?>" <?php if(!empty($host['payment_status']) && $host['payment_status'] == 'free'){echo "selected";} ?>>Free Subscription</option>
					                            <option value="no_<?php echo $host['id'] ?>" <?php if(!empty($host['payment_status']) && $host['payment_status'] == 'no')?>>Remove Free Subscription</option>
					                        </select> 
					                    <?php else:?>
					                    <b>paid</b>
				                        <?php endif;?>
			
				                        <br/>
				                        <input type="hidden" name="id" value="<?php echo $host['id'] ?>" />
				           
					                         <div id="select_<?php echo $host['id'] ?>"  class="free_<?php echo $host['id'] ?> add"  style="display:none;">
											      <input id="select" type='submit' value='add' oncancel="cancel" onclick="if(confirm ('Please click on OK to give free subscription.')) {return true;} else { return false;}">
											 </div>

											 <div id="select_<?php echo $host['id'] ?>"  class="no_<?php echo $host['id'] ?> remove"  style="display:none;">
											      <input id="select" type='submit' value='remove' oncancel="cancel" onclick="if(confirm ('Please click on OK to remove free subscription.')) {return true;} else { return false;}">
											 </div>

					                   </div>                       
				                </div>
				                <?= $this->Form->end() ?>
                            	</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->

	<div class="row">
		<div class="col-md-6">
	 	<?php
			echo $this->Paginator->counter('Showing {{start}} to {{end}} of {{count}} entries');
		?>
		</div>
		<div class="col-md-6">
			<ul class="pagination" style="float: right; margin: 0!important;">
	 		<?php
				echo $this->Paginator->prev('< ' . __('previous'));
				echo $this->Paginator->numbers();
				echo $this->Paginator->next(__('next') . ' >');
			?>
		 	</ul>
		</div>
	</div>
</section><!-- /.content -->

<script type="text/javascript">
$(function() {
    setTimeout(function() {
      $(".alert-success").hide('blind', {}, 500)
    }, 5000);
});
</script>

<!-- <script type="text/javascript">

$(document).ready(function () {
    $('.user-filter').change(function () {
        $('#'+$(this).val()).hide();
        if($(this).val().substring(0,4) == "free") {
           $('.'+$(this).val()).show();
        }

    });
});
</script> -->

<script type="text/javascript">
$(document).ready(function () {
    $('.user-filter').change(function () {
    	// alert('#'+$(this).val());
        $('#'+$(this).val()).hide();
        if($(this).val().substring(0,4) == "free") {
           $('.'+$(this).val()).show();
           $('.remove').hide();
        }
        else if($(this).val().substring(0,2) == "no") {
        	$('.'+$(this).val()).show();
        	$('.add').hide();
        }

    });
});
</script>