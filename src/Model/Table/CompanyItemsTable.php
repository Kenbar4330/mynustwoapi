<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CompanyItems Model
 *
 * @property \App\Model\Table\CompanyCategoriesTable|\Cake\ORM\Association\BelongsTo $CompanyCategories
 * @property \App\Model\Table\CompanyItemsOptionsTable|\Cake\ORM\Association\HasMany $CompanyItemsOptions
 *
 * @method \App\Model\Entity\CompanyItem get($primaryKey, $options = [])
 * @method \App\Model\Entity\CompanyItem newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CompanyItem[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CompanyItem|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CompanyItem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CompanyItem[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CompanyItem findOrCreate($search, callable $callback = null, $options = [])
 */
class CompanyItemsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('company_items');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('CompanyCategory', [
            'foreignKey' => 'company_category_id',
            'joinType' => 'INNER'
        ]);
		
        $this->hasMany('CompanyItemsOptions', [
            'foreignKey' => 'company_item_id',
            'dependent'  => true,
            'cascadeCallbacks' => true
        ]);
        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
        ]);
		
		$this->hasMany('BookingItems', [
            'foreignKey' => 'company_item_id',
            'dependent'  => true,
            'cascadeCallbacks' => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 125)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('size')
            ->requirePresence('size', 'create')
            ->notEmpty('size');

        $validator
            ->scalar('image')
            ->maxLength('image', 75)
            ->allowEmpty('image');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_category_id'], 'CompanyCategory'));

        return $rules;
    }
}
