<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Routing\Router;
use Cake\Filesystem\Folder;

class ImageUploadComponent extends Component{
/*
* Upload the images and send the response
*
* @param POST
* @return json
* @throws \Cake\Http\Exception\NotFoundException
*/
public function uploadCompanyitemImages($imageData) {

    try {
            $name = $imageData['name'];
            $temp = $imageData['tmp_name'];
            $ext = substr(strrchr($name , '.'), 1);
            $arr_ext = array('jpg', 'jpeg', 'png');
            $NewFileName = time().rand(0, 4);
            $setNewFileName = $NewFileName.$name;
            if (!in_array($ext, $arr_ext)) {
                $result = array('status'=>'failure', 'message' => 'please upload correct format');
                echo json_encode($result); die;
            }
            $path = 'images' . DS . 'itemimages' . DS ; // path where the images will be uploaded
            if (!file_exists(WWW_ROOT . $path)) { // create the image folder if it does not exist
                  mkdir(WWW_ROOT . $path, 0777, true);
            }

            if(move_uploaded_file($temp, WWW_ROOT . 'images/itemimages/' . $setNewFileName)) {
                return $setNewFileName;
            }
            else {
                return null;
            }
        }
        catch (InternalErrorException $exception) {
            throw new NotFoundException();
        }
    }   
}

?>
