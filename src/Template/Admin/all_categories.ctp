<!-- Content Header (Page header) -->
<section class="content-header">
    <?php echo $this->Flash->render(); ?>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Categories Listing</h3>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-body">
					<table id="categoryLists" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>id</th>
								<th>Category Name</th>
								<th>Visibilty</th>
								<th>Date</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($categories as $category): ?>
							<tr>
								<td><?= $this->Number->format($category['id']) ?></td>
								<td><?= h($category['name']) ?></td>
								<td><?php
									if(!empty($category['status']) && $category['status'] == 'inactive')
										echo "Guest";
									else
										echo "Host/Guest";
									?> </td>
								<td><?= h($category['created']->format('d F Y')) ?></td>
								<td class="actions">
									<?= $this->Html->link(
									     __('Edit'),
									    ['action' => 'updateCategory', $category['id']],
									    ['escape' => false, 'title' => __('Edit'), 'class' => 'btn btn-primary btn-xs']
									) ?>

									<?php
									if(!empty($category['status']) && $category['status'] == 'inactive') {
										?>
										 <?= $this->Form->postLink(__("guest"), ['action' => 'changeStatus', $category['id'], 'active','category'], ['class' => 'btn btn-warning btn-xs changeCatStatus','confirm' => __('After Click on OK, '.$category['name'].' is visible to Both Users?')]) ?>

									<?php
									} else {
									?>
									  <?= $this->Form->postLink(__("host and guest"), ['action' => 'changeStatus', $category['id'], 'inactive','category'], ['class' => 'btn btn-success btn-xs changeCatStatus','confirm' => __('After Click on OK, '.$category['name'].' is visible to Guest only ?')]) ?>

									<?php
									}
									?> 
                            	</td>  
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</section><!-- /.content -->

<script type="text/javascript">
$(function () {

	//Active/inactive record
    $(document).on('click', '.changeCatStatus', function (e) {
        e.preventDefault();
        
        var that = $(this);
        $.ajax({
            type: "POST",
            url: 'changeStatus',
            data: {"id": $(this).data('id'), "status": $(this).data('status')},
            success: function (jsonResponse) {
            	var res = JSON.parse(jsonResponse);
            	if (res.rstatus == 'inactive') {
                    that.text('Inactive');
                    that.data('status', 'active');
                    that.removeClass( "btn-success" ).addClass( "btn-danger" );

                } else {
                	that.text('Active');
                    that.data('status', 'inactive');
                    that.removeClass( "btn-danger" ).addClass( "btn-success" );
                }
            }
        });
    });

    setTimeout(function() {
      $(".alert-success").hide('blind', {}, 500)
    }, 5000);
});
</script>
