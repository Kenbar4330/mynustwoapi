<?php

namespace App\Controller\Api;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Filesystem\Folder;
use Cake\Log\Log;
require ROOT."/vendor/autoload.php";
// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

class CompaniesController extends AppController
{
    private $userObj;
    private $categoryObj;
    private $customerObj;
    private $scheduleObj;
    private $employeeObj;
    private $bookingObj;
    private $ratingObj;
    private $phoneObj;
	private $compImgObj;
	private $empImgObj;
	private $businessTimeObj;
	private $countryObj;
    private $restaurantObj;
    private $compCatObj;
    private $companyItems;
    private $companyItemsOptions;
    private $bookingItems;
    private $cusDelivery;
    private $cusPayment;

    public function initialize()
    {
        parent::initialize();     

        header("Access-Control-Allow-Origin: *");
		//header("Access-Control-Allow-Origin: http://live.mynustwo.com");
        //header("Access-Control-Allow-Credentials: true");
        header('Content-Type: application/json'); 
        header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
        //header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, authorization, Authorization, customauthorization, Customauthorization");
		header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, customauthorization");

        if ($this->request->is('options')) { 
            $this->response->statusCode(204); 
            $this->response->send(); 
            die(); 
        }
        $this->loadComponent('ImageUpload');
		$this->Auth->allow();
        //$this->Auth->allow('myBookingReminder', 'deleteAfterClose', 'deleteCartPassed');

        $this->userObj         = TableRegistry::get('Users');
        $this->categoryObj     = TableRegistry::get('Categories');
        $this->customerObj     = TableRegistry::get('Customers');
        $this->scheduleObj     = TableRegistry::get('Schedules');
		$this->slotObj         = TableRegistry::get('Slots');
        $this->employeeObj     = TableRegistry::get('Employees');
		$this->bookingObj      = TableRegistry::get('Booking');
        $this->phoneObj        = TableRegistry::get('Phones');
		$this->compImgObj      = TableRegistry::get('company_images');
        $this->empImgObj       = TableRegistry::get('employee_images');
		$this->businessTimeObj = TableRegistry::get('business_timings');
		$this->countryObj      = TableRegistry::get('countries');
        $this->restaurantObj   = TableRegistry::get('restaurant_arrangement');
        $this->compCatObj      = TableRegistry::get('CompanyCategory');
        $this->companyItems    = TableRegistry::get('CompanyItems');
        $this->companyItemsOptions = TableRegistry::get('CompanyItemsOptions');
        $this->bookingItems = TableRegistry::get('BookingItems');
        $this->cusDelivery = TableRegistry::get('CustomerDeliveryAddress');
        $this->cusPayment = TableRegistry::get('CustomerPayment');
		
		//Country Code
		$this->countryCode = '+1';
		//$this->countryCode = $this->getCountryCode("Visitor", "countrycode");
    }  

    /**
     * API: Add Company/Individual/Employee Profile
     * PARAMS(Company/Individual): category_id, company_name, first_name, last_name, about, address, city, state, country, zipcode, latitude, longitude, type(profile_type), offer ,wait
     * PARAMS(Employee): category_id, company_id, first_name, last_name, about, email, profile_pic, type
     */
    public function addCompanyProfile()
    {
        if ($this->request->is('post')) 
        {
            $params = $this->request->data;
            //Log::notice($params);
            $params['user_id'] = $this->Auth->identify()['id'];
            $params['phone'] = $this->Auth->identify()['phone'];
            
			if(!empty($params['type']) && ($params['type'] == 'company' || $params['type'] == 'individual')) {
				//Save company and individual details
				$company = $this->Companies->newEntity(); 
				$company = $this->Companies->patchEntity($company, $params);
				if($this->Companies->save($company)) {
                    //Update payment status in users table
                    //$this->userObj->updateAll(['payment_status'=>'free'], ['id'=>$params['user_id']]);
                    
                    //If profile_type is individual then add single employee
                    $profile_id = $company->id;
                    $profile_type = $company->type;
                    if($params['type'] == 'individual') {
                        $employee = $this->employeeObj->newEntity();
                        $employee->company_id = $company->id;
                        $employee = $this->employeeObj->patchEntity($employee, $params);
                        $this->employeeObj->save($employee);
                        $profile_id = $company->id;
                    }
					$result = array('status'=>'success', 'message'=>"Company details saved successfully.",
                        'profile_id' => $profile_id, 'profile_type' => $profile_type);
				} else {
					$errors = $company->errors();
					$erorMessage = array(); 
					$i = 0; 
					$keys = array_keys($errors); 
					foreach ($errors as $errors) { 
						$key = key($errors); 
						foreach($errors as $error){ 
							$erorMessage = ucfirst($keys[$i]) . " :- " . $error;
							//$erorMessage = $error;
						}
						$i++;
					}
					$result = array('status'=>'error', 'message'=>$erorMessage); 
				}
			} elseif(!empty($params['type']) && $params['type'] == 'employee') {
				//Save employee details if profile_pic is uploaded
                $uploadPicFlag = 1;
                if(isset($_FILES['profile_pic']['name']) && !empty($_FILES['profile_pic']['name'])) 
                {
                    $name = $_FILES['profile_pic']['name'];
                    $temp = $_FILES['profile_pic']['tmp_name'];
                    $ext = substr(strrchr($name , '.'), 1);
                    $arr_ext = array('jpg', 'jpeg', 'png');
                    $NewFileName = time().rand(00, 99);
                    $setNewFileName = $NewFileName.".".$ext;
                    if (!in_array($ext, $arr_ext)) {
                        $uploadPicFlag = 0;
                    }
                    $params['profile_pic'] = $setNewFileName;                    
                }

                if(!$uploadPicFlag) {
                    $result = array('status'=>'failure', 'message'=>'Please upload correct image format.');
                } else {
                    $params['approve'] = "no";
                    $employee = $this->employeeObj->newEntity();
    				$employee = $this->employeeObj->patchEntity($employee, $params);
    				if($this->employeeObj->save($employee)) {
                         $profile_id = $employee->id;
                         $profile_type = 'employee';
                        //Upload profile_pic
                        if(isset($_FILES['profile_pic']['name']) && !empty($_FILES['profile_pic']['name'])) {
                            $url = WWW_ROOT."images/employees/".$employee->id;
                            //Create directory with name profile_id
                            if (!file_exists($url)) {
                                mkdir($url, 0755, true);
                            }
                            $path = Router::url('/','true')."webroot/images/employees/".$employee->id.DS.$setNewFileName;
                            
                            move_uploaded_file($temp, $url . "/" . $setNewFileName);  
                        }                 
                        
    					$result = array('status'=>'success', 'message'=>"Employee details saved successfully.",
                             'profile_id' => $profile_id, 'profile_type' => $profile_type);

                           //notification to company.
                        if($result) {
                            $employee = $this->employeeObj->findById($profile_id )->contain(['Users'=>['fields'=>['Users.id', 'Users.device_type', 'Users.device_token','Users.noti_alert']]])->select(['id', 'user_id', 'first_name', 'last_name'])->enableHydration(false)->first();

                            if(!empty($employee['first_name'])) {
                                $name = $employee['first_name'].' '.$employee['last_name'];
                            }

                            $company = $this->Companies->findById($params['company_id'] )->contain(['Users'=>['fields'=>['Users.id', 'Users.device_type', 'Users.device_token','Users.noti_alert']]])->select(['id', 'user_id', 'first_name', 'last_name'])->enableHydration(false)->first();

                            $device_type = $company['user']['device_type'];
                            $device_token = $company['user']['device_token'];

                            if(!empty($device_token) && !empty($device_type)) {
                            //Save notification
                            $data = [];
                            $data['to_user_id']  = $company['user_id'];
                            $data['from_user_id']  = $employee['user_id'];
                            $data['message']       = $name.' has registered on your business';
                            $data['type']          = "employee_register";
                            $this->saveNotification($data);

                            //Get unread notification count(badge_count) and update in users table
                            $badgeCount = $this->getBadgeCount($company['user_id']);
                            
                            //Send notification 
                            if($employee['user']['noti_alert'] == "on") {   
                             $this->sendPushNotification($device_type, $device_token, $data['message'], $data['type'], $badgeCount);
                            }
                                }             
                            }
    				} else {
                        $errors = $employee->errors();
                        $erorMessage = array(); 
                        $i = 0; 
                        $keys = array_keys($errors); 
                        foreach ($errors as $errors) { 
                            $key = key($errors); 
                            foreach($errors as $error){ 
                                $erorMessage = ucfirst($keys[$i]) . " :- " . $error;
                                //$erorMessage = $error;
                            }
                            $i++;
                        }
                        $result = array('status'=>'failure', 'message'=>$erorMessage);
    				}
                }
            } else {
                $result = array('status'=>'failure', 'message'=>"Provided details are incorrect.");
            } 

            $this->set([
                'status'       => $result['status'],
                'message'      => $result['message'],
                'profile_id'   => !empty($result['profile_id']) ? $result['profile_id'] : '',
                'profile_type' => !empty($result['profile_type']) ? $result['profile_type'] : '',
                '_serialize'   => ['status', 'message','profile_id','profile_type']
            ]);
        }
    }

    /**
     * API: View Company/Employee/Room Employee/Others Profile
	 * PARAMS: profile_id(company_id/employee_id/room_employee_id/room_id), profile_type(company/individual/employee/room_employee/other)
     */
    public function viewProfileDetails()
    {
		if ($this->request->is('post')) 
        {
			$params = $this->request->data;
			
			if(!empty($params['profile_type']) && ($params['profile_type']=='company' || $params['profile_type']=='individual')) {

				$profiles = $this->Companies->findById($params['profile_id'])->contain(['Categories'=>['fields'=>['Categories.id', 'Categories.name']], 'Users'=>['fields'=>['Users.id', 'Users.visibility','Users.phone']]])->enableHydration(false)->first();

			} elseif(!empty($params['profile_type']) && $params['profile_type']=='employee') {

				$profiles = $this->employeeObj->findById($params['profile_id'])->contain(['Categories'=>['fields'=>['Categories.id', 'Categories.name']], 'Users'=>['fields'=>['Users.id', 'Users.visibility','Users.phone']],
                    'Companies'])->enableHydration(false)->first();

			} elseif(!empty($params['profile_type']) && $params['profile_type']=='room_employee') {

				$profiles = $this->employeeObj->find('all')->where(['Employees.id'=>$params['profile_id'], 'Employees.type'=>'employee', 'Employees.custom'=>'yes'])->contain(['Categories'=>['fields'=>['Categories.id', 'Categories.name']], 'Users'=>['fields'=>['Users.id', 'Users.visibility','Users.phone']]])->enableHydration(false)->first();

			} elseif(!empty($params['profile_type']) && $params['profile_type']=='other') {

				$profiles = $this->employeeObj->find('all')->where(['Employees.id'=>$params['profile_id'], 'Employees.type'=>'other', 'Employees.custom'=>'yes'])->contain(['Categories'=>['fields'=>['Categories.id', 'Categories.name']], 'Users'=>['fields'=>['Users.id', 'Users.visibility','Users.phone']]])->enableHydration(false)->first();

			} else {
				$profiles = [];
			}
			$profileDetails = array();
			if(!empty($profiles)) {
				$profileDetails['category_id']   = strval($profiles['category_id']);
				$profileDetails['category_name'] = $profiles['category']['name'];
				$profileDetails['first_name']    = $profiles['first_name'];
				$profileDetails['last_name']     = $profiles['last_name'];
				$profileDetails['about']         = $profiles['about'];
				$profileDetails['profile_id']    = $profiles['id'];
				$profileDetails['visibility']    = $profiles['user']['visibility'];
				if(!empty($params['profile_type']) && ($params['profile_type']=='company' || $params['profile_type']=='individual')) {
					$profileDetails['company_id']    = $profiles['id'];
                    $profileDetails['wait']          = $profiles['wait'];
					$profileDetails['wait_status']   = $profiles['wait_status'];
					$profileDetails['company_name']  = $profiles['company_name'];
					$profileDetails['phone']         = $profiles['user']['phone'];
                    $profileDetails['business_phone'] = $profiles['business_phone'];
                    $profileDetails['booking_amount'] = number_format($profiles['booking_amount'],2);
                    $profileDetails['delivery']       = $profiles['delivery'];
                    $profileDetails['delivery_radius'] = number_format($profiles['delivery_radius'],2);
                    $profileDetails['delivery_amount'] = number_format($profiles['delivery_amount'],2);
                    $profileDetails['tax']           = number_format($profiles['tax'],2);
                    $profileDetails['client_id']     = $profiles['client_id'];
					$profileDetails['email']         = $profiles['email'];
					$profileDetails['rating']        = $this->businessRating($profiles['id']);
					$profileDetails['rateUserCount'] = $this->countRating($profiles['id']);
					$profileDetails['address']       = $profiles['address'];
					$profileDetails['city']          = $profiles['city'];
					$profileDetails['state']         = $profiles['state'];
					$profileDetails['country']       = $profiles['country'];
					$profileDetails['zipcode']       = $profiles['zipcode'];
					$profileDetails['offers']        = $profiles['offers'];
					$profileDetails['type']          = $profiles['type'];
					$profileDetails['profile_type']  = $profiles['type'];
				} elseif(!empty($params['profile_type']) && $params['profile_type']=='employee') {
					$profileDetails['company_id']    = $profiles['company_id'];
                    $profileDetails['company_name']  = $profiles['company']['company_name'];
                    $profileDetails['address']       = $profiles['company']['address'];
                    $profileDetails['wait']          = $profiles['company']['wait'];
					$profileDetails['employee_id']   = $profiles['id'];
					$profileDetails['email']         = $profiles['email'];
					$profileDetails['rating']        = $this->employeeRating($profiles['id']);
					$profileDetails['rateUserCount'] = $this->countRatingUsers($profiles['id']);
					$profileDetails['type']          = $params['profile_type'];
					$profileDetails['profile_type']  = $params['profile_type'];
					$noImage = Router::url('/','true')."webroot/images".DS."noImage.png";
					$profileDetails['profile_pic']   = !empty($profiles['profile_pic']) ? Router::url('/','true')."webroot/images/employees/".$profiles['id']."/".$profiles['profile_pic'] :  $noImage;
				} elseif(!empty($params['profile_type']) && $params['profile_type']=='room_employee') {
                    $profileDetails['company_id']    = $profiles['company_id'];
                    $profileDetails['employee_id']   = $profiles['id'];
                    $profileDetails['email']         = $profiles['email'];
                    $profileDetails['rating']        = $this->employeeRating($profiles['id']);
					$profileDetails['rateUserCount'] = $this->countRatingUsers($profiles['id']);
                    $profileDetails['type']          = $params['profile_type'];
                    $profileDetails['profile_type']  = $params['profile_type'];
                    $noImage = Router::url('/','true')."webroot/images".DS."noImage.png";
                    $profileDetails['profile_pic']   = !empty($profiles['profile_pic']) ? Router::url('/','true')."webroot/images/employees/".$profiles['id']."/".$profiles['profile_pic'] :  $noImage;
                } elseif(!empty($params['profile_type']) && $params['profile_type']=='other') {
                    $profileDetails['name']          = $profiles['first_name'];
                    $profileDetails['company_id']    = $profiles['company_id'];
                    $profileDetails['employee_id']   = $profiles['id'];
                    $profileDetails['email']         = $profiles['email'];
                    $profileDetails['rating']        = $this->employeeRating($profiles['id']);
					$profileDetails['rateUserCount'] = $this->countRatingUsers($profiles['id']);
                    $profileDetails['type']          = $params['profile_type'];
                    $profileDetails['profile_type']  = $params['profile_type'];
                    $noImage = Router::url('/','true')."webroot/images".DS."noImage.png";
                    $images  = $this->galleryImages($profiles['id'], 'other');
                    $profileDetails['profile_pic'] = !empty($images) ? $images[0]['image'] : $noImage;
                }
				$result = array('status'=>'success', 'message'=>'Profile details.', 'data'=>$profileDetails);
			} else {
				$result = array('status'=>'failure', 'message'=>'No record found', 'data'=>$profileDetails);
			}

			$this->set([
				'status'     => $result['status'],
				'message'    => $result['message'],
				'data'       => $result['data'],
				'_serialize' => ['status', 'message','data']
			]);
		}
    }

    /**
     * API: Update Company/Individual/Employee/Room Employee/Others Profile
     * PARAMS(Company/Individual): profile_type, company_id, company_name, first_name, last_name, about, address, city, state, country, zipcode, phone, offers
     * PARAMS(Employee): profile_type, employee_id, first_name, last_name, about, email, profile_pic
     * PARAMS(Room Employee): profile_type(room_employee), employee_id, first_name, last_name, about, profile_pic
     * PARAMS(Other): profile_type(other), employee_id, name, about 
     */
    public function updateProfile()
    {
        if ($this->request->is('post')) 
        {
            $params = $this->request->data;
            //Log::notice($params);
            $params['user_id'] = $this->Auth->identify()['id'];
			$countrycode = $this->Auth->identify()['countrycode'];
            //Flag if phone change
            $change = "no";
			$flag = 1;
            if((isset($params['phone']) && !empty($params['phone'])) && (!empty($params['profile_type']) && $params['profile_type'] != 'employee')) {
				$exists = $this->userObj->findByPhone($params['phone'])->enableHydration(false)->first();
                //Check if phone exist in users table
                if(!$exists) {
                    //Generate and Send otp if number changed
                    $otp = $this->generateOTP();
                    $res = $this->sendOTP($countrycode, $params['phone'], $otp);
                    if($res) {
                        //If phone change then save in phones table
                        $changePhone = $this->phoneObj->newEntity();
                        $changePhone->otp = $otp;
						$changePhone->user_id = $params['user_id'];
                        $changePhone = $this->phoneObj->patchEntity($changePhone, $params);
                        $this->phoneObj->save($changePhone);

                        //Do not change phone number till OTP not verify
                        $change = "yes";
                        $params['phone'] = $this->Auth->user('phone');
						$flag = 1;
                    }
                } else {
					if($exists['id'] != $params['user_id']) {
                        $flag = 0;
                        $result = array('status'=>'failure', 'message'=>"Phone number already exist.", 'change'=>$change);
                        echo json_encode($result); die;
                    } 
				}
            }
            if(!empty($params['profile_type']) && ($params['profile_type'] == 'company' || $params['profile_type'] == 'individual') && $flag) {
                //Save company and individual details
                $company = $this->Companies->newEntity();
                $company->id = $params['company_id']; 
                $company = $this->Companies->patchEntity($company, $params);
                if($this->Companies->save($company)) {
                    //If individual update employee details
                    if($params['profile_type'] == 'individual') {
                        //get employee_id
                        $empId = $this->employeeObj->findByUserId($params['user_id'])->select(['Employees.id'])->enableHydration(false)->first();
                        $employee = $this->employeeObj->newEntity();
                        $employee->id = $empId['id']; 
                        $employee = $this->employeeObj->patchEntity($employee, $params);
                        $this->employeeObj->save($employee);
                    }
                    
                    $result = array('status'=>'success', 'message'=>"Company details saved successfully.", 'change'=>$change);
                } else {
                    $errors = $company->errors();
                    $erorMessage = array(); 
                    $i = 0; 
                    $keys = array_keys($errors); 
                    foreach ($errors as $errors) { 
                        $key = key($errors); 
                        foreach($errors as $error){ 
                            $erorMessage = ucfirst($keys[$i]) . " :- " . $error;
                            //$erorMessage = $error;
                        }
                        $i++;
                    }
                    $result = array('status'=>'failure', 'message'=>$erorMessage, 'change'=>$change); 
                }
            } elseif(!empty($params['profile_type']) && ($params['profile_type'] == 'employee' || $params['profile_type'] == 'room_employee')) {
                //update employee details if profile_pic is uploaded
                $uploadPicFlag = 1;
                if(isset($_FILES['profile_pic']['name']) && !empty($_FILES['profile_pic']['name'])) 
                {
                    $name = $_FILES['profile_pic']['name'];
                    $temp = $_FILES['profile_pic']['tmp_name'];
                    $ext = substr(strrchr($name , '.'), 1);
                    $arr_ext = array('jpg', 'jpeg', 'png');
                    $NewFileName = time().rand(00, 99);
                    $setNewFileName = $NewFileName.".".$ext;
                    if (!in_array($ext, $arr_ext)) {
                        $uploadPicFlag = 0;
                    }
                    $params['profile_pic'] = $setNewFileName;                    
                }

                if(!$uploadPicFlag) {
                    $result = array('status'=>'failure', 'message'=>'Please upload correct image format.');
                } else {
                    //update employee details
                    $employee = $this->employeeObj->newEntity();
                    $employee->id = $params['employee_id']; 
                    $employee = $this->employeeObj->patchEntity($employee, $params);
                    if($this->employeeObj->save($employee)) {
                        //Upload profile_pic
                        if(isset($_FILES['profile_pic']['name']) && !empty($_FILES['profile_pic']['name'])) {
                            $url = WWW_ROOT."images/employees/".$employee->id;
                            //Create directory with name profile_id
                            if (!file_exists($url)) {
                                mkdir($url, 0755, true);
                            }
                            $path = Router::url('/','true')."webroot/images/employees/".$employee->id.DS.$setNewFileName;
                            
                            move_uploaded_file($temp, $url . "/" . $setNewFileName);  
                        } 

                        $result = array('status'=>'success', 'message'=>"Employee details saved successfully.", 'change'=>$change);
                    } else {
                        $errors = $employee->errors();
                        $erorMessage = array(); 
                        $i = 0; 
                        $keys = array_keys($errors); 
                        foreach ($errors as $errors) { 
                            $key = key($errors); 
                            foreach($errors as $error){ 
                                $erorMessage = ucfirst($keys[$i]) . " :- " . $error;
                                //$erorMessage = $error;
                            }
                            $i++;
                        }
                        $result = array('status'=>'failure', 'message'=>$erorMessage, 'change'=>$change); 
                    }
                }
            } elseif(!empty($params['profile_type']) && $params['profile_type'] == 'other') {
                //update rooms details
                $employee = $this->employeeObj->newEntity();
                $params['first_name'] = $params['name'];
                $employee->id = $params['employee_id']; 
                $employee = $this->employeeObj->patchEntity($employee, $params);
                if($this->employeeObj->save($employee)) {
                    
                    $result = array('status'=>'success', 'message'=>"Details saved successfully.", 'change'=>$change);
                } else {
                    $errors = $employee->errors();
                    $erorMessage = array(); 
                    $i = 0; 
                    $keys = array_keys($errors); 
                    foreach ($errors as $errors) { 
                        $key = key($errors); 
                        foreach($errors as $error){ 
                            $erorMessage = ucfirst($keys[$i]) . " :- " . $error;
                            //$erorMessage = $error;
                        }
                        $i++;
                    }
                    $result = array('status'=>'failure', 'message'=>$erorMessage, 'change'=>$change); 
                }
            }                  
            
            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                'change'     => $result['change'],
                '_serialize' => ['status', 'message', 'change']
            ]);
        }
    }
	
	/**
     * API: Create Slots
     * PARAMS: profile_type(company/individual/employee/room_employee/other), profile_id, days, start_time, end_time
     */
    public function createSlots()
    {
        if ($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            if(!empty($params['profile_type']) && ($params['profile_type']=='individual' || $params['profile_type']=='employee' || $params['profile_type']=='room_employee' || $params['profile_type']=='other' || $params['profile_type']=='company')) {
                
                //Get individual business employee_id
                if($params['profile_type']=='individual') {
                    $empId = $this->employeeObj->findByCompanyId($params['profile_id'])->select(['id'])->first();
                    $params['profile_id'] = $empId['id'];
                }
                $params['employee_id'] = $params['profile_id'];

                if($params['profile_type']=='company') {
                    $params['employee_id'] = null;
                    $params['company_id'] = $params['profile_id'];
                    $scheduleDetails = $this->scheduleObj->find()->where(['company_id'=>$params['company_id'], 'days'=>$params['days']])->select(['Schedules.id'])->order(['Schedules.created'=>'DESC'])->enableHydration(false)->first();
                }
                else {
                   //Check if record already exists
                   $scheduleDetails = $this->scheduleObj->find()->where(['employee_id'=>$params['employee_id'], 'days'=>$params['days']])->select(['Schedules.id'])->order(['Schedules.created'=>'DESC'])->enableHydration(false)->first();
                }
                
                if(!empty($scheduleDetails)) {
                    foreach($params['slots'] as $slotS){
                        $slot = $this->slotObj->newEntity();
                        $slot->schedule_id = $scheduleDetails['id'];
                        $slot->start_time = $slotS['start_time'];
                        $slot->end_time = $slotS['end_time'];
                        // $slot = $this->slotObj->patchEntity($slot, $params);
                        $this->slotObj->save($slot);
                    }
                    $result = array('status'=>'success', 'message'=>"Slot saved successfully.");
                } else {
                    //Save schedule date on schedules table and slots array in slots table with schedule_id
                    $schedule = $this->scheduleObj->newEntity(); 
                    $schedule = $this->scheduleObj->patchEntity($schedule, $params);
                    if($this->scheduleObj->save($schedule)) {
                        //Save slots
                        // foreach($params['slots'] as $slotS) {
                        //     $slot = $this->slotObj->newEntity();
                        //     $slot->schedule_id = $schedule->id;
                        //     $slot->start_time = $slotS['start_time'];
                        //     $slot->end_time = $slotS['end_time'];
                        //     // $slot = $this->slotObj->patchEntity($slot, $params);
                        //     $this->slotObj->save($slot);
                        // }
                        $result = array('status'=>'success', 'message'=>"Slot saved successfully.");
                    } else {
                        $errors = $schedule->errors();
                        $erorMessage = array(); 
                        $i = 0; 
                        $keys = array_keys($errors); 
                        foreach ($errors as $errors) { 
                            $key = key($errors); 
                            foreach($errors as $error){ 
                                $erorMessage = ucfirst($keys[$i]) . " :- " . $error;
                                //$erorMessage = $error;
                            }
                            $i++;
                        }
                        $result = array('status'=>'failure', 'message'=>$erorMessage);
                    }
                }
            } else {
                $result = array('status'=>'failure', 'message'=>"Only individual business or employees can create slots.");
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }
    }
	
	/**
     * API: All Created Slots
     * PARAMS(New): profile_type(individual/employee/room_employee/other), profile_id
     * PARAMS(old): employee_id
     */
	public function allSlots()
	{
		if ($this->request->is('post')) {
			
            $params = $this->request->data;
			//Log::notice($params);
            if(!empty($params['profile_type']) && ($params['profile_type']=='individual' || $params['profile_type']=='employee' || $params['profile_type']=='room_employee' || $params['profile_type']=='other' || $params['profile_type']=='company')) {
                
                //Get individual business employee_id
                if($params['profile_type']=='individual') {
                    $empId = $this->employeeObj->findByCompanyId($params['profile_id'])->select(['id'])->first();
                    $params['profile_id'] = $empId['id'];
                }

                $params['employee_id'] = $params['profile_id'];

                if($params['profile_type']=='company') {
                   $params['company_id'] = $params['profile_id'];
                   $schedules = $this->scheduleObj->find()->where(['company_id'=>$params['company_id']])->contain(['Slots'=>['sort'=>['Slots.start_time'=>'ASC']]])->enableHydration(false)->toArray();
                }
                else {
                   //All slots
                   $schedules = $this->scheduleObj->find()->where(['employee_id'=>$params['employee_id']])->contain(['Slots'=>['sort'=>['Slots.start_time'=>'ASC']]])->enableHydration(false)->toArray();
                }

    			$data = [];
    			if(!empty($schedules)) {
    				foreach($schedules as $key=>$schedule) {
    					if(!empty($schedule['days'])) {
                            foreach($schedule['slots'] as $key2=>$slot) {
                                $data[$key]['schedule_id'] = $schedule['id'];
                                $data[$key]['days'] = $schedule['days'];
                                $data[$key]['slots'][$key2]['slot_id'] = $slot['id'];
                                $data[$key]['slots'][$key2]['start_time'] = date('H:i',strtotime($slot['start_time']));
                                $data[$key]['slots'][$key2]['end_time']   = date('H:i',strtotime($slot['end_time']));
                                
                                $data[$key]['slots'] = array_values($data[$key]['slots']);
                            }
                        }
    				}
    				$result = array('status'=>'success', 'message'=>'Slots List.', 'data'=>array_values($data));
    			} else {
    				$result = array('status'=>'failure', 'message'=>'No data found.', 'data'=>$data);
    			}
            } else {
                $result = array('status'=>'failure', 'message'=>"Only individual business or employees can create slots.", 'data'=>array());
            }
			
			$this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'data' =>  $result['data'],
                '_serialize' => ['status','message','data']
            ]);
		}
	}
	
	/**
     * API: Uploaded photos
     * PARAMS: profile_type(Company/Individual/Employee), profile_id(company_id/employee_id), imgfile
     */
    public function uploadPhotos() 
	{
        if($this->request->is('post')) {
            $params = $this->request->getData();
            //Log::notice($params);
            $result = array();
            if(!empty($_FILES['imgfile']['name'])) {
                $name = $_FILES['imgfile']['name'];
                $temp = $_FILES['imgfile']['tmp_name'];
                $ext = substr(strrchr($name , '.'), 1);
                $arr_ext = array('jpg', 'jpeg', 'png');
                $NewFileName = time().rand(00, 99);

                $setNewFileName = $NewFileName."_".$params['profile_id'].".".$ext;
                            
                if(!empty($params['profile_type']) && ($params['profile_type'] == 'company' || $params['profile_type'] == 'individual')) {

                    $url = WWW_ROOT."images/companies/" . $params['profile_id'];
                    $path = Router::url('/','true')."webroot/images/companies/".$params['profile_id'].DS.$setNewFileName;

                } elseif(!empty($params['profile_type']) && $params['profile_type'] == 'employee') {
                    
                    $url = WWW_ROOT."images/employees/" . $params['profile_id'];
                    $path = Router::url('/','true')."webroot/images/employees/".$params['profile_id'].DS.$setNewFileName;
                }
    			//Create directory with name profile_id
    			if (!file_exists($url)) {
    				mkdir($url, 0777, true);
    			}
    			 			
                if (in_array($ext, $arr_ext)) {
                    if(move_uploaded_file($temp, $url . "/" . $setNewFileName)) {
                        if(!empty($params['profile_type']) && ($params['profile_type'] == 'company' || $params['profile_type'] == 'individual')) {
                            $imagesData = $this->compImgObj->newEntity();
                            $imagesData->company_id = $params['profile_id'];
                            $imagesData->name = $setNewFileName;
                            if($this->compImgObj->save($imagesData)) {
                                $result = array('status'=>'success', 'message'=>'Image uploaded successfully.', 'image_id'=>$imagesData->id, 'image_path'=>$path);
                            }
                        } elseif(!empty($params['profile_type']) && $params['profile_type'] == 'employee') {
                            $imagesData = $this->empImgObj->newEntity();
                            $imagesData->employee_id = $params['profile_id'];
                            $imagesData->name = $setNewFileName;
                            if($this->empImgObj->save($imagesData)) {
                                $result = array('status'=>'success', 'message'=>'Image uploaded successfully.', 'image_id'=>$imagesData->id, 'image_path'=>$path);
                            }
                        }
                    } else {
                        $result = array('status'=>'failed', 'message'=>'Something went wrong.', 'image_id'=>null, 'image_path'=>null);
                    }
                } else {
                    $result = array('status'=>'failed', 'message'=>'Please upload correct image format.', 'image_id'=>null, 'image_path'=>null);
                }

                $this->set([
                    'status' => $result['status'],
                    'message' => $result['message'],
                    'image_id' => $result['image_id'],
                    'image_path' => $result['image_path'],
                    '_serialize' => ['status', 'message', 'image_id', 'image_path']
                ]);
            }
        }
    }
	
	/**
    * API: Companies List
    */
    public function companiesList()
    {
        $companies = $this->Companies->find()->where(['category_id !='=> 5])->enableHydration(false)->toArray();
        $company = array();
        foreach ($companies as $key => $value) {
              $company[$key]['id'] = $value['id'];
              $company[$key]['category_id'] = $value['category_id'];
              $company[$key]['company_name'] = $value['company_name'];
              $company[$key]['business_address'] = $value['address'].",".$value['city'].",".$value['state'];
        }

        if(!empty($company)) {
            $result = array('status'=>'success', 'message'=>'List of Companies','data'=>$company);
        } else {
            $result = array('status'=>'failure', 'message'=>'No Record found','data'=>array());
        }
        
        $this->set([
            'status' => $result['status'],
            'message' => $result['message'],
            'data' =>  $result['data'],
            '_serialize' => ['status','message','data']
        ]);
    }

    /**
     * API: Delete Slots
     * PARAMS: profile_type(individual/employee), profile_id, schedule_id(delete all slots) or slot_id(delete single slot)
     */
	public function deleteSlots()
    {
        if($this->request->is('post')) {  
            $params = $this->request->data;
            //Log::notice($params);
            if(!empty($params['profile_type']) && ($params['profile_type']=='individual' || $params['profile_type']=='employee' || $params['profile_type']=='company')){

                //Get individual business employee_id
                if($params['profile_type']=='individual') {
                    $empId = $this->employeeObj->findByCompanyId($params['profile_id'])->select(['id'])->first();
                    $params['profile_id'] = $empId['id'];
                }

                if(!empty($params['schedule_id'])) {

                    //Check if records to delete is exist in booking list
                    $chkRes = $this->checkBookedSlots($params['profile_id'], $params['schedule_id']);
                    if($chkRes) {
                        $result = array('status'=>'failure', 'message'=>"Can not delete due to booking request for this schedule.");
                    } else {
                        $schedule = $this->scheduleObj->findById($params['schedule_id'])->contain(['Slots'])->first();
                    
                        if(!empty($schedule) && $this->scheduleObj->delete($schedule)) {
                            $result = array('status'=>'success', 'message'=>"Deleted successfully.");
                        } else {
                            $result = array('status'=>'failure', 'message'=>"Some issue to delete slots.");
                        }
                    }
                } elseif (!empty($params['slot_id'])) {

                    //Check if slots exist in booking list
                    $existInBooking = $this->bookingObj->find()->where(['employee_id'=>$params['profile_id'], 'slot_id'=>$params['slot_id']])->enableHydration(false)->first();
                    if($existInBooking) {
                        $result = array('status'=>'failure', 'message'=>"Can not delete due to booking request for this schedule.");
                    } else {
                        $slotDetails = $this->slotObj->findById($params['slot_id'])->first();
                        if(!empty($slotDetails) && $this->slotObj->delete($slotDetails)) {
                            $result = array('status'=>'success', 'message'=>"Deleted successfully.");
                        } else {
                            $result = array('status'=>'failure', 'message'=>"Some issue to delete slot.");
                        }
                    }
                }
            } else {
                $result = array('status'=>'failure', 'message'=>"Only individual business or employees can delete slots.");
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }
    }

    /**
     * API: booking List Single
     * PARAMS: booking_id
     *
     */
    public function bookingListSingle()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            $itemCond = ['BookingItems.payment_status' => 'yes'];
            $bookingDetails = $this->bookingObj->find()->where(['Booking.id'=>$params['booking_id'],'Booking.payment_status'=>"yes"])->order(['Booking.id'=>'DESC'])->contain(['Customers'=>['fields'=>['Customers.id', 'Customers.name','Customers.phone','Customers.email', 'Customers.profile_pic'], 'Users'=>['fields'=>['Users.id', 'Users.visibility']]], 'Slots','RestaurantArrangement','CustomerPayment','BookingItems'=>function ($q) use ($itemCond) { 
                    return $q->where($itemCond);
                 },'CustomerDeliveryAddress'])->enableHydration(false)->first();
               //Log::notice($bookingDetails);
             // PR($bookingDetails); die;
            $bookings = array();
            if(!empty($bookingDetails)) {
                     if(empty($bookingDetails['date'])) {
                        $booking['date'] = $bookingDetails['modified'];
                     }
                    
                    $bookings['booking_id']  = $bookingDetails['id'];
                    $bookings['order_type']  = $bookingDetails['order_type'];
                    if($bookingDetails['order_type'] == "deliver") {
                        $bookings['delivery_amount']  = $bookingDetails['delivery_amount'];
                        $bookings['order_type']  = "deliver";
                        $bookings['customer_delivery_address'] = $bookingDetails['customer_delivery_addres']['address']; 
                    }
                    if($bookingDetails['order_type'] == "pick") {
                        $bookings['order_type']  = "pick up";
                    }
                    $bookings['booking_amount']  = $bookingDetails['booking_amount'];
                    $bookings['is_booking_item']  = $bookingDetails['is_booking_item'];
                    $bookings['booking_amount']  = $bookingDetails['customer_payment'][0]['booking_amount']; 
                    //$bookings[$key]['tax_amount']  = $booking['customer_payment']['tax_amount']; 
                    $bookings['name']        = $bookingDetails['customer']['name'];
                    $bookings['profile_pic'] = !empty($bookingDetails['customer']['profile_pic']) ? Router::url('/','true')."webroot/images".DS.$bookingDetails['customer']['profile_pic'] : $noImage;
                    $bookings['phone']       = $bookingDetails['customer']['phone'];
                    if(!empty($bookingDetails['date']) && $bookingDetails['order_type'] == "reservation") {
                        $bookings['date']  = $bookingDetails['date']->format('l, jS F, Y');
                        $bookings['start_time']  = date('H:i',strtotime($bookingDetails['slot']['start_time']));
                        $bookings['end_time']    = date('H:i',strtotime($bookingDetails['slot']['end_time']));
                    }
                    else {
                        $bookings['date']  = $bookingDetails['modified']->format('l, jS F, Y');
                        $bookings['time']  = $bookingDetails['modified']->format('H:i');
                    }
                    $bookings['email']       = $bookingDetails['customer']['email'];
                    $bookings['visibility']  = $bookingDetails['customer']['user']['visibility'];
                    //$bookings[$key]['status']      = $booking['status'];
                    // Add below code after enhancement
                    $bookings['table_id']  = $bookingDetails['restaurant_arrangement']['name'];
                    $bookings['no_of_guest']  = $bookingDetails['no_of_guest'];
                    $bookings['comment']  = $bookingDetails['comment'];
                    //$bookings[$key]['booking_items'] = $booking['booking_items'];
                    if(!empty($bookingDetails['booking_items'])) {
                        $totalprice = 0;
                        foreach ($bookingDetails['booking_items'] as $keyb => $value2) {
                            $bookings['booking_items'][$keyb]['booking_item_id']  = $value2['id'];
                            $bookings['booking_items'][$keyb]['company_item_id']  = $value2['company_item_id'];
                            $bookings['booking_items'][$keyb]['name']  = $value2['name'];
                            $bookings['booking_items'][$keyb]['price']  = $value2['price'];
                            $bookings['booking_items'][$keyb]['size']  = $value2['size'];
                            $bookings['booking_items'][$keyb]['qty']  = $value2['qty'];
                            $bookings['booking_items'][$keyb]['extra_instruction']  = $value2['extra_instruction'];
                            $bookings['booking_items'][$keyb]['final_price']  = $value2['final_price'];
                            $bookings['booking_items'][$keyb]['company_items_options']  = json_decode($value2['company_items_options']);
                            $totalprice = $totalprice + $value2['final_price']; 
                          }
                            $bookings['total_payble'] = $totalprice;
                        }
                    else {
                            $bookings['booking_items'] = [];
                    }
                if(!empty($bookings)) {
                    $result = array('status'=>'success', 'message'=>'Booking Data.','data'=>$bookings);
                } else {
                    $result = array('status'=>'failure', 'message'=>'No result found.','data'=>$bookings);
                }
            } else {
                $result = array('status'=>'failure', 'message'=>"No record found.", 'data'=>$bookings);
            }
            
            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'data' =>  $result['data'],
                '_serialize' => ['status','message','data']
            ]);
        }
    }
	
	/**
     * API: Booking List
     * PARAMS: profile_id(individual_id / employee_id / room_employee_id, room_id)
     * PARAMS: profile_type(individual / employee / room_employee / other)
     *
     */
	public function bookingList()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            $profile_id = 'Booking.employee_id';
            //Get individual business employee_id
            if($params['profile_type']=='individual') {
                $empId = $this->employeeObj->findByCompanyId($params['profile_id'])->select(['id'])->first();
                $params['profile_id'] = $empId['id'];
                $profile_id = 'Booking.employee_id';
            }

            if($params['profile_type']=='company'){
                $profile_id = 'Booking.company_id';
            }
            //'Booking.payment_status'=>'yes'
            $itemCond = ['BookingItems.payment_status' => 'yes'];
			$bookingDetails = $this->bookingObj->find()->where([$profile_id=>$params['profile_id'],'Booking.payment_status'=>"yes"])->order(['Booking.id'=>'DESC'])->contain(['Customers'=>['fields'=>['Customers.id', 'Customers.name','Customers.phone','Customers.email', 'Customers.profile_pic'], 'Users'=>['fields'=>['Users.id', 'Users.visibility']]], 'Slots','RestaurantArrangement','CustomerPayment','BookingItems'=>function ($q) use ($itemCond) { 
                    return $q->where($itemCond);
                 },'CustomerDeliveryAddress'])->enableHydration(false)->toArray();
			   //Log::notice($bookingDetails);
              // PR($bookingDetails); die;
			$bookings = array();
			if(!empty($bookingDetails)) {
				foreach($bookingDetails as $key=>$booking) {

                     if(empty($booking['date'])) {
                        $booking['date'] = $booking['modified'];
                     }
                    
					 if(strtotime($booking['date']) >= strtotime(date('Y-m-d'))) {
                        $bookings[$key]['booking_id']  = $booking['id'];
                        $bookings[$key]['order_type']  = $booking['order_type'];
                        if($booking['order_type'] == "deliver") {
                            $bookings[$key]['delivery_amount']  = $booking['delivery_amount'];
                            $bookings[$key]['order_type']  = "deliver";
                            $bookings[$key]['customer_delivery_address'] = $booking['customer_delivery_addres']['address']; 
                        }
                        if($booking['order_type'] == "pick") {
                            $bookings[$key]['order_type']  = "pick up";
                        }
                        $bookings[$key]['booking_amount']  = $booking['booking_amount'];
                        $bookings[$key]['is_booking_item']  = $booking['is_booking_item'];
                        $bookings[$key]['booking_amount']  = $booking['customer_payment'][0]['booking_amount']; 
                        //$bookings[$key]['tax_amount']  = $booking['customer_payment']['tax_amount']; 
                        $bookings[$key]['name']        = $booking['customer']['name'];
                        $noImage = Router::url('/','true')."webroot/images".DS."noImage.png";
                        $bookings[$key]['profile_pic'] = !empty($booking['customer']['profile_pic']) ? Router::url('/','true')."webroot/images".DS.$booking['customer']['profile_pic'] : $noImage;
                        $bookings[$key]['phone']       = $booking['customer']['phone'];
                        if(!empty($booking['date']) && $booking['order_type'] == "reservation") {
                        $bookings[$key]['date']  = $booking['date']->format('l, jS F, Y');
                        $bookings[$key]['start_time']  = date('H:i',strtotime($booking['slot']['start_time']));
                        $bookings[$key]['end_time']    = date('H:i',strtotime($booking['slot']['end_time']));
                        }
                        else {
                        $bookings[$key]['date']  = $booking['modified']->format('l, jS F, Y');
                        $bookings[$key]['time']  = $booking['modified']->format('H:i');
                        }

                        $bookings[$key]['email']       = $booking['customer']['email'];
                        $bookings[$key]['visibility']  = $booking['customer']['user']['visibility'];
                        //$bookings[$key]['status']      = $booking['status'];
                        // Add below code after enhancement
                        $bookings[$key]['table_id']  = $booking['restaurant_arrangement']['name'];
                        $bookings[$key]['no_of_guest']  = $booking['no_of_guest'];
                        $bookings[$key]['comment']  = $booking['comment'];
                        //$bookings[$key]['booking_items'] = $booking['booking_items'];
                        if(!empty($booking['booking_items'])) {
                        	$totalprice = 0;
                            foreach ($booking['booking_items'] as $keyb => $value2) {
                                $bookings[$key]['booking_items'][$keyb]['booking_item_id']  = $value2['id'];
                                $bookings[$key]['booking_items'][$keyb]['company_item_id']  = $value2['company_item_id'];
                                $bookings[$key]['booking_items'][$keyb]['name']  = $value2['name'];
                                $bookings[$key]['booking_items'][$keyb]['price']  = $value2['price'];
                                $bookings[$key]['booking_items'][$keyb]['size']  = $value2['size'];
                                $bookings[$key]['booking_items'][$keyb]['qty']  = $value2['qty'];
                                $bookings[$key]['booking_items'][$keyb]['extra_instruction']  = $value2['extra_instruction'];
                                $bookings[$key]['booking_items'][$keyb]['final_price']  = $value2['final_price'];
                                $bookings[$key]['booking_items'][$keyb]['company_items_options']  = json_decode($value2['company_items_options']);
                                $totalprice = $totalprice + $value2['final_price']; 
                            }
                            $bookings[$key]['total_payble'] = $totalprice;
                        }
                        else {
                               $bookings[$key]['booking_items'] = [];
                        }
                     }
				}
                if(!empty($bookings)) {
				    $result = array('status'=>'success', 'message'=>'Booking List.','data'=>array_values($bookings));
                } else {
                    $result = array('status'=>'failure', 'message'=>'No result found.','data'=>array_values($bookings));
                }
			} else {
				$result = array('status'=>'failure', 'message'=>"No Bookings Yet.", 'data'=>$bookings);
			}
			
			$this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'data' =>  $result['data'],
                '_serialize' => ['status','message','data']
            ]);
		}
	}
	
	/**
     * API: View Booking Details
     * PARAMS: profile_id(individual_id / employee_id / room_employee_id / other), booking_id
     */
	public function viewBooking() 
	{
		if($this->request->is('post')) {
            $params = $this->request->data;

			$bookingDetail = $this->bookingObj->find()->where(['Booking.id'=>$params['booking_id'], 'employee_id'=>$params['profile_id']])->contain(['Customers'=>['fields'=>['Customers.id', 'Customers.name','Customers.phone','Customers.email', 'Customers.profile_pic'], 'Users'=>['fields'=>['Users.id', 'Users.visibility']]], 'Slots'=>['fields'=>[ 'Slots.schedule_id','Slots.start_time','Slots.end_time', 'Slots.created']]])->enableHydration(false)->first();
			
			$booking = array();
			if(!empty($bookingDetail)) {
				
				$booking['booking_id']  = $bookingDetail['id'];
				$booking['name']        = $bookingDetail['customer']['name'];
				$booking['profile_pic'] = !empty($bookingDetail['customer']['profile_pic']) ? Router::url('/','true')."webroot/images".DS.$bookingDetail['customer']['profile_pic'] : '';
				$booking['phone']       = $bookingDetail['customer']['phone'];
				$booking['date']        = $bookingDetail['date']->format('l, jS F, Y');
				$booking['start_time']  = date('H:i',strtotime($bookingDetail['slot']['start_time']));
				$booking['end_time']    = date('H:i',strtotime($bookingDetail['slot']['end_time']));
				$booking['email']       = $bookingDetail['customer']['email'];
                $booking['visibility']  = $bookingDetail['customer']['user']['visibility'];
				$booking['status']      = $bookingDetail['status'];
				
				$result = array('status'=>'success', 'message'=>'Booking List.','data'=>$booking);
			} else {
				$result = array('status'=>'failure', 'message'=>"No data found.", 'data'=>$booking);
			}
			
			$this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'data' =>  $result['data'],
                '_serialize' => ['status','message','data']
            ]);
		}
	}

    /**
     * API: Delete booking API's
     * PARAMS: booking_id
     */
    public function deleteBooking() 
    {  
        if($this->request->is('post')) {
            $params = $this->request->data;
            $bookingId = $params['booking_id'];
            $entity = $this->bookingObj->get($bookingId);
            $result = $this->bookingObj->delete($entity);
            if($result) {
                 $result = array('status'=>'success', 'message'=>'Booking deleted successfully.');
            }
            else {
                 $result = array('status'=>'failure', 'message'=>'Something went wrong');
            }
            $this->set([
                    'status'     => $result['status'],
                    'message'    => $result['message'],
                    '_serialize' => ['status', 'message']
            ]);
        }
    }
	
	/**
     * API: Accept/Reject Booking
     * PARAMS: profile_id(individual_id/employee_id/room_employee_id/other), booking_id, status(accepted/rejected/completed)
     * PARAMS: profile_type : "individual/employee/room_employee/other"
     */
	public function acceptRejectBooking()
	{
		if($this->request->is('post')) {
			$params = $this->request->data;

			if(!empty($params['booking_id']) && !empty($params['status'])) {
                //Get individual business employee_id
                if($params['profile_type']=='individual') {
                    $empId = $this->employeeObj->findByCompanyId($params['profile_id'])->select(['id','first_name','last_name'])->first();
                    $params['profile_id'] = $empId['id'];
                    $params['name'] = $empId['first_name'];
                } else {
                    $empId = $this->employeeObj->findById($params['profile_id'])->select(['id','first_name','last_name'])->first();
                    $params['profile_id'] = $empId['id'];
                    $params['name'] = $empId['first_name'];
                }				
				//Get guest details
				$guest = $this->bookingObj->find()->where(['Booking.id'=>$params['booking_id'], 'employee_id'=>$params['profile_id']])->contain(['Customers'=>['fields'=>['Customers.id', 'Customers.user_id'],'Users'=>['fields'=>['Users.id', 'Users.device_type', 'Users.device_token', 'Users.noti_alert']]]])->enableHydration(false)->first();

				if(!empty($guest)) {
					if((strtotime(date('Y-m-d')) >= strtotime($guest['date'])) || ($params['status'] == 'accepted' || $params['status'] == 'rejected')) {
						//Update status in booking table
						$this->bookingObj->updateAll(['status'=>$params['status']], ['id'=>$params['booking_id']]);

						//Send push notification
						if(!empty($guest['customer']['user']['device_type']) && !empty($guest['customer']['user']['device_token'])) {

							//Save notification
							$data = [];
							$data['from_user_id'] = $this->Auth->identify()['id'];
							$data['to_user_id']   = $guest['customer']['user']['id'];
							$data['message']      = 'Booking '.$params['status'];
							$data['type']         = 'guest_booking';
							$this->saveNotification($data);

							//Get unread notification count(badge_count) and update in users table
							$badgeCount = $this->getBadgeCount($guest['customer']['user']['id']);

							//Send push notification
							$device_type = $guest['customer']['user']['device_type'];
							$device_token = $guest['customer']['user']['device_token'];
							if($params['status'] == 'completed') {
								if($guest['customer']['user']['noti_alert'] == "on") {  
								  $this->sendPushNotification($device_type, $device_token,$params['name'].": ".'Your service has been completed, Please
								rates us', 'star_rating', $badgeCount, $params['name']);
								}
							} else {
								if($guest['customer']['user']['noti_alert'] == "on") {
								$this->sendPushNotification($device_type, $device_token, $params['name'].": ".'Booking '.$params['status'], 'guest_booking', $badgeCount, $params['name']);
								}
							}
						}
						$result = array('status'=>'success', 'message'=>'Booking '.$params['status']);
					} else {
						$result = array('status'=>'failure', 'message'=>'Can not complete booking before time.');
					}
				} else {
					$result = array('status'=>'failure', 'message'=>'Booking not exist.');
				}
			} else {
				$result = array('status'=>'failure', 'message'=>'Somthing went wrong.');
			}
			
			$this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message']
            ]);
		}
	}
	
	/**
	 * API: View Gallery
	 * Params: company_id, type('company/other')
	 */
	public function viewGallery()
	{
		if($this->request->is('post')) {
			$params = $this->request->data;
			$gallery = [];
			if(!empty($params['company_id']) && (isset($params['type']) && $params['type'] == 'other')) {
                //Get business gallery images
                $images = $this->galleryImages($params['company_id'], 'other');
								
				if(!empty($images)) {
					$gallery = $images;
					$result = array('status'=>'success', 'message'=>'Gallery image listing.', 'data'=>$gallery);
				} else {
					$result = array('status'=>'failure', 'message'=>'Please add gallery pics.', 'data'=>$gallery);
				}
			} elseif(!empty($params['company_id'])) {
                //Get business gallery images
                $images = $this->galleryImages($params['company_id']);
								
				if(!empty($images)) {
					$gallery = $images;
					$result = array('status'=>'success', 'message'=>'Gallery image listing.', 'data'=>$gallery);
				} else {
					$result = array('status'=>'failure', 'message'=>'Please add gallery pics.', 'data'=>$gallery);
				}
			} else {
				$result = array('status'=>'failure', 'message'=>'Incorrect details.', 'data'=>$gallery);
			}
			
			$this->set([
				'status'     => $result['status'],
				'message'    => $result['message'],
				'data'       => $result['data'],
				'_serialize' => ['status', 'message','data']
			]);
		}
	}
	
	/**
     * API(33): Delete Image (Changed api)
     * Params: company_id(company_id/room_id), image_id, type(company/individual/employee/other)
     */
    public function deleteImage()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;

            if(empty($params['type'])) {
                $params['type'] = 'company';
            }
            
            if(!empty($params['company_id']) && !empty($params['image_id']) && (!empty($params['type']) && $params['type'] == 'other')) {
                $entity = $this->empImgObj->find()->where(['id'=>$params['image_id'], 'employee_id'=>$params['company_id']])->first();
                if(!empty($entity)) {
                    $result = $this->empImgObj->delete($entity);
                    if($result) {
                        $result = array('status'=>'success', 'message'=>'Image deleted successfully.');
                    } else {
                        $result = array('status'=>'failure', 'message'=>'Image not deleted.');
                    }
                } else {
                    $result = array('status'=>'failure', 'message'=>'Incorrect details.');
                }
            } elseif (!empty($params['company_id']) && !empty($params['image_id']) && (!empty($params['type']) && ($params['type'] == 'company' || $params['type'] == 'individual'))) {
                $entity = $this->compImgObj->find()->where(['id'=>$params['image_id'], 'company_id'=>$params['company_id']])->first();
                if(!empty($entity)) {
                    $result = $this->compImgObj->delete($entity);
                    if($result) {
                        $result = array('status'=>'success', 'message'=>'Image deleted successfully.');
                    } else {
                        $result = array('status'=>'failure', 'message'=>'Image not deleted.');
                    }
                } else {
                    $result = array('status'=>'failure', 'message'=>'Incorrect details.');
                }
            } else {
                $result = array('status'=>'failure', 'message'=>'Incorrect details.');
            }
            
            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }
    }

    //Check booked slots
    public function checkBookedSlots($profileId, $scheduleId)
    {
        if(!empty($profileId) && !empty($scheduleId)) {
            //Get all slots of schedule id
			$slots = $this->scheduleObj->findById($scheduleId)->contain(['Slots'=>['fields'=>['Slots.id', 'Slots.schedule_id']]])->enableHydration(false)->first();
            if(!empty($slots)) {
                $slotIds = [];
                foreach ($slots['slots'] as $key => $value) {
                    $slotIds[$key] = $value['id'];
                }

                //Check if slots exist in booking list
                $booking = $this->bookingObj->find()->where(['employee_id'=>$profileId, 'slot_id IN'=>$slotIds])->enableHydration(false)->first();
                if($booking) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    public function bookingHistory()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            $profile_id = 'Booking.employee_id';
            //Get individual business employee_id
            if($params['profile_type']=='individual') {
                $empId = $this->employeeObj->findByCompanyId($params['profile_id'])->select(['id'])->first();
                $params['profile_id'] = $empId['id'];
                $profile_id = 'Booking.employee_id';
            }

            if($params['profile_type']=='company'){
                $profile_id = 'Booking.company_id';
            }
            //'Booking.payment_status'=>'yes'
            $itemCond = ['BookingItems.payment_status' => 'yes'];
            $bookingDetails = $this->bookingObj->find()->where([$profile_id=>$params['profile_id'],'Booking.payment_status'=>"yes"])->contain(['Customers'=>['fields'=>['Customers.id', 'Customers.name','Customers.phone','Customers.email', 'Customers.profile_pic'], 'Users'=>['fields'=>['Users.id', 'Users.visibility']]], 'Slots','RestaurantArrangement','CustomerPayment','BookingItems'=>function ($q) use ($itemCond) { 
                    return $q->where($itemCond);
                 },'CustomerDeliveryAddress'])->enableHydration(false)->toArray();
               //Log::notice($bookingDetails);
            $bookings = array();
            if(!empty($bookingDetails)) {
                foreach($bookingDetails as $key=>$booking) {
                    if(empty($booking['date'])) {
                        $booking['date'] = $booking['modified'];
                     }
                    
                     if(strtotime($booking['date']) < strtotime(date('Y-m-d'))) {
                        $bookings[$key]['booking_id']  = $booking['id'];
                        $bookings[$key]['order_type']  = $booking['order_type'];
                        $bookings[$key]['is_booking_item']  = $booking['is_booking_item'];
                        if($booking['order_type'] == "deliver") {
                            $bookings[$key]['delivery_amount']  = $booking['delivery_amount'];
                            $bookings[$key]['customer_delivery_address'] = $booking['customer_delivery_addres']['address']; 
                        }
                        if($booking['order_type'] == "pick") {
                            $bookings[$key]['order_type']  = "pick up";
                        }
                        $bookings[$key]['booking_amount']  = $booking['booking_amount'];
                        $bookings[$key]['is_booking_item']  = $booking['is_booking_item'];
                        $bookings[$key]['booking_amount']  = $booking['customer_payment'][0]['booking_amount'];
                        $bookings[$key]['tax_amount']      = $booking['customer_payment'][0]['tax_amount']; 
                        $bookings[$key]['delivery_amount']  = $booking['delivery_amount'];
                        $bookings[$key]['name']        = $booking['customer']['name'];
                        $noImage = Router::url('/','true')."webroot/images".DS."noImage.png";
                        $bookings[$key]['profile_pic'] = !empty($booking['customer']['profile_pic']) ? Router::url('/','true')."webroot/images".DS.$booking['customer']['profile_pic'] : $noImage;
                        $bookings[$key]['phone']       = $booking['customer']['phone'];
                        if(!empty($booking['date']) && $booking['order_type'] == "reservation") {
                        $bookings[$key]['date']  = $booking['date']->format('l, jS F, Y');
                        $bookings[$key]['start_time']  = date('H:i',strtotime($booking['slot']['start_time']));
                        $bookings[$key]['end_time']    = date('H:i',strtotime($booking['slot']['end_time']));
                        }
                        else {
                            $bookings[$key]['date']  = $booking['modified']->format('l, jS F, Y');
                            $bookings[$key]['time']  = $booking['modified']->format('H:i');
                        }

                        $bookings[$key]['email']       = $booking['customer']['email'];
                        $bookings[$key]['visibility']  = $booking['customer']['user']['visibility'];
                        //$bookings[$key]['status']      = $booking['status'];
                        // Add below code after enhancement
                        $bookings[$key]['table_id']  = $booking['restaurant_arrangement']['name'];
                        $bookings[$key]['no_of_guest']  = $booking['no_of_guest'];
                        $bookings[$key]['comment']  = $booking['comment'];
                        //$bookings[$key]['booking_items'] = $booking['booking_items'];
                        if(!empty($booking['booking_items'])) {
                            $totalprice = 0;
                            foreach ($booking['booking_items'] as $keyb => $value2) {
                                $bookings[$key]['booking_items'][$keyb]['booking_item_id']  = $value2['id'];
                                $bookings[$key]['booking_items'][$keyb]['company_item_id']  = $value2['company_item_id'];
                                $bookings[$key]['booking_items'][$keyb]['name']  = $value2['name'];
                                $bookings[$key]['booking_items'][$keyb]['price']  = $value2['price'];
                                $bookings[$key]['booking_items'][$keyb]['size']  = $value2['size'];
                                $bookings[$key]['booking_items'][$keyb]['qty']  = $value2['qty'];
                                $bookings[$key]['booking_items'][$keyb]['extra_instruction']  = $value2['extra_instruction'];
                                $bookings[$key]['booking_items'][$keyb]['final_price']  = $value2['final_price'];
                                $bookings[$key]['booking_items'][$keyb]['company_items_options']  = json_decode($value2['company_items_options']);
                                $totalprice = $totalprice + $value2['final_price']; 
                            }
                            $bookings[$key]['total_payble'] = $totalprice;
                        }
                        else {
                               $bookings[$key]['booking_items'] = [];
                        }
                     }
                }
                if(!empty($bookings)) {
                    $result = array('status'=>'success', 'message'=>'Booking List.','data'=>array_values($bookings));
                } else {
                    $result = array('status'=>'failure', 'message'=>'No result found.','data'=>array_values($bookings));
                }
            } else {
                $result = array('status'=>'failure', 'message'=>"No Bookings Yet.", 'data'=>$bookings);
            }
            
            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'data' =>  $result['data'],
                '_serialize' => ['status','message','data']
            ]);
        }
    }

    /**
     * API: Join wait List
     * PARAMS: customer_id, company_id
     */
    public function joinWaitList()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            $params['type'] = 'wait';
             $bookingDetails = $this->bookingObj->find()->where(['customer_id'=>$params['customer_id'], 'status' =>'pending',
             'company_id' => $params['company_id'],'type'=>'wait'])->order(['Booking.created'=>'ASC'])->contain(['Customers'])->toArray();

            if(!empty($bookingDetails)) {
                $waitNo = $this->customerWating($params['company_id'],$bookingDetails[0]['created']);
                $result = array('status'=>'success', 'message'=>"You are already in waiting List, your waiting number is ".$waitNo );
                echo json_encode($result); die;
            }

            if(empty($params['customer_id'])) {
                $cusDetails = $this->customerObj->findByUserId($this->Auth->identify()['id'])->first();
                $params['customer_id'] = $cusDetails['id'];
            }
            $booking = $this->bookingObj->newEntity();

            $booking = $this->bookingObj->patchEntity($booking, $params);
            if ($this->bookingObj->save($booking))
            {
                $company =$this->Companies->findById($params['company_id'])->contain(['Users'=>['fields'=>['Users.id','Users.device_type','Users.device_token']]])->select('user_id')->enableHydration(false)->first();

                $waitNo = $this->customerWating($params['company_id'],$booking->created);

                $device_type = $company['user']['device_type'];
                $device_token = $company['user']['device_token'];
                //Send push notification
                if(!empty($device_token) && !empty($device_type)) {
                    //Save notification
                    $data = [];
                    $data['from_user_id'] = $this->Auth->identify()['id'];
                    $data['to_user_id']   = $company['user_id'];
                    $data['message']      = 'Waiting Request';
                    $data['type']         = 'waiting_request';
                    $this->saveNotification($data);

                    //Get unread notification count(badge_count) andupdate in users table
                    $badgeCount = $this->getBadgeCount($company['user_id']);

                    //Send notification
                    $this->sendPushNotification($device_type,$device_token, 'Waiting Request', 'waiting_request',$badgeCount,'no');
                }

                $result = array('status'=>'success','message'=>"Waiting request send successfully.",'data'=>array('wait_id'=>$booking->id , 'waiting_no' => $waitNo));

            } else {
                $errors = $booking->errors();
                $erorMessage = array();
                $i = 0;
                $keys = array_keys($errors);
                foreach ($errors as $errors) {
                    $key = key($errors);
                    foreach($errors as $error){
                        $erorMessage = ucfirst($keys[$i]) . " :- " . $error;
                        //$erorMessage = $error;
                    }
                    $i++;
                }
                $result = array('status'=>'error','message'=>$erorMessage, 'data'=>array());
            }

            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'data' =>  $result['data'],
                '_serialize' => ['status','message','data']
            ]);
        }
    }

    /**
     * API: company wait List
     * PARAMS: company_id
     */
    public function companyWaitList() 
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            $bookingDetails = $this->bookingObj->find()->where(['company_id'=>$params['company_id'] , 'status'=>'pending','Booking.type' =>'wait'])->order(['Booking.created'=>'ASC'])->contain(['Customers'])->toArray();
            $noImage = Router::url('/','true')."webroot/images".DS."noImage.png";

            $waitList = array();
            if(!empty($bookingDetails)) {
            foreach($bookingDetails as $key => $value) {
                $waitList[$key]['wait_id'] = $value['id'];
                $waitList[$key]['customer_name'] = $value['customer']['name'];
                $waitList[$key]['profile_pic'] = !empty($value['customer']['profile_pic']) ? Router::url('/','true')."webroot/images".DS.$value['customer']['profile_pic'] : $noImage;
                $waitList[$key]['number'] = $value['customer']['phone'];
                $waitList[$key]['comment'] = $value['comment'];
                $waitList[$key]['join'] = $value['created']->format('Y-m-d h:i:s');
                $waitList[$key]['status'] = $value['status'];
                $waitList[$key]['waiting_no'] = $key+1;
            }
                $result = array('status'=>'success', 'message'=>'Waiting List.','data'=>$waitList);
            }
            else {
                $result = array('status'=>'failure', 'message'=>"Waiting List is empty.", 'data'=>$waitList);
            }
            $this->set([
                    'status' => $result['status'],
                    'message' => $result['message'],
                    'data' =>  $result['data'],
                    '_serialize' => ['status','message','data']
            ]);
        }
    }

    /**
     * API: guest wait List
     * PARAMS: customer_id
     */
    public function guestWaitList() 
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            $bookingDetails = $this->bookingObj->find()->where(['customer_id'=> $params['customer_id'] , 'Booking.status'=>'pending','Booking.type' =>'wait'])->contain('Companies.Categories')->order(['Booking.created'=>'ASC'])->toArray();

            $noImage = Router::url('/','true')."webroot/images".DS."noImage.png";

            $waitList = array();
            if(!empty($bookingDetails)) {
            foreach($bookingDetails as $key => $value) {
                $waitList[$key]['wait_id'] = $value['id'];
                $waitList[$key]['company_id'] = $value['company_id'];
                $images = $this->galleryImages($value['company_id']);
                $compImage = !empty($images) ? $images[0]['image'] : $noImage;
                $waitList[$key]['image'] = $compImage;
                $waitList[$key]['category_name'] = $value['company']['category']['name'];
                $waitList[$key]['owner_name'] = $value['company']['first_name'].' '.$value['company']['last_name'];
                $waitList[$key]['company_name'] = $value['company']['company_name'];
                $waitList[$key]['address'] = $value['company']['address'];
                $waitList[$key]['city'] = $value['company']['city'];
				$waitList[$key]['state'] = $value['company']['state'];
				$waitList[$key]['country'] = $value['company']['country'];
                $waitList[$key]['zipcode'] = $value['company']['zipcode'];
                $waitList[$key]['join'] = $value['created']->format('Y-m-d h:i:s');
                $waitList[$key]['status'] = $value['status'];
                // waiting no. of customer for particular business
                $waitList[$key]['waiting_no'] = $this->customerWating($value['company_id'],$value['created']);
                // Total no. of waiting no. customers for particular business
                $waitList[$key]['total_waiting'] = $this->totalWaiting($value['company_id']);

            }
                $result = array('status'=>'success', 'message'=>'Wating List.','data'=>$waitList);
            }
            else {
                $result = array('status'=>'failure', 'message'=>"No data found.", 'data'=>$waitList);
            }
            $this->set([
                    'status' => $result['status'],
                    'message' => $result['message'],
                    'data' =>  $result['data'],
                    '_serialize' => ['status','message','data']
            ]);
        }
    }

    // waiting no. of customer for particular business
    public function customerWating($companyId, $created) 
    {
        $total = $this->bookingObj->find()->where(['company_id'=>$companyId ,'Booking.status' =>'pending', 'created <' => $created,'type'=>'wait'])->count();
        $wait = $total + 1;
        return $wait;
    }

    // Total no. of waiting no. customers for particular business
    public function totalWaiting($companyId) 
    {
        $total = $this->bookingObj->find()->where(['company_id'=>$companyId ,'Booking.status'=>'pending','type'=>'wait'])->order(['Booking.created'=>'ASC'])->count();
        return $total;
    }

    /**
     * API: guest waiting Line
     * PARAMS: wait_id
     */
    public function waitingLine() 
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            $wait_id = $params['wait_id'];
            $wait = $this->bookingObj->findById($wait_id)->where(['status' => 'pending'])->contain('Customers')->first(); 
            $noImage = Router::url('/','true')."webroot/images".DS."noImage.png";
            $i = 1;
            $company  = $this->Companies->findById($wait['company_id'])->select(['business_phone'])->first();
            $waiting_no = $this->customerWating($wait['company_id'],$wait['created']);
            //Total no. of waiting no. customers for particular business
            $total_waiting = $this->totalWaiting($wait['company_id']);
            $waitingLine = array();
            if(!empty($wait)) {
                for($i = 1; $i<= $total_waiting; $i++) {
                    $waitingLine[$i-1]['waiting_no'] = $i;
                    $waitingLine[$i-1]['profile_pic'] = $noImage;
                    if($i == $waiting_no) {
                        $waitingLine[$i-1]['wait_id'] = $wait_id;
                        $waitingLine[$i-1]['waiting_no'] = $i;
                        $waitingLine[$i-1]['profile_pic'] = !empty($wait['customer']['profile_pic']) ? Router::url('/','true')."webroot/images".DS.$wait['customer']['profile_pic'] : $noImage;
                        $waitingLine[$i-1]['customer_name'] = $wait['customer']['name'];
                        //$waitingLine[$i-1]['phone'] = $wait['customer']['phone'];
                        $waitingLine[$i-1]['phone'] = $company['business_phone'];
                        $waitingLine[$i-1]['comment'] = $wait['comment'];
                    }
                }
                $result = array('status'=>'success', 'message'=>'Waiting Line.','data'=>$waitingLine);
            }
            else {
                $result = array('status'=>'failure', 'message'=>"No People in line.", 'data'=>$waitingLine);
            }
            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'data' =>  $result['data'],
                '_serialize' => ['status','message','data']
            ]);
        }
    }

    /**
     * API: Add comment from waiting Line
     * PARAMS: wait_id , comment
     */
    public function addWaitComment() 
    {
        if($this->request->is('post')) {
            $params = $this->request->data;

            $wait_id = $params['wait_id'];
			$params['comment'] = $params['message'];
            $wait = $this->bookingObj->get($wait_id);
            $wait = $this->bookingObj->patchEntity($wait,$params);
            if($this->bookingObj->save($wait)) {
                $result = array('status'=>'success', 'message'=>'Comment saved.');
            }
            else {
                $result = array('status'=>'failure', 'message'=>'Something went wrong.');
            }
            $this->set([
                        'status' => $result['status'],
                        'message' => $result['message'],
                        '_serialize' => ['status','message']
            ]);
        }
    }

    /**
     * API: Complete/Remove Booking
     * PARAMS: company_id, wait_id, status(rejected/completed), type(guest_waiting/company_waiting)
     */
    public function waitingAction()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            if($params['type'] == 'guest_waiting') {
                $receiveNoti = $this->bookingObj->find()->where(['Booking.id'=>$params['wait_id'], 'company_id'=>$params['company_id']])->contain(['Customers'=>['fields'=>['Customers.id', 'Customers.user_id', 'Customers.name'],'Users'=>['fields'=>['Users.id', 'Users.device_type', 'Users.device_token', 'Users.noti_alert']]],
                    'Companies'=>['fields'=>['Companies.id','Companies.user_id','Companies.first_name']]])->enableHydration(false)->first();

                $fromName     = $receiveNoti['company']['first_name'];
                $fromUserId   = $receiveNoti['company']['user_id'];
                $toUserId     = $receiveNoti['customer']['user']['id'];
                $device_type  = $receiveNoti['customer']['user']['device_type'];
                $device_token = $receiveNoti['customer']['user']['device_token'];
            } else {
                $receiveNoti = $this->bookingObj->find()->where(['Booking.id'=>$params['wait_id'], 'company_id'=>$params['company_id']])->contain(['Companies'=>['fields'=>['Companies.id', 'Companies.user_id'],'Users'=>['fields'=>['Users.id', 'Users.device_type', 'Users.device_token', 'Users.noti_alert']]],
                    'Customers'=>['fields'=>['Customers.id','Customers.user_id','Customers.name']]])->enableHydration(false)->first();

                $fromName     = $receiveNoti['customer']['name'];
                $fromUserId   = $receiveNoti['customer']['user_id'];
                $toUserId     = $receiveNoti['company']['user']['id'];
                $device_type  = $receiveNoti['company']['user']['device_type'];
                $device_token = $receiveNoti['company']['user']['device_token'];
            }

            if(!empty($receiveNoti)) {
				//Update status in booking table
				$res = $this->bookingObj->updateAll(['status'=>$params['status']], ['id'=>$params['wait_id']]);
				if($res) {
					//Send push notification
					if(!empty($device_type) && !empty($device_token)) {
						//Save notification
						$data = [];
						$data['from_user_id'] = $this->Auth->identify()['id'];
						$data['to_user_id']   = $toUserId;
						$data['message']      = $fromName.': Job '.$params['status'];
						$data['type']         = $params['type'];
						$this->saveNotification($data);
						//Get unread notification count(badge_count) and update in users table
						$badgeCount = $this->getBadgeCount($toUserId);
						$this->sendPushNotification($device_type, $device_token, $data['message'],$data['type'],$badgeCount, $fromName);
					}
					$result = array('status'=>'success', 'message'=>'Job '.$params['status']);
					$this->sendWaitingNotification($params['company_id']);
				} else {
					$result = array('status'=>'failure', 'message'=>'Waiting status not updated');
				}
            }
            else {
                $result = array('status'=>'failure', 'message'=>'Somthing went wrong.');
            }
            
            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message']
            ]);
        }
    }

    public function sendWaitingNotification($companyId) 
    {
        $company  = $this->Companies->findById($companyId)->select(['id','first_name','last_name'])->first();
        $fromName = $company['first_name']." ".$company['last_name'];

        $waitingDetails =$this->bookingObj->find()->where(['company_id'=>$companyId ,'Booking.status'=>'pending','Booking.type'=>'wait'])->order(['Booking.created'=>'ASC'])->toArray();

        foreach ($waitingDetails as $key => $value) {
            $customer[$key] = $this->customerObj->findById($value['customer_id'])->contain(['Users'=>['fields'=>['Users.id', 'Users.device_type', 'Users.device_token','Users.noti_alert']]])->select(['user_id','customer_id'=>$value['customer_id'], 'waiting_no'=>$key + 1])->enableHydration(false)->first();

            $device_type = $customer[$key]['user']['device_type'];
            $device_token = $customer[$key]['user']['device_token'];
			
            //Send push notification
            if(!empty($device_token) && !empty($device_type)) {
                $data = [];
                $data['message'] = $fromName.': Your waiting no is '.$customer[$key]['waiting_no'];
                $data['type']    = 'guest_waiting';
                //Get unread notification count(badge_count) and update in users table
                $badgeCount = $this->getBadgeCount($customer[$key]['user_id']);
                //Send notification 
                if($customer[$key]['user']['noti_alert'] == "on") {   
                    $this->sendPushNotification($device_type, $device_token, $data['message'], $data['type'], $badgeCount,'no');
                }
            }
        }
    }
	
	/**
     * API: Add Business Timing
     * PARAMS: profile_id(company_id/individual_id), profile_type(company/individual), day, start_time, end_time
     * Request Body:
        {
            "profile_id": 48,
            "profile_type": "company",
            "timings": [
                {
                    "day": "monday",
                    "start_time": "09:30",
                    "end_time": "06:00" 
                }
            ]
        }
     */
    public function addBusinessTimings()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            if(!empty($params['profile_id']) && !empty($params['profile_type']) && !empty($params['timings'])) {
                
                $params['company_id'] = $params['profile_id'];
                $params['type'] = $params['profile_type'];

                foreach ($params['timings'] as $key => $value) {
                    $timing = $this->businessTimeObj->newEntity();
                    
                    $busId = $this->getBussTimeId($params['company_id'], $params['type'], $value['day']);
                    if(!empty($busId)) {
                        $timing->id = $busId;
                    }

                    $timing->day = $value['day'];
                    $timing->start_time = $value['start_time'];
                    $timing->end_time = $value['end_time'];
                    $timing = $this->businessTimeObj->patchEntity($timing, $params);
                    $this->businessTimeObj->save($timing);
                }
                $result = array('status'=>'success', 'message'=>"Timing saved successfully.");
            } else {
                $result = array('status'=>'failure', 'message'=>"Timing not saved.");
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }
    }
    
    public function getBussTimeId($custId, $type, $day)
    {
        $busId ='';
        $res = $this->businessTimeObj->find('all')->where(['company_id'=>$custId, 'type'=>$type, 'day'=>$day])->select(['id'])->enableHydration(false)->first();
        if(!empty($res)) {
            $busId = $res['id'];
        }
        return $busId;
    }

    /**
     * API: View Business Timings
     * PARAMS: profile_id, profile_type
     */
    public function getBusinessTimings()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;

            $businessTiming = array();
            if(!empty($params['profile_id']) && !empty($params['profile_type'])) {
                $timings = $this->businessTimeObj->find('all')->where(['company_id'=>$params['profile_id'], 'type'=>$params['profile_type']])->select(['id', 'company_id', 'type', 'day', 'start_time', 'end_time'])->enableHydration(false)->toArray();


                if(!empty($timings)) {
                    $businessTiming['profile_id'] = $timings[0]['company_id'];
                    $businessTiming['profile_type'] = $timings[0]['type'];
                    foreach ($timings as $key => $value) {
                        $businessTiming['timings'][$key]['id'] = $value['id'];
                        $businessTiming['timings'][$key]['day'] = $value['day'];
                        $businessTiming['timings'][$key]['start_time'] = date('H:i',strtotime($value['start_time']));
                        $businessTiming['timings'][$key]['end_time'] = date('H:i',strtotime($value['end_time']));
                    }

                    $result = array('status'=>'success', 'message'=>"Business time listing.", 'data'=>$businessTiming);
                } else {
                    $result = array('status'=>'failure', 'message'=>"No record found.", 'data'=>$businessTiming);
                }
            } else {
                $result = array('status'=>'failure', 'message'=>"No record found.", 'data'=>$businessTiming);
            }
        
            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                'data'       => $result['data'],
                '_serialize' => ['status', 'message', 'data']
            ]);
        }
    }
    
    /**
     * API: Delete business timing API's
     * PARAMS: id
     */
    public function deleteBusinessTiming() 
    {  
        if($this->request->is('post')) {
            $params = $this->request->data;
            $businessTimeId = $params['id'];
			$entity = $this->businessTimeObj->findById($businessTimeId)->first();
			if(!empty($entity)) {
				$result = $this->businessTimeObj->delete($entity);
				if($result) {
					$result = array('status'=>'success', 'message'=>'Business timing deleted.');
				} else {
					$result = array('status'=>'failure', 'message'=>'Something went wrong');
				}
			} else {
				$result = array('status'=>'failure', 'message'=>'No record available to delete.');
			}
            
            $this->set([
				'status'     => $result['status'],
				'message'    => $result['message'],
				'_serialize' => ['status', 'message']
            ]);
        }
    }

    /**
     * API: Open/Close wait list
     * PARAMS: profile_id, wait_status(open/close)
     */
    public function changeWaitStatus()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;

            if(!empty($params['profile_id']) && !empty($params['wait_status'])) {

                $res = $this->Companies->updateAll(['wait_status'=>$params['wait_status']], ['id'=>$params['profile_id']]);
                if ($res) {
                    $result = array('status'=>'success', 'message'=>"Waiting status ".$params['wait_status']);
                } else {
                    $result = array('status'=>'failure', 'message'=>"Waiting status not updated");
                }
            } else {
                $result = array('status'=>'failure', 'message'=>"Something went wrong.");
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }
    }

    //CRON: Send notification to guest when waiting status is pending & bussiness setting is close
    public function deleteAfterClose() {
        Log::notice("waiting status is pending & bussiness setting is close.");
        $bookingDetails = $this->bookingObj->find('all')
											->where(['Booking.status'=>'pending','Booking.type' =>'wait'])
											->contain(['Companies'=> function ($q) { 
													return $q->where(['Companies.wait_status' => "close"]);
												}])
											->order(['Booking.created'=>'ASC'])
											->toArray();
		$waitClose = array();
		foreach ($bookingDetails as $key => $value) {
			$waitClose[$key]['wait_id']      = $value['id'];
			$waitClose[$key]['company_id']   = $value['company_id'];
			$waitClose[$key]['customer_id']  = $value['customer_id'];
			$waitClose[$key]['company_name'] = $value['company']['company_name'];
			$waitClose[$key]['owner_name']   = $value['company']['first_name']." ".$value['company']['last_name'];
			$waitClose[$key]['created']      = $value['created']->format('Y-m-d');

			$customer[$key] = $this->customerObj->findById($value['customer_id'])->contain(['Users'=>['fields'=>['Users.id', 'Users.device_type', 'Users.device_token','Users.noti_alert']]])->select(['user_id'])->enableHydration(false)->first();
			$entity = $this->bookingObj->find()->where(['id'=>$value['id']])->first();
			$timing = $this->getBusinessTime($value['company_id'], date('D',strtotime(date('Y-m-d H:i:s')))."day");
			if(!empty($timing)) {
				$result = date('h:i:s') > $timing['end_time']->format('h:i:s');
			} else {
				$result = date('Y-m-d') > $value['created']->format('Y-m-d');
			}
         
			//delete pending status if business setting is closed
			if($result) {
			    $result = $this->bookingObj->delete($entity);
			    $device_type =  $customer[$key]['user']['device_type'];
			    $device_token = $customer[$key]['user']['device_token'];
				//Send push notification
			    if(!empty($device_token) && !empty($device_type)) {
					$data = [];
					$data['message'] = $waitClose[$key]['owner_name'].': Waiting deleted due to business close time. ';
					$data['type']    = 'guest_waiting'; 
					//Get unread notification count(badge_count)
					$badgeCount = $this->getBadgeCount($customer[$key]['user_id']);
					//Send notification 
					if($customer[$key]['user']['noti_alert'] == "on") {   
						$this->sendPushNotification($device_type, $device_token, $data['message'], $data['type'], $badgeCount,'no');
					}
				}
			}
		}
    }

    /**** New changes (Enhancement) ****/

    /**
     * API: restautant table lsiting
     * PARAMS: company_id
	 */
    public function restaurantTableList() {
        if ($this->request->is('post')) {
        
            $params = $this->request->data;

            $tableLists = $this->restaurantObj->find('all')->where(['company_id'=>$params['company_id']])->order(['id'=>'DESC'])->enableHydration(false)->toArray();

            if(!empty($tableLists)) {

                $result = array('status'=>'success', 'message'=>"List of tables.", 'data'=>$tableLists);

            } else {

                $result = array('status'=>'failure', 'message'=>"No record found.", 'data'=>$tableLists);
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                'data'    => $result['data'],
                '_serialize' => ['status', 'message','data']
            ]);
        }
    }

    /**
     * API: Add company items
     * PARAMS: company_id, name, capacity, description
    */
    public function addRestaurantTable() {
        if ($this->request->is('post')) {
        
            $params = $this->request->data;
            //Log::notice($params);
            if(!empty($params['id'])) {
             $restaurant = $this->restaurantObj->get($params['id']);
             $msg = "Updated successfully";
            }
            else {
             $restaurant = $this->restaurantObj->newEntity(); 
             $msg = "Saved successfully";
            }

            $restaurant = $this->restaurantObj->patchEntity($restaurant, $params);
            if($this->restaurantObj->save($restaurant)) {
                    $result = array('status'=>'success', 'message'=> $msg );
            } 
            else {
                        $errors = $customer->errors();
                        $erorMessage = array(); 
                        $i = 0; 
                        $keys = array_keys($errors); 
                        foreach ($errors as $errors) { 
                             $key = key($errors); 
                            foreach($errors as $error){ 
                                    $erorMessage = ucfirst($keys[$i]) . " :- " . $error;
                                    //$erorMessage = $error;
                                 }
                                $i++;
                            }
                        $result = array('status'=>'failure', 'message'=>$erorMessage);
            } 

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }
    }

    /**
    * API: Delete RestaurantTable API's
    * PARAMS: id
    */
    public function deleteRestaurantTable() 
    {  
        if($this->request->is('post')) {
            $params = $this->request->data;
            $entity = $this->restaurantObj->get($params);
            $result = $this->restaurantObj->delete($entity);
            if($result) {
                 $result = array('status'=>'success', 'message'=>'Deleted successfully.');
            }
            else {
                 $result = array('status'=>'failure', 'message'=>'Something went wrong');
            }
            $this->set([
                    'status'     => $result['status'],
                    'message'    => $result['message'],
                    '_serialize' => ['status', 'message']
            ]);
        }
    } 

    // Add new caterory
    public function addUpdateCompanyCategory() 
    {   
        if($this->request->is('post')) {
            $params = $this->request->data;
            if(!empty($params['company_category_id'])) {
             $category = $this->compCatObj->get($params['company_category_id']);
             $msg = "Updated successfully";
            }
            else {
             $category = $this->compCatObj->newEntity(); 
             $msg = "Saved successfully";
            }
            $category = $this->compCatObj->patchEntity($category, $params);
            if($category = $this->compCatObj->save($category)) {
               $result = array('status'=>'success', 'message'=> $msg );
            } else {
                $result = array('status'=>'failure', 'message'=>'Something went wrong');
            }
            $this->set([
                    'status'     => $result['status'],
                    'message'    => $result['message'],
                    '_serialize' => ['status', 'message']
            ]);                     
        }        
    }

    /**
    * API: List of categories
    * PARAMS: type(guest/host)
    */
    public function companyCategoryList() 
    {
        if($this->request->is('Post')) {
            $params = $this->request->data;
            if($params['type'] == 'guest') {
				$categoryData = $this->compCatObj->find()->select(['CompanyCategory.id', 'CompanyCategory.name','CompanyCategory.restricted'])->contain(['CompanyItems'=>['CompanyItemsOptions'=>['fields'=>['id', 'name','price','company_item_id']],'sort' =>['CompanyItems.id' => 'DESC']]])->where(['company_id'=> $params['company_id']])->order(['CompanyCategory.id'=>'DESC'])->toArray();
            } else {
                $categoryData = $this->compCatObj->find()->select(['CompanyCategory.id', 'CompanyCategory.name','CompanyCategory.restricted'])->where(['company_id'=> $params['company_id']])->order(['CompanyCategory.id'=>'DESC'])->toArray(); 
            }
            
            $cat = array();
            if(!empty($categoryData)) {
                foreach ($categoryData as $key => $value) {
                    $cat[$key]['company_category_id'] = $value['id'];
                    $cat[$key]['name'] = $value['name'];
                    $cat[$key]['restricted'] = $value['restricted'];
              
                    if($params['type'] == 'guest') {
                        $cat[$key]['company_items'] = $value['company_items'];
                        $count = count($value['company_items']);
                        $noImage = Router::url('/','true')."webroot/images".DS."noImage.png";
                        
                        for ($i=0; $i < $count; $i++) { 
                            unset($cat[$key]['company_items'][$i]['company_category_id']);
                             $cat[$key]['company_items'][$i]['company_item_id'] =   $cat[$key]['company_items'][$i]['id'];
                            $cat[$key]['company_items'][$i]['image'] =  !empty($value['company_items'][$i]['image']) ?  Router::url('/','true')."webroot/images/itemimages/".$value['company_items'][$i]['image'] : $noImage;
                            $cat[$key]['company_items'][$i]['size'] = json_decode($value['company_items'][$i]['size']);
                        }
                    }
                }
                $result = array('status'=>'success', 'message'=>'List of company categories','data'=>$cat);
            } else {
                $result = array('status'=>'failure', 'message'=>'No Record found','data' => array());
            }
            $this->set([
                'data' =>  $result['data'],
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message','data']
            ]); 
        }
    }

    public function viewCompanyItems() {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            $data = $this->companyItems->find()
                                       ->where(['id'=> $params['company_item_id']])->contain(['CompanyItemsOptions'=>['fields'=>['id','name','price','company_item_id']]])->enableHydration(false)->first();
            $companyItemsData = array();
            if(!empty($data))
			{
				$companyItemsData['company_item_id'] = $data['id'];
				$companyItemsData['name'] = $data['name'];
				$noImage = Router::url('/','true')."webroot/images".DS."noImage.png";
				$companyItemsData['image'] = !empty($data['image']) ? Router::url('/','true')."webroot/images/itemimages/".$data['image'] : $noImage;
				$companyItemsData['price'] = $data['price'];
				$companyItemsData['description'] = $data['description'];
				$size = json_decode($data['size'],true);
				foreach ($size as $key1 => $value1) {
					$companyItemsData['size'][$key1]['size'] = $value1['size'];
					$companyItemsData['size'][$key1]['price'] = (float)$value1['price']; 
				}
				//$companyItemsData['size'] = json_decode($data['size']);
				$companyItemsData['company_items_options'] = $data['company_items_options'];
				$result = array('status'=>'success', 'message'=>'Item Detail.','data'=>$companyItemsData);
			}
            else{
                $result = array('status'=>'failure', 'message'=>"No record found",'data'=>$companyItemsData);
			} 

			$this->set([
				'data'   => $result['data'],
				'status' => $result['status'],
				'message' => $result['message'],
				'_serialize' => ['status','message','data']
			]); 
        }
    }

    public function addCompanyItems() {
        if($this->request->is('post')) {
            $params = $this->request->data;
            $params['company_items_options'] = json_decode($params['company_items_options'], true);
            //Log::notice($params);
            if(!empty($params['image'])) {
                $uploadResult = $this->ImageUpload->uploadCompanyitemImages($params['image']);
                $params['image'] =  $uploadResult;
            }
            //pr($params); die;
            $companyItemsData = $this->companyItems->newEntity();
            $companyItemsData = $this->companyItems->patchEntity($companyItemsData, $params, ['associated' => ['CompanyItemsOptions']]);

            if($this->companyItems->save($companyItemsData)) {
                $result = array('status'=>'success', 'message'=>'Food Item added successfully.','company_item_id'=>$companyItemsData->id);
            } else {
				$errors = $companyItemsData->errors();
				$erorMessage = array(); 
				$i = 0; 
				$keys = array_keys($errors); 
				foreach ($errors as $errors) { 
					$key = key($errors); 
					foreach($errors as $error){ 
						$erorMessage = $keys[$i] . " :- " . $error;
							//$erorMessage = $error;
					}
					$i++;
				}
				$result = array('status'=>'failure', 'message'=>$erorMessage,'company_item_id'=>null);
            } 

            $this->set([
                'company_item_id' =>  $result['company_item_id'],
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message','company_item_id']
            ]); 
        }
    }

    public function addCompanyItems1() {
        if($this->request->is('post')) {
            $params = $this->request->data;
            $params['company_items_options'] = json_decode($params['company_items_options'], true);
            //Log::notice($params);
            if(!empty($params['image'])) {
                $uploadResult = $this->ImageUpload->uploadCompanyitemImages($params['image']);
                $params['image'] =  $uploadResult;
            }
            
            $companyItemsData = $this->companyItems->newEntity();
            $companyItemsData = $this->companyItems->patchEntity($companyItemsData, $params, ['associated' => ['CompanyItemsOptions']]);

            if($this->companyItems->save($companyItemsData)) {
                $result = array('status'=>'success', 'message'=>'Food Item added successfully.','company_item_id'=>$companyItemsData->id);
            } else {
				$errors = $companyItemsData->errors();
				$erorMessage = array(); 
				$i = 0; 
				$keys = array_keys($errors); 
				foreach ($errors as $errors) { 
					$key = key($errors); 
					foreach($errors as $error){ 
						$erorMessage = $keys[$i] . " :- " . $error;
						//$erorMessage = $error;
					}
					$i++;
				}
				$result = array('status'=>'failure', 'message'=>$erorMessage,'company_item_id'=>null);
            } 

            $this->set([
                'company_item_id' =>  $result['company_item_id'],
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message','company_item_id']
            ]); 
        }
    }

    public function companyItemLists() {
        if($this->request->is('post')) {
            $params = $this->request->data;
                    //Log::notice($params);
            $data = $this->companyItems->find()
                                       ->where(['CompanyItems.company_id'=> $params['company_id']])->order(['CompanyItems.id'=>'DESC'])->contain(['CompanyItemsOptions' , 'CompanyCategory'])->toArray();
            $companyItemsData = array();
            if(!empty($data))
                {
                  foreach ($data as $key => $value) {
                        $companyItemsData[$key]['company_item_id'] = $value['id'];
                        $companyItemsData[$key]['name'] = $value['name'];
                        $noImage = Router::url('/','true')."webroot/images".DS."noImage.png";
                        $companyItemsData[$key]['image'] = !empty($value['image']) ? Router::url('/','true')."webroot/images/itemimages/".$value['image'] : $noImage;
                        $companyItemsData[$key]['price'] = $value['price'];
                        $companyItemsData[$key]['description'] = $value['description'];
                        $companyItemsData[$key]['company_category_id'] = $value['company_category']['id'];
                        $companyItemsData[$key]['category_name'] = $value['company_category']['name'];
                        $companyItemsData[$key]['size'] = json_decode($value['size']);
                        $companyItemsData[$key]['company_items_options'] = $value['company_items_options'];
                    }
                    $result = array('status'=>'success', 'message'=>'Item Listing.','data'=>$companyItemsData);
                }

            else{
                   
                $result = array('status'=>'failure', 'message'=>"No record found",'data'=>$companyItemsData);
            } 

            $this->set([
                'data'   => $result['data'],
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message','data']
            ]); 
        }
    }

    public function companyItemLists1() {
        if($this->request->is('post')) {
            $params = $this->request->data;
                    //Log::notice($params);
            $data = $this->companyItems->find()
                                       ->where(['CompanyItems.company_id'=> $params['company_id']])->order(['CompanyItems.id'=>'DESC'])->contain(['CompanyItemsOptions' , 'CompanyCategory'])->toArray();
            $companyItemsData = array();
            if(!empty($data))
                {
                  foreach ($data as $key => $value) {
                        $companyItemsData[$key]['company_item_id'] = $value['id'];
                        $companyItemsData[$key]['name'] = $value['name'];
                        $noImage = Router::url('/','true')."webroot/images".DS."noImage.png";
                        $companyItemsData[$key]['image'] = !empty($value['image']) ? Router::url('/','true')."webroot/images/itemimages/".$value['image'] : $noImage;
                        $companyItemsData[$key]['price'] = $value['price'];
                        $companyItemsData[$key]['description'] = $value['description'];
                        $companyItemsData[$key]['company_category_id'] = $value['company_category']['id'];
                        $companyItemsData[$key]['category_name'] = $value['company_category']['name'];
                        $companyItemsData[$key]['size'] = json_decode($value['size']);
                        $companyItemsData[$key]['company_items_options'] = $value['company_items_options'];
                    }
                    $result = array('status'=>'success', 'message'=>'Item Listing.','data'=>$companyItemsData);
                }

            else{
                   
                $result = array('status'=>'failure', 'message'=>"No record found",'data'=>$companyItemsData);
            } 

            $this->set([
                'data'   => $result['data'],
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message','data']
            ]); 
        }
    }

    public function updateCompanyItems() {
        if($this->request->is('post')) {
            $params = $this->request->data;
            $params['company_items_options'] = json_decode($params['company_items_options'], true);
            //Log::notice($params);
            $companyItemsData = $this->companyItems->get($params['company_item_id'],[
                'contain' => ['CompanyItemsOptions']
            ]);

            if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])) {
                $uploadResult = $this->ImageUpload->uploadCompanyitemImages($params['image']);
                $params['image'] =  $uploadResult;
            }
            else {
                $params['image'] = $companyItemsData['image'];
            }
            //pr($params); die;
        
            if (file_exists(WWW_ROOT."images/itemimages/".$companyItemsData['image']) && !empty($_FILES['image']['name'])) { 
                 unlink(WWW_ROOT."images/itemimages/".$companyItemsData['image']);
            }

            $companyItemsData = $this->companyItems->patchEntity($companyItemsData, $params, ['associated' => ['CompanyItemsOptions']]);
            if($this->companyItems->save($companyItemsData)) {

                $result = array('status'=>'success', 'message'=>'Item Updated successfully.','company_item_id'=>$companyItemsData->id);
            } 
            else{
                    $errors = $companyItemsData->errors();
                    $erorMessage = array(); 
                    $i = 0; 
                    $keys = array_keys($errors); 
                    // foreach ($errors as $errors) { 
                    //     $key = key($errors); 
                    //     foreach($errors as $error){ 
                    //             $erorMessage = $keys[$i] . " :- " . $error;
                    //             //$erorMessage = $error;
                    //         }
                    //         $i++;
                    //     }
                    $result = array('status'=>'failure', 'message'=>$errors,'company_item_id'=>null);
            } 

            $this->set([
                'company_item_id' =>  $result['company_item_id'],
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message','company_item_id']
            ]); 
        }
    }

    /**
    * API: Delete companyItems API's
    * PARAMS: company_item_id
    */
    public function deleteCompanyItems() 
    {  
        if($this->request->is('post')) {
            $params = $this->request->data;
                  //Log::notice($params);
            if(!empty($params['company_item_id'])) {
                $entity = $this->companyItems->get($params);
                $result = $this->companyItems->delete($entity);
            }
            if(!empty($params['id'])) {
                $entity = $this->companyItemsOptions->get($params);
                $result = $this->companyItemsOptions->delete($entity);
            }
            if($result) {
                 $result = array('status'=>'success', 'message'=>'Deleted successfully.');
            }
            else {
                 $result = array('status'=>'failure', 'message'=>'Something went wrong');
            }
            $this->set([
                    'status'     => $result['status'],
                    'message'    => $result['message'],
                    '_serialize' => ['status', 'message']
            ]);
        }
    }

    /**
    * API: Company Details
    * PARAMS: company_id
    */
    public function viewCompanySchedule()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Current logged in user
            $cusDetails = $this->customerObj->findByUserId($this->Auth->identify()['id'])->first();
            $params['customer_id'] = $cusDetails['id'];

            $bookingDetails = $this->bookingObj->find()->where(['customer_id'=>$params['customer_id'], 'status' =>'pending',
             'company_id' => $params['company_id'],'type'=>'wait'])->order(['Booking.created'=>'ASC'])->contain(['Customers'])->toArray();

            if(!empty($bookingDetails)) {
                $result = array('status'=>'failure', 'message'=>"You are currently on the waitlist, please remove yourself to book a slot or wait your turn.");
                echo json_encode($result); die;
            }


            //$cusDetails = $this->customerObj->findByUserId($user_id)->select('id')->enableHydration(false)->first(); 
               
            //$res = $this->checkSelectedSlot($params['company_id'], 1, date('Y-m-d'));

            //$res = $this->checkSelectedSlot($params['company_id'], $cusDetails['id'], date('Y-m-d'));
            $res = [];
                
            $days = date('l');
            //echo $days; die;
            $schedule = $this->scheduleObj->find('all')->where(['company_id' => $params['company_id'], 'days'=>$days])->contain(['Slots'=>['fields'=>['Slots.id','Slots.start_time','Slots.end_time','Slots.schedule_id'], 'sort' => ['Slots.start_time' => 'ASC']]])->enableHydration(false)->first();
            $tableLists = $this->restaurantObj->find('all')->select(['table_id'=>'id','name','capacity','description'])->where(['company_id'=>$params['company_id']])->enableHydration(false)->toArray();

            $scheduleDetails = [];
                if(!empty($schedule)) {                     
                    foreach ($schedule['slots'] as $key1 => $value1) {
                        if(!in_array($value1['id'], $res)) {
                             if((trim($schedule['days']) == $days) && (strtotime(date('H:i')) < strtotime(date('H:i',strtotime($value1['start_time']))))) {
                                //echo "as"; die;
                                $scheduleDetails['slots'][$key1]['slot_id'] = $value1['id'];
                                $scheduleDetails['slots'][$key1]['start_time'] =  date('H:i',strtotime($value1['start_time']));
                                $scheduleDetails['slots'][$key1]['end_time'] = date('H:i',strtotime($value1['end_time']));
                                //$scheduleDetails['slots'][$key1]['status'] = $this->checkSlotStatus($value1['id'], date('Y-m-d'));
                             }
                        }
                        if(isset($scheduleDetails['slots']) && !empty($scheduleDetails['slots'])) {
                                   $scheduleDetails['slots'] = array_values($scheduleDetails['slots']);
                        } else {
                                $scheduleDetails = array();
                        }
                    }
                     $scheduleDetails['table_list'] = $tableLists;
                     $result = array('status'=>'success', 'message'=>'Schedules & Table List','data'=>$scheduleDetails);  
                } 
                else {
                     $result = array('status'=>'success', 'message'=>'Schedules & Table List','data'=>$scheduleDetails);  
                }

                $this->set([
                    'data' =>  $result['data'],
                    'status' => $result['status'],
                    'message' => $result['message'],
                    '_serialize' => ['status','message','data']
                ]);
        }
    }

    /**
    * API: Company Details
    * PARAMS: company_id
    */
    public function viewCompanySchedule1()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Current logged in user
            $user_id =  $this->Auth->identify()['id'];

            //$cusDetails = $this->customerObj->findByUserId($user_id)->select('id')->enableHydration(false)->first(); 
               
            //$res = $this->checkSelectedSlot($params['company_id'], 1, date('Y-m-d'));

            //$res = $this->checkSelectedSlot($params['company_id'], $cusDetails['id'], date('Y-m-d'));
            $res = [];
                
            $days = date('l');
            $schedule = $this->scheduleObj->find('all')->where(['company_id' => $params['company_id'], 'days'=>$days])->contain(['Slots'=>['fields'=>['Slots.id','Slots.start_time','Slots.end_time','Slots.schedule_id'], 'sort' => ['Slots.start_time' => 'ASC']]])->enableHydration(false)->first();

            $scheduleDetails = [];
                if(!empty($schedule)) {                     
                    foreach ($schedule['slots'] as $key1 => $value1) {
                        if(!in_array($value1['id'], $res)) {
                            if((trim($schedule['days']) == $days) && (strtotime(date('H:i')) < strtotime(date('H:i',strtotime($value1['start_time']))))) {
                                $scheduleDetails['slots'][$key1]['slot_id'] = $value1['id'];
                                $scheduleDetails['slots'][$key1]['start_time'] =  date('H:i',strtotime($value1['start_time']));
                                $scheduleDetails['slots'][$key1]['end_time'] = date('H:i',strtotime($value1['end_time']));
                            }
                        }
                        if(isset($scheduleDetails['slots']) && !empty($scheduleDetails['slots'])) {
                                   $scheduleDetails['slots'] = array_values($scheduleDetails['slots']);
                        } else {
                                $scheduleDetails = array();
                        }
                    }
                     $result = array('status'=>'success', 'message'=>'Schedules List','data'=>$scheduleDetails);  
                } 
                else {
                     $result = array('status'=>'success', 'message'=>'No Slots available','data'=>$scheduleDetails);  
                }

                $this->set([
                    'data' =>  $result['data'],
                    'status' => $result['status'],
                    'message' => $result['message'],
                    '_serialize' => ['status','message','data']
                ]);
        }
    }

    /**
     * API: View table according to Slot wise.
     * PARAMS: customer_id, company_id, slot_id, date(2018-8-3)
     */
    public function viewTable(){
        if($this->request->is('post')) {
            $params = $this->request->data;
            $tableLists = $this->restaurantObj->find('all')->select(['table_id'=>'id','name','capacity','description'])->where(['company_id'=>$params['company_id']])->enableHydration(false)->toArray();
            $tableImage = Router::url('/','true')."webroot/images".DS."kenchoice1.png";
            foreach ($tableLists as $key => $value) {
                $tableLists[$key]['table_id'] = $value['table_id'];
                $tableLists[$key]['image'] = $tableImage;
                $tableLists[$key]['name'] = $value['name'];
                $tableLists[$key]['capacity'] = $value['capacity'];
                $tableLists[$key]['description'] = $value['description'];
                $tableLists[$key]['status'] = $this->checkTableStatus($params['slot_id'],$value['table_id'],$params['date']);
            }
            if(!empty($tableLists)){
                   $result = array('status'=>'success', 'message'=>'table List','data'=>array_values($tableLists));
            }
            else {
                   $result = array('status'=>'failure', 'message'=>'No Table available','data'=>array_values($tableLists));    
            }
            $this->set([
                'data' =>  $result['data'],
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message','data']
            ]);    
        }
    }

    public function checkTableStatus($slotId,$tableId, $date) 
    { 
        $checkStatus = $this->bookingObj->find('all')->where(['slot_id'=>$slotId, 'date'=>$date, 'table_id' => $tableId, 'payment_status' =>'yes'])->select(['slot_id'])->enableHydration(false)->toArray();
        if(!empty($checkStatus)) {
            return "not_available";
        }
        else {
            return "available";
        }
    }

    /**
    * API: Booking amount on setting page
    * PARAMS: profile_id, booking_amount
    */
    public function bookingAmount()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            if(!empty($params['profile_id']) && !empty($params['booking_amount'])) {

                $res = $this->Companies->get($params['profile_id']); 
                $res = $this->Companies->patchEntity($res, $params);
                if($this->Companies->save($res)) {
                    $result = array('status'=>'success', 'message'=>"Booking amount updated");
                } else {
                    $result = array('status'=>'failure', 'message'=>"Booking amount not updated");
                }
            } else {
                $result = array('status'=>'failure', 'message'=>"Something went wrong.");
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }
    }

    /**
    * API: Booking amount on setting page
    * PARAMS: employee_id, booking_amount
    */
    public function approveEmpRequest()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            if(!empty($params['employee_id']) && !empty($params['approve'])) {
                $res = $this->employeeObj->get($params['employee_id']); 
                $res = $this->employeeObj->patchEntity($res, $params);
                if($this->employeeObj->save($res)) {
                    if($params['approve'] == 'yes') {
                        $msg = "Approved";
						//Send notification
						$reciever = $this->employeeObj->find()->where(['Employees.id'=>$res->id])->select(['Employees.first_name', 'Employees.last_name'])->contain(['Users'=>['fields'=>['Users.id','Users.phone','Users.device_type', 'Users.device_token','Users.noti_alert']]])->select('user_id')->enableHydration(false)->first();
						
						$device_type = $reciever['user']['device_type'];
						$device_token = $reciever['user']['device_token'];
						$name = $reciever['first_name'].' '.$reciever['last_name'];
						if(!empty($device_token) && !empty($device_type)) {
							//Save notification
							$data = [];
							$data['from_user_id'] = $this->Auth->identify()['id'];
							$data['to_user_id']   = $reciever['user_id'];
							$data['message']      = 'Request approved';
							$data['type']         = 'send_to_host';
							$this->saveNotification($data);
							//Get unread notification count(badge_count) and update in users table
							$badgeCount = $this->getBadgeCount($reciever['user_id']);
							//Send notification
							if($reciever['user']['noti_alert'] == "on") {
								$this->sendPushNotification($device_type, $device_token, $name.": ".$data['message'],'send_to_host', $badgeCount, $name);
							}
						}
                    } elseif($params['approve'] == 'no') {
                        $msg = "Rejected";
                    }
                    $result = array('status'=>'success', 'message'=> $msg);
                } else {
                    $result = array('status'=>'failure', 'message'=>"Error to approve request");
                }
            } else {
                $result = array('status'=>'failure', 'message'=>"Something went wrong.");
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }
    }

    public function getNonBusinessPhone() {

	    	if($this->request->is('post')) {
	    		
		    	$placeId = $this->request->data['place_id'];

		    	$url = 'https://maps.googleapis.com/maps/api/place/details/json?placeid='.$placeId.'&fields=formatted_phone_number&key=AIzaSyAxrlnzjA6PwkkYjsIJUd20JhdxIW3iiR4';

		    	$ch = curl_init(); 
				curl_setopt_array($ch, [
				    CURLOPT_RETURNTRANSFER => 1,
				    CURLOPT_URL            =>  $url
				]);
				$new_url = curl_exec($ch);

				echo $new_url; die;
				 
				$this->set([
			            'data'       => json_decode($new_url),
				        '_serialize' => ['data']
				]);
				
				curl_close($ch);
	    	}
	}

    /**
    * API: Add to cart details
    * PARAMS: booking_id, company_item_id, name, price, size, qty, final_price
    */
    public function addToCart() {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            if(!empty($params['pick_up'])) {
                $res  = $this->bookingObj->find()->where(['customer_id'=>$params['customer_id'], 'company_id'=>$params['company_id'],'slot_id IS'=>NULL, 'payment_status'=>'no' ,'type'=>'other'])->first();
                if(!empty($res)) {
                    $params['booking_id'] = $res['id'];
                } else {
                    $booking  = $this->bookingObj->newEntity();
                    $booking->customer_id = $params['customer_id'];
                    $booking->company_id =  $params['company_id'];
                    $this->bookingObj->save($booking);
                    $params['booking_id'] = $booking->id;
                }
            }
            $bookingitem = $this->bookingItems->newEntity(); 
            $params['company_items_options'] = json_encode($params['company_items_options']);
            $bookingitem = $this->bookingItems->patchEntity($bookingitem, $params);
            if($this->bookingItems->save($bookingitem)) 
            {    
                if(empty($params['is_booking_item'])) {
                    $isItem = null;
                } else {
                    $isItem = $params['is_booking_item'];
                }
                if(!empty($params['pay_more'])){
                    $this->bookingObj->updateAll(['pay_more'=>'no','is_booking_item'=>$isItem],['id'=>$params['booking_id']]);
                }               
                $result = array('status'=>'success', 'message'=>"Items added to cart successfully.", 'data'=>array('booking_item_id'=>$bookingitem->id));           
            } else {
                $errors = $bookingitem->errors();
                $erorMessage = array(); 
                $i = 0; 
                $keys = array_keys($errors); 
                foreach ($errors as $errors) { 
                    $key = key($errors); 
                    foreach($errors as $error){ 
                        $erorMessage = ucfirst($keys[$i]) . " :- " . $error;
                        //$erorMessage = $error;
                    }
                    $i++;
                }
                $result = array('status'=>'error', 'message'=>$erorMessage, 'data'=>array());  
            }

            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'data' =>  $result['data'],
                '_serialize' => ['status','message','data']
            ]);
        }
    }

    /**
    * API: Update cart details
    * PARAMS: booking_item_id, qty,final_price
    */
    public function updateCart() {
        if($this->request->is('post')) {
            $params = $this->request->data;
            if(!empty($params['booking_item_id']) && !empty($params['qty']) && !empty($params['final_price'])) {

                $res = $this->bookingItems->get($params['booking_item_id']); 
                $res = $this->bookingItems->patchEntity($res, $params);
                if($this->bookingItems->save($res)) {
                    $result = array('status'=>'success', 'message'=>"Cart updated");
                } else {
                    $result = array('status'=>'failure', 'message'=>"Something went wrong to update cart detail");
                }
            } else {
                $result = array('status'=>'failure', 'message'=>"Something went wrong.");
            }

            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message']
            ]);
        }
    }

    /**
     * API: Cart Details (Guest)
     * PARAMS: customer_id
     */
    public function getCart()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            if(empty($params['customer_id'])) {
                $cusDetails = $this->customerObj->findByUserId($this->Auth->identify()['id'])->first();
                $params['customer_id'] = $cusDetails['id'];
            }

            $noImage = Router::url('/','true')."webroot/images".DS."noImage.png";
            $itemCond = ['BookingItems.payment_status' => 'no'];
            $booking = $this->bookingObj->find()
											->where(['customer_id'=>$params['customer_id'], 'pay_more'=>'no', 'Booking.type'=>'other'])
											->order(['Booking.id'=>'DESC'])
											->contain([
												'Slots'=>[
													'Schedules'=>[
														'Companies'=>['fields'=>['id', 'client_id', 'company_name', 'booking_amount']],
														'Employees.Companies'
													]],
													'RestaurantArrangement'=>['fields'=>['id', 'name']],
												'BookingItems'=>function ($q) use ($itemCond) { 
													return $q->where($itemCond);
												}
											])
											->enableHydration(false);
            //pr($booking->toArray());die;
			$data = array();
            $dateT = '';

			foreach ($booking as $keya => $value1) {
				if(!empty($value1['slot']['schedule_id'])) {
					$dateTT = $this->scheduleDate($value1['slot']['schedule_id']);
					$dateT = date('l, jS F, Y', strtotime($dateTT));
				}
					
				$alreadyDateNotExist = true;
				if($alreadyDateNotExist) { 
					$data['booking'][$keya]['booking_id']    = $value1['id'];
					$data['booking'][$keya]['slot_id']    =  $value1['slot_id'];
					$data['booking'][$keya]['is_booking_item'] = $value1['is_booking_item'];
					
					$currentAmt = 0;
					if(!empty($value1['booking_items'])) {
						$totalprice = 0;
						foreach ($value1['booking_items'] as $keyb => $value2) {
							$data['booking'][$keya]['booking_items'][$keyb]['booking_item_id'] = $value2['id'];
							$data['booking'][$keya]['booking_items'][$keyb]['company_item_id'] = $value2['company_item_id'];
							$data['booking'][$keya]['booking_items'][$keyb]['name'] = $value2['name'];
							$data['booking'][$keya]['booking_items'][$keyb]['price'] = $value2['price'];
							$data['booking'][$keya]['booking_items'][$keyb]['size'] = $value2['size'];
							$data['booking'][$keya]['booking_items'][$keyb]['qty'] = $value2['qty'];
							$data['booking'][$keya]['booking_items'][$keyb]['extra_instruction'] = $value2['extra_instruction'];
							$data['booking'][$keya]['booking_items'][$keyb]['final_price'] = $value2['final_price'];
							$data['booking'][$keya]['booking_items'][$keyb]['company_items_options'] = json_decode($value2['company_items_options']);
							$totalprice = $totalprice + $value2['final_price']; 
						}
						$data['booking'][$keya]['total_payble'] = $totalprice;
						$currentAmt = $totalprice;
					} else {
						$data['booking'][$keya]['booking_items'] = [];
					}
					
					if(!empty($value1['slot_id'])) {
						$data['booking'][$keya]['company_id']   = $value1['slot']['schedule']['company']['id'];
						//new line
						$companyInfo = $this->getCompanyName($value1['slot']['schedule']['company']['id']);
						$data['booking'][$keya]['booking_amount'] = $companyInfo['booking_amount'];
						
						//Remaining booking amount
						$remBookAmt = $this->remainingBookingAmount($value1['id'], $value1['customer_id'], $currentAmt, $companyInfo['booking_amount']);
						$data['booking'][$keya]['remaining_booking_amount'] = $remBookAmt;						
						
						$data['booking'][$keya]['tax'] = $companyInfo['tax'];
						$clientId = $value1['slot']['schedule']['company']['client_id'];
						$data['booking'][$keya]['client_id'] = !empty($clientId ) ? trim($clientId):$clientId;
						$images  = $this->galleryImages($value1['slot']['schedule']['company']['id']);
						$data['booking'][$keya]['image'] = !empty($images) ? $images[0]['image'] : $noImage;
						$data['booking'][$keya]['company_name'] = $value1['slot']['schedule']['company']['company_name'];
						// $data['booking'][$keya]['first_name'] = $value1['slot']['schedule']['company']['first_name'];
						// $data['booking'][$keya]['last_name'] = $value1['slot']['schedule']['company']['last_name'];
						$data['booking'][$keya]['table_id'] = $value1['restaurant_arrangement']['id'];
						$data['booking'][$keya]['table_no'] = $value1['restaurant_arrangement']['name'];
						$data['booking'][$keya]['no_of_guest'] = $value1['no_of_guest'];
						$data['booking'][$keya]['comment'] = $value1['comment'];
						$data['booking'][$keya]['date'] = !empty($value1['date']) ? $value1['date']->format('l, jS F, Y') : $dateT;
						$data['booking'][$keya]['date_send'] = !empty($value1['date']) ? $value1['date']->format('Y-m-d') : $dateT;
						$data['booking'][$keya]['slot'][0]['start_time'] = date('H:i',strtotime($value1['slot']['start_time']));
						$data['booking'][$keya]['slot'][0]['end_time'] = date('H:i',strtotime($value1['slot']['end_time']));
						if(empty($value1['company_id'])) {
							$data['booking'][$keya]['company_name'] = $value1['slot']['schedule']['employee']['company']['company_name'];
							$data['booking'][$keya]['company_id'] = $value1['slot']['schedule']['employee']['company']['id'];
							$data['booking'][$keya]['employee_id'] = $value1['slot']['schedule']['employee']['id'];
							if($value1['slot']['schedule']['employee']['type'] == 'employee') {
								$images = !empty($value1['slot']['schedule']['employee']['profile_pic']) ? Router::url('/','true')."webroot/images/employees/".$value1['slot']['schedule']['employee']['id']."/".$value1['slot']['schedule']['employee']['profile_pic'] : $noImage;
								$data['booking'][$keya]['image']  =  $images;
							}
							//$images = $this->galleryImages($value1['slot']['schedule']['employee']['company']['id']);
							$data['booking'][$keya]['type'] = "employee";
							$data['booking'][$keya]['image'] = $images;
							//new line
							$data['booking'][$keya]['booking_amount'] = $this->getCompanyName($value1['slot']['schedule']['employee']['company']['id'])['booking_amount'];
							$clientId =  $value1['slot']['schedule']['employee']['company']['client_id'];
							$data['booking'][$keya]['client_id'] = !empty($clientId ) ? trim($clientId):$clientId;;
							$data['booking'][$keya]['first_name'] = $value1['slot']['schedule']['employee']['first_name'];
							$data['booking'][$keya]['last_name'] = $value1['slot']['schedule']['employee']['last_name'];
						}
					} else {
						$data['booking'][$keya]['pick_up'] = "yes";
						$data['booking'][$keya]['company_id'] =  $value1['company_id'];
						$data['booking'][$keya]['booking_amount'] = 0;
						$images = $this->galleryImages($value1['company_id']);
						$data['booking'][$keya]['image'] = !empty($images) ? $images[0]['image'] : $noImage;
						$companyInfo = $this->getCompanyName($value1['company_id']);
						$data['booking'][$keya]['company_name'] = $companyInfo['company_name'];
						$data['booking'][$keya]['latitude'] = $companyInfo['latitude'];
						$data['booking'][$keya]['longitude'] = $companyInfo['longitude'];
						$data['booking'][$keya]['delivery'] = $companyInfo['delivery'];
						$data['booking'][$keya]['delivery_radius'] = $companyInfo['delivery_radius'];
						$data['booking'][$keya]['delivery_amount'] = $companyInfo['delivery_amount'];
						$data['booking'][$keya]['tax'] = $companyInfo['tax'];
						$data['booking'][$keya]['client_id'] = $companyInfo['client_id'];
					}
					
					
					//Final payment
					$data['booking'][$keya]['total_payble'] = !empty($data['booking'][$keya]['total_payble']) ? $data['booking'][$keya]['total_payble'] : $data['booking'][$keya]['booking_amount'];
				}
				$data['booking'] = array_values($data['booking']);
			}
		
            if(!empty($data)) {
                $result = array('status'=>'success', 'message'=>'Items on Cart','data'=>$data);
            } else {
                $result = array('status'=>'failure', 'message'=>"", 'data'=>array());
            }

            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'data' => $result['data'],
                '_serialize' => ['status','message','data']
            ]);
        }
    }

    public function getCompanyName($comp_id) 
    {
        if(!empty($comp_id)){
            $result = $this->Companies->find()->where(['id'=>$comp_id])->enableHydration(false)->first();
            return $result;
        }
    }
	
	//Remaining booking amount
	public function remainingBookingAmount($bookingId, $customerId, $currentAmt, $bookAmt)
	{
		if(!empty($bookingId) && !empty($customerId)){
            $results = $this->cusPayment->find()->where(['customer_id'=>$customerId, 'booking_id'=>$bookingId])->enableHydration(false)->toArray();
			//pr($results);die;
			$remBookAmt = 0;
			if(!empty($results)) {
				$remBookAmt = $bookAmt;
				$amt = 0;
				$bookingAmt = 0;
				$offlineAmt = 0;
				foreach($results as $res) {
					$amt = $amt + $res['amount'];
					$bookingAmt = $bookingAmt + $res['booking_amount'];
					$offlineAmt = $offlineAmt + $res['offline_amount'];
				}
				
				//$amt = $amt + $currentAmt + $offlineAmt;
				$amt = $amt + $offlineAmt;
				if($bookingAmt > 0) {
					$amt = $amt - $bookingAmt;
				}
				
				if($bookingAmt > 0 && $bookingAmt > $amt) {
					$remBookAmt = $bookingAmt - $amt;
					if($remBookAmt <= 0) {
						$remBookAmt = 0;
					}
				} elseif($bookingAmt > 0 && $bookingAmt < $amt){
					$remBookAmt = $bookingAmt - $offlineAmt;
					if($remBookAmt <= 0) {
						$remBookAmt = 0;
					}
				} elseif($bookAmt == $offlineAmt){
					$remBookAmt = 0;
				} elseif($offlineAmt =='' || $offlineAmt ==0){
					$remBookAmt = 0;
				}
			}
            return $remBookAmt;
        }
	}

    /**
    * API: Delete Cart Items API's
    * PARAMS: Booking_id, booking_item_id
    */
    public function deleteCartItems() 
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            if(!empty($params['booking_id'])) {
                $entity = $this->bookingObj->get($params);
                $result = $this->bookingObj->delete($entity);
            }
            if(!empty($params['booking_item_id'])) {
                $entity = $this->bookingItems->get($params);
                $result = $this->bookingItems->delete($entity);
            }
            if($result) {
                 $result = array('status'=>'success', 'message'=>'Removed successfully.');
            }
            else {
                 $result = array('status'=>'failure', 'message'=>'Something went wrong');
            }
            $this->set([
                    'status'     => $result['status'],
                    'message'    => $result['message'],
                    '_serialize' => ['status', 'message']
            ]);
        }
    }

    /**
    * API: Delivery options set by host on setting page
    * PARAMS: profile_id, company_id, delivery, delivery_amount, delivery_radius
    */
    public function deliveryHostOptions()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            if(!empty($params['company_id']) && !empty($params['delivery'])) {

                $res = $this->Companies->get($params['company_id']); 
                $res = $this->Companies->patchEntity($res, $params);
                if($this->Companies->save($res)) {
                    $result = array('status'=>'success', 'message'=>"Delivery options setting updated");
                } else {
                    $result = array('status'=>'failure', 'message'=>"Delivery options setting not updated");
                }
            } else {
                $result = array('status'=>'failure', 'message'=>"Something went wrong.");
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }
    }

    /**
    * API: Set Tax % on setting page
    * PARAMS: profile_id, tax
    */
    public function taxPerc()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            if(!empty($params['company_id'])) {

                $res = $this->Companies->get($params['company_id']); 
                $res = $this->Companies->patchEntity($res, $params);
                if($this->Companies->save($res)) {
                    $result = array('status'=>'success', 'message'=>"Tax percentage updated");
                } else {
                    $result = array('status'=>'failure', 'message'=>"Tax percentage not updated");
                }
            } else {
                $result = array('status'=>'failure', 'message'=>"Something went wrong.");
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }
    }

    /**
    * API: Set delivery address on payment page
    * PARAMS: customer_id, booking_id, address, cus_delivery_id (for update case)
    */
    public function addUpdateCustomerDelAddress() {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            $entity = $this->cusDelivery->findByBookingId($params['booking_id'])->first();
            if(!empty($entity)) {
              $result = $this->cusDelivery->delete($this->cusDelivery->get($entity['id']));
            }
            // if(!empty($params['cus_delivery_id'])) {
            //     $cusDeliveryAddress = $this->cusDelivery->get($params['cus_delivery_id']);
            //     $msg = 'Address updated successfully.';
            // }
            // else {
            $cusDeliveryAddress = $this->cusDelivery->newEntity();
            $msg = 'Address added successfully.';
            // }

            $cusDeliveryAddress = $this->cusDelivery->patchEntity($cusDeliveryAddress, $params);

            if($this->cusDelivery->save($cusDeliveryAddress)) {
                $result = array('status'=>'success', 'message'=>$msg,'cus_delivery_id'=>$cusDeliveryAddress->id);
            } 
            else{
                    $errors = $cusDeliveryAddress->errors();
                    $erorMessage = array(); 
                    $i = 0; 
                    $keys = array_keys($errors); 
                    foreach ($errors as $errors) { 
                        $key = key($errors); 
                        foreach($errors as $error){ 
                                $erorMessage = $keys[$i] . " :- " . $error;
                                    //$erorMessage = $error;
                            }
                            $i++;
                        }
                    $result = array('status'=>'failure', 'message'=>$erorMessage,'cus_delivery_id'=>null);
            } 

            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'cus_delivery_id' =>  $result['cus_delivery_id'],
                '_serialize' => ['status','message','cus_delivery_id']
            ]); 
        }
    }

    /**
     * API: Save Payment Details
     * PARAMS: customer_id, company_id, booking_id, transaction_id, amount, payment_status, delivery_amount(optional), booking_amount(optional)
	 * PARAMS(not used): total_amount, final_payment_status
     */
    public function savePaymentDetails() 
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            if($params['payment_status'] == 'success') {
				$params['payment_status'] = 'yes';

				//Check if partial payment done
				/*if($params['total_amount'] != $params['amount']) {
					$params['final_payment_status'] = 'no';
				}*/
								
				if($params['transaction_id'] == 'none') {
					//$params['booking_amount'] = abs($params['amount']);
					$params['offline_amount'] = abs($params['amount']);
					$params['amount'] = 0;
				}
				
				if($params['amount'] < 0) {
					//$params['booking_amount'] = abs($params['amount']);
					$params['amount'] = 0;
				}
				
				$CustomerPayment = $this->cusPayment->newEntity();
				$CustomerPayment = $this->cusPayment->patchEntity($CustomerPayment,$params);
				if($this->cusPayment->save($CustomerPayment)) {
					if($params['order_type'] == "pick") {
						$params['order_type'] = "pick up";
					}
					if(empty($params['delivery_amount'])) {
						$params['delivery_amount'] = 0.00;
					}
					if(empty($params['booking_amount'])) {
						$params['booking_amount'] = 0.00;
					}
					// 'booking_amount'=>$params['booking_amount']
					$params['modified'] = date('Y-m-d H:i:s');

					//Update booking
					$res = $this->bookingObj->updateAll(['payment_status'=>'yes', 'pay_more'=>'yes', 'order_type'=>$params['order_type'], 'delivery_amount'=>$params['delivery_amount'], 'booking_amount'=>$params['booking_amount'], 'modified'=>$params['modified']], ['id'=>$params['booking_id']]);
					
					//Update payment status
					$bookingItems = $this->bookingItems->query()
									->update()
									->set(['payment_status' => 'yes'])
									->where(['booking_id'=>$params['booking_id'],'payment_status'=>'no'])
									->execute();
					$result = array('status'=>'success', 'message'=>"Payment done successfully.");
					if($result) {
						$cusDetails = $this->customerObj->findById($params['customer_id'])->first();
						$custName = $cusDetails['name'];

						if(empty($params['employee_id'])){
							$params['employee_id'] = "";
						}

						$cond = ['Employees.custom' => 'no', 'Employees.id' => $params['employee_id']];
						$company = $this->Companies->findById($params['company_id'])
													->contain([
														'Users'=>[
																'fields'=>['Users.id', 'Users.device_type', 'Users.device_token','Users.noti_alert']
															],
														'Employees.Users'=>function ($q) use ($cond) { 
																			return $q->where($cond);
																		}])
																		->select(['id', 'user_id'])
																		->enableHydration(false)
																		->first();
																		
						$profileDetails = $this->bookingObj->findById($params['booking_id'])->first();
						$profile_type = $profileDetails['profile_type'];
						$device_type = $company['user']['device_type'];
						$device_token = $company['user']['device_token'];

						if(!empty($company['employees'])){
							$edevice_type = $company['employees'][0]['user']['device_type'];
							$edevice_token = $company['employees'][0]['user']['device_token'];
						}
						//Send push notification
						if(!empty($device_token) && !empty($device_type)) {
							//Save notification
							$data = [];
							$data['from_user_id']  = $this->Auth->identify()['id'];
							$data['to_user_id']    = $company['user_id'];
							$data['message']       = $params['order_type'];
							$data['type']          = $profile_type;
							$data['redirect_id']   = $params['booking_id'];
							$this->saveNotification($data);

							//Get unread notification count(badge_count) and update in users table
							$badgeCount = $this->getBadgeCount($company['user_id']);
							
							//Send notification 
							if($company['user']['noti_alert'] == "on") { 
								$this->sendPushNotification($device_type, $device_token, $custName.": ".$data['message'], $data['type'], $badgeCount,$custName, $params['booking_id']);
							}
						}

						if(!empty($edevice_type) && !empty($edevice_token)) {
							$data1 = [];
							$data1['from_user_id']  = $this->Auth->identify()['id'];
							$data1['to_user_id']    = $company['employees'][0]['user_id'];
							$data1['message']       = $params['order_type'];
							$data1['type']          = $profile_type;
							$data1['redirect_id']   = $params['booking_id'];
							$this->saveNotification($data1);

							//Get unread notification count(badge_count) and update in users table
							$badgeCount = $this->getBadgeCount($company['employees'][0]['user_id']);
							
							//Send notification 
							$this->sendPushNotification($edevice_type, $edevice_token, $custName.": ".$data1['message'], $data1['type'], $badgeCount,$custName, $params['booking_id']);
						}
					}
				} else {
					$result = array('status'=>'success', 'message'=>"Some issue to update the payment details");
				}
            } else {
				$result = array('status'=>'failure', 'message'=>'Some issue to make payment at the moment.');
            }
            
            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }
    }

    /**
     * API: Check Order Availabilty on cart page
     * PARAMS: slot_id, table_id, date
     */
    public function checkOrderAvailabilty() 
    { 
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            if(empty($params['slot_id'])) {
                $result = array('status'=>'success', 'message'=>"Your order is Available");
                echo json_encode($result); die;
            }
            if(!empty($params['table_id'])) {
                $table_id = 'table_id';
            }else {
                $params['table_id'] = NULL;
                $table_id = 'table_id IS';
            }
            $checkStatus = $this->bookingObj->find('all')->where(['slot_id'=>$params['slot_id'], 'date'=>$params['date'],  $table_id=>$params['table_id'],'pay_more' =>'yes'])->enableHydration(false)->toArray();
            if(!empty($checkStatus)) {
                $result = array('status'=>'failure', 'message'=>"You can't pay for this order as someone booked before you");        
            } else {
                $result = array('status'=>'success', 'message'=>"Your order is Available");
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }
    }

    //CRON: Delete reservation on cart If date passed
    public function deleteCartPassed() {
        Log::notice("Delete passed cart");
        $bookingDetails = $this->bookingObj->find('all')->where(['Booking.payment_status'=>'no','Booking.type' =>'other'])
                                                        ->order(['Booking.created'=>'ASC'])->toArray();
        $cart = array();
        foreach ($bookingDetails as $key => $value) {
           $cart[$key]['booking_id']   = $value['id'];
           $cart[$key]['company_id']   = $value['company_id'];
           $cart[$key]['customer_id']  = $value['customer_id'];
           $cart[$key]['date']         = $value['date'];
           $cart[$key]['created']      = $value['created'];
           //$customer[$key] = $this->customerObj->findById($value['customer_id'])->contain(['Users'=>['fields'=>['Users.id', 'Users.device_type', 'Users.device_token','Users.noti_alert']]])->select(['user_id'])->enableHydration(false)->first();

           $entity = $this->bookingObj->find()->where(['id'=>$value['id']])->first();
           if(!empty($value['date'])) {
                $result = date('Y-m-d') > $value['date']->format('Y-m-d');
           }
           else {
                $result = null;
           }
           if($result) {
                $result = $this->bookingObj->delete($entity);
                // $device_type =  $customer[$key]['user']['device_type'];
                // $device_token = $customer[$key]['user']['device_token'];
                // //Send push notification
                // if(!empty($device_token) && !empty($device_type)) {
                //     $data = [];
                //     $data['message'] = $cart[$key]['owner_name'].': Cart Item deleted due to date passed. ';
                //     $data['type']    = 'delete_waiting';
                //     //Get unread notification count(badge_count)
                //     $badgeCount = $this->getBadgeCount($customer[$key]['user_id']);
                //     //Send notification 
                //     if($customer[$key]['user']['noti_alert'] == "on") {   
                //         $this->sendPushNotification($device_type, $device_token, $data['message'], $data['type'], $badgeCount,'no');
                //     }
                // }
           }
        }
    }

    //CRON:Send reminder before the start time of booking
    public function myBookingReminder() {
        Log::notice("Booking Reminder");
        $bookingDetails = $this->bookingObj->find('all')->where(['Booking.payment_status'=>'yes','Booking.type' =>'other','Booking.order_type' =>'reservation','Booking.date' =>date('Y-m-d')])->contain(['Slots'=>['Schedules'=>['Companies','Employees.Companies']]])->order(['Booking.created'=>'ASC'])->toArray();
        
        $booking = array();
        if(!empty($bookingDetails)) {
            foreach ($bookingDetails as $key => $value) {
				$booking[$key]['booking_id']   = $value['id'];
				$booking[$key]['customer_id']  = $value['customer_id'];
				if(empty($value['company_id'])) {
					$booking[$key]['company_id'] = $value['slot']['schedule']['employee']['company']['id'];
					$booking[$key]['company_name'] = $value['slot']['schedule']['employee']['company']['company_name'];
				}
				else {
					$booking[$key]['company_id']   = $value['slot']['schedule']['company']['id'];
					$booking[$key]['company_name']  = $value['slot']['schedule']['company']['company_name'];
				}
				$booking[$key]['date']         = $value['date'];
				$booking[$key]['profile_type'] = $value['profile_type'];
				$booking[$key]['start_time'] = date('h:i:s',strtotime($value['slot']['start_time']));
				$start_time = date('h:i:a',strtotime($value['slot']['start_time']));
				$booking[$key]['current_time'] = date('h:i:s');
				if(date('Y-m-d') == $value['date']->format('Y-m-d')) {
					$result = date('H:i:s') < date('H:i:s',strtotime($value['slot']['start_time']));
                    if($result) {
                        $val1 =  date('H:00:00');
                        $val2 =  date('H:00:00',strtotime($value['slot']['start_time']));
                        $datetime1 = new \DateTime($val1);
                        $datetime2 = new \DateTime($val2);
                        $days      = $datetime1->diff($datetime2)->format('%a');
                        $hours     = ($days*24) + $datetime1->diff($datetime2)->format('%h');
                        if($hours >= 1 && $hours < 2) {
                            //Log::notice("Booking Reminder True");
							$result = 1;
                        }
                        else {
                            $result = 0;
                        } 
                    }
					
                    if($result) {
                        $customer[$key] = $this->customerObj->findById($value['customer_id'])->contain(['Users'=>['fields'=>['Users.id', 'Users.device_type', 'Users.device_token','Users.noti_alert']]])->select(['user_id'])->enableHydration(false)->first();
                        $device_type =  $customer[$key]['user']['device_type'];
                        $device_token = $customer[$key]['user']['device_token'];
                        //Send push notification
                        if(!empty($device_token) && !empty($device_type)) {
                            $data = [];
                            $data['message'] =  $booking[$key]['company_name'].': REMINDER - You have Booking at '.$start_time;
                            $data['type']    = "guest_booking";
                            //Get unread notification count(badge_count)
                            $badgeCount = $this->getBadgeCount($customer[$key]['user_id']);
                            //Send notification 
                            if($customer[$key]['user']['noti_alert'] == "on") {
								//Log::notice("Booking Reminder Noti sent");
                                $this->sendPushNotification($device_type, $device_token, $data['message'], $data['type'], $badgeCount,'no',$value['id']);
                            }
                        }
                    }
                    /*else {
                        echo "no booking";
                    }*/
                }
            }
        }
    }

    /**
     * API: Add Paypal client key on setting page
     * PARAMS: profile_id, client_id
     */
    public function addUpdatePaymentKey()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            if(!empty($params['profile_id']) && !empty($params['client_id'])) {
                $res = $this->Companies->get($params['profile_id']); 
                $res = $this->Companies->patchEntity($res, $params);
                if($this->Companies->save($res)) {
                    $result = array('status'=>'success', 'message'=>"Paypal key updated.");
                } else {
                    $result = array('status'=>'failure', 'message'=>"Paypal key not updated");
                }
            } else {
                $result = array('status'=>'failure', 'message'=>"Something went wrong.");
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }
    }
	
	/**
     * API: Delete menu category
     * PARAMS: categoryId
     */
	public function deleteMenuCategory()
	{
		$params = $this->request->data;
		if(!empty($params['categoryId'])) {
			//Check if any order/booking pending related this category
			$result = $this->compCatObj->find()->select(['CompanyCategory.id', 'CompanyCategory.name','CompanyCategory.restricted'])->contain(['CompanyItems'=>['fields'=>['id','company_category_id'],'BookingItems'=>['fields'=>['id','company_item_id', 'payment_status']]]])->where(['id'=> $params['categoryId']])->order(['CompanyCategory.id'=>'DESC'])->enableHydration(false)->first();
			
			$bookingStatus = '';
			if(!empty($result['company_items'])) {
				foreach($result['company_items'] as $values) {
					if(!empty($values['booking_items'])) {
						foreach($values['booking_items'] as $value) {
							if($value['payment_status'] == 'no') {
								$bookingStatus = 'no';
							}
						}
					}
				}
			}
			
			if(empty($bookingStatus) && $bookingStatus != 'no') {
				$menuCat = $this->compCatObj->findById($params['categoryId'])->contain(['CompanyItems'])->first();
				if (!empty($menuCat) && $this->compCatObj->delete($menuCat)) {
					$result = array('status'=>'success', 'message'=>'Menu category deleted successfully.');
					echo json_encode($result);die;
				} else {
					$result = array('status'=>'failure', 'message'=>'Something Wrong. Please try again.');
					echo json_encode($result);die;
				}
			} else {
				$result = array('status'=>'failure', 'message'=>'Please complete/reject all the pending orders. Then you can delete this category.');
				echo json_encode($result);die;
			}
		} else {
			$result = array('status'=>'failure', 'message'=>'Something Wrong. Please try again.');
			echo json_encode($result);die;
		} 
	}

}