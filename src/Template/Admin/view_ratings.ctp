<!-- Content Header (Page header) -->
<style>
.checked {
    color: orange;
}
</style>
<section class="content-header">
    <?php echo $this->Flash->render(); ?>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3><?= h($results[0]['company']['company_name'].': Customers') ?></h3>
            </div>
			<div class="pull-right">
                <button class="btn btn-primary btn-xs" onclick="window.history.back();">Back</button>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-body">
					<table id="categoryLists" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Id</th>
								<th>Guest Name</th>
								<th>Rated To(Employees/Rooms)</th>
								<th>Rating</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							foreach($results as $result): 
								foreach($result['ratings'] as $value):
							?>
								<tr>
									<td><?= $this->Number->format($value['customer']['id']) ?></td>
									<td><?= h($value['customer']['name']) ?></td>
									<td><?= h($result['first_name'].' '.$result['last_name']) ?></td>
									<td>
										<?php
										for ($x = 0; $x < $value['value']; $x++) {
											echo '<span class="fa fa-star checked"></span>';
										}
										for ($x = 0; $x < 5-$value['value']; $x++) {
											echo '<span class="fa fa-star"></span>';
										}
										?>
									</td>
									<td>
										<?= $this->Form->postLink(__('Delete'), ['action' => 'deleteRating', $value['id'], $results[0]['company']['id']], ['class' => 'btn btn-danger btn-xs deleteBussRate','confirm' => __('Are you sure you want to delete this rating?')]) ?>
									</td>
								</tr>
							<?php 
								endforeach; 
							endforeach;
							?>
						</tbody>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</section><!-- /.content -->

<script type="text/javascript">
$(function () {
    setTimeout(function() {
      $(".alert-success").hide('blind', {}, 500)
    }, 5000);
});
</script>