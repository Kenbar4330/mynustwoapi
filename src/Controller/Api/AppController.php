<?php

namespace App\Controller\Api;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Log\Log;
require ROOT."/vendor/autoload.php";
// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

class AppController extends Controller
{
    use \Crud\Controller\ControllerTrait;

    private $scheduleObj;
    private $notificationObj;
    private $userObj;
    private $compImgObj;
	private $ratingObj;
	private $employeeObj;
    private $empImgObj;
	private $bookingObj;
	private $customerObj;
	private $businessTimeObj;
	private $countryObj;
    
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        header("Access-Control-Allow-Origin: *");
		//header("Access-Control-Allow-Origin: http://live.mynustwo.com");
        //header("Access-Control-Allow-Credentials: true");
        header('Content-Type: application/json'); 
        header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
        //header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, authorization, Authorization, customauthorization, Customauthorization");
		header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, customauthorization");

        if ($this->request->is('options')) { 
            $this->response->statusCode(204); 
            $this->response->send(); 
            die(); 
        }

        $this->loadComponent('RequestHandler');
		
        $this->loadComponent('Auth', [
            'storage' => 'Memory',
            'authenticate' => [
                'Form' => [
                    //'scope' => ['Users.status' => 'active'],
                    'fields' => [
                        'username' => 'phone'
                    ]
                ],
                'ADmad/JwtAuth.Jwt' => [
                    'parameter' => 'token',
                    'userModel' => 'Users',
                    //'scope' => ['Users.status' => 'active'],
                    'fields' => [
                        'username' => 'id'
                    ],
                    'queryDatasource' => true
                ]
            ],
            'unauthorizedRedirect' => false,
            'checkAuthIn' => 'Controller.initialize'
        ]);
        //$this->Auth->allow();
        $this->scheduleObj     = TableRegistry::get('Schedules');
        $this->notificationObj = TableRegistry::get('Notifications');
        $this->userObj         = TableRegistry::get('Users');
        $this->compImgObj      = TableRegistry::get('CompanyImages');
		$this->employeeObj     = TableRegistry::get('Employees');
        $this->ratingObj       = TableRegistry::get('Ratings');
        $this->empImgObj       = TableRegistry::get('employee_images');
		$this->bookingObj      = TableRegistry::get('Booking');
		$this->customerObj     = TableRegistry::get('Customers');
		$this->businessTimeObj = TableRegistry::get('business_timings');
		$this->countryObj      = TableRegistry::get('countries');

        if($this->request->params['action'] != "logout" && $this->request->params['action'] != "login"
        && $this->request->params['action'] != "signUp" && $this->request->params['action'] != "verifyOTP"
        && $this->request->params['action'] != "resendOTP" && $this->request->params['action'] != "forgotPassword"
        && $this->request->params['action'] != "verifyPassword" && $this->request->params['action'] != "deleteAfterClose" && $this->request->params['action'] != "getCountryList" && $this->request->params['action'] != "deleteCartPassed" && $this->request->params['action'] != "myBookingReminder"  && $this->request->params['action'] != "pushNotification") {

            $user = $this->Auth->identify();
            /*if($user['status'] == 'inactive' && $user['is_otp_verified'] == 'yes') {
                $result = array('status'=>'inactive', 'message'=>"account is not active");
                echo json_encode($result); die;
            }*/
            // if(empty($user)) {
            //     $result = array('status'=>'inactive', 'message'=>"Account is deleted");
            //     echo json_encode($result); die;
            // }
        }
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Http\Response|null|void
     */
    public function beforeRender(Event $event)
    {            
        // Note: These defaults are just to get started quickly with development
        // and should not be used in production. You should instead set "_serialize"
        // in each action as required.
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    /**
     * Generate Token based on userId
     */
    public function token($userId)
    {
        $token = JWT::encode([
                    'sub' => $userId,
                    'id'  => $userId,
                    'exp' => strtotime("+100 year")
                ],
                Security::salt());
        return $token;
    }
	
	/**
     * Email setting
     */
    public function sendMail($to, $subject, $msg)
    {
       $email = new Email('default');
       $email
            ->transport('gmail')
            ->from(['arun.evontech@gmail.com' => 'MynusTwo'])
            ->to($to)
            ->subject($subject)
            ->emailFormat('html')
            ->viewVars(array('msg' => $msg))
            ->send($msg);
    }

    /**
     * Generate OTP
     */
    public function generateOTP() 
    {
        $chars = "0123456789";
        $res = "";
        for ($i = 0; $i < 4; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars)-1)];
        }
        return $res;
    }

    /**
     * Send SMS/OTP
     */
    public function sendOTP($countrycode, $phone, $otp)
    {
        //Send SMS code start
        $sid = 'ACf142069fb73a767739e4de2d824c71cd';
        $stoken = 'a118574886a7a1f012f4fae2b446cd95';
        $client = new Client($sid, $stoken);
        
        //$phone = $this->countryCode.$phone;
		$phone = !empty($countrycode) ? '+'.$countrycode.$phone : $this->countryCode.$phone;
        $validPhone = $this->validatePhoneNumber($phone, $sid, $stoken);
        if(empty($validPhone)) {
           $checkPhone = array(
                    'status' => "failure",
                    'message' => "Not a valid mobile number"
                );

           echo json_encode($checkPhone); die;
        }
        $sms = $client->messages->create(
            // the number you'd like to send the message to
            "'".$phone."'",
            array(
                // A Twilio phone number you purchased at twilio.com/console
                'from' => '+16572363149',
                // the body of the text message you'd like to send
                'body' => 'Your MYNUS 2 verification code is: '.$otp
                //'statusCallback' => 'http://54.210.15.234/kenbar/api/users/testSMS/12345'
            )
        );
        
        if($sms) {
            return true;
        } else {
            return false;
        }
        //Send SMS code end
    }

    /**
     *
     * Validate Phone Number
     */
    public function validatePhoneNumber($phoneNumber,$sid,$token) 
    {
        $result = exec('curl -XGET "https://lookups.twilio.com/v1/PhoneNumbers/' . $phoneNumber . '?Type=carrier&Type=caller-name" -u "' . $sid . ':' . $token . '"');
        $data = json_decode($result, true);
        $arr = (array) $data;
        if (isset($arr['code'])) {
            return false;
        }
        elseif($arr['carrier']['type'] == "landline") {
            return false;
        }
        return true;
    }

    //Validation
    public function validateParams($val) {
        $result = false;
        if (isset($val) && !empty($val) && $val != NULL) {
            $result = true;
        }
        return $result;
    }

    //Get schedule date based on schedule_id
    public function scheduleDate($scheduleId)
    {
        $res = '';
        if(!empty($scheduleId)) {
            $date = $this->scheduleObj->findById($scheduleId)->select(['date'])->first();
            if(!empty($date['date'])) {
                $res = $date['date'];
            }
        }
        return $res;
    }
	
	/** 
	 * For Push Notifications.
	 * PARAMS: device_type(android / ios), device_token, notification
	 */
	public function sendPushNotification($device_type = null, $device_token = null, $notification = null, $type = null, $badge = null , $from = null, $booking_id = null) 
    {
		if((is_null($device_type)) || (is_null($device_token)) || (is_null($notification)) || (is_null($type))) {
			return false;
		}
		
		switch($device_type) {
			case 'android':
				$return=$this->send_notification($device_token, $notification, $type, $badge , $from, $booking_id);
			break;
			
			case 'ios': 
				$return=$this->send_notification($device_token, $notification, $type, $badge, $from, $booking_id);
			break;
			
			default: 
				$return = true;
			break;
		}
		
		return $return; 
	}
	
    //Push Notification Function
	function send_notification($device_token, $message, $type, $badge, $from, $booking_id)
	{   
        if (!defined('API_ACCESS_KEY'))
		//define( 'API_ACCESS_KEY', 'AAAAS6iKBt4:APA91bGaEWTcVXxXX1LrvWTZ2S2q0TuLqgT-AQHn4KXavYa4Dol79cET8iUgav-jpjxP42bFhO81i_hKx6rmNx-0wxCkiNO3iZQFWtc2Y66UmdTLSEd5QRMTDb8mW1FHv66mI8fAapGKZb1TDdcwAENNBigqzGOayA');
		//define( 'API_ACCESS_KEY', 'AAAAvxjtPJ4:APA91bGWR7784xh3NVbYJbarTZtrAsox2D21ErK3_A8tL3kXpG3KCFw-JY8O20Wb612mcTDzmTPzYrGQJa9bo1Yws2bQEhoto8Okxoi2rmfuIjAAAwtTlGYUAyE2PC4lZNXF2AtLWZB1');
		define( 'API_ACCESS_KEY', 'AAAAS6iKBt4:APA91bGaEWTcVXxXX1LrvWTZ2S2q0TuLqgT-AQHn4KXavYa4Dol79cET8iUgav-jpjxP42bFhO81i_hKx6rmNx-0wxCkiNO3iZQFWtc2Y66UmdTLSEd5QRMTDb8mW1FHv66mI8fAapGKZb1TDdcwAENNBigqzGOayA');
        $notiIcon = Router::url('/','true')."webroot/images".DS."icon.png";
		$msg = array(
				"body"         => $message,
				"forceStart" =>  "1",
				"sound"        => "default",
				"type"         => $type,
                "badge_count"  => $badge,
                "booking_id"   => $booking_id,
				//"click_action" => "FCM_PLUGIN_ACTIVITY",
                "icon"         => "fcm_push_icon"
			);
		
		$data = array(
				"message"     => $message,
				"forceStart" => "1",
				"type"        => $type,
                "badge_count" => $badge,
                "booking_id"  => $booking_id,
                "icon"        => "fcm_push_icon"
			);
		
		$aps = array(
				"badge_count" => $badge
			);
			
		$fields = array(
				'to'           => $device_token,
				'notification' => $msg,
				'data'         => $data,
				'aps'          => $aps,
				'priority'     => 'high'
			);

		$headers = array(
				'Authorization: key=' . API_ACCESS_KEY,
				'Content-Type: application/json'
			);
		//Log::notice($fields);
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		curl_setopt( $ch,CURLOPT_POST, true );
		curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		$result = curl_exec($ch );
		//Log::notice('here........');
		//Log::notice($result);
		//Log::notice('end........');
		curl_close( $ch );
	}

    //Function to save notifications for badge and notification read/unread status
    public function saveNotification($params)
    {
        if(!empty($params['from_user_id']) && !empty($params['to_user_id'])) {
            $params['created'] = date('Y-m-d H:i:s');
            $params['modified'] = date('Y-m-d H:i:s');
            $notification = $this->notificationObj->newEntity();
            $notification = $this->notificationObj->patchEntity($notification, $params);
            
            if($this->notificationObj->save($notification)) {
                $result = array('status'=>'success', 'message'=>"Notification saved successfully.");
            } else {
                $result = array('status'=>'failure', 'message'=>"Something went wrong.");
            }
        }
    }

    //Get unready message
    public function getBadgeCount($receiverId)
    {
        if(!empty($receiverId)) {
            //Get notification count
            $result = 0;
            $result = $this->notificationObj->find()->where(['to_user_id'=>$receiverId, 'mark_as'=>'unread'])->count();
            
            //Update badge_status in users table
            $this->userObj->updateAll(['badge_count'=>$result], ['id'=>$receiverId]);

            return $result;
        }
    }

    //Get business gallery images 
    public function galleryImages($compId, $type='')
    {
        $gallery = [];
        if(!empty($compId) && !empty($type) && ($type == 'other' || $type == 'employee')) {
            $images = $this->empImgObj->find('all')->where(['employee_id'=>$compId])->select(['id', 'name'])->order(['created'=>'DESC'])->limit(5)->enableHydration(false)->toArray();
            
            if(!empty($images)) {
                foreach($images as $key=>$image) {
                    $gallery[$key]['id'] = $image['id'];
                    $gallery[$key]['image'] = !empty($image['name']) ? Router::url('/','true')."webroot/images/employees/".$compId.'/'.$image['name'] : '';
                }
            }
        } else {           
            $images = $this->compImgObj->find('all')->where(['company_id'=>$compId])->select(['id', 'name'])->order(['created'=>'DESC'])->limit(5)->enableHydration(false)->toArray();
            
            if(!empty($images)) {
                foreach($images as $key=>$image) {
                    $gallery[$key]['id'] = $image['id'];
                    $gallery[$key]['image'] = !empty($image['name']) ? Router::url('/','true')."webroot/images/companies/".$compId.'/'.$image['name'] : '';
                }
            }
        }
        return $gallery;
    }
	
	//Get rating of business
    public function businessRating($businessId)
    {
        if(!empty($businessId)) {
            $employees = $this->employeeObj->find()->where(['company_id'=>$businessId])->select(['id'])->enableHydration(false)->toArray();
            $totalRating = 0;
            if(!empty($employees)) {
                $rating = 0;
                $count = 0;
                foreach ($employees as $key => $value) {
                    $rate = $this->employeeRating($value['id']);
                    if($rate>0) {
                        $rating = $rating + $rate;
                        $count++;
                    }
                }

                if(!empty($rating)) {
                    $totalRating = $rating / $count;
                }
            }
            else {
                $cmpRatings = $this->ratingObj->find()->where(['employee_id'=>$businessId,'type'=>'company'])->select(['value'])->enableHydration(false)->toArray();
                if(!empty($cmpRatings)) {
                    $rating = 0;
                    $count = 0;
                    foreach ($cmpRatings as $key => $value) {
                        $rating = $rating + $value['value'];
                        $count++;
                    }
                }
                if(!empty($rating)) {
                    $totalRating = $rating / $count;
                }

            }
            return round($totalRating);  
        }
    }

    //Get rating of business
    public function employeeRating($empId)
    {
        if(!empty($empId)) {
            $totalRating = 0;            
            //Get rating of all employees
            $empRatings = $this->ratingObj->find()->where(['employee_id'=>$empId])->select(['value'])->enableHydration(false)->toArray();

            if(!empty($empRatings)) {
                $rating = 0;
                $count = 0;
                foreach ($empRatings as $key => $value) {
                    $rating = $rating + $value['value'];
                    $count++;
                }
            }

            if(!empty($rating)) {
                $totalRating = $rating / $count;
            }
            return round($totalRating);  
        }
    }
	
	//Get country code
    public function getCountryCode($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
        $output = NULL;
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
        $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America"
        );

        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {

            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip='".$ip.'"'));

            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                
                switch ($purpose) {
                    case "location":
                        $output = array(
                            "city"           => @$ipdat->geoplugin_city,
                            "state"          => @$ipdat->geoplugin_regionName,
                            "country"        => @$ipdat->geoplugin_countryName,
                            "country_code"   => @$ipdat->geoplugin_countryCode,
                            "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                            "continent_code" => @$ipdat->geoplugin_continentCode
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (@strlen($ipdat->geoplugin_regionName) >= 1)
                            $address[] = $ipdat->geoplugin_regionName;
                        if (@strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                        $output = implode(", ", array_reverse($address));
                        break;
                    case "city":
                        $output = @$ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = @$ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = @$ipdat->geoplugin_countryCode;
                        break;
                }
            }
        }
        if(!empty($output)) {
            $idd=json_decode(file_get_contents("https://restcountries.eu/rest/v1/alpha?codes=$output"));
            if(!empty($idd)) {
                $output = "+".$idd[0]->callingCodes[0];
            }
        }
        
        return $output;
    }

    //Date Time format
    public function dateTimeFormat($format, $time)
    {
        $res = '';
        if(!empty($format) && !empty($time)) {
            $res = date($format, strtotime($time));
        }
        return $res;
    }

    //Number of customers who rate business
    public function countRating($businessId)
    {
        //echo $businessId;die;
		if(!empty($businessId)) {
            $employees = $this->employeeObj->find('all')->where(['Employees.company_id'=>$businessId])->contain(['Ratings'=>['fields'=>['Ratings.employee_id', 'Ratings.customer_id']]])->select(['Employees.id'])->enableHydration(false)->toArray();
            
            $totalCount = 0;
            $uArray = [];
            if(!empty($employees)) {
				foreach($employees as $emp) {
					foreach ($emp['ratings'] as $key => $value) {
						$uArray[] = $value['customer_id'];
					}
				}
                $unique = array_unique($uArray);
                $totalCount = count($unique);
            }
            else {
                $users = $this->ratingObj->find()->where(['employee_id'=>$businessId,'type'=>'company'])->select(['employee_id', 'customer_id'])->enableHydration(false)->toArray();
                $totalCount = 0;
                $uArray = [];
                if(!empty($users)) {
                    foreach ($users as $key => $value) {
                        $uArray[] = $value['customer_id'];
                    }
                    $unique = array_unique($uArray);
                    $totalCount = count($unique);
                }

            }
            return $totalCount;
        }
    }
	
	//Number of customers who rate employees
    public function countRatingUsers($empId)
    {
		if(!empty($empId)) {
            $users = $this->ratingObj->find()->where(['employee_id'=>$empId])->select(['employee_id', 'customer_id'])->enableHydration(false)->toArray();
            $totalCount = 0;
            $uArray = [];
            if(!empty($users)) {
                foreach ($users as $key => $value) {
                    $uArray[] = $value['customer_id'];
                }
                $unique = array_unique($uArray);
                $totalCount = count($unique);
            }
            return $totalCount;
        }
    }
	
	// Total no. of waiting customers for particular business
	public function totalWaiting($companyId) 
    {
        $total = $this->bookingObj->find()->where(['company_id'=>$companyId ,'Booking.status'=>'pending','Booking.type'=>'wait'])->order(['Booking.created'=>'ASC'])->count();
        return $total;
    }
	
	public function getBusinessTime($bussId, $day)
    {
        if(!empty($bussId) && !empty($day)) {
            $timing = '';
            $result = $this->businessTimeObj->find('all')->where(['company_id'=>$bussId, 'day'=>$day])->select(['id', 'company_id', 'type', 'day', 'start_time', 'end_time'])->enableHydration(false)->first();
            if(!empty($result)) {
                $timing = $result;
            }
            return $timing;
        }
    }

	
}