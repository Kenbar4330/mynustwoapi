<?php

namespace App\Controller\Api;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Filesystem\Folder;
use Cake\Log\Log;
require ROOT."/vendor/autoload.php";
// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

class RoomsController extends AppController
{
    private $userObj;
    private $companyObj;
    private $categoryObj;
    private $customerObj;
    private $scheduleObj;
    private $employeeObj;
    private $bookingObj;
    private $ratingObj;
    private $phoneObj;
	private $compImgObj;
	private $empImgObj;
    private $noImage;

    public function initialize()
    {
        parent::initialize();     

        header("Access-Control-Allow-Origin: *");
		//header("Access-Control-Allow-Origin: http://live.mynustwo.com");
        //header("Access-Control-Allow-Credentials: true");
        header('Content-Type: application/json'); 
        header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
        //header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, authorization, Authorization, customauthorization, Customauthorization");
		header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, customauthorization");

        if ($this->request->is('options')) { 
            $this->response->statusCode(204); 
            $this->response->send(); 
            die(); 
        }

        $this->Auth->allow();

        $this->userObj     = TableRegistry::get('Users');
        $this->companyObj  = TableRegistry::get('Companies');
        $this->categoryObj = TableRegistry::get('Categories');
        $this->customerObj = TableRegistry::get('Customers');
        $this->scheduleObj = TableRegistry::get('Schedules');
		$this->slotObj     = TableRegistry::get('Slots');
        $this->employeeObj = TableRegistry::get('Employees');
		$this->bookingObj  = TableRegistry::get('Booking');
        $this->phoneObj    = TableRegistry::get('Phones');
		$this->compImgObj  = TableRegistry::get('company_images');
        $this->empImgObj   = TableRegistry::get('employee_images');

        //Blank image url
        $this->noImage = Router::url('/','true')."webroot/images".DS."noImage.png";
		
		//Country Code
		$this->countryCode = '+1';
		//$this->countryCode = $this->getCountryCode("Visitor", "countrycode");
    }  

    /**
     * API: Add Rooms/Employees in business
     * PARAMS(other): company_id, name, about, type(employee/other)
     * PARAMS(employee): company_id, first_name, last_name, about, type(employee/other), profile_pic
     */
    public function addRooms()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
               Log::notice($params);
            $compDetails = $this->companyObj->find('all')->where(['id'=>$params['company_id']])->select(['id', 'user_id', 'category_id', 'email', 'phone'])->enableHydration(false)->first();
            
            if($compDetails){
                $params['user_id']     = $compDetails['user_id'];
                $params['category_id'] = $compDetails['category_id'];
                $params['email']       = $compDetails['email'];
            }
            
            $params['custom']     = 'yes';
			
			$id = '';
            if(!empty($params['type']) && $params['type'] == 'other') {
				
				$params['first_name'] = $params['name'];
				
                $employee = $this->employeeObj->newEntity();
                $employee = $this->employeeObj->patchEntity($employee, $params);

                if($this->employeeObj->save($employee)) {    
                    
                    $result = array('status'=>'success', 'message'=>"New entity added successfully.", 'id'=>$employee->id);

                } else {
                    $errors = $employee->errors();
                    $erorMessage = array(); 
                    $i = 0; 
                    $keys = array_keys($errors); 
                    foreach ($errors as $errors) { 
                        $key = key($errors); 
                        foreach($errors as $error){ 
                            $erorMessage = ucfirst($keys[$i]) . " :- " . $error;
                            //$erorMessage = $error;
                        }
                        $i++;
                    }
                    $result = array('status'=>'failure', 'message'=>$erorMessage, 'id'=>$id);
                }

            } elseif (!empty($params['type']) && $params['type'] == 'employee') {
                //Save employee details if profile_pic is uploaded
                $uploadPicFlag = 1;
                if(isset($_FILES['profile_pic']['name']) && !empty($_FILES['profile_pic']['name'])) 
                {
                    $name = $_FILES['profile_pic']['name'];
                    $temp = $_FILES['profile_pic']['tmp_name'];
                    $ext = substr(strrchr($name , '.'), 1);
                    $arr_ext = array('jpg', 'jpeg', 'png');
                    $NewFileName = time().rand(00, 99);
                    $setNewFileName = $NewFileName.".".$ext;
                    if (!in_array($ext, $arr_ext)) {
                        $uploadPicFlag = 0;
                    }
                    $params['profile_pic'] = $setNewFileName;                    
                }

                if(!$uploadPicFlag) {
                    $result = array('status'=>'failure', 'message'=>'Please upload correct image format.', 'id'=>$id);
                } else {
                    $employee = $this->employeeObj->newEntity();
                    $employee = $this->employeeObj->patchEntity($employee, $params);
                    if($this->employeeObj->save($employee)) {
                        //Upload profile_pic
                        if(isset($_FILES['profile_pic']['name']) && !empty($_FILES['profile_pic']['name'])) {
                            $url = WWW_ROOT."images/employees/".$employee->id;
                            //Create directory with name profile_id
                            if (!file_exists($url)) {
                                mkdir($url, 0755, true);
                            }
                            $path = Router::url('/','true')."webroot/images/employees/".$employee->id.DS.$setNewFileName;
                            
                            move_uploaded_file($temp, $url . "/" . $setNewFileName);  
                        }                 
                        
                        $result = array('status'=>'success', 'message'=>"Employee added successfully.", 'id'=>$employee->id);
                    } else {
                        $errors = $employee->errors();
                        $erorMessage = array(); 
                        $i = 0; 
                        $keys = array_keys($errors); 
                        foreach ($errors as $errors) { 
                            $key = key($errors); 
                            foreach($errors as $error){ 
                                $erorMessage = ucfirst($keys[$i]) . " :- " . $error;
                                //$erorMessage = $error;
                            }
                            $i++;
                        }
                        $result = array('status'=>'failure', 'message'=>$erorMessage, 'id'=>$id);
                    }
                }
            } else {
                $result = array('status'=>'failure', 'message'=>"Some issue to add Room/Employee. Please try again.", 'id'=>$id);
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
				'id'         => $result['id'],
                '_serialize' => ['status','message','id']
            ]);
        }
    }

    /**
     * API: Rooms/Employees listing
     * PARAMS: company_id
     */
    public function getRoomEmpList()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            Log::notice($params);
            $roomLists = $this->employeeObj->find('all')->where(['company_id'=>$params['company_id'], 'type'=>'other', 'custom'=>'yes'])->select(['id','user_id', 'first_name', 'last_name', 'about', 'type', 'profile_pic', 'custom'])->enableHydration(false)->toArray();
            Log::notice($roomLists);
            $records = array();
            if(!empty($roomLists)) {
                foreach ($roomLists as $key => $value) {
                    
                    $records[$key]['id']        = $value['id'];
                    $records[$key]['user_id']   = $value['user_id'];
                    $records[$key]['custom']    = $value['custom'];
                    $records[$key]['name']      = trim($value['first_name'].' '.$value['last_name']);
                    $records[$key]['about']     = $value['about'];
                    $records[$key]['room_type'] = $value['type'];
					$records[$key]['rating']     = $this->employeeRating($value['id']);
					$records[$key]['rateUserCount'] = $this->countRatingUsers($value['id']);
					
                    if(!empty($value['type']) && $value['type'] == 'employee') {
                        
                        $records[$key]['image'] = !empty($value['profile_pic']) ? Router::url('/','true')."webroot/images/employees/".$value['id']."/".$value['profile_pic'] :  $this->noImage;

					} elseif(!empty($value['type']) && $value['type'] == 'other') {
                        
                        $images = $this->galleryImages($value['id'], 'other');
                        $records[$key]['image'] = !empty($images) ? $images[0]['image'] : $this->noImage;

                    } else {
						$records[$key]['image'] = $this->noImage;
					}
                }

                $result = array('status'=>'success', 'message'=>"Room/Employee listing.", 'data'=>$records);

            } else {

                $result = array('status'=>'failure', 'message'=>"No record found.", 'data'=>$records);
            }

            $this->set([
                'status'  => $result['status'],
                'message' => $result['message'],
                'data'    => $result['data'],
                '_serialize' => ['status', 'message', 'data']
            ]);
        }
    }

    /**
     * API: Add Room Gallery
     * PARAMS: id(room_id/employee_id), type(employee/other), imgfile
     */
    public function addRoomGallery() 
    {
        if($this->request->is('post')) {
            $params = $this->request->getData();
			$params['type'] = 'other';
            Log::notice($params);
            $result = array();
            if(!empty($_FILES['imgfile']['name'])) {
                $name = $_FILES['imgfile']['name'];
                $temp = $_FILES['imgfile']['tmp_name'];
                $ext = substr(strrchr($name , '.'), 1);
                $arr_ext = array('jpg', 'jpeg', 'png');
                $NewFileName = time().rand(00, 99);

                $setNewFileName = $NewFileName."_".$params['id'].".".$ext;
                            
                if(!empty($params['type']) && $params['type'] == 'other') {
                    
                    $url = WWW_ROOT."images/employees/" . $params['id'];
                    $path = Router::url('/','true')."webroot/images/employees/".$params['id'].DS.$setNewFileName;
                }
                //Create directory with name id
                if (!file_exists($url)) {
                    mkdir($url, 0755, true);
                }
                            
                if (in_array($ext, $arr_ext)) {
                    if(move_uploaded_file($temp, $url . "/" . $setNewFileName)) {
                        if(!empty($params['type']) && $params['type'] == 'other') {
                            $imagesData = $this->empImgObj->newEntity();
                            $imagesData->employee_id = $params['id'];
                            $imagesData->name = $setNewFileName;
                            $imagesData->type = $params['type'];
                            if($this->empImgObj->save($imagesData)) {
                                $result = array('status'=>'success', 'message'=>'Image uploaded successfully.', 'image_id'=>$imagesData->id, 'image_path'=>$path);
                            }
                        }
                    } else {
                        $result = array('status'=>'failed', 'message'=>'Something went wrong.', 'image_id'=>null, 'image_path'=>null);
                    }
                } else {
                    $result = array('status'=>'failed', 'message'=>'Please upload correct image format.', 'image_id'=>null, 'image_path'=>null);
                }

                $this->set([
                    'status'     => $result['status'],
                    'message'    => $result['message'],
                    'image_id'   => $result['image_id'],
                    'image_path' => $result['image_path'],
                    '_serialize' => ['status', 'message', 'image_id', 'image_path']
                ]);
            }
        }
    }

    /**
     * API(33): Delete Image (Changed api)
     * Params: id(company_id/employee_id or room_id), image_id, type(company/individual/employee/other)
     */
    public function deleteImage()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            
            if(!empty($params['id']) && !empty($params['image_id']) && (!empty($params['type'] && $params['type'] == 'other'))) {
                $entity = $this->empImgObj->find()->where(['id'=>$params['image_id'], 'employee_id'=>$params['id']])->first();
                if(!empty($entity)) {
                    $result = $this->empImgObj->delete($entity);
                    if($result) {
                        $result = array('status'=>'success', 'message'=>'Image deleted successfully.');
                    } else {
                        $result = array('status'=>'failure', 'message'=>'Image not deleted.');
                    }
                } else {
                    $result = array('status'=>'failure', 'message'=>'Incorrect details.');
                }
            } elseif (!empty($params['id']) && !empty($params['image_id']) && (!empty($params['type'] && $params['type'] == 'company'))) {
                $entity = $this->compImgObj->find()->where(['id'=>$params['image_id'], 'company_id'=>$params['id']])->first();
                if(!empty($entity)) {
                    $result = $this->compImgObj->delete($entity);
                    if($result) {
                        $result = array('status'=>'success', 'message'=>'Image deleted successfully.');
                    } else {
                        $result = array('status'=>'failure', 'message'=>'Image not deleted.');
                    }
                } else {
                    $result = array('status'=>'failure', 'message'=>'Incorrect details.');
                }
            } else {
                $result = array('status'=>'failure', 'message'=>'Incorrect details.');
            }
            
            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }
    }

    //API: 23. View profile details

    //API: 27. updateProfile

    //API: 24. createSlots

    //API: 25. allSlots


	
	

	
}