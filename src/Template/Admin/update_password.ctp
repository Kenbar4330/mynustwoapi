<html>
 <head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->css(array('bootstrap/css/bootstrap.min.css','https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css','dist/css/AdminLTE.min.css', 'dist/css/skins/_all-skins.min.css','plugins/iCheck/flat/blue.css','plugins/morris/morris.css','plugins/jvectormap/jquery-jvectormap-1.2.2.css',
        'plugins/daterangepicker/daterangepicker-bs3.css', 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')); ?>
    <?= $this->Html->meta('icon') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<?= $this->element('header') ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="login-box">
      <div class="login-box-body">
      <?php echo $this->Flash->render(); ?>  
 
         <?= $this->Form->create('updatepassword', ['class' => 'form-horizontal', 'id' => 'update-password-form']) ?>
          <div class="form-group has-feedback">
            <label>Old Password</label>
            <?= $this->Form->control('old_password', ['class' => 'form-control', 'placeholder' => 'Oldpassword', 'type'=>'Password', 'label' => false]); ?>
          </div>
          <div class="form-group has-feedback">
             <label>New Password</label>
             <?= $this->Form->control('new_password', ['class' => 'form-control', 'placeholder' => 'Newpassword', 'type'=>'Password', 'label' => false]); ?>
          </div>
          <div class="form-group has-feedback">
             <label>Confirm Password</label>
            <?= $this->Form->control('confirm_password', ['class' => 'form-control', 'placeholder' => 'Confirmpassword',  'type'=>'Password','label' => false]); ?>
          </div>
          <div class="row">
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
            </div><!-- /.col -->
          </div>
        </form>

      <?php echo $this->Form->end(); ?>

      </div><!-- /.login-box-body -->
</div><!-- /.login-box -->
<?= $this->element('footer') ?>
<?= $this->Html->script(array('plugins/jQuery/jQuery-2.1.4.min.js','bootstrap/js/bootstrap.min.js','https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js','plugins/morris/morris.min.js', 'https://code.jquery.com/ui/1.11.4/jquery-ui.min.js', 'plugins/sparkline/jquery.sparkline.min.js', 'plugins/jvectormap/jquery-jvectormap-1.2.2.min.js', 'plugins/jvectormap/jquery-jvectormap-world-mill-en.js','plugins/knob/jquery.knob.js','https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js','plugins/daterangepicker/daterangepicker.js','plugins/slimScroll/jquery.slimscroll.min.js', 'plugins/fastclick/fastclick.min.js','dist/js/app.min.js','dist/js/demo.js','plugins/ckeditor/pluginsbootstrap3-wysihtml5.all.min.js','plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js')); ?>
</body>
</html>


                    