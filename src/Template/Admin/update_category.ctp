<section class="content-header">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Update Category</h3>
            </div>
            <div class="pull-right">
                <button class="btn btn-primary btn-xs" onclick="window.history.back();">Back</button>
            </div>
        </div>
    </div>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <?= $this->Form->create($categoryData, ['id'=>'AddCatForm']) ?>
        <div class="box-body">
          <div class="form-group">
            <label>Category Name</label>
             <?= $this->Form->control('name', ['class' => 'form-control', 'placeholder' => 'Category Name', 'label' => false ]); ?> 
          </div>
        </div>
        <div class="box-footer">
          <?= $this->Form->button(__('Update'),['class'=>'btn btn-primary btn-mg']); ?> 
        </div>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</section>

    