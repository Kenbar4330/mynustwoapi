<section class="content-header">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Update Business Details</h3>
            </div>
            <div class="pull-right">
                <button class="btn btn-primary btn-xs" onclick="window.history.back();">Back</button>
            </div>
        </div>
    </div>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <?= $this->Form->create($user, ['id'=>'AddCatForm']) ?>
        <div class="box-body">
          <div class="form-group">
            <label>Business Name:</label>
             <?= $this->Form->control('company.company_name', ['class' => 'form-control', 'placeholder' => 'Business Name', 'label' => false ]); ?> 
          </div>

          <div class="form-group">
            <label>First Name:</label>
             <?= $this->Form->control('company.first_name', ['class' => 'form-control', 'placeholder' => 'First Name', 'label' => false ]); ?> 
          </div>

          <div class="form-group">
            <label>Last Name:</label>
             <?= $this->Form->control('company.last_name', ['class' => 'form-control', 'placeholder' => 'Last Name', 'label' => false ]); ?> 
          </div>

          <div class="form-group">
            <label>About:</label>
             <?= $this->Form->textarea('company.about', ['class' => 'form-control', 'placeholder' => 'About', 'label' => false ]); ?> 
          </div>

          <div class="form-group">
            <label>Email:</label>
             <?= $this->Form->control('company.email', ['class' => 'form-control', 'placeholder' => 'Email', 'label' => false ]); ?> 
          </div>

          <div class="form-group">
            <label>Address:</label>
             <?= $this->Form->control('company.address', ['class' => 'form-control', 'placeholder' => 'Address', 'label' => false ]); ?> 
          </div>

          <div class="form-group">
            <label>City:</label>
             <?= $this->Form->control('company.city', ['class' => 'form-control', 'placeholder' => 'City', 'label' => false ]); ?> 
          </div>
		  
		  <div class="form-group">
            <label>State:</label>
             <?= $this->Form->control('company.state', ['class' => 'form-control', 'placeholder' => 'State', 'label' => false ]); ?> 
          </div>
		  
		  <div class="form-group">
            <label>Country:</label>
             <?= $this->Form->control('company.country', ['class' => 'form-control', 'placeholder' => 'Country', 'label' => false ]); ?> 
          </div>
		  
		  <div class="form-group">
            <label>Zip Code:</label>
             <?= $this->Form->control('company.zipcode', ['class' => 'form-control', 'placeholder' => 'Zip Code', 'label' => false ]); ?> 
          </div>
		  
		<div class="form-group">
			<label>Status:</label>
			<div>
				<select name="status">
					<option value="active" <?php if(!empty($user['status']) && $user['status'] == 'active'){echo "selected";} ?>>Active</option>
					<option value="inactive" <?php if(!empty($user['status']) && $user['status'] == 'inactive'){echo "selected";} ?>>Inactive</option>
				</select>
			</div>                     
		</div>

        </div>
        <div class="box-footer">
          <?= $this->Form->button(__('Update'),['class'=>'btn btn-primary btn-sm']); ?>
        </div>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</section>

    