<?php
namespace App\Controller;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
require ROOT."/vendor/autoload.php";
// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

class AppController extends Controller
{    
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authorize' => 'Controller',
            'authenticate' => [
                'Form' => [
                    'scope' => ['Users.status' => 'active', 'Users.type' => 'admin'],
                    //fields used in login form
                    'fields' => [
                        'username' => 'phone',
                        'password' => 'password'
                    ]
                ]
            ],
            // login Url
            'loginAction' => [
                'controller' => 'Admin',
                'action' => 'login'
            ],
            'loginRedirect' => [
                'controller' => 'Admin',
                'action' => 'adminDashboard'
            ],
            // where to be redirected after logout  
            /* 'logoutRedirect' => [
              'controller' => 'Topics',
              'action' => 'index'//,
              //'home'
              ], */
            // if unauthorized user go to an unallowed action he will be redirected to this url
            'unauthorizedRedirect' => [
                'controller' => 'Users',
                'action' => 'index'//,
            //'home'
            ],
            'authError' => 'Did you really think you are allowed to see that?',
        ]);

        //$this->email = new Email();
    }

    /*
     * Email setting
     */
    public function sendMail($to, $subject, $msg)
    {
       $email = new Email('default');
       $email
            ->transport('mynustwo')
            ->from(['no-reply@mynustwo.com' => 'MynusTwo'])
            ->to($to)
            ->subject($subject)
            ->emailFormat('html')
            ->viewVars(array('msg' => $msg))
            ->send($msg);
    }
    
    public function isAuthorized($user) {
        return true;
    }

    /** 
   * For Push Notifications.
   * PARAMS: device_type(android / ios), device_token, notification
   */
  public function sendPushNotification($device_type = null, $device_token = null, $notification = null, $type = null, $badge = null , $from = null) 
    {
    if((is_null($device_type)) || (is_null($device_token)) || (is_null($notification)) || (is_null($type))) {
      return false;
    }
    
    switch($device_type) {
      case 'android':
        $return=$this->send_notification($device_token, $notification, $type, $badge, $from);
      break;
      
      case 'ios': 
        $return=$this->send_notification($device_token, $notification, $type, $badge, $from);
      break;
      
      default: 
        $return = true;
      break;
    }
    
    return $return; 
  }
  
    //Push Notification Function
  function send_notification($device_token, $message, $type, $badge, $from)
  {
    if (!defined('API_ACCESS_KEY'))
    define( 'API_ACCESS_KEY', 'AAAAS6iKBt4:APA91bGaEWTcVXxXX1LrvWTZ2S2q0TuLqgT-AQHn4KXavYa4Dol79cET8iUgav-jpjxP42bFhO81i_hKx6rmNx-0wxCkiNO3iZQFWtc2Y66UmdTLSEd5QRMTDb8mW1FHv66mI8fAapGKZb1TDdcwAENNBigqzGOayA');

    $notiIcon = Router::url('/','true')."webroot/images".DS."icon.png";
    $msg = array(
        "title"  => "MYNUS 2",
        "body"   => $message,
        "sound"  => "default",
        "type"   => $type,
        "badge_count"  => $badge,
        "click_action" => "FCM_PLUGIN_ACTIVITY",
        "icon"   => "fcm_push_icon"
      );
    
    $data = array(
        "title" => "MYNUS 2",
        "message" => $message,
        "type" => $type,
        "badge_count" => $badge,
        "icon" => "fcm_push_icon"
      );
    
    $aps = array(
        "badge_count" => $badge
      );
      
    $fields = array(
        'to' => $device_token,
        'notification' => $msg,
        'data' => $data,
        'aps' => $aps,
        'priority' => 'high'
      );

    $headers = array(
        'Authorization: key=' . API_ACCESS_KEY,
        'Content-Type: application/json'
      );

    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
    $result = curl_exec($ch );
    curl_close( $ch );
  }

}
