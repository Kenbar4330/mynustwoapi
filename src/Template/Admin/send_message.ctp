<section class="content-header">
 <?php echo $this->Flash->render(); ?>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Send Email</h3>
            </div>
        </div>
    </div>
</section>

<section class="content">
  <div class="row mt">
    <div class="col-lg-12">
      <div class="box">
        <div class="box-body">
        <div class="form-panel">
            <div class="loading">
                <i class="fa fa-spinner fa-spin" style="font-size:24px; display: none;" alt="sending" id="loading_image"></i>
            </div>
            <h4 class="mb"></h4>
            <!-- <form class="form-horizontal style-form" name="myForm" method="post" action="/kenbar/admin/email_to" onsubmit="return(validate());"> -->
            <?= $this->Form->create('Admin', ['class'=>'form-horizontal style-form']) ?>
                <input id="emailMsg" type="hidden" name="msg_type" value="email"/>
                
                <!-- <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label"><b>Option:</b></label>
                    <div class="col-sm-10">
                        <input id="emailMsg" type="radio" name="msg_type" value="email" checked/> Email
                        <br />
                        <input id="pushMsg" type="radio" name="msg_type" value="push" /> Push Notification
                    </div>
                </div> -->

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label"><b>Filter:</b></label>
                    <div class="col-sm-3">
                        <select name="filter_type" class="form-control user-filter">
                            <option value="allUsers">All Users</option>
                            <option value="allHosts">Hosts</option>
                            <option value="allGuests">Guests</option>
                            <option value="filterCriteria">Filter Criteria</option>
                        </select>
                    </div>                     
                </div>

                <!-- Filter criteria -->
                <div id="filterCriteria" class="filterClass" style="display:none;">
    
                    <!-- Users selection-->
                    <div class="form-group userType">
                        <label class="col-sm-2 col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            <label class="col-sm-2 col-sm-2 control-label"><b>User Type:</b></label>
                            <div class="col-sm-8">
                                <input type="radio" name="user" value="host" /> Host &nbsp;&nbsp;&nbsp;
                                <input type="radio" name="user" value="guest" /> Guest &nbsp;&nbsp;&nbsp;
                            </div>            
                        </div>
                    </div>

                    <!-- Search by name -->
                    <!-- <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                          <label class="col-sm-2 col-sm-2 control-label"><b>Name:</b></label>
                          <div class="col-sm-8">
                            <input type="text" name="username" size="25" class="search" id="searchid" placeholder="Search by name" autocomplete="off"/>
                            <input type="hidden" name="userid" id="userid"/>
                            <div id="result" class="searchByName" style="display:none;"></div>
                          </div>           
                        </div>
                    </div> -->
                    <!-- Search by name end -->
                    
                    <!-- Date of registration selection -->
                    <div class="form-group dateRange">
                        <label class="col-sm-2 col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            <label class="col-sm-2 col-sm-2 control-label"><b>Registration Date:</b></label>
                            <div class="col-sm-8">
                                From: <input type="text" id="reg_start_date" name="reg_start_date" class="datepicker" size="15" placeholder="Start date" />
                                
                                To: <input type="text" id="reg_end_date" name="reg_end_date" class="datepicker" size="15" placeholder="End date" />
                            </div>            
                        </div>
                    </div>

                </div>
                <!-- Filter criteria end -->

                <div class="form-group subject">
                    <label class="col-sm-2 col-sm-2 control-label"><b>Subject:</b></label>
                    <div class="col-sm-10">
                      <input type="text" name="subject" placeholder="Subject" size="50" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label"><b>Message:</b></label>
                    <div class="col-sm-10">
                        <textarea rows="5" name="message" class="col-xs-10" placeholder="Message" required></textarea>
                    </div>
                </div>
       
                <input type="submit" id="save_value" class="btn btn-primary" value="Send">
            </form>
        </div>
        </div>
      </div>
    </div><!-- col-lg-12-->
  </div><!-- /row -->
</section>

<script type="text/javascript">
$(document).ready(function () {
    
    $('.user-filter').change(function () {
        $('.filterClass').hide();
        $('.userType input').prop('checked', false);
        $('.datepicker').val('');
        $('#'+$(this).val()).show();
    });

    $('#save_value').click(function() {


        /*var val = [];
        $(':checkbox:checked').each(function(i){
          val[i] = $(this).val();
        });*/
    });

    $("input[name$='msg_type']").click(function() {
        var test = $(this).val();
        if(test == 'email') {
          $("div.subject").show();
        } else {
          $("div.subject").hide();
          $(".subject input").val('');
        }
    });
});

function isInteger(value) {
    if ((undefined === value) || (null === value)) {
        return false;
    }
    return value % 1 == 0;
}

//Auto search
$(function(){
    $(".search").keyup(function() 
    { 
        var searchid = $(this).val();
        var dataString = 'search='+ searchid;
        if(searchid!='')
        {
            $.ajax({
                type: "POST",
                url: "/autosearch.php",
                data: dataString,
                cache: false,
                success: function(html)
                {
                    $("#result").html(html).show();
                }
            });
        }return false;
    });

    jQuery("#result").on("click",'a',function(e) {
        var userid = $(this).data('id');
        var name = $(this).data('name');
        $('#searchid').val(name);
        $('#userid').val(userid);
    });

    /*jQuery(document).on("click", 'a',function(e) { 
        var $clicked = $(e.target);
        if (! $clicked.hasClass("search")){
        jQuery("#result").fadeOut(); 
        }
    });*/

    $('#searchid').click(function(e){
        e.preventDefault();
        jQuery("#result").fadeIn();
    });

    $(window).click(function() {
        jQuery("#result").fadeOut(); 
    });

    $('#searchid').click(function(event){
        event.stopPropagation();
    });

});

//Registeration date format and validation
$( function() {
    from = $( "#reg_start_date" ).datepicker({
        dateFormat: "yy-mm-dd",
        maxDate: "0",
    })
    .on( "change", function() {
        to.datepicker( "option", "minDate", getDate( this ) );
    });
    
    to = $( "#reg_end_date" ).datepicker({
        dateFormat: "yy-mm-dd",
        maxDate: "0",
    });
    
    function getDate( element ) {
        var date;
        try {
            date = $.datepicker.parseDate( 'yy-mm-dd', element.value );
        } catch( error ) {
            date = null;
        }

        return date;
    }
} );
</script>

<script>
$("form").submit(function() {
    if ($('#loading_image').length == 0) { //is the image on the form yet?
        // add it just before the submit button
        $(':submit').before('<div class="loading"><i class="fa fa-spinner fa-spin" style="font-size:24px;display:none;" alt="sending" id="loading_image"></i></div>');
    }
    $('#loading_image').show(); // show the animated image
    $(':submit',this).attr('disabled','disabled'); // disable double submits
    return true; // allow regular form submission
});
</script>

<script type="text/javascript">
$(function() {
    setTimeout(function() {
      $(".alert-success").hide('blind', {}, 500)
    }, 5000);
});
</script>