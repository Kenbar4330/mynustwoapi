<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Company Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $category_id
 * @property string $company_name
 * @property string $first_name
 * @property string $last_name
 * @property string $phone_number
 * @property string $address
 * @property string $city
 * @property string $zipcode
 * @property float $latitude
 * @property float $longitude
 * @property string $about
 * @property string $status
 * @property string $type
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Category $category
 */
class Company extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'category_id' => true,
        'company_name' => true,
        'first_name' => true,
        'last_name' => true,
        'about' => true,
        'address' => true,
        'city' => true,
		'state' => true,
		'country' => true,
        'zipcode' => true,
        'latitude' => true,
        'longitude' => true,
        'phone' => true,
		'email' => true,
        'business_phone' => true,
        'booking_amount' => true,
        'offers' => true,
        'status' => true,
        'type' => true,
        'wait' => true,
        'delivery' => true,
        'delivery_amount' => true,
        'delivery_radius' => true,
        'tax' => true,
        'client_id' => true,
        'created' => true,
        'modified' => true
    ];
}
