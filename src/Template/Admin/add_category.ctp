<section class="content-header">
  <div class="row">
      <div class="col-lg-12 margin-tb">
          <div class="pull-left">
              <h3>Add New Category</h3>
          </div>
          <div class="pull-right">
              <button class="btn btn-primary btn-xs" onclick="window.history.back();">Back</button>
          </div>
      </div>
  </div>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <?= $this->Form->create('Admin', ['id'=>'AddCatForm']) ?>
        <div class="box-body">
          <div class="form-group">
            <label>Category Name</label>
            <input type="text" name="name" class="form-control" placeholder="Enter Category Name" maxlength="50" required>
          </div>
        </div>
        <div class="box-footer">
          <?= $this->Form->button(__('Add'),['class'=>'btn btn-primary btn-mg']); ?>
        </div>
        <?= $this->Form->end() ?>
      </div>
      <div class="col-md-4"></div>
    </div>
  </div>
</section>