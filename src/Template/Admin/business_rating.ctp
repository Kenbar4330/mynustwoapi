<!-- Content Header (Page header) -->
<style>
.checked {
    color: orange;
}
</style>
<section class="content-header">
    <?php echo $this->Flash->render(); ?>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Business Listing With Ratings</h3>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-body">
					<table id="ratingLists" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Id</th>
								<th>Business Name</th>
								<th>Category</th>
								<th>Empl./Rooms</th>
								<!-- <th>Guests</th> -->
								<th>Rating</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($hosts as $host): ?>
							<tr>
								<td><?= $this->Number->format($host['id']) ?></td>
								<td><?= h($host['company']['company_name']) ?></td>
								<td><?= h($host['company']['category']['name']) ?></td>
								<td><?= h(count($host['company']['employees'])) ?></td>
								<!--<td>
									<?php
									if(!empty($host['company']['userRateCount']['customers'])) {
										foreach ($host['company']['userRateCount']['customers'] as $key=>$cust) {
											echo $key+1;
											echo ". ";
											echo !empty($cust) ? $cust : '';
											echo "<br />";
										}
									}
									?>
								</td> -->
								<td>
								<?php 
								for ($x = 0; $x < $host['company']['rating']; $x++) {
								    echo '<span class="fa fa-star checked"></span>';
								}
								for ($x = 0; $x < 5-$host['company']['rating']; $x++) {
								    echo '<span class="fa fa-star"></span>';
								} 
								 
								$count = !empty($host['company']['userRateCount']['count']) ? $host['company']['userRateCount']['count'] : 0;
								
									echo " (".$count." Rating)";
					
								?>

								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</section><!-- /.content -->

<script>
$(function () {
	$("#ratingLists").DataTable({
		stateSave: true
	});
	
});
</script>
