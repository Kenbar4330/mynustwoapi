<!-- Content Header (Page header) -->
<?php
$company = $filter = '';

if(isset($_REQUEST['company_name'])) { 
    $company = $_REQUEST['company_name']; 
}

if(isset($_REQUEST['filter'])) { 
    $filter = $_REQUEST['filter']; 
}
?>
<section class="content-header">
 <?php echo $this->Flash->render(); ?>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Payment Details</h3>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
	<div class="row well">
	    <div class="col-md-12 col-lg-12 margin-tb searchBoxCls">
	        <!-- Search form -->
	        <div class="row">
	            <form role="form" method="GET" action="">

	                <div class="col-md-2">
	                    <input type="text" class="form-control" name="company_name" placeholder="Company Name" value="<?php if(isset($company)){echo "$company"; } ?>">
	                </div>

	                <?php
                	$filters = ['current_week'=>'Current Week', 'current_month'=>'Current Month', 'current_year'=>'Current Year'];
                ?>

	                <div class="col-md-2">
						<select class="form-control" name="filter">
							<option value="">Select Filter</option>
							<?php
							foreach($filters as $key=>$value):
							?>
								<option value="<?= $key ?>" <?php if(isset($filter) && !empty($filter) && $filter==$key){echo "selected";} else {echo "";} ?>><?= h($value) ?></option>
							<?php
	                        endforeach
	                        ?>
	                    </select>
	                </div>            

	                <div class="col-md-1">
	                    <button class="btn btn-default btn-sm form-control" type="submit">
	                        <i class="fa fa-search"></i>
	                    </button>
	                </div>

	                <div class="col-md-1">
	                    <a class="btn btn-default btn-sm form-control pull-left" href="payment_details"><strong>Reset</strong></a>
	                </div>
	  
	            </form>
	        </div>
	        <!-- End search form -->
	    </div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-body">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><?= $this->Paginator->sort('id', 'Id') ?></th>
								<th>Company Name</th>
								<th>Amount</th>
								<th>Payment Date</th>
								<th>Renew On</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							foreach($payDetails as $result): 

								if(!empty($result['created']) && !empty($result['receipt'])) {
					                $result['payment_status'] = 'yes';
					                $result['valid_till'] = date('d F Y', strtotime("+3 months", strtotime($result['created'])));
					                
					            } elseif(!empty($result['created']) && empty($result['receipt'])) {

					                $result['valid_till'] = date('d F Y', strtotime("+3 months", strtotime($result['user']['created'])));
					                
					            } else {
					                $result['payment_status'] = 'free';
					                $result['valid_till'] = date('d F Y', strtotime("+3 months", strtotime($result['user']['created'])));
					            }
							?>
							<tr>
								<td><?= $this->Number->format($result['id']) ?></td>
								<td><?= h($result['company']['company_name']) ?></td>
								<td><?= h($result['amount']) ?></td>
								<td><?= h(date('d F Y', strtotime($result['created']))) ?></td>
								<td><?= h($result['valid_till']) ?></td>
								<td class="actions">
									<?= $this->Html->link(
									     __('Delete'),
									    ['action' => 'deletePayment', $result['id']],
									    ['escape' => false, 'title' => __('Delete'), 'class' => 'btn btn-danger btn-xs',
									    'confirm' => __('Are you sure you want to Delete ?')]
									) ?>
                            	</td>  
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->

	<div class="row">
		<div class="col-md-6">
	 	<?php
			echo $this->Paginator->counter('Showing {{start}} to {{end}} of {{count}} entries');
		?>
		</div>
		<div class="col-md-6">
			<ul class="pagination" style="float: right; margin: 0!important;">
	 		<?php
				echo $this->Paginator->prev('< ' . __('previous'));
				echo $this->Paginator->numbers();
				echo $this->Paginator->next(__('next') . ' >');
			?>
		 	</ul>
		</div>
	</div>
</section><!-- /.content -->

<script type="text/javascript">
$(function() {
    setTimeout(function() {
      $(".alert-success").hide('blind', {}, 500)
    }, 5000);
});
</script>