<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CompanyItemsOptions Model
 *
 * @property \App\Model\Table\CompanyItemsTable|\Cake\ORM\Association\BelongsTo $CompanyItems
 *
 * @method \App\Model\Entity\CompanyItemsOption get($primaryKey, $options = [])
 * @method \App\Model\Entity\CompanyItemsOption newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CompanyItemsOption[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CompanyItemsOption|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CompanyItemsOption patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CompanyItemsOption[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CompanyItemsOption findOrCreate($search, callable $callback = null, $options = [])
 */
class CompanyItemsOptionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('company_items_options');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('CompanyItems', [
            'foreignKey' => 'company_item_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 75)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_item_id'], 'CompanyItems'));

        return $rules;
    }
}
