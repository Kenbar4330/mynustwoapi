<?php

namespace App\Controller\Api;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\Log\Log;
require ROOT."/vendor/autoload.php";
// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

class UsersController extends AppController
{
    private $companyObj;
    private $categoryObj;
    private $customerObj;
    private $scheduleObj;
    private $employeeObj;
    private $bookingObj;
    private $ratingObj;
    private $phoneObj;
	private $payDetailObj;
    private $subsObj;
    private $notificationObj;
    private $businessTimeObj;
    private $noImage;
	private $countryObj;
    private $restaurantObj;

    public function initialize()
    {
        parent::initialize();     

        header("Access-Control-Allow-Origin: *");
		//header("Access-Control-Allow-Origin: http://live.mynustwo.com");
        //header("Access-Control-Allow-Credentials: true");
        header('Content-Type: application/json'); 
        header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
        //header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, authorization, Authorization, customauthorization, Customauthorization");
		header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, customauthorization");
		
        if ($this->request->is('options')) { 
            $this->response->statusCode(204); 
            $this->response->send(); 
            die(); 
        }

        $this->Auth->allow(['signUp', 'getNonBusinessPhone', 'verifyOTP', 'login', 'logout', 'resendOTP', 'forgotPassword', 'verifyPassword', 'categoryList', 'checkGoole', 'getCountryList', 'pushNotification']);
        
        $this->companyObj   = TableRegistry::get('Companies');
        $this->categoryObj  = TableRegistry::get('Categories');
        $this->customerObj  = TableRegistry::get('Customers');
        $this->scheduleObj  = TableRegistry::get('Schedules');
        $this->employeeObj  = TableRegistry::get('Employees');
		$this->bookingObj   = TableRegistry::get('Booking');
        $this->ratingObj    = TableRegistry::get('Ratings');
        $this->favoriteObj  = TableRegistry::get('Favorite');
        $this->phoneObj     = TableRegistry::get('Phones');
		$this->payDetailObj = TableRegistry::get('PaymentDetails');
        $this->subsObj      = TableRegistry::get('Subscriptions');
        $this->notificationObj = TableRegistry::get('Notifications');
        $this->businessTimeObj = TableRegistry::get('business_timings');
		$this->countryObj = TableRegistry::get('countries');
        $this->restaurantObj = TableRegistry::get('restaurant_arrangement');
        //Blank image url
        $this->noImage = Router::url('/','true')."webroot/images".DS."noImage.png";
		
		//Country Code
		$this->countryCode = '+1';
		//$this->countryCode = $this->getCountryCode("Visitor", "countrycode");
    }  

    /**
	 * API: Country Code Listing
	 */
	public function getCountryList()
	{
		$countries = $this->countryObj->find('all')->select(['countries.id', 'countries.name', 'countries.countrycode'])->enableHydration(false)->toArray();
		if($countries) {
			$result = array('status'=>'success', 'message'=>"Countries List.", 'data'=>$countries);
		} else {
			$result = array('status'=>'failure', 'message'=>"No record found.", 'data'=>'');
		}
		
		$this->set([
			'status'     => $result['status'],
			'message'    => $result['message'],
			'data'       => $result['data'],
			'_serialize' => ['status', 'message', 'data']
		]);
	}
	
	/**
     * API: Register Guests/Hosts
     * PARAMS: phone, password, type(guest/host), device_token, device_type, country_name, countrycode
     */
    public function signUp()
    {
        try {
            if ($this->request->is('post')) {
				$params = $this->request->getData();
				//Log::notice($params);
				$deviceToken = !empty($params['device_token']) ? $params['device_token'] : '';
				$deviceType  = !empty($params['device_type']) ? $params['device_type'] : '';
				$country     = !empty($params['country_name']) ? $params['country_name'] : '';
				$countrycode = !empty($params['countrycode']) ? $params['countrycode'] : '';
                //Generate OTP and save in database
				$otp = $this->generateOTP();
				//Send SMS code start
				$res = $this->sendOTP($countrycode, $params['phone'], $otp);
				//$res = true;
				if($res) {
					$user = $this->Users->newEntity(); 
					$user = $this->Users->patchEntity($user, $params);
					//OTP save in database
					$user->otp = $otp;
					$user->device_token = $deviceToken;
					$user->device_type  = $deviceType;
					$user->country      = $country;
					$user->countrycode  = $countrycode;
					//Generate access_token and save in database
					$user->access_token = $this->token($user->id);
					if ($this->Users->save($user)) {
						
						if(!empty($params['type']) && $params['phone'] == 'guest') {
                            //Update details in customer table
                            $customer = $this->customerObj->newEntity();
                            $customer->user_id = $user->id;
                            $customer = $this->customerObj->patchEntity($customer, $params);
                            $this->customerObj->save($customer);
                        } 
                        /*elseif(!empty($params['type']) && $params['phone'] == 'host') {
                            //Update details in companies table
                            $company = $this->Companies->newEntity();
                            $company->user_id = $user->id;
                            $company = $this->Companies->patchEntity($company, $params);
                            $this->Companies->save($company);
                        }*/
						
						$result = array('status'=>'success', 'message'=>"Registration successful.", 'data'=>array('id'=>$user->id, 'device_token'=>$deviceToken, 'device_type'=>$deviceType,'otp'=>$otp));
						
					} else {
						$errors = $user->errors();
						$erorMessage = array(); 
						$i = 0; 
						$keys = array_keys($errors); 
						foreach ($errors as $errors) { 
							$key = key($errors); 
							foreach($errors as $error){ 
								$erorMessage = ucfirst($keys[$i]) . " :- " . $error;
								//$erorMessage = $error;
							}
							$i++;
						}
						$result = array('status'=>'error', 'message'=>$erorMessage, 'data'=>array()); 
					}
				
				} else {
					$result = array('status'=>'failure', 'message'=>"OTP not send.", 'data'=>array('id'=>$user->id));
				}
				                
                $this->set([
                    'status'     => $result['status'],
                    'message'    => $result['message'],
                    'data'       => $result['data'],
                    '_serialize' => ['status', 'message', 'data']
                ]);
            }
        } catch(PDOException $e) {
            return $e->getMessage();
        }
    }

    /**
     * API: Resend Send OTP
     * PARAMS: phone, change(yes/no)
     */
    public function resendOTP()
    {
        if ($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            //Check if phone changed and resend otp
            if(isset($params['change']) && !empty($params['change']) && $params['change']=='yes') {
                $phoneDetails = $this->phoneObj->find()->where(['phone'=>$params['phone']])->select(['Phones.id'])->order(['created'=>'DESC'])->enableHydration(false)->first();
                if(!empty($phoneDetails)) {
                    //Generate OTP and save in database
                    $otp = $this->generateOTP();
                    
                    //Send SMS code start
                    $res = $this->sendOTP($params['phone'], $otp);
                    if($res) {
                        //Update OTP in DB
                        $this->phoneObj->updateAll(['otp'=>$otp], ['id'=>$phoneDetails['id']]);

                        $result = array('status'=>'success', 'message'=>"OTP sent successfully.", 'data'=>array('id'=>$phoneDetails['id']));
                    } else {
                        $result = array('status'=>'failure', 'message'=>"OTP not send.", 'data'=>array('id'=>$phoneDetails['id']));
                    }
                } else {
                    $result = array('status'=>'failure', 'message'=>"Phone number does not exist.", 'data'=>array());
                }
            } else {
                $userDetails = $this->Users->find('all', ['conditions'=>['phone'=>$params['phone']]])->select(['Users.id', 'Users.countrycode'])->enableHydration(false)->first();
                if(!empty($userDetails)) {
                    //Generate OTP and save in database
                    $otp = $this->generateOTP();
                    
                    //Send SMS code start
                    $res = $this->sendOTP($userDetails['countrycode'], $params['phone'], $otp);
                    if($res) {
                        //Update OTP in DB
                        $this->Users->updateAll(['otp'=>$otp], ['id'=>$userDetails['id']]);
                        
                        $result = array('status'=>'success', 'message'=>"OTP sent successfully.", 'data'=>array('id'=>$userDetails['id']));
                    } else {
                        $result = array('status'=>'failure', 'message'=>"OTP not send.", 'data'=>array('id'=>$userDetails['id']));
                    }
                } else {
                    $result = array('status'=>'failure', 'message'=>"Phone number does not exist.", 'data'=>array());
                }
            }
            
            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                'data'       => $result['data'],
                '_serialize' => ['status', 'message', 'data']
            ]);  
        }
    }

    /**
     * API: Check OTP for verification
     * PARAMS: phone, otp, type(guest/host), change(yes/no)
     */
    public function verifyOTP()
    {
        if ($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            //Check if phone changed and verify otp then update phone on the users table
            if(isset($params['change']) && !empty($params['change']) && $params['change']=='yes') {
                $phoneDetails = $this->phoneObj->find()->where(['phone'=>$params['phone'], 'otp'=>$params['otp']])->order(['id'=>'DESC'])->enableHydration(false)->first();
                if(isset($phoneDetails) && !empty($phoneDetails)) {
                    //Update changed phone in users and customer table
                    $updateActivate = $this->Users->updateAll(['phone'=>$params['phone']], ['id'=>$phoneDetails['user_id']]);
					if(!empty($params['type']) && $params['type']=='guest') {
						$customerUpdate = $this->customerObj->updateAll(['phone'=>$params['phone']], ['user_id'=>$phoneDetails['user_id']]);
					} else {
						$userDetails = $this->Users->find()->where(['Users.id'=>$phoneDetails['user_id']])->contain(['Companies', 'Employees'])->enableHydration(false)->first();
						
						if(!empty($userDetails['company'])) {
							$companyUpdate = $this->companyObj->updateAll(['phone'=>$params['phone']], ['id'=>$userDetails['company']['id']]);
						} else {
							$employeeUpdate = $this->employeeObj->updateAll(['phone'=>$params['phone']], ['id'=>$userDetails['employee']['id']]);
						}
					}
                                        
                    $result = array(
                                'status'=>'success', 
                                'message'=>"Account verified successfully.", 
                                'data'=>array(
                                        'id'=>$phoneDetails['user_id'], 
                                        'type'=>$params['type']
                                    )
                            );
                } else {
                    $result = array(
                                'status'=>'error', 
                                'message'=>'OTP is not correct.', 
                                'data'=>array()
                            );
                }
            } else {
                $userDetails = $this->Users->find()->where(['phone'=>$params['phone'], 'otp'=>$params['otp'], 'type'=>$params['type']])->enableHydration(false)->first();
                if(isset($userDetails) && !empty($userDetails)) {
                    if($userDetails['status'] == 'active' && $userDetails['is_otp_verified'] == 'yes') {
                        $result = array(
                                    'status'=>'success', 
                                    'message'=>"Account verified successfully.", 
                                    'data'=>array(
                                            'id'=>$userDetails['id'], 
                                            'type'=>$userDetails['type']
                                            )
                                    );
                    } else {
                        //Update user status
                        $updateActivate = $this->Users->updateAll(['status'=> 'active', 'is_otp_verified'=>'yes'], ['id'=> $userDetails['id']]);
                        $result = array(
                                    'status'=>'success', 
                                    'message'=>"Account verified successfully.", 
                                    'data'=>array(
                                            'id'=>$userDetails['id'], 
                                            'type'=>$userDetails['type']
                                        )
                                );
                    }
                } else {
                    $result = array('status'=>'error', 'message'=>'OTP is not correct.', 'data'=>array()); 
                }
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                'data'       => $result['data'],
                '_serialize' => ['status', 'message', 'data']
            ]);
        }
    }

    /**
     * API: Account Deactivate by self (guest & company/individual)
     * PARAMS: status
     */
    public function accountDeactivate() 
	{
        if($this->request->is('post')) {
            $id = $this->Auth->identify()['id'];
            $params = $this->request->data;
            $result = $this->Users->updateAll(['status'=>$params['status']], ['id'=>$id]);
            if($result) {
                $result = array('status'=>'success', 'message'=>'Account '.$params['status'].' successfully.');
            }
            else {
                $result = array('status'=>'failure', 'message'=>'Something went wrong');
            }
            $this->set([
				'status'     => $result['status'],
				'message'    => $result['message'],
				'_serialize' => ['status', 'message']
            ]);
        }
    }

    /**
     * API: Employee account deactivate by company
     * PARAMS: status, user_id
     */
    public function employeeDeactivate() 
	{
        if($this->request->is('post')) {
            $params = $this->request->data;
            $result = $this->Users->updateAll(['status'=>$params['status']], ['id'=>$params['user_id']]);
            if($result) {
                $result = array('status'=>'success', 'message'=>'Account '.$params['status'].' successfully.');
            } else {
                $result = array('status'=>'failure', 'message'=>'Something went wrong');
            }
            $this->set([
				'status'     => $result['status'],
				'message'    => $result['message'],
				'_serialize' => ['status', 'message']
            ]);
        }
    }

    /**
     * API: complete account delete by self (guest & company/individual)
     */
    public function accountDelete() 
	{ 
		$user = $this->Auth->identify()['id'];
		//Log::notice($user);
		$entity = $this->Users->get($user);
		$result = $this->Users->delete($entity);
		if($result) {
			$result = array('status'=>'success', 'message'=>'Account deleted successfully.');
		} else {
			$result = array('status'=>'failure', 'message'=>'Something went wrong');
		}
		$this->set([
			'status'     => $result['status'],
			'message'    => $result['message'],
			'_serialize' => ['status', 'message']
		]);
    }

    /**
     * API: employee complete account delete by company
     * PARAMS: id, user_id, custom: yes/no
     */
    public function employeeDelete() 
	{    
        if($this->request->is('post')) {
            $params = $this->request->data;
			//Log::notice($params);die;
			$reciever = $this->employeeObj->find()->where(['Employees.id'=>$params['id']])->select(['Employees.first_name', 'Employees.last_name'])->contain(['Users'=>['fields'=>['Users.id','Users.phone','Users.device_type', 'Users.device_token','Users.noti_alert']]])->select('user_id')->enableHydration(false)->first();
			
            //remove if & main else after this if (remove after new build ).
            if(empty($params['custom'])) {
                $user = $params['user_id'];
                $entity = $this->Users->get($user);
                $result = $this->Users->delete($entity);
            } else {
                if($params['custom'] == "yes") {
                    $user = $params['id'];
                    $entity = $this->employeeObj->get($user);
                    $result = $this->employeeObj->delete($entity);
                } else {
                    $user = $params['user_id'];
                    $entity = $this->Users->get($user);
                    $result = $this->Users->delete($entity);
                }     
            }
            if($result) {
				/*
				//Send notification				
				$device_type = $reciever['user']['device_type'];
				$device_token = $reciever['user']['device_token'];
				$name = $reciever['user']['first_name'].' '.$reciever['user']['last_name'];
				
				if(!empty($device_token) && !empty($device_type)) {
					//Save notification
					$data = [];
					$data['from_user_id'] = $this->Auth->identify()['id'];
					$data['to_user_id']   = $reciever['user_id'];
					$data['message']      = 'Account deleted';
					$data['type']         = 'send_to_host';
					$this->saveNotification($data);
					//Get unread notification count(badge_count) and update in users table
					$badgeCount = $this->getBadgeCount($reciever['user_id']);
					//Send notification
					if($reciever['user']['noti_alert'] == "on") {
						$this->sendPushNotification($device_type, $device_token, $name.": ".$data['message'],'send_to_host', $badgeCount, $name);
					}
				}*/	
                $result = array('status'=>'success', 'message'=>'Deleted successfully.');
            }
            else {
                $result = array('status'=>'failure', 'message'=>'Something went wrong');
            }
            $this->set([
				'status'     => $result['status'],
				'message'    => $result['message'],
				'_serialize' => ['status', 'message']
            ]);
        }
    }

    /**
     * API: Login Guests/Hosts
     * PARAMS: phone, password, device_token, device_type(android/ios), type
     */
    public function login()
    {
        if ($this->request->is('post')) {
			$params = $this->request->data;
            //Log::notice($params);
			$deviceToken = !empty($params['device_token']) ? $params['device_token'] : '';
			$deviceType  = !empty($params['device_type']) ? $params['device_type'] : '';
			
            $user = $this->Auth->identify();
            if (isset($user) && !empty($user)) {

                if($user['type'] != $params['type']) {
					$result = array('status'=>'failure', 'message'=>'Wrong Username & password');
					echo json_encode($result); die;
                }

                if($user['status'] == 'inactive' && $user['is_otp_verified'] == 'yes') {
                    $result = array('status'=>'failure', 'message'=>'Your account is deactivate', 'data'=>array());
                    echo json_encode($result); die;
                }
                //Create token for update
                $uniquecode = $this->token($user['id']);
				//Send OTP code if otp not verified
				if(!empty($user['is_otp_verified']) && $user['is_otp_verified'] == 'no') {
					//Generate OTP
					$user['otp'] = $this->generateOTP();
					$this->sendOTP($user['countrycode'], $user['phone'], $user['otp']);
				}
				//Update access token and otp in database
				$this->Users->updateAll(['access_token'=>$uniquecode, 'otp'=>$user['otp'], 'device_token'=>$deviceToken, 'device_type'=>$deviceType], ['id'=> $user['id']]);
				                
                //Get company/employee name
                if($user['type'] == 'guest') { 
					$is_profile_completed = 'no';
					$guest = $this->Users->find('all')->where(['Users.id'=>$user['id']])->contain(['Customers'])->select(['Customers.id','Customers.name'])->enableHydration(false)->first();
					if(!empty($guest['Customers']['id']) || !empty($guest['Customers']['name'])) {
						$customer_id = $guest['Customers']['id'];
						$name = $guest['Customers']['name'];
						if(!empty($name)) {
							$is_profile_completed = 'yes';
						}
					}

                    $cartCount = $this->bookingObj->find()->where(['customer_id'=>$guest['Customers']['id'],'payment_status'=>'no','Booking.type'=>'other'])->count();
										
					$data = [
                        'id'                   => $user['id'],
						'customer_id'          => !empty($customer_id) ? $customer_id : '',
						'name'                 => !empty($name)?$name:'',
						'type'                 => $user['type'],
						'status'               => $user['status'],  
						'token'                => $uniquecode,
						'is_otp_verified'      => $user['is_otp_verified'],
						'is_profile_completed' => $is_profile_completed,
                        'visibility'           => $user['visibility'],
                        'noti_alert'           => $user['noti_alert'],
						'device_token'         => $deviceToken,
						'device_type'          => $deviceType,
						'badge_count'          => $user['badge_count'],
                        'cart_badge'           => $cartCount
					];
					
                } else if($user['type'] == 'host') {
                    $profile_type = '';
					$profile_id   = '';
					$waitStatus   = '';
                    $is_profile_completed = 'no';
                    $host = $this->Users->find('all')->where(['Users.id'=>$user['id']])->contain(['Companies', 'Employees'])->select(['Companies.id','Companies.company_name','Companies.category_id','Companies.type', 'Companies.wait_status', 'Employees.id','Employees.category_id','Employees.first_name'])->enableHydration(false)->first();
                    
                    if(!empty($host['Companies']['id']) || !empty($host['Companies']['company_name'])) {
                        $name = $host['Companies']['company_name'];
                        if(!empty($name)) {
                            $is_profile_completed = 'yes';
                        }
                        $profile_type = $host['Companies']['type'];
						$profile_id = $host['Companies']['id'];
						$waitStatus = $host['Companies']['wait_status'];
                        $categoryId = strval($host['Companies']['category_id']);
                    } else if(!empty($host['Employees']['id']) || !empty($host['Employees']['first_name'])) {
                        $name = $host['Employees']['first_name'];
                        if(!empty($name)) {
                            $is_profile_completed = 'yes';
                        }
                        $profile_type = 'employee';
						$profile_id = $host['Employees']['id'];
                        $categoryId = strval($host['Employees']['category_id']);
                    }

                    $data = [
						'id'                   => $user['id'],
                        'name'                 => !empty($name)?$name:'',
						'type'                 => $user['type'],
						'status'               => $user['status'],  
						'token'                => $uniquecode,
                        'profile_type'         => $profile_type,
						'profile_id'           => $profile_id,
                        'category_id'          => !empty($categoryId)?$categoryId:'',
						'wait_status'          => $waitStatus,
						'is_otp_verified'      => $user['is_otp_verified'],
                        'is_profile_completed' => $is_profile_completed,
                        'visibility'           => $user['visibility'],
                        'noti_alert'           => $user['noti_alert'],
						'device_token'         => $deviceToken,
						'device_type'          => $deviceType,
						'badge_count'          => $user['badge_count']
					];
                }
				
                $result = array('status'=>'success', 'message'=>'Login successful.', 'data'=>$data);

            } else {
                $result = array('status'=>'failure', 'message'=>'Invalid phone number or password', 'data'=>array());
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                'data'       => $result['data'],
                '_serialize' => ['status', 'message', 'data']
            ]);
        }
    }
	
	/**
	 * API: Guest/Host Logout
	 */
    public function logout()
    {
        if($this->Auth->logout()) {
        	$result = array('status'=>'success', 'message'=>'Logout successfully.');
        }
        
        $this->set([
	        'status' => $result['status'],
            'message' => $result['message'],
	        '_serialize' => ['status', 'message']
	    ]);
    }
    
    /**
     * API: Forgot Password
     * PARAMS: phone
     */
    public function forgotPassword()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            $userDetails = $this->Users->find()->where(['phone'=>$params['phone']])->select(['Users.id', 'Users.type', 'Users.status', 'Users.countrycode'])->first();

            if(isset($userDetails) && !empty($userDetails) && $userDetails['status'] == 'active') {
                //Generate password and save in database
                $password = $this->generatePassword();
                //Update password in DB
                $userDetails->password = $password;
                $this->Users->save($userDetails);

                $phone = !empty($userDetails['countrycode']) ? '+'.$userDetails['countrycode'].$params['phone'] : $this->countryCode.$params['phone'];

                //Send password as SMS
                $sid = 'ACf142069fb73a767739e4de2d824c71cd';
                $stoken = 'a118574886a7a1f012f4fae2b446cd95';
                $client = new Client($sid, $stoken);
				
				$validPhone = $this->validatePhoneNumber($phone,$sid ,$stoken);
				if(empty($validPhone)) {
				    $checkPhone = array(
							'status' => "failure",
							'message' => "Not a valid mobile number"
						);

				   echo json_encode($checkPhone); die;
				}
				
                $sms = $client->messages->create(
                    // the number you'd like to send the message to
                    "'".$phone."'",
                    array(
                        // A Twilio phone number you purchased at twilio.com/console
                        'from' => '+16572363149',
                        // the body of the text message you'd like to send
                        'body' => 'Your MYNUS 2 new password is: '.$password
                    )
                );

                if($sms) {
                    $result = array('status'=>'success', 'message'=>"Password sent successfully.", 'data'=>array('id'=>$userDetails['id']));
                } else {
                    $result = array('status'=>'failure', 'message'=>"Password not send.", 'data'=>array('id'=>$userDetails['id']));
                }
                
            } else {
                if($userDetails['status'] == 'inactive') {
                    $result = array('status'=>'failure', 'message'=>"Please verify your account.", 'data'=>array());
                } else {
                    $result = array('status'=>'failure', 'message'=>"Phone number does not exist.", 'data'=>array());
                }
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                'data'       => $result['data'],
                '_serialize' => ['status', 'message', 'data']
            ]);
        }
    }

    /**
     * API: Verify Password
     * PARAMS: phone, password
     */
    public function verifyPassword()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            
            $user = $this->Users->findByPhone($params['phone'])->first();
            
            $user = $this->Users->patchEntity($user, [
                'old_password'      => $params['password'],
                'password'          => $params['password']
                ],['validate' => 'password']   
            );

            if ($this->Users->save($user)) {
                $result = array('status'=>'success', 'message'=>'Your password has been verified and updated.');
            } else {
                $result = array('status'=>'failure', 'message'=>'Wrong password entered.');
            }
            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }  
    }

    /**
     * API: Update Password
     * PARAMS: old_password, new_password, confirm_password
     */
    public function updatePassword()
    {
        $user = $this->Users->get($this->Auth->identify()['id']);
        if(!empty($this->request->data))
        {
            $user = $this->Users->patchEntity($user, [
                'old_password'      => $this->request->data['old_password'],
                'password'          => $this->request->data['new_password'],
                'new_password'      => $this->request->data['new_password'],
                'confirm_password'  => $this->request->data['confirm_password']
                ],['validate' => 'password']   
            );

            if($this->Users->save($user))
            {
                $result = array('status'=>'success', 'message'=>'Your password has been updated successfully.');
            } else {
                if ($errors = $user->errors()) { 
                    $erorMessage = array(); 
                    $i = 0; 
                    $keys = array_keys($errors); 
                    foreach ($errors as $errors) { 
                        $key = key($errors); 
                        foreach($errors as $error){ 
                            $erorMessage = $error;
                        }
                        $i++;
                    }
                    $result = array('status'=>'failure', 'message'=>$erorMessage, 'data'=>array());
                } else {
                    $result = array('status'=>'failure', 'message'=>'Error in changing password. Please try again!');
                }
            }
        }
        
        $this->set([
            'status'     => $result['status'],
            'message'    => $result['message'],
            '_serialize' => ['status', 'message']
        ]);
    }

    /**
     * Generate Password
     */
    private function generatePassword() 
    {
        $chars = "0123456789abcdefghijklmnopqrstuvwxyz";
        $res = "";
        for ($i = 0; $i < 6; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars)-1)];
        }
        return $res;
    }
	
    /**
     * API: Customer Add Profile
     * PARAMS: name, email, profile_pic
     */
	public function addCustomerProfile()
	{
		if ($this->request->is('post')) { 
		
			$params = $this->request->data;
			$params['user_id'] =  $this->Auth->identify()['id'];
			$params['phone'] =  $this->Auth->identify()['phone'];
            //Log::notice($params);
			if(isset($_FILES['profile_pic']['tmp_name'])) 
			{
				$temp = $_FILES['profile_pic']['tmp_name'];
				$name = $_FILES['profile_pic']['name'];
				$ext = substr(strrchr($name , '.'), 1);
				$arr_ext = array('jpg', 'jpeg', 'png');
				$NewFileName = time().rand(00, 99);
				$setNewFileName =  $NewFileName."_".$name;
				$params['profile_pic'] =  $setNewFileName;
				if (in_array($ext, $arr_ext)) {

					if(move_uploaded_file($temp, WWW_ROOT . 'images/' . $setNewFileName)) {
	                     $cusDetails = $this->customerObj->findByUserId($params['user_id'])->first();
	                        if(!empty($cusDetails)){
	                            $customer = $this->customerObj->get($cusDetails['id']); 
	                        } else {
	                            $customer = $this->customerObj->newEntity(); 
	                        } 
						$customer = $this->customerObj->patchEntity($customer, $params);
						if($this->customerObj->save($customer)) {
							$result = array('status'=>'success', 'message'=>"Saved successfully");
						} else {
                                $errors = $customer->errors();
                                $erorMessage = array(); 
                                $i = 0; 
                                $keys = array_keys($errors); 
                                foreach ($errors as $errors) { 
                                    $key = key($errors); 
                                    foreach($errors as $error){ 
                                        $erorMessage = ucfirst($keys[$i]) . " :- " . $error;
                                        //$erorMessage = $error;
                                    }
                                    $i++;
                                }
                                $result = array('status'=>'failure', 'message'=>$erorMessage);
						}                      
					} else {
						$result = array('status'=>'failure', 'message' => 'something went wrong');
					}
				} else {
					$result = array('status'=>'failed', 'message' => 'please upload correct format');
				}
			} else {
				$cusDetails = $this->customerObj->findByUserId($params['user_id'])->first();
				if(!empty($cusDetails)){
					$customer = $this->customerObj->get($cusDetails['id']); 
				} else {
					$customer = $this->customerObj->newEntity(); 
				} 
				$customer = $this->customerObj->patchEntity($customer, $params);
				if($this->customerObj->save($customer)) {
					$result = array('status'=>'success', 'message'=>"Saved successfully" );
				} 
                else {
                        $errors = $customer->errors();
                        $erorMessage = array(); 
                        $i = 0; 
                        $keys = array_keys($errors); 
                        foreach ($errors as $errors) { 
                             $key = key($errors); 
                            foreach($errors as $error){ 
                                    $erorMessage = ucfirst($keys[$i]) . " :- " . $error;
                                    //$erorMessage = $error;
                                 }
                                $i++;
                            }
                        $result = array('status'=>'failure', 'message'=>$erorMessage);
                } 
			}

			$this->set([
				'status'     => $result['status'],
				'message'    => $result['message'],
				'_serialize' => ['status', 'message']
			]);
		}
    }
    
    /**
     * API: Edit Customer Profile
     * PARAMS: name, email, profile_pic, customer_id, phone
     */
    public function editCustomerProfile()
    {
        if ($this->request->is('post')) 
        {   
            $params = $this->request->getData();
            //Log::notice($params);
            $params['user_id'] = $this->Auth->identify()['id'];
			$countrycode = $this->Auth->identify()['countrycode'];
            
            //Flag if phone change
            $change = "no";
            $flag = 1;
            if(isset($params['phone']) && !empty($params['phone'])) {
                //Check if phone exist in users table
                $exists = $this->Users->findByPhone($params['phone'])->enableHydration(false)->first();

                if(!$exists) {
                    //Generate and Send otp if number changed
                    $otp = $this->generateOTP();
                    $res = $this->sendOTP($countrycode, $params['phone'], $otp);
                    if($res) {
                        //If phone change then save in phones table
                        $changePhone = $this->phoneObj->newEntity();
                        $changePhone->otp = $otp;
                        $changePhone = $this->phoneObj->patchEntity($changePhone, $params);
                        $this->phoneObj->save($changePhone);

                        //Do not change phone number till OTP not verify
                        $change = "yes";
                        $params['phone'] = $this->Auth->identify()['phone'];
                        $flag = 1;
                    }
                } else {
                    if($exists['id'] != $params['user_id']) {
						$flag = 0;
						$result = array('status'=>'failure', 'message'=>"Phone number already exist.", 'change'=>$change);
						echo json_encode($result); die;
					} 
                }
            }

            if(isset($_FILES['profile_pic']['tmp_name'])) 
            {
                $temp = $_FILES['profile_pic']['tmp_name'];
                $name = $_FILES['profile_pic']['name'];
                $ext = substr(strrchr($name , '.'), 1);
                $arr_ext = array('jpg', 'jpeg', 'png');
                $NewFileName = time().rand(00, 99);
                $setNewFileName =  $NewFileName."_".$name;
                $params['profile_pic'] =  $setNewFileName;
                if (in_array($ext, $arr_ext)) {
                    if(move_uploaded_file($temp, WWW_ROOT . 'images/' . $setNewFileName)) {            
                        
                        $customer = $this->customerObj->get($params['customer_id']); 
                        $customer = $this->customerObj->patchEntity($customer, $params);
                        if($this->customerObj->save($customer)) {
                            $result = array('status'=>'success', 'message'=>"Updated successfully", 'change'=>$change);
                        } else {
							$errors = $customer->errors();
							$erorMessage = array(); 
							$i = 0; 
							$keys = array_keys($errors); 
							foreach ($errors as $errors) { 
								$key = key($errors); 
								foreach($errors as $error){ 
									$erorMessage = ucfirst($keys[$i]) . " :- " . $error;
									//$erorMessage = $error;
								}
								$i++;
							}
							$result = array('status'=>'failure', 'message'=>$erorMessage);
                        }                    
                    } else {
                        $result = array('status'=>'failure', 'message'=>'Something went wrong', 'change'=>$change);
                    }
                } else {
                    $result = array('status'=>'failed', 'message'=>'Please upload correct format', 'change'=>$change);
                }
            } else {    
                $customer = $this->customerObj->get($params['customer_id']); 
                $customer = $this->customerObj->patchEntity($customer, $params);
                if($this->customerObj->save($customer)) {
                    $result = array('status'=>'success', 'message'=>"Updated successfully", 'change'=>$change);
                }
            }
            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                'change'     => $result['change'],
                '_serialize' => ['status', 'message', 'change']
            ]);
        }
    }
    
	/**
     * API: View Customer Profile
     */
    public function viewCustomerProfile() 
    {
		$user_id =  $this->Auth->identify()['id'];
		$phone =  $this->Auth->identify()['phone'];
		$cusDetails = $this->customerObj->findByUserId($user_id)->contain(['Users'=>['fields'=>['Users.id', 'Users.visibility']]])->first(); 
		$customerDetails = array();
		if(!empty($cusDetails)) {
			$customerDetails['customer_id'] = $cusDetails['id'];
			$customerDetails['name']        = $cusDetails['name'];
			$customerDetails['email']       = $cusDetails['email'];
			$customerDetails['phone']       = $phone;
			$customerDetails['visibility']  = $cusDetails['user']['visibility'];
			$customerDetails['profile_pic'] = !empty($cusDetails['profile_pic']) ? Router::url('/','true')."webroot/images".DS.$cusDetails['profile_pic'] : $this->noImage;
			$result = array('status'=>'success', 'message' => 'customer details','data'=>$customerDetails);
		}
		else {
			$result = array('status'=>'failure', 'message' => 'no record found' ,'data'=>null);
		}
		$this->set([
			'status'     => $result['status'],
			'message'    => $result['message'],
			'data'       => $result['data'],
			'_serialize' => ['status', 'message','data']
		]);
    }

	/**
     * API: List of categories
     * PARAMS: type(guest/host)
     */
    public function categoryList()
    {
        if($this->request->is('Post')) {

            $params = $this->request->data;
            if($params['type'] == 'host' ) {
                $categoryData = $this->categoryObj->find()->where(['status'=>'active'])->select(['Categories.id', 'Categories.name'])->contain(['Companies'=>['fields'=>['Companies.id','Companies.category_id','Companies.company_name']]])->order(['Categories.name' => 'ASC'])->toArray();
            } else {
                $categoryData = $this->categoryObj->find()->select(['Categories.id', 'Categories.name'])->order(['Categories.name' => 'ASC'])->toArray();
            }

            if(!empty($categoryData)) {
                $result = array('status'=>'success', 'message'=>'List of categories','data'=>$categoryData);
            } else {
                $result = array('status'=>'failure', 'message'=>'No Record found','data' => array());
            }
            $this->set([
                'data' =>  $result['data'],
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message','data']
            ]); 
        }
    }

    /**
     * API: Add favorite business
     * PARAMS: customer_id, company_id, is_favorite
     */
    public function addRemoveFavoriteBusiness()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            if(empty($params['customer_id'])) {
                $cusDetails = $this->customerObj->findByUserId($this->Auth->identify()['id'])->first();
                $params['customer_id'] = $cusDetails['id'];
            }
            $id  = $this->favoriteObj->find()->where(['company_id'=>$params['company_id'],
                'customer_id'=>$params['customer_id']])->first();
            
            if(!empty($id)) {
				$favorite = $this->favoriteObj->get($id['id']);
				if($params['is_favorite'] == 'yes') {
					$msg = "Business added in your favorite list.";
				} else {
					$msg = "Business removed from favorite list.";
				}
            } else {
				$favorite = $this->favoriteObj->newEntity();
				$msg = "Business added in favorite list.";
            }

            $favorite = $this->favoriteObj->patchEntity($favorite, $params);
            if ($this->favoriteObj->save($favorite)) 
            {
                $result = array('status'=>'success', 'message'=>$msg, 'data'=>array('favorite_id'=>$favorite->id));
                        
            } else {
                $errors = $favorite->errors();
                $erorMessage = array(); 
                $i = 0; 
                $keys = array_keys($errors); 
                foreach ($errors as $errors) { 
                    $key = key($errors); 
                    foreach($errors as $error){ 
                        $erorMessage = ucfirst($keys[$i]) . " :- " . $error;
                        //$erorMessage = $error;
                    }
                    $i++;
                }
                $result = array('status'=>'error', 'message'=>$erorMessage, 'data'=>array()); 
            }
            
            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'data' =>  $result['data'],
                '_serialize' => ['status','message','data']
            ]);
        }
    }

    /**
     * API: Favortire Business List (Guest)
     * PARAMS: customer_id, latitude, longitude
     */
    public function favoriteBusinessList()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            $favorite = $this->customerObj->find()->where(['id'=>$params['customer_id']])
            //->contain(['Favorite.Companies.Categories'])
			->select(['id', 'name'])
			->contain([
				'Favorite' => [
					'fields'=>['Favorite.id', 'Favorite.is_favorite', 'Favorite.customer_id'],
					'Companies' => [
						'fields'=>[
							'Companies.id', 'Companies.company_name', 'Companies.first_name', 'Companies.last_name', 'Companies.address', 'Companies.city', 'Companies.state', 'Companies.country', 'Companies.zipcode'
						],
						'Categories' => [
							'fields'=>['Categories.id', 'Categories.name']
						]
					]
				]
			])
            ->enableHydration(false)->first();
			
            $companyDetails = array();
            if(!empty($favorite['favorite'])) {
                foreach ($favorite['favorite'] as $key => $value) {
                    $companyDetails[$key]['favorite_id'] = $value['id'];
                    $companyDetails[$key]['is_favorite'] = $value['is_favorite'];
                    $companyDetails[$key]['company_id'] = $value['company']['id'];
                    $companyDetails[$key]['category_name'] = $value['company']['category']['name'];
                    $companyDetails[$key]['company_name'] = $value['company']['company_name'];
                    $companyDetails[$key]['owner_name'] = $value['company']['first_name']." ".$value['company']['last_name'];
                    //$companyDetails[$key]['availability'] = $this->businessAvailabitily($value['company']['id']);
					$companyDetails[$key]['availability'] = 'AVAILABLE';
                    $companyDetails[$key]['address'] = $value['company']['address'];
                    $companyDetails[$key]['city'] = $value['company']['city'];
					$companyDetails[$key]['state'] = $value['company']['state'];
					$companyDetails[$key]['country'] = $value['company']['country'];
                    $companyDetails[$key]['zipcode'] = $value['company']['zipcode'];
                    $images = $this->galleryImages($value['company']['id']);
                    $companyDetails[$key]['image'] = !empty($images) ? $images[0]['image'] : $this->noImage;
					
					//Get business distance
					$companyDetails[$key]['distance'] = $this->getBusinessDistance($value['company']['id'], $params['latitude']="", $params['longitude']="");
                }
            }

            if(!empty($companyDetails)) {
                $result = array('status'=>'success', 'message'=>'List of favorite business','data'=>array_values($companyDetails));
            } else {
                $result = array('status'=>'failure', 'message'=>'No Record found','data'=>$companyDetails);
            }
            
            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'data' =>  $result['data'],
                '_serialize' => ['status','message','data']
            ]);
        }
    }
	
	public function getBusinessDistance($compId, $latitude='30.37011801', $longitude='78.17143701') 
	{
		$distanceField = '(3959 * acos (cos ( radians(:latitude) )
            * cos( radians( latitude ) )
            * cos( radians( longitude )
            - radians(:longitude) )
            + sin ( radians(:latitude) )
            * sin( radians( latitude ) )))';
			
		$result = $this->companyObj->find()
            ->select([
                'id', 'distance' => $distanceField
             ])
            ->where(['Companies.id'=>$compId])
            ->bind(':latitude', $latitude, 'float')
            ->bind(':longitude', $longitude, 'float')
            ->order(['distance' => 'ASC'])->enableHydration(false)->first();
		
		if(!empty($result['distance'])) {
			return $result['distance'];
		} else {
			return '';
		}
	}

    /**
     * API: List of business
     * PARAMS: latitude, longitude, distance, category_id, search_val, open
     */
    public function businessList() 
    {
        if($this->request->is('Post')) {
            $params = $this->request->data;
            //Log::notice($params);
            $latitude = $params['latitude'];
            $longitude = $params['longitude'];
            $distance = $params['distance'];
            if(empty($params['customer_id'])) {
                $params['customer_id'] = 0;
            }
            //Calcuate distance between customer & business cordinates 
            $distanceField = '(3959 * acos (cos ( radians(:latitude) )
            * cos( radians( latitude ) )
            * cos( radians( longitude )
            - radians(:longitude) )
            + sin ( radians(:latitude) )
            * sin( radians( latitude ) )))';

            $conditions = array();
            if(isset($params['category_id']) && !empty($params['category_id'])) {
                $cateName = $this->categoryObj->findById($params['category_id'])->select(['name'])->enableHydration(false)->first();
                $cateName = $cateName['name'];
            } else {
                if(!empty($params['search_val'])) {
                    $cateName = $params['search_val'];
                    $catId = $this->categoryObj->findByName(trim($params['search_val']))->select(['id'])->enableHydration(false)->first();
                    $params['category_id'] = $catId['id'];
                } else {
                    $cateName = "";
                }
            }

            if(empty($params['open'])) {
                $open = 'no';
            } else {
                $open = $params['open']; 
            }
            //search business according to lat, long & distance (only 3 param required)
            $conditions['AND'] = [ "$distanceField < " => $distance];

            //search business according to lat, long , distance & category ID (add one more params['category_id'])
            if(isset($params['category_id']) && !empty($params['category_id'])) {
                $conditions['AND']['OR'] = [
									'category_id'=>$params['category_id']
								];
            }
            //search business according to lat, long , distance & search value (add one more params['search_val'])
            elseif(isset($params['search_val']) && empty($params['category_id'])) {
                $conditions['AND']['OR'] = [
									'company_name LIKE' => '%'.$params['search_val'].'%',
									'zipcode LIKE' => $params['search_val']
								]; 
            }

            $companyData = $this->companyObj->find()
            ->select([
                'id','user_id','category_id','Categories.name', 'company_name', 'first_name', 'last_name', 'address', 'city', 'state', 'country', 'zipcode', 'latitude', 'longitude', 'offers', 'distance' => $distanceField
             ])
            ->where($conditions)
            ->contain(['Categories'=>['fields'=>['Categories.id','Categories.name','Categories.status']], 'Users'=>['fields'=>['Users.id', 'Users.status', 'Users.payment_status']]])
            ->bind(':latitude', $latitude, 'float')
            ->bind(':longitude', $longitude, 'float')
            ->order(['distance' => 'ASC'])->enableHydration(false);

            //pr($companyData->toArray()); die;
            $googleDetails = $this->curlExec($latitude,$longitude,$distance,$cateName,$open);

            $companyDetails = array();
            //print_r($companyDetails); die;
            if(!empty($companyDetails) || !empty($companyData)) {
                foreach ($companyData as $key => $value) {
                    //Check if category is active and Company is active with payment_status yes
                    if( (!empty($value['category']['status']) && $value['category']['status'] == 'active') && ( (!empty($value['user']['status']) && $value['user']['status'] == 'active') && (!empty($value['user']['payment_status']) && ($value['user']['payment_status'] == 'yes' || $value['user']['payment_status'] == 'free')) ) ) {
                        $companyDetails[$key]['id']            = $value['id'];
                        $companyDetails[$key]['category_id']   = $value['category_id'];
                        $companyDetails[$key]['category_name'] = $value['category']['name'];
                        $companyDetails[$key]['is_favorite']   = $this->isFeature($value['id'], $params['customer_id']);
                        $companyDetails[$key]['company_name']  = $value['company_name'];
                        $companyDetails[$key]['owner_name']    = $value['first_name']." ".$value['last_name'];
                        $companyDetails[$key]['address']       = $value['address'];
                        $companyDetails[$key]['city']          = $value['city'];
                        $companyDetails[$key]['state']         = $value['state'];
                        $companyDetails[$key]['country']       = $value['country'];
                        $companyDetails[$key]['zipcode']       = $value['zipcode'];
                        $companyDetails[$key]['latitude']      = $value['latitude'];
                        $companyDetails[$key]['longitude']     = $value['longitude'];
                        $companyDetails[$key]['offers']        = $value['offers'];
                        $companyDetails[$key]['type']          = "register";
                        //$companyDetails[$key]['availability']= $this->businessAvailabitily($value['id']);
						$companyDetails[$key]['availability']  = 'AVAILABLE';
                        $companyDetails[$key]['distance']      = $value['distance'];
                        $companyDetails[$key]['rating']        = $this->businessRating($value['id']);
                        $companyDetails[$key]['rateUserCount'] = $this->countRating($value['id']);
                        $images                                = $this->galleryImages($value['id']);
                        $companyDetails[$key]['image']         = !empty($images) ? $images[0]['image'] : $this->noImage;
                    }    
                }

                if(isset($params['category_id']) && !empty($params['category_id'])) {
                    $companyDetails = array_merge($companyDetails,$googleDetails);
                } else {
                    $companyDetails = [];
                    $companyDetails = array_merge($companyDetails,$googleDetails);
                }

                //$companyDetails = array_merge($companyDetails,$googleDetails);

                if(!empty($companyDetails)) {
                    $result = array('status'=>'success', 'message'=>'List of business','data'=>array_values($companyDetails));
                } else {
                    $result = array('status'=>'failure', 'message'=>'No Record found','data'=>$companyDetails);
                }
            } else {
                $result = array('status'=>'failed', 'message'=>'No Record found', 'data'=>array());
            }

            $this->set([
                'data' =>  $result['data'],
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message','data']
            ]); 
        }
    }

    public function curlExec($latitude,$longitude,$distance,$cateName,$open) {
        $range = $distance;
        $distance = $distance*1.60934*1000;
        $newar = array();
        $ch = curl_init();
        if(!empty($cateName)) {
            $cateName = str_replace(' ', '', $cateName);
            if($open == "no") {
				$url = 'https://maps.googleapis.com/maps/api/place/search/json?location='.$latitude.','.$longitude.'&radius='.$distance.'&keyword='.$cateName.'&key=AIzaSyAxrlnzjA6PwkkYjsIJUd20JhdxIW3iiR4';
            } else {
				$url = 'https://maps.googleapis.com/maps/api/place/search/json?location='.$latitude.','.$longitude.'&radius='.$distance.'&opennow='."true".'&keyword='.$cateName.'&key=AIzaSyAxrlnzjA6PwkkYjsIJUd20JhdxIW3iiR4';
            }
            
            if($cateName == "Ball Rooms" || $cateName == "Meeting Rooms") {
				$url = 'https://maps.googleapis.com/maps/api/place/search/json?location='.$latitude.','.$longitude.'&radius='.$distance.'&type=hotels&keyword=hotels&key=AIzaSyAxrlnzjA6PwkkYjsIJUd20JhdxIW3iiR4';
            }
        } else {
           // $url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='.$latitude.','.$longitude.'&radius='.$distance.'&key=AIzaSyAxrlnzjA6PwkkYjsIJUd20JhdxIW3iiR4';
            return $newar;
        }
		
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $contents = curl_exec($ch);
        $json = json_decode($contents, true);

        $newar = array();
        if(!empty($json['results'])) {
			foreach($json['results'] as $key =>  $item) {
                $distance = $this->GetDrivingDistance($latitude,$item['geometry']['location']['lat'],$longitude,$item['geometry']['location']['lng']);
				
                $newar[$key]['company_name'] = $item['name'];
                $newar[$key]['latitude'] = $item['geometry']['location']['lat'];
                $newar[$key]['longitude'] = $item['geometry']['location']['lng'];
                $newar[$key]['category_name'] = $item['types'][0];
                
				// if(!empty($item['photos'][0]['photo_reference'])) {
                //     $newar[$key]['image'] = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference='.$item['photos'][0]['photo_reference'].'&key=AIzaSyAxrlnzjA6PwkkYjsIJUd20JhdxIW3iiR4';
                // }
				
                if(!empty($item['photos'][0]['photo_reference'])) {
                    $newar[$key]['photo_reference'] = $item['photos'][0]['photo_reference'];
                    $newar[$key]['image'] = "";
                } else {
                    $newar[$key]['photo_reference'] = "";
                    $newar[$key]['image'] = $this->noImage;
                }
				
                if(!empty($item['opening_hours'])) {
                    $newar[$key]['opening_hours'] = $item['opening_hours']['open_now'];
                } else {
                    $newar[$key]['opening_hours'] = "Business timing not added by host";
                }
				
                $newar[$key]['icon'] = $item['icon'];
                $newar[$key]['type'] = "unregister";
                $newar[$key]['vicinity'] = $item['vicinity'];
                $newar[$key]['place_id'] = $item['place_id'];
                if($distance <= $range) {
                    $newar[$key]['distance'] = $distance;
                } else {
                    $newar[$key]['distance'] = $range;
                }
			}
        }
        return $newar;
    }
	
	public function businessListdemo() 
    {
        if($this->request->is('Post')) {

            $params = $this->request->data;
            $latitude = $params['latitude'];
            $longitude = $params['longitude'];
            $distance = $params['distance'];
            if(empty($params['customer_id'])) {
                $params['customer_id'] = 0;
            }
            //Calcuate distance between customer & business cordinates 
            $distanceField = '(3959 * acos (cos ( radians(:latitude) )
            * cos( radians( latitude ) )
            * cos( radians( longitude )
            - radians(:longitude) )
            + sin ( radians(:latitude) )
            * sin( radians( latitude ) )))';

            $conditions = array();

            //search business according to lat, long & distance (only 3 param required)
            $conditions['AND'] = [ "$distanceField < " => $distance];

            //search business according to lat, long , distance & category ID (add one more params['category_id'])
            if(isset($params['category_id']) && !empty($params['category_id']) && empty($params['search_val'])) {
                $conditions['AND']['OR'] = [
                                'category_id'=>$params['category_id']
                              ];
            }
            //search business according to lat, long , distance & search value (add one more params['search_val'])
            elseif(isset($params['search_val']) && empty($params['category_id'])) {
                 $conditions['AND']['OR'] = [
                                 'company_name LIKE' => '%'.$params['search_val'].'%',
                                 'zipcode LIKE' => $params['search_val']
                               ]; 
            }

            //search business according to lat, long , distance, category_id & serach_value
            elseif(isset($params['category_id']) && isset($params['search_val'])) {
                $conditions['AND'] = [ "$distanceField < " => $distance,'category_id'=>$params['category_id']];
                $conditions['AND']['OR'] = [
                                            'company_name LIKE' => '%'.$params['search_val'].'%',
                                            'zipcode LIKE' => $params['search_val']
                                           ];
            }

            $companyData = $this->companyObj->find()
            ->select([
                'id','user_id','category_id','Categories.name', 'company_name', 'first_name', 'last_name', 'address', 'city', 'state', 'country', 'zipcode', 'latitude', 'longitude', 'offers', 'distance' => $distanceField
             ])
            ->where($conditions)
            ->contain(['Categories'=>['fields'=>['Categories.id','Categories.name','Categories.status']], 'Users'=>['fields'=>['Users.id', 'Users.status', 'Users.payment_status']]])
            ->bind(':latitude', $latitude, 'float')
            ->bind(':longitude', $longitude, 'float')
            ->order(['distance' => 'ASC'])->enableHydration(false);
			
            $companyDetails = array();
            if(!empty($companyData->toArray())) {
                foreach ($companyData as $key => $value) {
                    //Check if category is active and Company is active with payment_status yes
                    if( (!empty($value['category']['status']) && $value['category']['status'] == 'active') && ( (!empty($value['user']['status']) && $value['user']['status'] == 'active') && (!empty($value['user']['payment_status']) && ($value['user']['payment_status'] == 'yes' || $value['user']['payment_status'] == 'free')) ) ) {

                        $companyDetails[$key]['id']            = $value['id'];
                        $companyDetails[$key]['category_id']   = $value['category_id'];
                        $companyDetails[$key]['category_name'] = $value['category']['name'];
                        $companyDetails[$key]['is_favorite']   = $this->isFeature($value['id'], $params['customer_id']);
                        $companyDetails[$key]['company_name']  = $value['company_name'];
                        $companyDetails[$key]['owner_name']    = $value['first_name']." ".$value['last_name'];
                        $companyDetails[$key]['address']       = $value['address'];
                        $companyDetails[$key]['city']          = $value['city'];
						$companyDetails[$key]['state']         = $value['state'];
						$companyDetails[$key]['country']       = $value['country'];
                        $companyDetails[$key]['zipcode']       = $value['zipcode'];
                        $companyDetails[$key]['latitude']      = $value['latitude'];
                        $companyDetails[$key]['longitude']     = $value['longitude'];
                        $companyDetails[$key]['offers']        = $value['offers'];
                        //$companyDetails[$key]['availability']  = $this->businessAvailabitily($value['id']);
						$companyDetails[$key]['availability']  = 'AVAILABLE';
                        $companyDetails[$key]['distance']      = $value['distance'];
                        $companyDetails[$key]['distance_2']    = $this->GetDrivingDistance($latitude, $value['latitude'], $longitude, $value['longitude']);
						$images                                = $this->galleryImages($value['id']);
                        $companyDetails[$key]['image']         = !empty($images) ? $images[0]['image'] : $this->noImage;
                    }    
                }

                if(!empty($companyDetails)) {
                    $result = array('status'=>'success', 'message'=>'List of business','data'=>array_values($companyDetails));
                } else {
                    $result = array('status'=>'failure', 'message'=>'No Record found','data'=>$companyDetails);
                }
            } else {
                $result = array('status'=>'failed', 'message'=>'No Record found', 'data'=>array());
            }

            $this->set([
                'data' =>  $result['data'],
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message','data']
            ]); 
        }
    }
	
	//Get distance
    public function GetDrivingDistance($lat1, $lat2, $long1, $long2)
    {
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&key=AIzaSyAxrlnzjA6PwkkYjsIJUd20JhdxIW3iiR4";
        
        //$url = "https://maps.googleapis.com/maps/api/distancematrix/xml?units=imperial&origins=dehradun&destinations=delhi&key=AIzaSyAHXt6lRBpA6PZYoR1UNX2-pmyk8WyC_5g";
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
        //pr($dist);
        $unit = preg_replace('/[0-9]+./', '', $dist);
        $dist = preg_replace("/[^0-9,.]/", "", $dist);
        if($unit == "m") {
            $dist = $dist*0.000621371;
        }
        else {
            $dist = $dist*0.621371;
        }
        // return array('distance' => $dist);
        return $dist;
    }

    //Check favoprite business by customer
    public function isFeature($companyId , $customerId) 
    {
        $data = $this->favoriteObj->find()->where(['company_id'=>$companyId, 'customer_id'=>$customerId])->select(['is_favorite'])->enableHydration(false)->first();

        if(!empty($data)) {
            return $data['is_favorite'];
        } else{
            return "no";
        }
    }

    //Check if business employees have available slots
    public function businessAvailabitily($companyId)
    {
        /*$allSlots = [];
        $allEmpls = [];
        $bookedSlots = [];

        //Get all slots of any company
        $details = $this->companyObj->find()->where(['Companies.id'=>$companyId])->contain(['Employees'=>['fields'=>['Employees.id', 'Employees.company_id'], 'Schedules'=>['fields'=>['Schedules.id', 'Schedules.employee_id'], 'Slots'=>['fields'=>['Slots.id', 'Slots.schedule_id']]]]])->select(['Companies.id'])->enableHydration(false)->first();

        if(!empty($details['employees'])) {
            foreach ($details['employees'] as $key => $value) {
                $allEmpls[] = $value['id'];
                foreach ($value['schedules'] as $key2 => $value2) {
                    foreach ($value2['slots'] as $key3 => $value3) {
                        $allSlots[] = $value3['id'];
                    }
                }
            }
        }

        //Get all bookings of any company
        $status = ['accepted', 'completed'];
        if(!empty($allEmpls)) {
            $bookings = $this->bookingObj->find()->where(['employee_id IN'=>$allEmpls, 'status IN'=>$status])->select(['slot_id'])->distinct(['slot_id'])->enableHydration(false)->toArray();

            if(!empty($bookings)) {
                foreach ($bookings as $key => $value) {
                    $bookedSlots[] = $value['slot_id'];
                }
            }
        }
        
        //Check if blank slots available
        $result = 'NOT AVAILABLE';
        if(!empty($allSlots) && !empty($bookedSlots)) {
            $blankSlots = array_diff($allSlots, $bookedSlots);
            if(!empty($blankSlots)) {
                $result = 'AVAILABLE';
            } else {
                $result = 'NOT AVAILABLE';
            }
        }*/
		//After change slots functionality result always available 
		$result = 'AVAILABLE';
        return ucwords($result);
    }
	    
	/**
     * API: Business Details
     * PARAMS: company_id, user_type(guest/host), day
     */
    public function viewBusinessDetails()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Business timing
            if(!empty($params['company_id']) && !empty($params['day'])) {
                $timing = $this->getBusinessTime($params['company_id'], $params['day']);
            }
			            
            //Check for user_type: remove after share build to client
            if(!isset($params['user_type'])) {
                $params['user_type'] = 'host';
            }

            if($params['user_type'] == 'guest') {
                $regEmpCond = ['Employees.approve' => 'yes'];
                $business = $this->companyObj->find()->where(['Companies.id'=>$params['company_id']])->contain(['Employees.Users'=>function($q) use ($regEmpCond)
                    { return $q->where($regEmpCond); }, 'Users'=>['fields'=>['Users.id', 'Users.visibility','Users.phone']]])->enableHydration(false)->first();
            } else {
                $business = $this->companyObj->find()->where(['Companies.id'=>$params['company_id']])->contain(['Employees.Users', 'Users'=>['fields'=>['Users.id', 'Users.visibility','Users.phone']]])->enableHydration(false)->first();
            }
            
            $businessDetails = [];
            if(!empty($business)) {

                $images = $this->galleryImages($business['id']);
                $image = array();
                if(!empty($images)) {
                    foreach ($images as $key => $value) {
                        $image[$key] = $value['image'];
                    }
                    $images = $image;
                }
                $noImage[] = $this->noImage;
                $businessDetails['image'] = !empty($images) ? $images : $noImage;

                $empDetails = [];
                if(!empty($business['employees'])) {
                    foreach($business['employees'] as $key=>$employees) {
                        if((!empty($params['user_type']) && $params['user_type'] == 'guest') && ($employees['type'] == 'employee' || $employees['type'] == 'other')) {
                            $empDetails[$key]['id']         = $employees['id'];
                            $empDetails[$key]['user_id']    = $employees['user']['id'];
                            $empDetails[$key]['status']     = $employees['user']['status'];
                            $empDetails[$key]['first_name'] = $employees['first_name'];
                            $empDetails[$key]['last_name']  = $employees['last_name'];
                            $empDetails[$key]['about']      = $employees['about'];
                            $empDetails[$key]['type']       = $employees['type'];
                            $empDetails[$key]['custom']     = $employees['custom'];
                            $empDetails[$key]['rating']     = $this->employeeRating($employees['id']);
							$empDetails[$key]['rateUserCount'] = $this->countRatingUsers($employees['id']);
                            
							if($employees['type'] == 'employee') {
								$empDetails[$key]['image']  = !empty($employees['profile_pic']) ? Router::url('/','true')."webroot/images/employees/".$employees['id']."/".$employees['profile_pic'] : $this->noImage;
							} elseif($employees['type'] == 'other') {
								$img = $this->galleryImages($employees['id'], $employees['type']);
								$empDetails[$key]['image']  = !empty($img) ? $img[0]['image'] : $this->noImage;
							}
							
                            if($business['type'] == 'individual') {
                                $empDetails[$key]['image']  = !empty($images) ? $images[0] : $noImage;
                            }
                        } elseif((!empty($params['user_type']) && $params['user_type'] == 'host') && $employees['type'] == 'employee') {
                            $empDetails[$key]['id']         = $employees['id'];
                            $empDetails[$key]['user_id']    = $employees['user']['id'];
                            $empDetails[$key]['status']     = $employees['user']['status'];
                            $empDetails[$key]['first_name'] = $employees['first_name'];
                            $empDetails[$key]['last_name']  = $employees['last_name'];
                            $empDetails[$key]['about']      = $employees['about'];
                            $empDetails[$key]['type']       = $employees['type'];
                            $empDetails[$key]['custom']     = $employees['custom'];
                            $empDetails[$key]['approve']    = $employees['approve'];
                            $empDetails[$key]['rating']     = $this->employeeRating($employees['id']);
							$empDetails[$key]['rateUserCount'] = $this->countRatingUsers($employees['id']);
                            $empDetails[$key]['image']      = !empty($employees['profile_pic']) ? Router::url('/','true')."webroot/images/employees/".$employees['id']."/".$employees['profile_pic'] : $this->noImage;
                            if($business['type'] == 'individual') {
                                $empDetails[$key]['image']  = !empty($images) ? $images[0] : $noImage;
                            }
                        }
                        $empDetails = array_values($empDetails);
                    }
                }
                
                $businessDetails['category_id']   =   $business['category_id'];
                $businessDetails['category_name'] = $this->getCategoryName($business['category_id']);
                $businessDetails['company_name']  = $business['company_name'];
                $businessDetails['first_name']    = $business['first_name'];
                $businessDetails['last_name']     = $business['last_name'];
                $businessDetails['phone']         = $business['user']['phone'];
                $businessDetails['business_phone'] = $business['business_phone'];
                $businessDetails['booking_amount'] = $business['booking_amount'];
                $businessDetails['email']         = $business['email'];
                $businessDetails['address']       = $business['address'];
                $businessDetails['city']          = $business['city'];
				$businessDetails['state']         = $business['state'];
				$businessDetails['country']       = $business['country'];
                $businessDetails['zipcode']       = $business['zipcode'];
                $businessDetails['about']         = $business['about'];
                $businessDetails['wait']          = $business['wait'];
				$businessDetails['waitCount']     = $this->totalWaiting($business['id']);
				$businessDetails['wait_status']   = $business['wait_status'];
                // if($business['category_id'] == 5) {
                //    $businessDetails['wait']   = "no"; 
                // }
                $businessDetails['type']          = $business['type'];
                $businessDetails['visibility']    = $business['user']['visibility'];
                $businessDetails['rating']        = $this->businessRating($business['id']);
                $businessDetails['rateUserCount'] = $this->countRating($business['id']);
                $businessDetails['start_time']    = !empty($timing['start_time']) ? $this->dateTimeFormat('h:i A', $timing['start_time']) : '';
                $businessDetails['end_time']      = !empty($timing['end_time']) ? $this->dateTimeFormat('h:i A', $timing['end_time']) : '';
                $businessDetails['employees']     = $empDetails;
				                
                $result = array('status'=>'success', 'message'=>'Business Details','data'=>$businessDetails);               
            } else {
                $result = array('status'=>'failed', 'message'=>'No Record found','data' => array());
            }
            $this->set([
                'data' =>  $result['data'],
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message','data']
            ]);
        }
    }

    /**
     * API: Business Details
     * PARAMS: company_id, user_type(guest/host), day
     */
    public function viewBusinessDetails1()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            //Business timing
            if(!empty($params['company_id']) && !empty($params['day'])) {
                $timing = $this->getBusinessTime($params['company_id'], $params['day']);
            }
                        
            //Check for user_type: remove after share build to client
            if(!isset($params['user_type'])) {
                $params['user_type'] = 'host';
            }

            if($params['user_type'] == 'guest') {
                $regEmpCond = ['Employees.approve' => 'yes'];
                $business = $this->companyObj->find()->where(['Companies.id'=>$params['company_id']])->contain(['Employees.Users'=>function($q) use ($regEmpCond)
                    { return $q->where($regEmpCond); }, 'Users'=>['fields'=>['Users.id', 'Users.visibility','Users.phone']]])->enableHydration(false)->first();
            }
            else {
                $business = $this->companyObj->find()->where(['Companies.id'=>$params['company_id']])->contain(['Employees.Users', 'Users'=>['fields'=>['Users.id', 'Users.visibility','Users.phone']]])->enableHydration(false)->first();
            }
            
            $businessDetails = [];
            if(!empty($business)) {

                $images = $this->galleryImages($business['id']);
                $image = array();
                if(!empty($images)) {
                    foreach ($images as $key => $value) {
                        $image[$key] = $value['image'];
                    }
                    $images = $image;
                }
                $noImage[] = $this->noImage;
                $businessDetails['image'] = !empty($images) ? $images : $noImage;


                $empDetails = [];
                if(!empty($business['employees'])) {
                    foreach($business['employees'] as $key=>$employees) {
                        if((!empty($params['user_type']) && $params['user_type'] == 'guest') && ($employees['type'] == 'employee' || $employees['type'] == 'other')) {
                            $empDetails[$key]['id']         = $employees['id'];
                            $empDetails[$key]['user_id']    = $employees['user']['id'];
                            $empDetails[$key]['status']     = $employees['user']['status'];
                            $empDetails[$key]['first_name'] = $employees['first_name'];
                            $empDetails[$key]['last_name']  = $employees['last_name'];
                            $empDetails[$key]['about']      = $employees['about'];
                            $empDetails[$key]['type']       = $employees['type'];
                            $empDetails[$key]['custom']     = $employees['custom'];
                            $empDetails[$key]['rating']     = $this->employeeRating($employees['id']);
                            $empDetails[$key]['rateUserCount'] = $this->countRatingUsers($employees['id']);
                            
                            if($employees['type'] == 'employee') {
                                $empDetails[$key]['image']  = !empty($employees['profile_pic']) ? Router::url('/','true')."webroot/images/employees/".$employees['id']."/".$employees['profile_pic'] : $this->noImage;
                            } elseif($employees['type'] == 'other') {
                                $img = $this->galleryImages($employees['id'], $employees['type']);
                                $empDetails[$key]['image']  = !empty($img) ? $img[0]['image'] : $this->noImage;
                            }
                            
                            if($business['type'] == 'individual') {
                                $empDetails[$key]['image']  = !empty($images) ? $images[0] : $noImage;
                            }
                        } elseif((!empty($params['user_type']) && $params['user_type'] == 'host') && $employees['type'] == 'employee') {
                            $empDetails[$key]['id']         = $employees['id'];
                            $empDetails[$key]['user_id']    = $employees['user']['id'];
                            $empDetails[$key]['status']     = $employees['user']['status'];
                            $empDetails[$key]['first_name'] = $employees['first_name'];
                            $empDetails[$key]['last_name']  = $employees['last_name'];
                            $empDetails[$key]['about']      = $employees['about'];
                            $empDetails[$key]['type']       = $employees['type'];
                            $empDetails[$key]['custom']     = $employees['custom'];
                            $empDetails[$key]['approve']     = $employees['approve'];
                            $empDetails[$key]['rating']     = $this->employeeRating($employees['id']);
                            $empDetails[$key]['rateUserCount'] = $this->countRatingUsers($employees['id']);
                            $empDetails[$key]['image']      = !empty($employees['profile_pic']) ? Router::url('/','true')."webroot/images/employees/".$employees['id']."/".$employees['profile_pic'] : $this->noImage;
                            if($business['type'] == 'individual') {
                                $empDetails[$key]['image']  = !empty($images) ? $images[0] : $noImage;
                            }
                        }
                        $empDetails = array_values($empDetails);
                    }
                }

                $businessDetails['category_id']   =  strval($business['category_id']);
                $businessDetails['category_name'] = $this->getCategoryName($business['category_id']);
                $businessDetails['company_name']  = $business['company_name'];
                $businessDetails['first_name']    = $business['first_name'];
                $businessDetails['last_name']     = $business['last_name'];
                $businessDetails['phone']         = $business['user']['phone'];
                $businessDetails['business_phone'] = $business['business_phone'];
                $businessDetails['booking_amount'] = $business['booking_amount'];
                $businessDetails['email']         = $business['email'];
                $businessDetails['address']       = $business['address'];
                $businessDetails['city']          = $business['city'];
                $businessDetails['state']         = $business['state'];
                $businessDetails['country']       = $business['country'];
                $businessDetails['zipcode']       = $business['zipcode'];
                $businessDetails['about']         = $business['about'];
                $businessDetails['wait']          = $business['wait'];
                $businessDetails['waitCount']     = $this->totalWaiting($business['id']);
                $businessDetails['wait_status']   = $business['wait_status'];
                $businessDetails['type']          = $business['type'];
                $businessDetails['visibility']    = $business['user']['visibility'];
                $businessDetails['rating']        = $this->businessRating($business['id']);
                $businessDetails['rateUserCount'] = $this->countRating($business['id']);
                $businessDetails['start_time']    = !empty($timing['start_time']) ? $this->dateTimeFormat('h:i A', $timing['start_time']) : '';
                $businessDetails['end_time']      = !empty($timing['end_time']) ? $this->dateTimeFormat('h:i A', $timing['end_time']) : '';
                $businessDetails['employees']     = $empDetails;
                                
                $result = array('status'=>'success', 'message'=>'Business Details','data'=>$businessDetails);               
            } else {
                $result = array('status'=>'failed', 'message'=>'No Record found','data' => array());
            }
            $this->set([
                'data' =>  $result['data'],
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message','data']
            ]);
        }
    }
    
    public function getBusinessTime($bussId, $day)
    {
		if(!empty($bussId) && !empty($day)) {
            $timing = '';
            $result = $this->businessTimeObj->find()->where(['company_id'=>$bussId, 'day'=>trim($day)])->enableHydration(false)->first();
			
            if(!empty($result)) {
                $timing = $result;
            }
            return $timing;
        }
    }
	
    //Get category name
	public function getCategoryName($cat_id) 
    {
		if(!empty($cat_id)){
			$result = $this->categoryObj->find()->where(['id'=>$cat_id])->select(['name'])->enableHydration(false)->first();
			return !empty($result['name']) ? $result['name'] : '';
		}
	}

      /**
     * API: Employee Slot Details
     * PARAMS: customer_id, employee_id, date(2018-8-3)
     */
    public function viewScheduleDetails()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Current logged in user
            $days = strtolower(date('l', strtotime($params['date'])));
            if(empty($params['profile_type'])) {
                $params['profile_type'] = "other";
            }

            if($params['profile_type'] == "company" || $params['profile_type'] == 'individual') {  
                $res = $this->checkSelectedSlot($params['company_id'], $params['customer_id'], $params['date']);  
                $res = [];    
                $schedule = $this->scheduleObj->find('all')->where(['company_id' => $params['company_id'],'days'=> $days])-> contain(['Slots'=>['fields'=>['Slots.id','Slots.start_time','Slots.end_time','Slots.schedule_id'], 'sort' => ['Slots.start_time' => 'ASC']]])->enableHydration(false)->toArray();
                      
            $tableLists = $this->restaurantObj->find('all')->select(['table_id'=>'id','name','capacity','description'])->where(['company_id'=>$params['company_id']])->enableHydration(false)->toArray();
            }
            else { 
                $res = $this->checkSelectedSlot($params['employee_id'], $params['customer_id'], $params['date']);
                $schedule = $this->scheduleObj->find('all')->where(['employee_id' => $params['employee_id'],'days'=> $days])-> contain(['Slots'=>['fields'=>['Slots.id','Slots.start_time','Slots.end_time','Slots.schedule_id'], 'sort' => ['Slots.start_time' => 'ASC']]])->enableHydration(false)->toArray();
            }  

            $scheduleDetails = array();
            if(!empty($schedule)) {
                foreach ($schedule as $key => $value) {
                    $scheduleDetails[$key]['schedule_id'] = $value['id'];
                    $scheduleDetails[$key]['employee_id'] = $value['employee_id'];
                    $scheduleDetails[$key]['company_id'] = $value['company_id'];
                    $scheduleDetails[$key]['days'] = trim($value['days']);
                    foreach ($value['slots'] as $key1 => $value1) {
                        if(!in_array($value1['id'], $res)) {
                            if((strtotime(date('H:i')) < strtotime($value1['start_time'])) && (strtotime(date('Y-m-d')) == strtotime($params['date']))) {
                                $scheduleDetails[$key]['slots'][$key1]['slot_id']    = $value1['id'];
                                $scheduleDetails[$key]['slots'][$key1]['start_time'] = date('H:i',strtotime($value1['start_time']));
                                $scheduleDetails[$key]['slots'][$key1]['end_time']   = date('H:i',strtotime($value1['end_time']));
                                if($params['profile_type'] == "other") {
                                  $scheduleDetails[$key]['slots'][$key1]['status']     = $this->checkSlotStatus($value1['id'], $params['date']);
                                }
                                else {
                                  $scheduleDetails[$key]['table_list'] = $tableLists; 
                                }
                            } 
                            elseif (strtotime(date('Y-m-d')) < strtotime($params['date'])) {
                                $scheduleDetails[$key]['slots'][$key1]['slot_id']    = $value1['id'];
                                $scheduleDetails[$key]['slots'][$key1]['start_time'] =  date('H:i',strtotime($value1['start_time']));
                                $scheduleDetails[$key]['slots'][$key1]['end_time']   = date('H:i',strtotime($value1['end_time']));
                                if($params['profile_type'] == "other") {
                                  $scheduleDetails[$key]['slots'][$key1]['status']     = $this->checkSlotStatus($value1['id'], $params['date']);
                                }
                                else {
                                  $scheduleDetails[$key]['table_list']['id'] = $tableLists[$key]['table_id'];
                                  $scheduleDetails[$key]['table_list'] = $tableLists;
                                }
                                
                            }
                        }
                    }
                    if(isset($scheduleDetails[$key]['slots']) && !empty($scheduleDetails[$key]['slots'])) {
                        $scheduleDetails[$key]['slots'] = array_values($scheduleDetails[$key]['slots']);
                    } else {
                        $scheduleDetails = array();
                    }
                }

                if(!empty($scheduleDetails)) {
                    $result = array('status'=>'success', 'message'=>'Schedules List','data'=>array_values($scheduleDetails));
                } else {
                    $result = array('status'=>'failure', 'message'=>'No Slots available','data'=>array_values($scheduleDetails));
                }
            } else {
                $result = array('status'=>'failure', 'message'=>'No Record Found','data'=>$scheduleDetails);
            }
            $this->set([
                'data' =>  $result['data'],
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message','data']
            ]);       
        }
    }

    /**
     * API: Employee Slot Details
     * PARAMS: customer_id, employee_id, date(2018-8-3)
     */
    public function viewScheduleDetails1()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;

            //Current logged in user
            $days = strtolower(date('l', strtotime($params['date'])));
            if(empty($params['profile_type'])) {
                $params['profile_type'] = "other";
            }

            if($params['profile_type'] == "company") {  
                $res = $this->checkSelectedSlot($params['company_id'], $params['customer_id'], $params['date']);  
                $res = [];    
                $schedule = $this->scheduleObj->find('all')->where(['company_id' => $params['company_id'],'days'=> $days])-> contain(['Slots'=>['fields'=>['Slots.id','Slots.start_time','Slots.end_time','Slots.schedule_id'], 'sort' => ['Slots.start_time' => 'ASC']]])->enableHydration(false)->toArray();
            }
            else { 
                $res = $this->checkSelectedSlot($params['employee_id'], $params['customer_id'], $params['date']);
                $schedule = $this->scheduleObj->find('all')->where(['employee_id' => $params['employee_id'],'days'=> $days])-> contain(['Slots'=>['fields'=>['Slots.id','Slots.start_time','Slots.end_time','Slots.schedule_id'], 'sort' => ['Slots.start_time' => 'ASC']]])->enableHydration(false)->toArray();
            }  

           // pr($tableLists); die;
            $scheduleDetails = array();
            if(!empty($schedule)) {
                foreach ($schedule as $key => $value) {
                    $scheduleDetails[$key]['schedule_id'] = $value['id'];
                    $scheduleDetails[$key]['employee_id'] = $value['employee_id'];
                    $scheduleDetails[$key]['company_id'] = $value['company_id'];
                    $scheduleDetails[$key]['days'] = trim($value['days']);
                    foreach ($value['slots'] as $key1 => $value1) {
                        if(!in_array($value1['id'], $res)) {
                            if((strtotime(date('H:i')) < strtotime($value1['start_time'])) && (strtotime(date('Y-m-d')) == strtotime($params['date']))) {
                                $scheduleDetails[$key]['slots'][$key1]['slot_id']    = $value1['id'];
                                $scheduleDetails[$key]['slots'][$key1]['start_time'] = date('H:i',strtotime($value1['start_time']));
                                $scheduleDetails[$key]['slots'][$key1]['end_time']   = date('H:i',strtotime($value1['end_time']));
                                if($params['profile_type'] == "other") {
                                  $scheduleDetails[$key]['slots'][$key1]['status'] = $this->checkSlotStatus($value1['id'], $params['date']);
                                }
                            } 
                            elseif (strtotime(date('Y-m-d')) < strtotime($params['date'])) {
                                $scheduleDetails[$key]['slots'][$key1]['slot_id']    = $value1['id'];
                                $scheduleDetails[$key]['slots'][$key1]['start_time'] =  date('H:i',strtotime($value1['start_time']));
                                $scheduleDetails[$key]['slots'][$key1]['end_time']   = date('H:i',strtotime($value1['end_time']));
                                if($params['profile_type'] == "other") {
                                  $scheduleDetails[$key]['slots'][$key1]['status']   = $this->checkSlotStatus($value1['id'], $params['date']);
                                }

                            }
                        }
                    }
                    if(isset($scheduleDetails[$key]['slots']) && !empty($scheduleDetails[$key]['slots'])) {
                        $scheduleDetails[$key]['slots'] = array_values($scheduleDetails[$key]['slots']);
                    } else {
                        $scheduleDetails = array();
                    }
                }

                if(!empty($scheduleDetails)) {
                    $result = array('status'=>'success', 'message'=>'Schedules List','data'=>array_values($scheduleDetails));
                } else {
                    $result = array('status'=>'failure', 'message'=>'No Slots available','data'=>array_values($scheduleDetails));
                }
            } else {
                $result = array('status'=>'failure', 'message'=>'No Record Found','data'=>$scheduleDetails);
            }
            $this->set([
                'data' =>  $result['data'],
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message','data']
            ]);       
        }
    }

    //Check if slot already selected
    public function checkSelectedSlot($empId, $custId, $date) 
    {
        $bookings = $this->bookingObj->find()->where(['employee_id'=>$empId, 'customer_id'=>$custId, 'date'=>trim($date)])->select(['slot_id'])->enableHydration(false)->toArray();
        $slots = [];
        foreach ($bookings as $key => $value) {
            $slots[$key] = $value['slot_id'];
        }
        return $slots;
    }
	
	/**
     * API: Employee Details
     * PARAMS: employee_id
     */
	public function viewEmployeeDetails()
	{
		if($this->request->is('post')) {
			$params = $this->request->data;
			
			$employee = $this->employeeObj->find()->where(['Employees.id'=>$params['employee_id']])->contain(['Users'=>['fields'=>['Users.id', 'Users.visibility']] , 'Companies'])->enableHydration(false)->first();
			
			$empDetails = [];
			if(!empty($employee)) {
                $empDetails['offer']         = $employee['company']['offers'];
				$empDetails['category_name'] = $this->getCategoryName($employee['category_id']);
				$empDetails['employee_id']   = $employee['id'];
				$empDetails['first_name']    = $employee['first_name'];
				$empDetails['last_name']     = $employee['last_name'];
				$empDetails['about']         = $employee['about'];
                $empDetails['phone']         = $employee['phone'];
                $empDetails['custom']        = $employee['custom'];
                if($employee['custom'] == 'yes') {
                    if($employee['type'] == 'other') { 
                        $empDetails['type'] = $employee['type'];
                    } else {
                        $empDetails['type'] = "room_employee";
                    }
                } else {
                    $empDetails['type'] = 'employee_booking';
                }
				$empDetails['email']      = $employee['email'];
                $empDetails['visibility'] = $employee['user']['visibility'];
				$empDetails['rating']     = $this->employeeRating($employee['id']);
				if($empDetails['type'] == 'other') {
					$img = $this->galleryImages($employee['id'], $employee['type']);
					$empDetails['imageList']  = !empty($img) ? $img : $this->noImage;
				} else {
					$empDetails['image']      = !empty($employee['profile_pic']) ? Router::url('/','true')."webroot/images/employees/".$employee['id']."/".$employee['profile_pic'] : $this->noImage;
				}

                if($employee['company']['type'] == 'individual') {
                    $images = $this->galleryImages($employee['company']['id']);
                    $empDetails['image'] = !empty($images) ? $images[0]['image'] : $this->noImage;
                }

                $empDetails['flag'] = 'yes';
				
				//Current logged in user
				$user_id =  $this->Auth->identify()['id'];
				$cusDetails = $this->customerObj->findByUserId($user_id)->select('id')->enableHydration(false)->first(); 
			   
				$res = $this->checkSelectedSlot($params['employee_id'], $cusDetails['id'], date('Y-m-d'));
				
                $days = date('l');
				$schedule = $this->scheduleObj->find('all')->where(['employee_id' => $params['employee_id'], 'days'=>$days])->contain(['Slots'=>['fields'=>['Slots.id','Slots.start_time','Slots.end_time','Slots.schedule_id'], 'sort' => ['Slots.start_time' => 'ASC']]])->enableHydration(false)->first();
				
				$scheduleDetails = [];
				if(!empty($schedule)) {						
					foreach ($schedule['slots'] as $key1 => $value1) {
						if(!in_array($value1['id'], $res)) {
							if((trim($schedule['days']) == $days) && (strtotime(date('H:i')) < strtotime(date('H:i',strtotime($value1['start_time']))))) {
								$scheduleDetails['slots'][$key1]['slot_id'] = $value1['id'];
								$scheduleDetails['slots'][$key1]['start_time'] =  date('H:i',strtotime($value1['start_time']));
								$scheduleDetails['slots'][$key1]['end_time'] = date('H:i',strtotime($value1['end_time']));
								$scheduleDetails['slots'][$key1]['status'] = $this->checkSlotStatus($value1['id'], date('Y-m-d'));
							}
						}
					}
					
					if($days != trim($schedule['days'])) {
						$scheduleDetails = array();
						$empDetails['flag'] = 'yes';
					} elseif($days == trim($schedule['days'])) {
						$scheduleDetails['schedule_id'] = $schedule['id'];
						$scheduleDetails['employee_id'] = $schedule['employee_id'];
						$scheduleDetails['days'] = trim($schedule['days']);
						$scheduleDetails['slots'] = !empty($scheduleDetails['slots']) ? array_values($scheduleDetails['slots']) : '';
						$empDetails['flag'] = 'yes';
					}
				} 
				$empDetails['schedule'] = !empty($scheduleDetails) ? array($scheduleDetails) : $scheduleDetails;
				$result = array('status'=>'success', 'message'=>'Schedules List','data'=>$empDetails);						
			} else {
				$result = array('status'=>'failure', 'message'=>'No Record Found','data'=>$empDetails);
			}
			
			$this->set([
                'data' =>  $result['data'],
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message','data']
            ]);
		}
	}
	
    public function checkSlotStatus($slotId, $date) 
    {
		$checkStatus = $this->bookingObj->find('all')->where(['slot_id'=>$slotId, 'date'=>$date])->select(['status'])->enableHydration(false)->first();
		if(!empty($checkStatus)){
			if($checkStatus['status'] == 'accepted'){
				return "accepted";
			} elseif($checkStatus['status'] == 'completed'){
				return "completed";
			} else {
				return "available";
			}
		} else {
			return "available";
		}
    }
	
	/**
     * API: Send Book Request
     * PARAMS: customer_id, employee_id, slot_id, date('2018-11-05'), profile_type(employee_room/other/employee_booking)
     */
    public function sendBookingRequest()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            Log::notice($params);
            if(empty($params['profile_type'])) {
                    $params['profile_type'] = 'employee_booking';
            }

            if($params['profile_type'] == 'employee_booking') {
                 $params['profile_type'] = 'room_employee';
            }
         
            $cusDetails = $this->customerObj->findByUserId($this->Auth->identify()['id'])->first();
            $params['customer_id'] = $cusDetails['id'];
            $params['name'] = $cusDetails['name'];

            if($params['profile_type'] == 'company') {
                $checkCart = $this->checkRestCart($params['customer_id'],$params['slot_id'],$params['date'],$params['table_id']);
            }
            $params['profile_type'] = 'company';
            $booking = $this->bookingObj->newEntity(); 
            $booking = $this->bookingObj->patchEntity($booking, $params);
            
            if ($this->bookingObj->save($booking)) 
            {
                $msg = "Above information added to your cart.";
                if($params['profile_type'] != 'company'){
                $msg = "Added to your cart.";
                $params['profile_type'] = 'company';
                $employee = $this->employeeObj->findById($params['employee_id'])->contain(['Users'=>['fields'=>['Users.id', 'Users.device_type', 'Users.device_token','Users.noti_alert']]])->select(['id', 'user_id', 'first_name', 'last_name'])->enableHydration(false)->first();
                $name = '';
                if(!empty($employee['first_name'])) {
                    $name = $employee['first_name'].' '.$employee['last_name'];
                }

                $device_type = $employee['user']['device_type'];
                $device_token = $employee['user']['device_token'];
                //Send push notification
                // if(!empty($device_token) && !empty($device_type)) {
                //     //Save notification
                //     $data = [];
                //     $data['from_user_id']  = $this->Auth->identify()['id'];
                //     $data['to_user_id']    = $employee['user_id'];
                //     $data['message']       = 'Booking Request for '.$name;
                //     $data['type']          = $params['profile_type'];
                //     $data['to_profile_id'] = $employee['id'];
                //     $this->saveNotification($data);

                //     //Get unread notification count(badge_count) and update in users table
                //     $badgeCount = $this->getBadgeCount($employee['user_id']);
                    
                //     //Send notification 
                //     if($employee['user']['noti_alert'] == "on") { 
                //         $this->sendPushNotification($device_type, $device_token, $params['name'].": ".'Booking Request', $data['type'], $badgeCount, $params['name']);
                //     }
                // }
            }
                $result = array('status'=>'success', 'message'=>$msg, 'data'=>array('id'=>$booking->id));
                        
            } else {
                $errors = $booking->errors();
                $erorMessage = array(); 
                $i = 0; 
                $keys = array_keys($errors); 
                foreach ($errors as $errors) { 
                    $key = key($errors); 
                    foreach($errors as $error){ 
                        $erorMessage = ucfirst($keys[$i]) . " :- " . $error;
                        //$erorMessage = $error;
                    }
                    $i++;
                }
                $result = array('status'=>'error', 'message'=>$erorMessage, 'data'=>array()); 
            }
            
            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'data' =>  $result['data'],
                '_serialize' => ['status','message','data']
            ]);
        }
    }

    public function checkRestCart($cust_id,$slot_id, $date, $table_id) {
            $checkRest = $this->bookingObj->find('all')->where(['customer_id'=>$cust_id,'slot_id'=>$slot_id, 'date'=>$date, 'table_id' =>$table_id])->enableHydration(false)->first();
            if(!empty($checkRest)) {
                $result = array('status'=>'failure', 'message'=>'All the above details already in cart, please check your cart');
                echo json_encode($result); die;
            }
    }
	
	/**
     * API: Ratings
     * PARAMS: customer_id, employee_id, value, booking_id
     */
	public function addRating()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            if(empty($params['employee_id'])) {
                $params['employee_id'] = $params['company_id'];
                $params['type'] = "company";
            }
            
            $res  = $this->ratingObj->find()->where(['customer_id'=>$params['customer_id'], 'employee_id'=>$params['employee_id']])->first();
            if(!empty($res)) {
			   $rating = $this->ratingObj->get($res['id']);
			   $msg = "Rating updated successfully.";
            } else {
				$rating = $this->ratingObj->newEntity();
				$msg = "Rating added successfully.";
            }
            $rating = $this->ratingObj->patchEntity($rating, $params);
            if ($this->ratingObj->save($rating)) {
                //Update rating status in booking table
				if(!empty($params['booking_id'])) {
					$this->bookingObj->updateAll(['rating'=>'yes'], ['id'=>$params['booking_id']]);
				}
				$result = array('status'=>'success', 'message'=>$msg , 'data'=>array('id'=>$rating->id, 'rating'=>$params['value']));
                        
            } else {
                $errors = $rating->errors();
                $erorMessage = array(); 
                $i = 0; 
                $keys = array_keys($errors); 
                foreach ($errors as $errors) { 
                    $key = key($errors); 
                    foreach($errors as $error){ 
                        $erorMessage = ucfirst($keys[$i]) . " :- " . $error;
                        //$erorMessage = $error;
                    }
                    $i++;
                }
                $result = array('status'=>'error', 'message'=>$erorMessage, 'data'=>array()); 
            }
            
            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'data' =>  $result['data'],
                '_serialize' => ['status','message','data']
            ]);
        }
    }
	
    /**
     * API: Send Message
     * PARAMS: customer_id, profile_id, message, type
     */
    public function sendMessage()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            $message = !empty($params['message']) ? $params['message'] : '';

            $sender = $this->customerObj->findByUserId($this->Auth->identify()['id'])->first();
            $params['name'] = $sender['name'];

            if($params['type'] == 'employee') {
                $reciever = $this->employeeObj->find()->where(['Employees.id'=>$params['profile_id']])->select(['Employees.first_name', 'Employees.last_name'])->contain(['Users'=>['fields'=>['Users.id','Users.phone','Users.device_type', 'Users.device_token','Users.noti_alert']]])->select('user_id')->enableHydration(false)->first();
            } else {
                $reciever = $this->companyObj->find()->where(['Companies.id'=>$params['profile_id']])->select(['Companies.first_name', 'Companies.last_name'])->contain(['Users'=>['fields'=>['Users.id','Users.phone','Users.device_type', 'Users.device_token','Users.noti_alert']]])->select(['user_id'])->enableHydration(false)->first();
            }
            $device_type = $reciever['user']['device_type'];
            $device_token = $reciever['user']['device_token'];

            //Send push notification
            if(!empty($device_token) && !empty($device_type)) {
				//Save notification
				$data = [];
				$data['from_user_id'] = $this->Auth->identify()['id'];
				$data['to_user_id']   = $reciever['user_id'];
				$data['message']      = $params['message'];
				$data['type']         = 'send_to_host';
				$this->saveNotification($data);
				//Get unread notification count(badge_count) and update in users table
				$badgeCount = $this->getBadgeCount($reciever['user_id']);
				//Send notification
				if($reciever['user']['noti_alert'] == "on") {
					$this->sendPushNotification($device_type, $device_token, $params['name'].": ".$data['message'],'send_to_host', $badgeCount, $params['name']);
				}
				$result = array('status'=>'success', 'message'=>'Message sent successfully.');
            } else {
                $result = array('status'=>'success', 'message'=>'Message not send.');
            }         
            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message']
            ]);         
        }
    }
	
	/**
     * API: Send Email
     * PARAMS: customer_id, employee_id, message
     */
    public function sendEmail()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
			
			$message = !empty($params['message']) ? $params['message'] : '';
            //Employee details
			$employee = $this->employeeObj->find()->where(['Employees.id'=>$params['employee_id']])->select(['Employees.first_name', 'Employees.last_name', 'Employees.email'])->enableHydration(false)->first();
			
			//Customer details
			$customer = $this->customerObj->find()->where(['id'=>$params['customer_id']])->select(['name'])->enableHydration(false)->first();
            			
			if(!empty($employee) && !empty($employee['email'])) {
				//Email Content Start
				$emailBody  = "<div>";
				$emailBody .= "Dear ".ucfirst($employee['first_name']). " " . ucfirst($employee['last_name']);
				$emailBody .= ",<br><br>";
				$emailBody .= $message;
				$emailBody .= "<br><br>";
				$emailBody .= "Thankyou";
				$emailBody .= "<br>";
				$emailBody .= ucfirst($customer['name']);
				$emailBody .= "</div>";

				$email = $employee['email'];
				$emailSubject = 'Request for booking.';
				
				$this->sendMail($email, $emailSubject, $emailBody);
				$result = array('status'=>'success', 'message'=>"Email sent successfully.");
				//Email End
			} else {
				$result = array('status'=>'failure', 'message'=>"Email not exist.");
			}
			
			$this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                '_serialize' => ['status','message']
            ]);			
        }
    }
	
	/**
     * API: Send Email
     * PARAMS: employee_id
     */
	public function employeeEmail()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
			
			$message = !empty($params['message']) ? $params['message'] : '';
            //Employee details
			$employee = $this->employeeObj->find()->where(['Employees.id'=>$params['employee_id']])->select(['Employees.first_name', 'Employees.last_name', 'Employees.email'])->enableHydration(false)->first();
			            			
			if(!empty($employee) && !empty($employee['email'])) {
				$result = array('status'=>'success', 'message'=>"Employee email.", 'data'=>$employee['email']);
			} else {
				$result = array('status'=>'failure', 'message'=>"Email not exist.", 'data'=>array());
			}
			
			$this->set([
                'status' => $result['status'],
                'message' => $result['message'],
				'data' =>  $result['data'],
                '_serialize' => ['status','message','data']
            ]);			
        }
    }

    /**
    * API: My Booking Single(Guest)
    * PARAMS: booking_id
    */
    public function myBookingSingle()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            // if(empty($params['customer_id'])) {
            //     $cusDetails = $this->customerObj->findByUserId($this->Auth->identify()['id'])->first();
            //     $params['customer_id'] = $cusDetails['id'];
            // }
            //echo  $params['customer_id']; die;
            $itemCond = ['BookingItems.payment_status' => 'yes'];

            //$cartCount = $this->bookingObj->find()->where(['customer_id'=>$params['customer_id'],'pay_more'=>'no','Booking.type'=>'other'])->count();

            $booking = $this->bookingObj->find()->where(['Booking.id'=>$params['booking_id'] ,'Booking.payment_status'=>'yes','Booking.type'=>'other'])->order(['Booking.id'=>'DESC'])->contain(['Slots'=>['Schedules'=>['Companies','Employees.Companies']],'RestaurantArrangement','BookingItems'
            =>function ($q) use ($itemCond) { 
                    return $q->where($itemCond);
                 },'CustomerDeliveryAddress','CustomerPayment'])->enableHydration(false);
            $booking = $booking->first();
            $data = array();
            $dateT = '';
            if(!empty($booking)){
                if(!empty($booking['slot']['schedule_id'])) {
                    $dateTT = $this->scheduleDate($booking['slot']['schedule_id']);
                    $dateT = date('l, jS F, Y', strtotime($dateTT));
                }

                if(empty($booking['date'])) {
                    $booking['date'] = $booking['modified'];
                }
                        
                $data['booking_id'] = $booking['id'];
                $data['payment_status'] = $booking['payment_status'];
                $data['order_type'] = $booking['order_type'];
                if($booking['order_type'] == "deliver") {
                    $data['order_type'] = "delivery";
                    $data['delivery_amount'] = $booking['delivery_amount'];
                }
                if($booking['order_type'] == "pick") {
                    $data['order_type'] = "pick up";
                }

                $data['slot_id'] =  $booking['slot_id'];

                $data['rate_status']   = $booking['rating'];

                    if(!empty($booking['slot_id'])) {
                        $data['booking_amount'] = 20;
                        $data['company_id']   = $booking['slot']['schedule']['company']['id'];
                            //new line
                            //$data['booking'][$keya]['booking_amount'] = $this->getCompanyName($value1['slot']['schedule']['company']['id'])['booking_amount'];
                        $data['booking_amount'] = $booking['booking_amount'];
                        //$data['tax'] = $this->getCompanyName($booking['slot']['schedule']['company']['id'])['tax'];
                        $images  = $this->galleryImages($booking['slot']['schedule']['company']['id']);
                        $data['type']          = "company";
                        $data['image']         = !empty($images) ? $images[0]['image'] : $this->noImage;
                        $data['company_name']  = $booking['slot']['schedule']['company']['company_name'];
                            // $data['booking'][$keya]['first_name']    = $value1['slot']['schedule']['company']['first_name'];
                            // $data['booking'][$keya]['last_name']     = $value1['slot']['schedule']['company']['last_name'];
                        $data['address']       = $booking['slot']['schedule']['company']['address'];
                        $data['city']          = $booking['slot']['schedule']['company']['city'];
                        $data['state']         = $booking['slot']['schedule']['company']['state'];
                        $data['country']       = $booking['slot']['schedule']['company']['country'];
                        $data['zipcode']       = $booking['slot']['schedule']['company']['zipcode'];
                        $data['table_no']      = $booking['restaurant_arrangement']['name'];
                        $data['no_of_guest']   = $booking['no_of_guest'];
                        $data['date']         = !empty($booking['date']) ? $booking['date']->format('l, jS F, Y') : $dateT;
                        $data['slot'][0]['start_time'] = date('H:i',strtotime($booking['slot']['start_time']));
                        $data['slot'][0]['end_time'] = date('H:i',strtotime($booking['slot']['end_time']));
                        if(empty($booking['company_id'])) {
                            $data['company_name'] = $booking['slot']['schedule']['employee']['company']['company_name'];
                            $data['company_id'] = $booking['slot']['schedule']['employee']['company']['id'];
                            $data['employee_id'] = $booking['slot']['schedule']['employee']['id'];
                            if($booking['slot']['schedule']['employee']['type'] == 'employee') {
                                $images = !empty($booking['slot']['schedule']['employee']['profile_pic']) ? Router::url('/','true')."webroot/images/employees/".$booking['slot']['schedule']['employee']['id']."/".$booking['slot']['schedule']['employee']['profile_pic'] : $this->noImage;
                                    $data['image']  =  $images;
                                }
                                // $images  = $this->galleryImages($value1['slot']['schedule']['employee']['company']['id']);
                            $data['type']          = "employee";
                            $data['image']         = $images;
                            //new line
                            $data['booking_amount'] = $this->getCompanyName($booking['slot']['schedule']['employee']['company']['id'])['booking_amount'];
                            $data['first_name']    = $booking['slot']['schedule']['employee']['first_name'];
                            $data['last_name']     = $booking['slot']['schedule']['employee']['last_name'];
                            $data['company_name']  = $booking['slot']['schedule']['employee']['company']['company_name'];
                            $data['address']       = $booking['slot']['schedule']['employee']['company']['address'];
                            $data['city']          = $booking['slot']['schedule']['employee']['company']['city'];
                            $data['state']         = $booking['slot']['schedule']['employee']['company']['state'];
                            $data['country']       = $booking['slot']['schedule']['employee']['company']['country'];
                            $data['zipcode']       = $booking['slot']['schedule']['employee']['company']['zipcode'];
                            }

                        }
                        else {
                            //$data['booking'][$keya]['pick_up']    =  "yes";
                            $data['company_id']    =  $booking['company_id'];
                            $images  = $this->galleryImages($booking['company_id']);
                            $data['image']         = !empty($images) ? $images[0]['image'] : $this->noImage;
                            $companyInfo                             = $this->getCompanyName($booking['company_id']);
                            $data['company_name']  = $companyInfo['company_name'];
                            $data['company_name']  = $companyInfo['company_name'];
                            $data['address']       = $companyInfo['address'];
                            $data['city']          = $companyInfo['city'];
                            $data['state']         = $companyInfo['state'];
                            $data['country']       = $companyInfo['country'];
                            $data['zipcode']       = $companyInfo['zipcode'];
                            // $data['booking'][$keya]['latitude']  =  $companyInfo['latitude'];
                            // $data['booking'][$keya]['longitude']  = $companyInfo['longitude'];
                            // $data['booking'][$keya]['delivery']  = $companyInfo['delivery'];
                            // $data['booking'][$keya]['delivery_radius']  = $companyInfo['delivery_radius'];
                            // $data['booking'][$keya]['delivery_amount']  = $companyInfo['delivery_amount'];
                            //$data['booking'][$keya]['tax']  = $companyInfo['tax'];
                        }
                        if(!empty($booking['booking_items'])) {
                        $totalprice = 0;
                        foreach ($booking['booking_items'] as $keyb => $value2) {
                            $data['booking_items'][$keyb]['booking_item_id']  = $value2['id'];
                            $data['booking_items'][$keyb]['company_item_id']  = $value2['company_item_id'];
                            $data['booking_items'][$keyb]['name']  = $value2['name'];
                            $data['booking_items'][$keyb]['price']  = $value2['price'];
                            $data['booking_items'][$keyb]['size']  = $value2['size'];
                            $data['booking_items'][$keyb]['qty']  = $value2['qty'];
                            $data['booking_items'][$keyb]['extra_instruction']  = $value2['extra_instruction'];
                            $data['booking_items'][$keyb]['final_price']  = $value2['final_price'];
                            $data['booking_items'][$keyb]['company_items_options']  = json_decode($value2['company_items_options']);
                            $totalprice = $totalprice + $value2['final_price']; 
                        }
                        $data['total_payble'] = $totalprice;
                       }
                       else {
                            $data['booking_items'] = [];
                       }
                     $result = array('status'=>'success', 'message'=>'Booking Data.', 'data'=>$data);
            }
            else {
                $result = array('status'=>'failure', 'message'=>"No Booking.", 'data'=>array());
            }

            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'data' =>  $result['data'],
                '_serialize' => ['status','message','cart_badge','data']
            ]);
        }
    }
	
    /**
     * API: My Bookings(Guest)
     * PARAMS: customer_id
     */
    public function myBookings()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            if(empty($params['customer_id'])) {
                $cusDetails = $this->customerObj->findByUserId($this->Auth->identify()['id'])->first();
                $params['customer_id'] = $cusDetails['id'];
            }
            $noImage = Router::url('/','true')."webroot/images".DS."noImage.png";

            $itemCond = ['BookingItems.payment_status' => 'yes'];

            $cartCount = $this->bookingObj->find()->where(['customer_id'=>$params['customer_id'],'pay_more'=>'no','Booking.type'=>'other'])->count();
            
            $booking = $this->bookingObj->find()
									->where(['Booking.customer_id'=>$params['customer_id'],'Booking.payment_status'=>'yes','Booking.type'=>'other'])
									->order(['Booking.id'=>'DESC'])
									->contain([
										'Slots'=>[
											'Schedules'=>[
												'Companies'=>[
													'fields'=>['id', 'user_id', 'category_id', 'company_name', 'address', 'city', 'state', 'country', 'zipcode']
												],
												'Employees.Companies'
											]
										],
										'RestaurantArrangement',
										'BookingItems'=>function ($q) use ($itemCond) { 
													return $q->where($itemCond);
												},
										'CustomerDeliveryAddress',
										'CustomerPayment'=>['fields'=>['id','booking_id', 'amount', 'offline_amount', 'final_payment_status']]
									])
									->enableHydration(false)->toArray();
			//pr($booking);die;
            $data = array();
            $dateT = '';
            foreach ($booking as $keya => $value1) {
				if(!empty($value1['slot']['schedule_id'])) {
					$dateTT = $this->scheduleDate($value1['slot']['schedule_id']);
					$dateT = date('l, jS F, Y', strtotime($dateTT));
				}

				if(empty($value1['date'])) {
					$value1['date'] = $value1['modified'];
				}
					
				$alreadyDateNotExist = true;
				if(strtotime($value1['date']) >= strtotime(date('Y-m-d'))) { 
					$data['booking'][$keya]['booking_id'] = $value1['id'];
					$data['booking'][$keya]['payment_status'] = $value1['payment_status'];
					$data['booking'][$keya]['order_type'] = $value1['order_type'];
					if($value1['order_type'] == "deliver") {
						$data['booking'][$keya]['order_type'] = "delivery";
						$data['booking'][$keya]['delivery_amount'] = $value1['delivery_amount'];
					}
					if($value1['order_type'] == "pick") {
						$data['booking'][$keya]['order_type'] = "pick up";
					}
					$data['booking'][$keya]['slot_id'] = $value1['slot_id'];

					if(strtotime($value1['date']) <= strtotime(date('Y-m-d'))) {
						if(date('H:i:s') > date('H:i:s',strtotime($value1['slot']['end_time']))) {
							$data['booking'][$keya]['booking_type'] = 'history';
						} else {
							$data['booking'][$keya]['booking_type'] = 'current';
						}
					} else {
						$data['booking'][$keya]['booking_type'] = 'current';
					}

					$data['booking'][$keya]['rate_status'] = $value1['rating'];

					if(!empty($value1['slot_id'])) {
						$data['booking'][$keya]['company_id']     = $value1['slot']['schedule']['company']['id'];
						$data['booking'][$keya]['booking_amount'] = $value1['booking_amount'];
						
						$images  = $this->galleryImages($value1['slot']['schedule']['company']['id']);
						
						$data['booking'][$keya]['type']          = "company";
						$data['booking'][$keya]['image']         = !empty($images) ? $images[0]['image'] : $this->noImage;
						$data['booking'][$keya]['company_name']  = $value1['slot']['schedule']['company']['company_name'];
						$data['booking'][$keya]['address']       = $value1['slot']['schedule']['company']['address'];
						$data['booking'][$keya]['city']          = $value1['slot']['schedule']['company']['city'];
						$data['booking'][$keya]['state']         = $value1['slot']['schedule']['company']['state'];
						$data['booking'][$keya]['country']       = $value1['slot']['schedule']['company']['country'];
						$data['booking'][$keya]['zipcode']       = $value1['slot']['schedule']['company']['zipcode'];
						$data['booking'][$keya]['table_no']      = $value1['restaurant_arrangement']['name'];
						$data['booking'][$keya]['no_of_guest']   = $value1['no_of_guest'];
						
						if(!empty($value1['date']) && !empty($value1['order_type']) && $value1['order_type'] == "reservation") { 
							$data['booking'][$keya]['date'] = !empty($value1['date']) ? $value1['date']->format('l, jS F, Y') : $dateT;
							$data['booking'][$keya]['slot'][0]['start_time'] = date('H:i',strtotime($value1['slot']['start_time']));
							$data['booking'][$keya]['slot'][0]['end_time'] = date('H:i',strtotime($value1['slot']['end_time']));
						} else { 
							$data['booking'][$keya]['date'] = $value1['modified']->format('l, jS F, Y');
							$data['booking'][$keya]['time'] = $value1['modified']->format('H:i');
						}
						if(empty($value1['company_id'])) {
							$data['booking'][$keya]['company_name'] = $value1['slot']['schedule']['employee']['company']['company_name'];
							$data['booking'][$keya]['company_id'] = $value1['slot']['schedule']['employee']['company']['id'];
							$data['booking'][$keya]['employee_id'] = $value1['slot']['schedule']['employee']['id'];
							if($value1['slot']['schedule']['employee']['type'] == 'employee') {
								$images = !empty($value1['slot']['schedule']['employee']['profile_pic']) ? Router::url('/','true')."webroot/images/employees/".$value1['slot']['schedule']['employee']['id']."/".$value1['slot']['schedule']['employee']['profile_pic'] : $this->noImage;
								$data['booking'][$keya]['image'] = $images;
							}
							$data['booking'][$keya]['type'] = "employee";
							$data['booking'][$keya]['image'] = $images;
							//new line
							$data['booking'][$keya]['booking_amount'] = $this->getCompanyName($value1['slot']['schedule']['employee']['company']['id'])['booking_amount'];
							$data['booking'][$keya]['first_name'] = $value1['slot']['schedule']['employee']['first_name'];
							$data['booking'][$keya]['last_name'] = $value1['slot']['schedule']['employee']['last_name'];
							$data['booking'][$keya]['company_name'] = $value1['slot']['schedule']['employee']['company']['company_name'];
							$data['booking'][$keya]['address'] = $value1['slot']['schedule']['employee']['company']['address'];
							$data['booking'][$keya]['city'] = $value1['slot']['schedule']['employee']['company']['city'];
							$data['booking'][$keya]['state'] = $value1['slot']['schedule']['employee']['company']['state'];
							$data['booking'][$keya]['country'] = $value1['slot']['schedule']['employee']['company']['country'];
							$data['booking'][$keya]['zipcode'] = $value1['slot']['schedule']['employee']['company']['zipcode'];
						}
					} else {
						$data['booking'][$keya]['company_id'] = $value1['company_id'];
						$images  = $this->galleryImages($value1['company_id']);
						$data['booking'][$keya]['image'] = !empty($images) ? $images[0]['image'] : $this->noImage;
						
						$companyInfo = $this->getCompanyName($value1['company_id']);
						
						$data['booking'][$keya]['company_name'] = $companyInfo['company_name'];
						$data['booking'][$keya]['address'] = $companyInfo['address'];
						$data['booking'][$keya]['city'] = $companyInfo['city'];
						$data['booking'][$keya]['state'] = $companyInfo['state'];
						$data['booking'][$keya]['country'] = $companyInfo['country'];
						$data['booking'][$keya]['zipcode'] = $companyInfo['zipcode'];
						$data['booking'][$keya]['date'] = $value1['modified']->format('l, jS F, Y');
						$data['booking'][$keya]['time'] = $value1['modified']->format('H:i');
					}
					//Booking item
					if(!empty($value1['booking_items'])) {
						$totalprice = 0;
						foreach ($value1['booking_items'] as $keyb => $value2) {
							$data['booking'][$keya]['booking_items'][$keyb]['booking_item_id'] = $value2['id'];
							$data['booking'][$keya]['booking_items'][$keyb]['company_item_id'] = $value2['company_item_id'];
							$data['booking'][$keya]['booking_items'][$keyb]['name'] = $value2['name'];
							$data['booking'][$keya]['booking_items'][$keyb]['price'] = $value2['price'];
							$data['booking'][$keya]['booking_items'][$keyb]['size'] = $value2['size'];
							$data['booking'][$keya]['booking_items'][$keyb]['qty'] = $value2['qty'];
							$data['booking'][$keya]['booking_items'][$keyb]['extra_instruction']  = $value2['extra_instruction'];
							$data['booking'][$keya]['booking_items'][$keyb]['final_price']  = $value2['final_price'];
							$data['booking'][$keya]['booking_items'][$keyb]['company_items_options']  = json_decode($value2['company_items_options']);
							$totalprice = $totalprice + $value2['final_price']; 
						}
						$data['booking'][$keya]['total_payble'] = $totalprice;
					} else {
						$data['booking'][$keya]['booking_items'] = [];
					}
					$data['booking'] = array_values($data['booking']);
					
					//Customer payment
					if(!empty($value1['customer_payment'])) {
						$pendingPayment = 0;
						foreach ($value1['customer_payment'] as $keyP => $valueP) {
							if($valueP['final_payment_status'] == 'no') {
								$pendingPayment = $pendingPayment + ($valueP['offline_amount'] - $valueP['amount']);
							}
						}
						$data['booking'][$keya]['pending_payment'] = $pendingPayment;
					}
				}
            }
            
            if(!empty($data)) {
                $result = array('status'=>'success', 'message'=>'Booking List.', 'cart_badge'=>$cartCount, 'data'=>$data);
            } else {
                $result = array('status'=>'failure', 'message'=>"No Booking Yet.",'cart_badge'=>$cartCount, 'data'=>array());
            }

            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'cart_badge' => $result['cart_badge'],
                'data' =>  $result['data'],
                '_serialize' => ['status','message','cart_badge','data']
            ]);
        }
    }

    /**
	 * API: My Booking History(Guest)
	 * PARAMS: customer_id
	 */
	public function myBookingHistory()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            //Log::notice($params);
            if(empty($params['customer_id'])) {
                $cusDetails = $this->customerObj->findByUserId($this->Auth->identify()['id'])->first();
                $params['customer_id'] = $cusDetails['id'];
            }
            $noImage = Router::url('/','true')."webroot/images".DS."noImage.png";
            $itemCond = ['BookingItems.payment_status' => 'yes'];
            $cartCount = $this->bookingObj->find()
										->where(['customer_id'=>$params['customer_id'],'pay_more'=>'no','Booking.type'=>'other'])
										->select(['id'])
										->count();
            
            $booking = $this->bookingObj->find()
										->where(['Booking.customer_id'=>$params['customer_id'],'Booking.payment_status'=>'yes','Booking.type'=>'other'])
										->order(['Booking.id'=>'DESC'])
										->contain([
											'Slots'=>[
												'Schedules'=>[
													'Companies',
													'Employees.Companies'
												]
											],
											'RestaurantArrangement',
											'BookingItems'=>function ($q) use ($itemCond) { 
												return $q->where($itemCond);
											},
											'CustomerDeliveryAddress',
											'CustomerPayment'
										])
											->enableHydration(false);

            $data = array();
            $dateT = '';

            foreach ($booking as $keya => $value1) {
				if(!empty($value1['slot']['schedule_id'])) {
					$dateTT = $this->scheduleDate($value1['slot']['schedule_id']);
					$dateT = date('l, jS F, Y', strtotime($dateTT));
				}

				 if(empty($value1['date'])) {
					$value1['date'] = $value1['modified'];
				 }
					
				$alreadyDateNotExist = true;
				if(strtotime($value1['date']) <= strtotime(date('Y-m-d'))) { 
					$data['booking'][$keya]['booking_id'] = $value1['id'];
					$data['booking'][$keya]['payment_status'] = $value1['payment_status'];
					$data['booking'][$keya]['order_type'] = $value1['order_type'];
					if($value1['order_type'] == "deliver") {
						$data['booking'][$keya]['delivery_amount'] = $value1['delivery_amount'];
					}
					
					if($value1['order_type'] == "pick") {
						$data['booking'][$keya]['order_type'] = "pick up";
					}
					
					$data['booking'][$keya]['slot_id'] =  $value1['slot_id'];

					if(strtotime($value1['date']) >= strtotime(date('Y-m-d'))) {
						if(date('H:i:s') > date('H:i:s',strtotime($value1['slot']['end_time']))) {
							$data['booking'][$keya]['booking_type'] = 'history';
						} else {
							$data['booking'][$keya]['booking_type'] = 'current';
						}
					} else {
						$data['booking'][$keya]['booking_type'] = 'history';
					}

					$data['booking'][$keya]['rate_status']   = $value1['rating'];

					if(!empty($value1['slot_id'])) {
						$data['booking'][$keya]['company_id']   = $value1['slot']['schedule']['company']['id'];
						$data['booking'][$keya]['booking_amount'] = $value1['booking_amount'];
						$images  = $this->galleryImages($value1['slot']['schedule']['company']['id']);
						$data['booking'][$keya]['type']          = "company";
						$data['booking'][$keya]['image']         = !empty($images) ? $images[0]['image'] : $noImage;
						$data['booking'][$keya]['company_name']  = $value1['slot']['schedule']['company']['company_name'];
						$data['booking'][$keya]['address']       = $value1['slot']['schedule']['company']['address'];
						$data['booking'][$keya]['city']          = $value1['slot']['schedule']['company']['city'];
						$data['booking'][$keya]['state']         = $value1['slot']['schedule']['company']['state'];
						$data['booking'][$keya]['country']       = $value1['slot']['schedule']['company']['country'];
						$data['booking'][$keya]['zipcode']       = $value1['slot']['schedule']['company']['zipcode'];
						$data['booking'][$keya]['table_no']      = $value1['restaurant_arrangement']['name'];
						$data['booking'][$keya]['no_of_guest']   = $value1['no_of_guest'];
						
						$data['booking'][$keya]['date'] = !empty($value1['date']) ? $value1['date']->format('l, jS F, Y') : $dateT;
						$data['booking'][$keya]['slot'][0]['start_time'] = date('H:i',strtotime($value1['slot']['start_time']));
						$data['booking'][$keya]['slot'][0]['end_time'] = date('H:i',strtotime($value1['slot']['end_time']));
					
						if(empty($value1['company_id'])) {
							$data['booking'][$keya]['company_name'] = $value1['slot']['schedule']['employee']['company']['company_name'];
							$data['booking'][$keya]['company_id'] = $value1['slot']['schedule']['employee']['company']['id'];
							$data['booking'][$keya]['employee_id'] = $value1['slot']['schedule']['employee']['id'];
							if($value1['slot']['schedule']['employee']['type'] == 'employee') {
								$images = !empty($value1['slot']['schedule']['employee']['profile_pic']) ? Router::url('/','true')."webroot/images/employees/".$value1['slot']['schedule']['employee']['id']."/".$value1['slot']['schedule']['employee']['profile_pic'] : $noImage;
								$data['booking'][$keya]['image']  =  $images;
							}
							
							$data['booking'][$keya]['type']          = "employee";
							$data['booking'][$keya]['image']         = $images;
							//new line
							$data['booking'][$keya]['booking_amount'] = $this->getCompanyName($value1['slot']['schedule']['employee']['company']['id'])['booking_amount'];
							$data['booking'][$keya]['first_name']    = $value1['slot']['schedule']['employee']['first_name'];
							$data['booking'][$keya]['last_name']     = $value1['slot']['schedule']['employee']['last_name'];
							$data['booking'][$keya]['company_name']  = $value1['slot']['schedule']['employee']['company']['company_name'];
							$data['booking'][$keya]['address']       = $value1['slot']['schedule']['employee']['company']['address'];
							$data['booking'][$keya]['city']          = $value1['slot']['schedule']['employee']['company']['city'];
							$data['booking'][$keya]['state']         = $value1['slot']['schedule']['employee']['company']['state'];
							$data['booking'][$keya]['country']       = $value1['slot']['schedule']['employee']['company']['country'];
							$data['booking'][$keya]['zipcode']       = $value1['slot']['schedule']['employee']['company']['zipcode'];
						}

					}
					else {
						$data['booking'][$keya]['company_id']    =  $value1['company_id'];
						$images  = $this->galleryImages($value1['company_id']);
						$data['booking'][$keya]['image']         = !empty($images) ? $images[0]['image'] : $noImage;
						$companyInfo                             = $this->getCompanyName($value1['company_id']);
						$data['booking'][$keya]['company_name']  = $companyInfo['company_name'];
						$data['booking'][$keya]['company_name']  = $companyInfo['company_name'];
						$data['booking'][$keya]['address']       = $companyInfo['address'];
						$data['booking'][$keya]['city']          = $companyInfo['city'];
						$data['booking'][$keya]['state']         = $companyInfo['state'];
						$data['booking'][$keya]['country']       = $companyInfo['country'];
						$data['booking'][$keya]['zipcode']       = $companyInfo['zipcode'];
						$data['booking'][$keya]['date']  = $value1['modified']->format('l, jS F, Y');
						$data['booking'][$keya]['time']  = $value1['modified']->format('H:i');
					}
					
					if(!empty($value1['booking_items'])) {
						$totalprice = 0;
						foreach ($value1['booking_items'] as $keyb => $value2) {
							$data['booking'][$keya]['booking_items'][$keyb]['booking_item_id']  = $value2['id'];
							$data['booking'][$keya]['booking_items'][$keyb]['company_item_id']  = $value2['company_item_id'];
							$data['booking'][$keya]['booking_items'][$keyb]['name']  = $value2['name'];
							$data['booking'][$keya]['booking_items'][$keyb]['price']  = $value2['price'];
							$data['booking'][$keya]['booking_items'][$keyb]['size']  = $value2['size'];
							$data['booking'][$keya]['booking_items'][$keyb]['qty']  = $value2['qty'];
							$data['booking'][$keya]['booking_items'][$keyb]['extra_instruction']  = $value2['extra_instruction'];
							$data['booking'][$keya]['booking_items'][$keyb]['final_price']  = $value2['final_price'];
							$data['booking'][$keya]['booking_items'][$keyb]['company_items_options']  = json_decode($value2['company_items_options']);
							$totalprice = $totalprice + $value2['final_price']; 
						}
						$data['booking'][$keya]['total_payble'] = $totalprice;
					} else {
						$data['booking'][$keya]['booking_items'] = [];
					}
					$data['booking'][$keya]['rating'] = $this->businessRating($data['booking'][$keya]['company_id']);
					$data['booking'][$keya]['rateUserCount'] = $this->countRating($data['booking'][$keya]['company_id']);
					$data['booking'] = array_values($data['booking']);
				}
            }
            
            if(!empty($data)) {
                $result = array('status'=>'success', 'message'=>'Booking History.', 'cart_badge'=>$cartCount, 'data'=>$data);
            } else {
                $result = array('status'=>'failure', 'message'=>"No Booking History.",'cart_badge'=>$cartCount, 'data'=>array());
            }

            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'cart_badge' => $result['cart_badge'],
                'data' =>  $result['data'],
                '_serialize' => ['status','message','cart_badge','data']
            ]);
        }
    }

    public function getCompanyName($comp_id) 
    {
        if(!empty($comp_id)){
            $result =  $this->companyObj->find()->where(['id'=>$comp_id])->enableHydration(false)->first();
            return $result;
        }
    }
	
	public function myBookingsdemo()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
            if(empty($params['customer_id'])) {
                $cusDetails = $this->customerObj->findByUserId($this->Auth->identify()['id'])->first();
                $params['customer_id'] = $cusDetails['id'];
            }

            $booking = $this->customerObj->find()->where(['id'=>$params['customer_id']])
            ->contain(['Booking'=>['Slots'=>['Schedules'=>['Employees'=>['Companies','Categories']]]]])
            ->enableHydration(false);

            $data = array();
			$dateT = '';
            foreach ($booking  as $key => $value) {
				foreach ($value['booking'] as $keya => $value1) {
					if(!empty($value1['slot']['schedule_id'])) {
						$dateTT = $this->scheduleDate($value1['slot']['schedule_id']);
						$dateT = date('l, jS F, Y', strtotime($dateTT));
					}
					$alreadyDateNotExist = true;

					if($alreadyDateNotExist) {
						if($value1['slot']['schedule']['employee']['type'] == 'employee') {
							$empImage = !empty($value1['slot']['schedule']['employee']['profile_pic']) ? Router::url('/','true')."webroot/images/employees/".$value1['slot']['schedule']['employee']['id']."/".$value1['slot']['schedule']['employee']['profile_pic'] : $this->noImage;
						} else {
							$img = $this->galleryImages($value1['slot']['schedule']['employee']['id'], $value1['slot']['schedule']['employee']['type']);
							$empImage  = !empty($img) ? $img[0]['image'] : $this->noImage;
						}
														
						if($value1['slot']['schedule']['employee']['company']['type'] == 'individual') {
							$images = $this->galleryImages($value1['slot']['schedule']['employee']['company']['id']);
							$empImage = !empty($images) ? $images[0]['image'] : $this->noImage;
						}
						
						//User can give star rating if status is completed
						if(strtotime($value1['created']) < strtotime(date('Y-m-d'))) {
							$data['booking'][$keya]['booking_type'] = 'history';
						} else {
							$data['booking'][$keya]['booking_type'] = 'current';
						}
						$data['booking'][$keya]['status'] = $value1['status'];
						
						$data['booking'][$keya]['booking_id']    = $value1['id'];
						$data['booking'][$keya]['category_name'] = $value1['slot']['schedule']['employee']['category']['name'];
						$data['booking'][$keya]['employee_id']   = $value1['slot']['schedule']['employee']['id'];
						$data['booking'][$keya]['image']         = $empImage;
						$data['booking'][$keya]['first_name']    = $value1['slot']['schedule']['employee']['first_name'];
						$data['booking'][$keya]['last_name']     = $value1['slot']['schedule']['employee']['last_name'];
						$data['booking'][$keya]['rating']        = $this->employeeRating($value1['slot']['schedule']['employee']['id']);
						$data['booking'][$keya]['rate_status']   = $value1['rating'];
						$data['booking'][$keya]['address']       = $value1['slot']['schedule']['employee']['company']['address'];
						$data['booking'][$keya]['city']          = $value1['slot']['schedule']['employee']['company']['city'];
						$data['booking'][$keya]['state']         = $value1['slot']['schedule']['employee']['company']['state'];
						$data['booking'][$keya]['country']       = $value1['slot']['schedule']['employee']['company']['country'];
						$data['booking'][$keya]['zipcode']       = $value1['slot']['schedule']['employee']['company']['zipcode'];
						$data['booking'][$keya]['date']          = !empty($value1['date']) ? $value1['date']->format('l, jS F, Y') : $dateT;
						$data['booking'][$keya]['slot'][$key]['start_time'] = date('H:i',strtotime($value1['slot']['start_time']));
						$data['booking'][$keya]['slot'][$key]['end_time'] = date('H:i',strtotime($value1['slot']['end_time']));
						$data['booking'][$keya]['slot'][$key]['status'] = $value1['status'];
						
						if($value1['status'] == 'completed') {
							$data['booking'][$keya]['status'] = 'completed';
						}
						
						$data['booking'][$keya]['slot'] = array_values($data['booking'][$keya]['slot']);
						
						if(!empty($data['booking'])) {
							foreach ($data['booking'] as $dKey => $dValue) {
							
								$from = $dValue["date"];
								$to = !empty($value1['date']) ? strtotime($value1['date']->format('l, jS F, Y')) : $dateT;
								if($from == $to && $dValue["employee_id"] == $value1['slot']['schedule']['employee']['id']) {

									$data['booking'][$dKey]['slot'][$keya]['start_time'] = date('H:i',strtotime($value1['slot']['start_time']));
									$data['booking'][$dKey]['slot'][$keya]['end_time'] = date('H:i',strtotime($value1['slot']['end_time']));
									$data['booking'][$dKey]['slot'][$keya]['status'] = $value1['status'];
								   
									$alreadyDateNotExist = false; 
									continue;
								}
							}
						}
					}
					$data['booking'] = array_values($data['booking']);					
				}
            }
			
            if(!empty($data)) {
                $result = array('status'=>'success', 'message'=>'Schedules List','data'=>$data);
            } else {
                $result = array('status'=>'failure', 'message'=>"No data found.", 'data'=>array());
            }

            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'data' =>  $result['data'],
                '_serialize' => ['status','message','data']
            ]);
        }
    }
	
	public function myBookingsdemo_old()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;
			
			$latitude = $params['latitude'];
            $longitude = $params['longitude'];
			
            if(empty($params['customer_id'])) {
                $cusDetails = $this->customerObj->findByUserId($this->Auth->identify()['id'])->first();
                $params['customer_id'] = $cusDetails['id'];
            }
			
			$distanceField = '(3959 * acos (cos ( radians('.$latitude.') )
            * cos( radians( latitude ) )
            * cos( radians( longitude )
            - radians('.$longitude.') )
            + sin ( radians('.$latitude.') )
            * sin( radians( latitude ) )))';
			
            $booking = $this->customerObj->find()->where(['id'=>$params['customer_id']])
            ->contain(['Booking'=>['Slots'=>['Schedules'=>['Employees'=>['Companies'=>['fields'=>['id', 'type', 'address', 'city', 'state', 'country', 'zipcode', 'latitude', 'longitude', 'distance'=>$distanceField]],'Categories']]]]])
			//->bind(':latitude', $latitude, 'float')
            //->bind(':longitude', $longitude, 'float')
            ->enableHydration(false);

            $data = array();
			$dateT = '';
            foreach ($booking  as $key => $value) {

				foreach ($value['booking'] as $keya => $value1) {
					if(!empty($value1['slot']['schedule_id'])) {
						$dateTT = $this->scheduleDate($value1['slot']['schedule_id']);
						$dateT = date('l, jS F, Y', strtotime($dateTT));
					}

					$alreadyDateNotExist = true;
					if(!empty($data)) {

						foreach ($data['booking'] as $dKey => $dValue) {
							
							$from = !empty($dValue["date"]) ? strtotime($dValue["date"]) : strtotime($dateTT);
							$to = !empty($value1['date']) ? strtotime($value1['date']->format('l, jS F, Y')) : $dateT;
							if($from == $to && $dValue["employee_id"] == $value1['slot']['schedule']['employee']['id']) {

								$data['booking'][$dKey]['slot'][$keya]['start_time'] = date('H:i',strtotime($value1['slot']['start_time']));
								$data['booking'][$dKey]['slot'][$keya]['end_time'] = date('H:i',strtotime($value1['slot']['end_time']));
								$data['booking'][$dKey]['slot'][$keya]['status'] = $value1['status'];
							   
								$alreadyDateNotExist = false; 
								continue;
							}
						}
						$data['booking'][$dKey]['slot'] = array_values($data['booking'][$dKey]['slot']);
					}

					if($alreadyDateNotExist) {
						if($value1['slot']['schedule']['employee']['type'] == 'employee') {
							$empImage = !empty($value1['slot']['schedule']['employee']['profile_pic']) ? Router::url('/','true')."webroot/images/employees/".$value1['slot']['schedule']['employee']['id']."/".$value1['slot']['schedule']['employee']['profile_pic'] : $this->noImage;
						} else {
							$img = $this->galleryImages($value1['slot']['schedule']['employee']['id'], $value1['slot']['schedule']['employee']['type']);
							$empImage  = !empty($img) ? $img[0]['image'] : $this->noImage;
						}
														
						if($value1['slot']['schedule']['employee']['company']['type'] == 'individual') {
							$images = $this->galleryImages($value1['slot']['schedule']['employee']['company']['id']);
							$empImage = !empty($images) ? $images[0]['image'] : $this->noImage;
						}
						
						//User can give star rating if status is accepted
						if(strtotime($value1['created']) < strtotime(date('Y-m-d'))) {
							$data['booking'][$keya]['booking_type'] = 'history';
						} else {
							$data['booking'][$keya]['booking_type'] = 'current';
						}
						$data['booking'][$keya]['status'] = $value1['status'];
						
						$data['booking'][$keya]['category_name'] = $value1['slot']['schedule']['employee']['category']['name'];
						$data['booking'][$keya]['employee_id']   = $value1['slot']['schedule']['employee']['id'];
						$data['booking'][$keya]['image']         = $empImage;
						$data['booking'][$keya]['first_name']    = $value1['slot']['schedule']['employee']['first_name'];
						$data['booking'][$keya]['last_name']     = $value1['slot']['schedule']['employee']['last_name'];
						$data['booking'][$keya]['address']       = $value1['slot']['schedule']['employee']['company']['address'];
						$data['booking'][$keya]['city']          = $value1['slot']['schedule']['employee']['company']['city'];
						$data['booking'][$keya]['state']         = $value1['slot']['schedule']['employee']['company']['state'];
						$data['booking'][$keya]['country']       = $value1['slot']['schedule']['employee']['company']['country'];
						$data['booking'][$keya]['zipcode']       = $value1['slot']['schedule']['employee']['company']['zipcode'];
						$data['booking'][$keya]['date']          = !empty($value1['date']) ? $value1['date']->format('l, jS F, Y') : $dateT;
						$data['booking'][$keya]['distance2']     = $value1['slot']['schedule']['employee']['company']['distance'];
														
						$data['booking'][$keya]['slot'][$key]['start_time'] = date('H:i',strtotime($value1['slot']['start_time']));
						$data['booking'][$keya]['slot'][$key]['end_time'] = date('H:i',strtotime($value1['slot']['end_time']));
						$data['booking'][$keya]['slot'][$key]['status'] = $value1['status'];
						
						if($value1['status'] == 'completed') {
							$data['booking'][$keya]['status'] = 'completed';
						}
						
						$data['booking'][$keya]['slot'] = array_values($data['booking'][$keya]['slot']);
					}

					$data['booking'] = array_values($data['booking']); 
				}
            }
			
            if(!empty($data)) {
                $result = array('status'=>'success', 'message'=>'Schedules List','data'=>$data);
            } else {
                $result = array('status'=>'failure', 'message'=>"No data found.", 'data'=>array());
            }

            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'data' =>  $result['data'],
                '_serialize' => ['status','message','data']
            ]);
        }
    }
	
	public function distance($lat1, $lon1, $lat2, $lon2, $unit='M') {
		if (($lat1 == $lat2) && ($lon1 == $lon2)) {
			return 0;
		} else {
			$theta = $lon1 - $lon2;
			$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
			$dist = acos($dist);
			$dist = rad2deg($dist);
			$miles = $dist * 60 * 1.1515;
			$unit = strtoupper($unit);

			if ($unit == "K") {
				return ($miles * 1.609344);
			} else if ($unit == "N") {
				return ($miles * 0.8684);
			} else {
				return $miles;
			}
		}
	}
	
	/**
     * API: Save Payment Details
     * PARAMS: company_id, transaction_id, quantity, product_id, auto_renew_product_id, original_transaction_id, purchase_date, original_purchase_date, subscription_expires_date, subscription_expiration_intent, subscription_auto_renew_status, adam_id, app_item_id, bundle_id, version_external_identifier, web_order_line_item_id, is_trial_period, is_in_billing_retry_period, latest_receipt 
     */
    public function savePaymentDetails() 
    {
    	if($this->request->is('post')) {
    		$params = $this->request->data;
			if(!empty($params['company_id']) && !empty($params['transaction_id'])) {
                //Subscription fee
                $subsFee = $this->subsObj->find()->select(['amount'])->enableHydration(false)->first();
                $payment = $this->payDetailObj->newEntity();
                $payment->user_id = $this->Auth->identify()['id'];
                $payment->amount = $subsFee['amount'];
                $payment = $this->payDetailObj->patchEntity($payment, $params);
                
                if($this->payDetailObj->save($payment)) {
                    //Verify payment
                    $data = [];
                    $data['device_type']    = $params['device_type'];
                    $data['transaction_id'] = $params['transaction_id'];
                    $data['receipt-data']   = $params['receipt'];
                    $data['product_id']     = $params['product_id'];
                    if($data['device_type'] == 'android') {
						$data['signature']     = $params['signature'];
                    }
                    //$payResult = $this->verifyPayment($data);
                    $paymentUpdate = $this->payDetailObj->get($payment->id);
                    $paymentUpdate = $this->payDetailObj->patchEntity($paymentUpdate, $data);
                    $data2 = $this->payDetailObj->save($paymentUpdate);
                      //Log::notice($data2);
                    $data1 = $this->Users->updateAll(['payment_status'=>'yes'], ['id'=>$this->Auth->identify()['id']]);
                    //Log::notice($data1);
                    $result = array('status'=>'success', 'message'=>"Payment details saved successfully.");
                }  else {
					$errors = $payment->errors();
					$erorMessage = array(); 
					$i = 0; 
					$keys = array_keys($errors); 
					foreach ($errors as $errors) { 
						$key = key($errors); 
						foreach($errors as $error){ 
							$erorMessage = ucfirst($keys[$i]) . " :- " . $error;
							//$erorMessage = $error;
						}
						$i++;
					}
					$result = array('status'=>'error', 'message'=>$erorMessage); 
                }
            } else {
                $result = array('status'=>'failure', 'message'=>"Something went wrong.");
            }
						
			$this->set([
				'status'     => $result['status'],
				'message'    => $result['message'],
				'_serialize' => ['status', 'message']
			]);
    	}
    }

    /**
     * API: Show/Hide contact details(phone/email)
     * PARAMS: user_id, visibility(yes/no)
     */
    public function changeVisibility()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;

            if(!empty($params['user_id']) && !empty($params['visibility'])) {
                $res = $this->Users->updateAll(['visibility'=>$params['visibility']], ['id'=>$params['user_id']]);
                if($params['visibility'] == 'yes') {
                    $msg = "Email is public.";
                }
                else {
                    $msg = "Email is private.";
                }
                if ($res) {
                    $result = array('status'=>'success', 'message'=>$msg);
                } else {
                    $result = array('status'=>'failure', 'message'=>"status not updated");
                }
            } else {
                $result = array('status'=>'failure', 'message'=>"Something went wrong.");
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }
    }

    /**
     * API: My Booking Details(Guest)
     * PARAMS: customer_id, employee_id
     */
    public function myBookingDetails()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;

            $bookingDetail = $this->bookingObj->find()
                                ->where([
                                    'customer_id'=>$params['customer_id'], 
                                    'Booking.employee_id'=>$params['employee_id']
                                ])
                                ->contain([
                                    'Employees'=>[
                                        'Companies'=>[
                                            'fields'=>['Companies.id', 'Companies.type','Companies.address', 'Companies.city', 'Companies.state', 'Companies.country', 'Companies.zipcode']
                                        ], 
                                        'Categories'=>[
                                            'fields'=>['Categories.name']
                                        ], 
                                        'EmployeeImages'=>[
                                            'fields'=>['EmployeeImages.name']
                                        ], 
                                        'fields'=>['Employees.id', 'Employees.first_name', 'Employees.last_name','Employees.profile_pic', 'Employees.type']
                                    ],
                                    'Slots'=>[
                                        'fields'=>['Slots.id', 'Slots.schedule_id', 'Slots.start_time', 'Slots.end_time']
                                    ]
                                ])
                                ->enableHydration(false)->toArray();
            
            $booking = array();
            if(!empty($bookingDetail)) {
				$dateT = '';
                foreach ($bookingDetail as $key => $value) {
					
					if(!empty($value['slot']['schedule_id'])) {
						$dateT = $this->scheduleDate($value['slot']['schedule_id']);
						$dateT = date('l, jS F, Y', strtotime($dateT));
					}

                    $booking['category_name']  = $value['employee']['category']['name'];
                    $booking['employee_id']  = $value['employee']['id'];
                    $booking['first_name']  = $value['employee']['first_name'];
                    $booking['last_name']  = $value['employee']['last_name'];
                    
					if($value['employee']['type'] == 'employee') {
						$booking['profile_pic'] = !empty($value['employee']['profile_pic']) ? Router::url('/','true')."webroot/images/employees/".$value['employee']['id']."/".$value['employee']['profile_pic'] : $this->noImage;
					} else {
						$img = $this->galleryImages($value['employee']['id'], $value['employee']['type']);
						$booking['profile_pic']  = !empty($img) ? $img[0]['image'] : $this->noImage;
					}
										
                    if($value['employee']['company']['type'] == 'individual') {
                        $images = $this->galleryImages($value['employee']['company']['id']);
                        $booking['profile_pic'] = !empty($images) ? $images[0]['image'] : $this->noImage;
                    }
                    $booking['address']  = $value['employee']['company']['address'];
                    $booking['city']     = $value['employee']['company']['city'];
					$booking['state']    = $value['employee']['company']['state'];
					$booking['country']  = $value['employee']['company']['country'];
                    $booking['zipcode']  = $value['employee']['company']['zipcode'];

                    $booking['slots'][$key]['booking_id']  = $value['id'];
					$booking['slots'][$key]['date']        = !empty($value['date']) ? $value['date']->format('l, jS F, Y') : $dateT;
                    $booking['slots'][$key]['start_time']  = date('H:i',strtotime($value['slot']['start_time']));
                    $booking['slots'][$key]['end_time']    = date('H:i',strtotime($value['slot']['end_time']));
                    $booking['slots'][$key]['status']      = $value['status'];
                }
                
                $result = array('status'=>'success', 'message'=>'Booking List.','data'=>$booking);
            } else {
                $result = array('status'=>'failure', 'message'=>"No data found.", 'data'=>$booking);
            }
            
            $this->set([
                'status' => $result['status'],
                'message' => $result['message'],
                'data' =>  $result['data'],
                '_serialize' => ['status','message','data']
            ]);
        }
    }

    /**
     * API: Cancel Booking
     * PARAMS: employee_id, booking_id
     */
    public function cancelBooking()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;

            if(!empty($params['employee_id']) && !empty($params['booking_id'])) {
                $entity = $this->bookingObj->find()->where(['id'=>$params['booking_id'], 'employee_id'=>$params['employee_id']])->first();
                if(!empty($entity)) {
                    
                    $employee = $this->employeeObj->findById($params['employee_id'])->contain(['Users'=>['fields'=>['Users.id', 'Users.device_type', 'Users.device_token','Users.noti_alert']]])->select(['user_id'])->enableHydration(false)->first();

                    $result = $this->bookingObj->delete($entity);

                    $device_type = $employee['user']['device_type'];
                    $device_token = $employee['user']['device_token'];
                    //Send push notification
                    if(!empty($device_token) && !empty($device_type)) {
                        //Save notification
                        $data = [];
                        $data['from_user_id'] = $this->Auth->identify()['id'];
                        $data['to_user_id']   = $employee['user_id'];
                        $data['message']      = 'Booking cancel';
                        $data['type']         = 'cancel_booking';
                        $this->saveNotification($data);

                        //Get unread notification count(badge_count) and update in users table
                        $badgeCount = $this->getBadgeCount($employee['user_id']);

                        $cusDetails = $this->customerObj->findByUserId($this->Auth->identify()['id'])->first();
						$name = $cusDetails['name'];
				
						//Send notification 
                        if($employee['user']['noti_alert'] == "on") {   
							$this->sendPushNotification($device_type, $device_token, $name.": ".'Booking cancel', 'cancel_booking', $badgeCount,'no');
                        }
                    }

                    if($result) {
                        $result = array('status'=>'success', 'message'=>'Booking cancelled successfully.');
                    } else {
                        $result = array('status'=>'failure', 'message'=>'Booking not cancelled.');
                    }
                } else {
                    $result = array('status'=>'failure', 'message'=>'Incorrect details.');
                }
            } else {
                $result = array('status'=>'failure', 'message'=>'Incorrect details.');
            }
            
            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }
    }
	
	/**
	 * API: Push Notification
	 * PARAMS: device_type(android / ios), device_token, notification
	 */
	public function pushNotification()
    {
        if($this->request->is('post')){
            $params = $this->request->data;
            
            if(!empty($params['device_token']) && !empty($params['notification'])) {
                $params['device_type'] = 'ios';
                
                $res = $this->sendPushNotification($params['device_type'], $params['device_token'], $params['notification'],$params['type'],'no','no',$params['booking_id']);
                if($res) {
                    $result = array('status'=>'success', 'message'=>'Notification sent successfully.');
                } else {
                    $result = array('status'=>'success', 'message'=>'Notification sent successfullyY.');
                }
            } else {
                $result = array('status'=>'failure', 'message'=>'Details incorrect.');
            }
            
            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }
    }

    /**
     * API: Get subscription amount and subscription renew date
     */
    public function subscriptionDetails()
    {
        $userId = $this->Auth->identify()['id'];
        $paymentSatatus = [];
        if(!empty($userId) && $this->Auth->identify()['type'] == 'host') {
            
            //Payment status
            $pay_status = $this->Auth->identify()['payment_status'];
            if(!empty($pay_status)) {
                $paymentSatatus['payment_status'] = $pay_status;
            }
            //Payment details
            $payDetails = $this->payDetailObj->findByUserId($userId)->select(['amount','created','receipt'])->order(['created'=>'DESC'])->enableHydration(false)->first();
            
            //Subscription fee
            $subsFee = $this->subsObj->find()->select(['amount'])->enableHydration(false)->first();
            $paymentSatatus['subscription'] = !empty($payDetails['amount']) ? $payDetails['amount'] : $subsFee['amount'];

            //Get expiration date: first 3 months free then auto renew after every 3 month
            if(!empty($payDetails['created']) && !empty($payDetails['receipt'])) {
                 //$paymentSatatus['payment_status'] = 'yes';
                $paymentSatatus['payment_status'] = $pay_status;
                $paymentSatatus['valid_till'] = date('m/d/Y', strtotime("+3 months", strtotime($payDetails['created'])));
                $result = array('status'=>'success', 'message'=>'Subscription details.', 'data'=>$paymentSatatus);
            } 
            elseif(!empty($payDetails['created']) && empty($payDetails['receipt'])) {
                $createdDate = $this->Auth->identify()['created'];
                $paymentSatatus['valid_till'] = date('m/d/Y', strtotime("+3 months", strtotime($createdDate)));
                $result = array('status'=>'success', 'message'=>'Subscription details.','data'=>$paymentSatatus);
            }
            else {
                $createdDate = $this->Auth->identify()['created'];
                //$paymentSatatus['subscription'] = null;
 	            $paymentSatatus['subscription'] = $paymentSatatus['subscription'];
                $paymentSatatus['payment_status'] = $pay_status;
                $paymentSatatus['valid_till'] = date('m/d/Y', strtotime("+3 months", strtotime($createdDate)));
                $result = array('status'=>'success', 'message'=>'Subscription details.','data'=>$paymentSatatus);
            }
        } else {
            $result = array('status'=>'failure', 'message'=>'Details incorrect.', 'data'=>$paymentSatatus);
        }
        
        $this->set([
            'status'     => $result['status'],
            'message'    => $result['message'],
            'data'       => $result['data'],
            '_serialize' => ['status', 'message', 'data']
        ]); 
    }

    /**
     * Verify payment details
     * Sandbox URL: https://sandbox.itunes.apple.com/verifyReceipt
     * Production URL: https://buy.itunes.apple.com/verifyReceipt
     */
    public function verifyPayment($params)
    {
        if(!empty($params['device_type']) && $params['device_type']=='android') {
          
			$public_key_base64 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzolI5LPxD2+ihv4tlK2P/BWVpzXhDwGn/TCH9N6sXVMDzVZxkBbLm5OOl3p6dvPOthRM28DoHasL1LXOPCuKQO2A0/yEldKLHYCczfZGQDuQNSBr6vJP+74a1VtnEHcNXQVbohNEM7tSW5Mbix1+wSGQ2+FXaCxRtmwaS+6Z9EJmc/6y5PgiPJv+NTJzBZl2sUazE6jlnz2PrkhrPwYTW2uFSbXGFgwCQi9+MNbWa5jwKMgcj14IhQcILOS7aeaK+Va9bmTxcNMRTxOvstw2VXZemnMSQvCdp0lQGLFiftfvNV2glw21dA9G/B6s8ELktJFQkNSlCrfT7QH5MmuJxQIDAQAB";
			
			$publicKey = "-----BEGIN PUBLIC KEY-----\n" . chunk_split($public_key_base64, 64, "\n") . '-----END PUBLIC KEY-----';
			
			$signature = $params['signature'];
			
			$receipt =  $params['receipt-data'];
			
			$key = openssl_get_publickey( $publicKey );
            
            $result = openssl_verify( $receipt, base64_decode( $signature ), $key, OPENSSL_ALGO_SHA1 );

            if($result == 1) {
                $data['receipt'] =  $receipt;
                $data['device_type'] =  $params['device_type'];
                return $data;
            } else {

            }
        } elseif (!empty($params['device_type']) && $params['device_type']=='ios' && !empty($params['receipt-data'])) {

            $fields = array(
                'receipt-data' => $params['receipt-data'],
                'password' => '52277aa723934298a158236da77afc7a',
                'exclude-old-transactions' => false
            );

            $headers = array(
                'Content-Type: application/json'
            );

            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://sandbox.itunes.apple.com/verifyReceipt' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec($ch );
            $response = json_decode($result,true);

            //$receipt['transaction_id'] = $response['latest_receipt_info'][0]['transaction_id'];
            $receipt['original_transaction_id'] = $response['latest_receipt_info'][0]['original_transaction_id'];
            $receipt['expiration_intent'] =  !empty($response['pending_renewal_info'][0]['expiration_intent']) ? $response['pending_renewal_info'][0]['expiration_intent'] : '';
            $receipt['auto_renew_status'] = $response['pending_renewal_info'][0]['auto_renew_status'];
            $receipt['device_type'] = $params['device_type'];
            curl_close( $ch );
            return $receipt;
        }
    }
	
	public function checkGoole()
	{
		$public_key_base64 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzolI5LPxD2+ihv4tlK2P/BWVpzXhDwGn/TCH9N6sXVMDzVZxkBbLm5OOl3p6dvPOthRM28DoHasL1LXOPCuKQO2A0/yEldKLHYCczfZGQDuQNSBr6vJP+74a1VtnEHcNXQVbohNEM7tSW5Mbix1+wSGQ2+FXaCxRtmwaS+6Z9EJmc/6y5PgiPJv+NTJzBZl2sUazE6jlnz2PrkhrPwYTW2uFSbXGFgwCQi9+MNbWa5jwKMgcj14IhQcILOS7aeaK+Va9bmTxcNMRTxOvstw2VXZemnMSQvCdp0lQGLFiftfvNV2glw21dA9G/B6s8ELktJFQkNSlCrfT7QH5MmuJxQIDAQAB";
			
			$publicKey = "-----BEGIN PUBLIC KEY-----\n" . chunk_split($public_key_base64, 64, "\n") . '-----END PUBLIC KEY-----';
			
			$signature ="rnGRTBExxmn0LWPhv18VSf11Z7HC6/TgYSsONdMvFDzLa8afocMZIAgu7NZYnB6wjnp6Rq+2E8A0ns+A2qVbJFj3E+o/5GkCOp2UelT5M7BIhgwK8vnjkInuxkkJrDALIUp/qzF++QEYeZ6Q6RH51aUUYacnOS/zi5vHIsu+LWgvhi+tR7SAsTmyHXDivHfu0XCBdeW1J62a5MFFfSFZYI0KqQFVl5ufeLSKLeToWaiYM26vbBDH6WEZ+L82OihBqlS8q3JLrLgJAQ9BgFgLdD7I0x8ZG6rBFkoFAKneVcIdfqUZz0mSlTnG8j1caoIiMj43He6kXIeqWJwUU7xV/Q==";
			
			$receipt =  "{\"orderId\":\"GPA.3312-5688-8401-53436\",\"packageName\":\"com.kenbar.mynus2\",\"productId\":\"com.mynus2.subscription99\",\"purchaseTime\":1534914306517,\"purchaseState\":0,\"purchaseToken\":\"gefffblagdakjleamfngklli.AO-J1Ow9Q5PncOoRk-oshlRBQ8kVqt3A4uIZuQi6InX7sr4bx2lNzjS-VjOXyMIwkl2G-afrI0fzoVLEADNZP2RWekoxwe4ko1M884JALYhaZsxo44U9DshbKJxbDNQHcCx9_z0yQpxc\",\"autoRenewing\":true}";
			
			$res = $this->isReceiptValid($publicKey, $receipt, $signature);
			
			pr($res);die;
	}

    public function test()
    {
        $val1 =  date('m-d-Y,h:i a');
       echo "paid for reservation at ".$val1; die;
        $val2 =  date('1:00:00');
        $datetime1 = new \DateTime($val1);
        $datetime2 = new \DateTime($val2);
        $days      = $datetime1->diff($datetime2)->format('%a');
        $hours     = ($days*24) + $datetime1->diff($datetime2)->format('%h');
        if($hours >= 1 && $hours < 3) {
           echo $hours; die;
        } 
                    $params = $this->request->data;

        // $company = $this->companyObj->findById(2)->contain(['Users'])->select(['id', 'user_id'])->enableHydration(false)->first();
        if(empty($params['employee_id'])){
            $params['employee_id'] = "";
        }
        $cond = ['Employees.custom' => 'no', 'Employees.id' => $params['employee_id']];
        $company = $this->companyObj->findById(27)->contain(['Users'=>['fields'=>['Users.id', 'Users.device_type', 'Users.device_token','Users.noti_alert']],
            'Employees.Users'=>function ($q) use ($cond) { 
                    return $q->where($cond);
                 }])->select(['id', 'user_id'])->enableHydration(false)->first();

        // $profileDetails = $this->bookingObj->findById($params['booking_id'])->first();
        // $profile_type = $profileDetails['profile_type'];

        $device_type = $company['user']['device_type'];
        $device_token = $company['user']['device_token'];

        if(!empty($company['employees'])){
            $edevice_type = $company['employees'][0]['user']['device_type'];
            $edevice_token = $company['employees'][0]['user']['device_token'];
        }
        if(!empty($device_token) && !empty($device_type)) {
                 //echo "single"; die;
                        //Save notification
                        // $data = [];
                        // $data['from_user_id']  = $this->Auth->identify()['id'];
                        // $data['to_user_id']    = $company['user_id'];
                        // $data['message']       = $params['order_type'];
                        // $data['type']          = $profile_type;
                        // $this->saveNotification($data);

                        // //Get unread notification count(badge_count) and update in users table
                        // $badgeCount = $this->getBadgeCount($company['user_id']);
                        
                        // //Send notification 
                        // if($company['user']['noti_alert'] == "on") { 
                        //     $this->sendPushNotification($device_type, $device_token, $custName.": ".$data['message'], $data['type'], $badgeCount,$custName);
                        // }
        }
        if(!empty($edevice_type) && !empty($edevice_token)) {

                       // $data = [];
                       //  $data['from_user_id']  = $this->Auth->identify()['id'];
                       //  $data['to_user_id']    = $company['employees'][0]['user_id'];
                       //  $data['message']       = $params['order_type'];
                       //  $data['type']          = $profile_type;
                       //  $this->saveNotification($data);

                       //  //Get unread notification count(badge_count) and update in users table
                       //  $badgeCount = $this->getBadgeCount($company['employees'][0]['user_id']);
                        
                       //  //Send notification 
                       //  $this->sendPushNotification($device_type, $device_token, $custName.": ".$data['message'], $data['type'], $badgeCount,$custName);
        }

        PR($company); die;

    }
	
    function isReceiptValid( $publicKey, $receiptdata, $signature ) 
    {
		//global $publicKey;
		
		//if ( $store == "google" ) {
			$key = openssl_get_publickey( $publicKey );
			
			$result = openssl_verify( $receiptdata, base64_decode( $signature ), $key, OPENSSL_ALGO_SHA1 );
		//}


		return $result;
	}
	
    //On tap of app badge count change to 0
    /**
     * API: Change application badge count
     * PARAMS: user_id, type(outer/inner)
     */
    public function updateBadgeCount()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;

            if(!empty($params['user_id']) && !empty($params['type'])) {
                if($params['type'] == 'outer') {
                    $this->Users->updateAll(['badge_count'=>0], ['id'=>$params['user_id']]);
                } else {
                    $this->notificationObj->updateAll(['mark_as'=>'read'], ['to_user_id'=>$params['user_id']]);
                }
               
                $result = array('status'=>'success', 'message'=>'Badge count updated successfully.', 'data'=>array('badge_count'=>0));
            } else {
                $result = array('status'=>'failure', 'message'=>'Badge count not updated.', 'data'=>array());
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                'data'       => $result['data'],
                '_serialize' => ['status', 'message', 'data']
            ]); 
        }
    }

    /**
     * API: Badge Listing
     * PARAMS: user_id
     */
    public function badgeList()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;

            $badgeList = [];
            if(!empty($params['user_id'])) {
                $badges = $this->notificationObj->find()->where(['to_user_id'=>$params['user_id']])->order(['id'=>'DESC'])->enableHydration(false)->toArray();

                if(!empty($badges)) {
                    foreach ($badges as $key => $value) {
                        $badgeList[$key]['notification_id'] = $value['id'];
                        $badgeList[$key]['title']           = $this->getSenderName($value['from_user_id']);
                        $badgeList[$key]['message']         = $value['message'];
                        $badgeList[$key]['date']            = $value['created']->format('m-d-Y');
                        $badgeList[$key]['time']            = $value['created']->format('h:i a');
                        $badgeList[$key]['type']            = $value['type'];
                        $badgeList[$key]['redirect_id']     = $value['redirect_id'];
                        if($value['type'] == "waiting_request" || $value['type'] == "company_waiting" || $value['type'] == "guest_waiting"){
                        $badgeList[$key]['code']           = "#FFCCCC";
                        }
                        elseif($value['type'] == "send_to_host") {
                        $badgeList[$key]['code']           = "#48CCCD";
                        }
                        elseif($value['message'] == "reservation" ) {
                        $badgeList[$key]['code']           = "#FFE87C";
                        }
                        elseif($value['message'] == "pick up" ) {
                        $badgeList[$key]['code']           = "#DEB887";
                        }
                        elseif($value['message'] == "deliver" ) {
                        $badgeList[$key]['code']           = "#FCDFFF";
                        }
                        elseif($value['type'] == "room_employee") {
                        $badgeList[$key]['code']           = "#FFE87C";
                        }
                        else {
                           $badgeList[$key]['code']           = "lightblue"; 
                        }
						$badgeList[$key]['profile_id']      = $value['to_profile_id'];
						//$badgeList[$key]['profile_name']    = $this->getReceiverName($value['to_profile_id']);
                        $badgeList[$key]['status']          = $value['mark_as'];
                    }
                    //Update badge count
                    //$this->notificationObj->updateAll(['mark_as'=>'read'], ['to_user_id'=>$params['user_id']]);
                    //$this->Users->updateAll(['badge_count'=> 0], ['id'=>$params['user_id']]);
                    $result = array('status'=>'success', 'message'=>'Badge list.', 'data'=>$badgeList);
                }
                else {
                    $result = array('status'=>'failure', 'message'=>'No data', 'data'=>$badgeList);
                }
            } else {
                $result = array('status'=>'failure', 'message'=>'No record found.', 'data'=>$badgeList);
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                'data'       => $result['data'],
                '_serialize' => ['status', 'message', 'data']
            ]); 
        }
    }

    /**
     * API: Read Notification on tab
     * PARAMS: user_id, notification_id, status(read/unread)
     */
    public function readNotification()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;

            if(!empty($params['notification_id']) && !empty($params['status'])) {
                $res = null;
                if($params['status'] == 'read') {
                    $res = $this->notificationObj->updateAll(['mark_as'=>$params['status']], ['id'=>$params['notification_id']]);
                }
                $badgeCount = $this->getBadgeCount($params['user_id']);
                if ($res) {
                    $result = array('status'=>'success', 'message'=>"message read");
                } else {
                    $result = array('status'=>'failure', 'message'=>"status not updated");
                }
            } else {
                $result = array('status'=>'failure', 'message'=>"Something went wrong.");
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }
    }

    public function getSenderName($fromId)
    {
        $type = $this->Auth->identify()['type'];
        $checkUsers = $this->Users->findById($fromId)->first();
        if(empty($checkUsers)) {
            return "(Account deleted)";
        }
        if($type == "guest") { 
            //$sender = $this->employeeObj->findByUserId($fromId)->first();
            $sender = $this->companyObj->findByUserId($fromId)->first();
            //$name = $sender['first_name']." ".$sender['last_name'];
            $name = $sender['company_name'];
        } else {
            $sender = $this->customerObj->findByUserId($fromId)->first();
            $name = $sender['name'];
        }
        return $name;
    }
	
	public function getReceiverName($fromId)
    {
        $type = $this->Auth->identify()['type'];
        $checkUsers = $this->employeeObj->findById($fromId)->select(['id', 'first_name', 'last_name', 'type'])->first();
        
		$name = '';
        if($type == "host") { 
            $receiver = $this->employeeObj->findById($fromId)->select(['id', 'first_name', 'last_name', 'type'])->enableHydration(false)->first();
            $name = $receiver['first_name']." ".$receiver['last_name'];
        }
        return $name;
    }

    /**
     * API: Delete Notiication on bell icon API's
     * PARAMS: notification_id
     */
    public function deleteNotification() 
    {  
        if($this->request->is('post')) {
            $params = $this->request->data;
            $notioficationId = $params['notification_id'];
            
            $entity = $this->notificationObj->get($notioficationId);
            $result = $this->notificationObj->delete($entity);
            $badgeCount = $this->getBadgeCount($this->Auth->identify()['id']);
            if($result) {
                $result = array('status'=>'success', 'message'=>'message deleted successfully.', 'badge_count'=>$badgeCount);
            } else {
                $result = array('status'=>'failure', 'message'=>'Something went wrong', 'badge_count'=>$badgeCount);
            }
            $this->set([
				'status'  => $result['status'],
				'message' => $result['message'],
				'badge_count'    => $result['badge_count'],
				'_serialize' => ['status', 'message', 'badge_count']
            ]);
        }
    }
	
	/**
     * API: Get Badge Counts
     * PARAMS: user_id
     */
    public function getBadgeCounts()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;

            $badgeCount = 0;
            if(!empty($params['user_id'])) {
                $badgeCount = $this->getBadgeCount($params['user_id']);
                
                $result = array('status'=>'success', 'message'=>'Badge counts.', 'count'=>$badgeCount);
            } else {
                $result = array('status'=>'failure', 'message'=>'No records.', 'count'=>$badgeCount);
            }
			Log::notice($result);
            $this->set([
                'status'      => $result['status'],
                'message'     => $result['message'],
                'badge_count' => $result['count'],
                '_serialize'  => ['status', 'message', 'badge_count']
            ]); 
        }
    }

    /**
     * API: on/off Nofification
     * PARAMS: user_id, noti_alert(on/off)
     */
    public function notiAlert()
    {
        if($this->request->is('post')) {
            $params = $this->request->data;

            if(!empty($params['user_id']) && !empty($params['noti_alert'])) {
                $res = $this->Users->updateAll(['noti_alert'=>$params['noti_alert']], ['id'=>$params['user_id']]);
                if ($res) {
                    $result = array('status'=>'success', 'message'=>"Notification turn ".$params['noti_alert']);
                } else {
                    $result = array('status'=>'failure', 'message'=>"notification alert not updated");
                }
            } else {
                $result = array('status'=>'failure', 'message'=>"Something went wrong.");
            }

            $this->set([
                'status'     => $result['status'],
                'message'    => $result['message'],
                '_serialize' => ['status', 'message']
            ]);
        }
    }

	//Cronjob: Send message to businesses to inform subscription expired and disable account
	public function subscription_message()
	{
		$payDetails = $this->payDetailObj->find('all')->contain(['Users'=>['fields'=>['Users.id', 'Users.created']], 'Companies'=>['fields'=>['Companies.id', 'Companies.user_id', 'Companies.company_name', 'Companies.email']]])->select(['id'=>'MAX(PaymentDetails.id)', 'user_id', 'company_id', 'amount', 'transaction_id',  'created'])->group(['PaymentDetails.company_id'])->order(['PaymentDetails.created'=>'DESC'])->enableHydration(false)->toArray();

		foreach($payDetails as $result) {

			if(!empty($result['created']) && !empty($result['receipt'])) {
				//$result['payment_status'] = 'yes';
				$valid_till = date('Y-m-d H:i:s', strtotime("+3 months", strtotime($result['created'])));
				
			} elseif(!empty($result['created']) && empty($result['receipt'])) {

				$valid_till = date('Y-m-d H:i:s', strtotime("+3 months", strtotime($result['user']['created'])));
				
			} else {
				//$result['payment_status'] = 'free';
				$valid_till = date('Y-m-d H:i:s', strtotime("+3 months", strtotime($result['user']['created'])));
			}
			
			//Send email conditions
			$val1      = $valid_till;
			$val2      = date('Y-m-d H:i:s');
			$datetime1 = new \DateTime($val1);
			$datetime2 = new \DateTime($val2);
			$days      = $datetime1->diff($datetime2)->format('%a');
			
			if(!empty($valid_till) && (strtotime($val2) < strtotime($val1)) && $days <= 5) {
				if (!empty($result['company']['email'])) {
					$email = $result['company']['email'];
					
					//Email Content Start
					$emailBody  = "<div>";
					$emailBody .= "Dear ".ucfirst($result['company']['company_name']);
					$emailBody .= ",<br><br>";
					$emailBody .= "Your subscription expire on ".date('d F Y', strtotime($valid_till)).". Please renew your subscription.";
					$emailBody .= "<br><br>";
					$emailBody .= "Thankyou";
					$emailBody .= "<br>";
					$emailBody .= 'MynusTwo Team';
					$emailBody .= "</div>";
					
					$subject = "MYNUS2: Subscription Expire";
					$this->sendMail($email, $subject, $emailBody);
					//Email End
				}
			} elseif(!empty($valid_till) && (strtotime($val1) < strtotime($val2))) {
				//Disable business if not completed the subscription
				$this->Users->updateAll(['status'=>'inactive'], ['id'=>$result['user']['id']]);
			}
		}
			
			
	}

    public function getNonBusinessPhone() {

        if($this->request->is('post')) {
                
                $placeId = $this->request->data['place_id'];

                $url = 'https://maps.googleapis.com/maps/api/place/details/json?placeid='.$placeId.'&fields=formatted_phone_number&key=AIzaSyAxrlnzjA6PwkkYjsIJUd20JhdxIW3iiR4';

                $ch = curl_init(); 
                curl_setopt_array($ch, [
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL            =>  $url
                ]);
                $new_url = curl_exec($ch);

                pr($new_url); die;
                 
                $this->set([
                        'data'       => json_decode($new_url),
                        '_serialize' => ['data']
                ]);
                
                curl_close($ch);
        }
    }
}