-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 16, 2020 at 08:26 AM
-- Server version: 5.7.30-0ubuntu0.16.04.1
-- PHP Version: 5.6.30-10+deb.sury.org~xenial+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kenbar`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking_items`
--

CREATE TABLE `booking_items` (
  `id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `company_item_id` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `final_price` decimal(10,2) NOT NULL,
  `size` varchar(25) DEFAULT NULL,
  `qty` int(5) DEFAULT NULL,
  `company_items_options` text,
  `extra_instruction` varchar(100) DEFAULT NULL,
  `payment_status` varchar(5) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking_items`
--

INSERT INTO `booking_items` (`id`, `booking_id`, `company_item_id`, `name`, `price`, `final_price`, `size`, `qty`, `company_items_options`, `extra_instruction`, `payment_status`) VALUES
(240, 187, 63, 'ada', '1.00', '1.00', 'none', 1, '[]', NULL, 'yes'),
(241, 196, 63, 'ada', '1.00', '2.00', 'none', 2, '[]', NULL, 'no'),
(247, 187, 63, 'ada', '1.00', '4.00', 'none', 4, '[]', NULL, 'yes'),
(248, 190, 56, 'Test new', '1.60', '3.60', 'none', 1, '[{"id":53,"name":"Coke","price":2,"company_item_id":56}]', NULL, 'yes'),
(249, 204, 56, 'Test new', '1.60', '1.60', 'none', 1, '[]', NULL, 'no'),
(250, 205, 63, 'ada', '1.00', '1.00', 'none', 1, '[]', NULL, 'no'),
(259, 210, 63, 'ada', '1.00', '8.00', 'none', 8, '[]', NULL, 'no'),
(262, 213, 63, 'ada', '1.00', '4.00', 'none', 4, '[]', NULL, 'no'),
(263, 214, 63, 'ada', '1.00', '10.00', 'none', 10, '[]', NULL, 'no'),
(264, 216, 63, 'ada', '1.00', '5.00', 'none', 5, '[]', NULL, 'no'),
(265, 217, 63, 'ada', '1.00', '16.00', 'none', 16, '[]', NULL, 'no'),
(266, 218, 63, 'ada', '1.00', '1.00', 'none', 1, '[]', NULL, 'no'),
(267, 219, 56, 'Test new', '1.60', '1.60', 'none', 1, '[{"id":54,"name":"Pepsi","price":0,"company_item_id":56}]', NULL, 'no'),
(270, 221, 69, 'Darubaaz', '2.00', '4.00', 'none', 1, '[{"id":55,"name":"Chakana","price":2,"company_item_id":69}]', NULL, 'no'),
(271, 222, 56, 'Test new', '1.60', '1.60', 'none', 1, '[]', NULL, 'no'),
(273, 225, 63, 'ada', '1.00', '2.00', 'none', 2, '[]', NULL, 'no'),
(275, 227, 63, 'ada', '1.00', '2.00', 'none', 2, '[]', NULL, 'no'),
(276, 229, 56, 'Test new', '1.60', '1.60', 'none', 1, '[]', NULL, 'no'),
(277, 230, 36, 'Vine 1', '3.50', '3.50', 'small', 1, '[]', NULL, 'yes'),
(278, 230, 69, 'Darubaaz', '2.00', '2.00', 'small', 1, '[]', NULL, 'yes'),
(279, 233, 56, 'Test new', '1.60', '1.60', 'none', 1, '[]', NULL, 'yes'),
(282, 227, 63, 'ada', '1.00', '4.00', 'none', 4, '[]', NULL, 'no'),
(283, 227, 63, 'ada', '1.00', '2.00', 'none', 2, '[]', NULL, 'no'),
(284, 227, 63, 'ada', '1.00', '2.00', 'none', 2, '[]', NULL, 'no'),
(285, 227, 63, 'ada', '1.00', '2.00', 'none', 2, '[]', NULL, 'no'),
(294, 243, 67, 'burger', '1.50', '1.50', 'none', 1, '[]', NULL, 'yes'),
(298, 258, 63, 'ada', '1.00', '1.00', 'none', 1, '[]', NULL, 'yes'),
(299, 259, 63, 'ada', '1.00', '1.00', 'none', 1, '[]', NULL, 'yes'),
(330, 290, 56, 'Test new', '1.60', '1.60', 'none', 1, '[]', NULL, 'yes'),
(340, 304, 50, 'White wine', '7.00', '7.00', 'large', 1, '[]', NULL, 'yes'),
(345, 308, 52, 'Toast and Eggs', '7.00', '7.00', 'large', 1, '[]', NULL, 'yes'),
(346, 308, 48, 'Tiramisu', '12.00', '12.00', 'none', 1, '[]', NULL, 'yes'),
(347, 309, 56, 'Test new', '1.60', '1.60', 'none', 1, '[]', NULL, 'yes'),
(348, 311, 48, 'Tiramisu', '12.00', '12.00', 'none', 1, '[]', NULL, 'yes'),
(349, 311, 15, 'Waffle ', '3.00', '3.00', 'medium', 1, '[]', NULL, 'yes'),
(352, 291, 65, 'Abcd', '1.60', '1.60', 'none', 1, '[]', NULL, 'yes'),
(354, 316, 15, 'Waffle ', '3.00', '3.00', 'medium', 1, '[]', NULL, 'yes'),
(355, 316, 50, 'White wine', '7.00', '7.00', 'large', 1, '[]', NULL, 'yes'),
(356, 316, 52, 'Toast and Eggs', '7.00', '7.00', 'large', 1, '[]', NULL, 'yes'),
(361, 321, 56, 'Test new', '1.60', '3.60', 'none', 1, '[{"id":53,"name":"Coke","price":2,"company_item_id":56}]', NULL, 'yes'),
(362, 322, 56, 'Test new', '1.60', '1.60', 'none', 1, '[]', NULL, 'yes'),
(363, 324, 56, 'Test new', '1.60', '8.00', 'none', 5, '[]', NULL, 'yes'),
(365, 330, 56, 'Test new', '1.60', '1.60', 'none', 1, '[]', NULL, 'yes'),
(367, 332, 56, 'Test new', '1.60', '1.60', 'none', 1, '[]', NULL, 'yes'),
(368, 333, 56, 'Test new', '1.60', '1.60', 'none', 1, '[]', NULL, 'yes'),
(369, 334, 72, 'Pasta', '1.50', '12.00', 'none', 8, '[]', NULL, 'yes'),
(370, 335, 72, 'Pasta', '1.50', '15.00', 'none', 10, '[]', NULL, 'yes'),
(376, 344, 72, 'Pasta', '1.50', '7.50', 'none', 5, '[]', NULL, 'yes'),
(377, 350, 56, 'Test new', '1.60', '3.60', 'none', 1, '[{"id":53,"name":"Coke","price":2,"company_item_id":56}]', NULL, 'yes'),
(378, 363, 56, 'Test new', '1.60', '1.60', 'none', 1, '[]', 'hello', 'yes'),
(379, 364, 69, 'Darubaaz', '2.00', '2.00', 'large', 1, '[]', NULL, 'yes'),
(380, 365, 17, 'Vodka tonic', '4.00', '4.00', 'large', 1, '[]', NULL, 'yes'),
(381, 365, 53, 'Wine and cheese', '7.00', '7.00', 'large', 1, '[]', NULL, 'yes'),
(382, 365, 50, 'White wine', '7.00', '7.00', 'large', 1, '[]', NULL, 'yes'),
(383, 365, 49, 'Grape wine', '27.00', '27.00', 'large', 1, '[]', NULL, 'yes'),
(384, 365, 66, 'Chocolate Sunday ', '12.50', '12.50', 'none', 1, '[]', NULL, 'yes'),
(385, 365, 48, 'Tiramisu', '12.00', '12.00', 'none', 1, '[]', NULL, 'yes'),
(386, 365, 52, 'Toast and Eggs', '7.00', '7.00', 'large', 1, '[]', NULL, 'yes'),
(387, 365, 47, 'Blueberry pancakes ', '7.00', '7.00', 'large', 1, '[]', NULL, 'yes'),
(388, 365, 17, 'Vodka tonic', '3.00', '3.00', 'medium', 1, '[]', NULL, 'yes'),
(389, 365, 16, 'Vegan Drinks', '3.00', '3.00', 'medium', 1, '[]', NULL, 'yes'),
(390, 365, 51, 'BBQ Pork', '7.00', '14.00', 'large', 2, '[]', 'No potato, add orange fri with lemonade and pizza ', 'yes'),
(391, 365, 46, 'Avocado Burger ', '7.00', '7.00', 'large', 1, '[]', NULL, 'yes'),
(392, 366, 72, 'Pasta', '1.50', '1.50', 'none', 1, '[]', 'With extra cheese ', 'yes'),
(393, 368, 48, 'Tiramisu', '12.00', '12.00', 'none', 1, '[]', NULL, 'yes'),
(394, 368, 47, 'Blueberry pancakes ', '7.00', '13.00', 'large', 1, '[{"id":48,"name":"Apple cuts","price":6,"company_item_id":47}]', 'Add pizza and salad to my reservatio please i meed', 'yes'),
(400, 375, 47, 'Blueberry pancakes ', '7.00', '7.00', 'large', 1, '[]', NULL, 'yes'),
(401, 375, 52, 'Toast and Eggs', '7.00', '7.00', 'large', 1, '[]', NULL, 'yes'),
(402, 377, 56, 'Test new', '1.60', '1.60', 'large', 1, '[]', NULL, 'yes'),
(407, 377, 73, 'Teat', '25.00', '25.00', 'small', 1, '[]', NULL, 'yes'),
(408, 377, 38, 'New food', '2.50', '2.50', 'none', 1, '[]', NULL, 'yes'),
(416, 383, 72, 'Pasta', '1.50', '3.00', 'none', 2, '[]', 'with more organo', 'yes'),
(417, 383, 74, 'Upma', '1.00', '7.10', 'small', 2, '[{"id":60,"name":"Abc","price":2.55,"company_item_id":74}]', 'Xudufhf', 'yes'),
(418, 384, 73, 'Teat', '25.00', '25.00', 'none', 1, '[]', NULL, 'yes'),
(419, 384, 69, 'Old monk latest edition', '2.00', '2.00', 'none', 1, '[]', NULL, 'yes'),
(420, 384, 56, 'Test new', '1.70', '1.70', 'medium', 1, '[]', NULL, 'yes'),
(421, 384, 73, 'Teat', '25.00', '25.00', 'large', 1, '[]', 'Add pizza and salad to this order please i meed', 'yes'),
(422, 384, 36, 'Vine 1', '10.00', '10.00', 'large', 1, '[]', 'Add extra everything please with some ice cream. ', 'yes'),
(424, 384, 56, 'Test new', '1.60', '3.60', 'none', 1, '[{"id":53,"name":"Coke","price":2,"company_item_id":56}]', NULL, 'yes'),
(425, 384, 56, 'Test new', '1.60', '3.60', 'none', 1, '[{"id":53,"name":"Coke","price":2,"company_item_id":56}]', '2 litre with some chips.', 'yes'),
(428, 387, 48, 'Tiramisu', '12.00', '12.00', 'none', 1, '[]', NULL, 'yes'),
(429, 387, 15, 'Waffle ', '3.00', '3.00', 'medium', 1, '[]', NULL, 'yes'),
(432, 391, 47, 'Blueberry pancakes ', '7.00', '20.00', 'large', 1, '[{"id":47,"name":"Whip cream","price":4,"company_item_id":47},{"id":48,"name":"Apple cuts","price":6,"company_item_id":47},{"id":49,"name":"Extra pancakes","price":3,"company_item_id":47}]', NULL, 'yes'),
(433, 391, 53, 'Wine and cheese', '6.00', '6.00', 'large', 1, '[]', NULL, 'yes'),
(434, 391, 16, 'Vegan Drinks', '3.00', '3.00', 'medium', 1, '[]', 'Need gfgdhb chugging hcighb Fuchs hcjghbfhfhg ', 'yes'),
(435, 391, 66, 'Chocolate Sunday ', '12.50', '12.50', 'none', 1, '[]', NULL, 'yes'),
(436, 391, 51, 'BBQ Pork', '6.00', '6.00', 'large', 1, '[]', NULL, 'yes'),
(437, 391, 52, 'Toast and Eggs', '7.00', '7.00', 'large', 1, '[]', NULL, 'yes'),
(438, 391, 48, 'Tiramisu', '12.00', '12.00', 'none', 1, '[]', NULL, 'yes'),
(440, 401, 48, 'Tiramisu', '12.00', '12.00', 'none', 1, '[]', 'Extra something please ', 'yes'),
(441, 401, 47, 'Blueberry pancakes ', '5.00', '18.00', 'medium', 1, '[{"id":47,"name":"Whip cream","price":4,"company_item_id":47},{"id":48,"name":"Apple cuts","price":6,"company_item_id":47},{"id":49,"name":"Extra pancakes","price":3,"company_item_id":47}]', NULL, 'yes'),
(442, 401, 66, 'Chocolate Sunday ', '12.50', '12.50', 'none', 1, '[]', NULL, 'yes'),
(443, 402, 48, 'Tiramisu', '12.00', '12.00', 'none', 1, '[]', NULL, 'yes'),
(444, 403, 53, 'Wine and cheese', '6.00', '6.00', 'large', 1, '[]', NULL, 'yes'),
(446, 414, 49, 'Grape wine', '27.00', '27.00', 'large', 1, '[]', 'Something nice please \n', 'yes'),
(447, 414, 48, 'Tiramisu', '12.00', '12.00', 'none', 1, '[]', NULL, 'yes'),
(448, 414, 46, 'Avocado Burger ', '7.00', '28.00', 'large', 4, '[]', NULL, 'yes'),
(449, 417, 73, 'Teat', '25.00', '25.00', 'none', 1, '[]', NULL, 'yes'),
(452, 428, 56, 'Test new', '1.60', '3.60', 'none', 1, '[{"id":53,"name":"Coke","price":2,"company_item_id":56},{"id":54,"name":"Pepsi","price":0,"company_item_id":56}]', NULL, 'yes'),
(453, 443, 73, 'Teat', '25.00', '25.00', 'none', 1, '[]', NULL, 'yes'),
(454, 444, 56, 'Test new', '1.60', '3.60', 'none', 1, '[{"id":53,"name":"Coke","price":2,"company_item_id":56}]', NULL, 'yes'),
(455, 445, 73, 'Teat', '25.00', '50.00', 'none', 1, '[{"id":59,"name":"Gghg","price":25,"company_item_id":73}]', NULL, 'yes'),
(462, 457, 69, 'Darubaaz', '2.00', '4.00', 'none', 1, '[{"id":55,"name":"Chakana","price":2,"company_item_id":69}]', NULL, 'yes'),
(464, 457, 56, 'Test new', '1.60', '1.60', 'none', 1, '[]', NULL, 'yes'),
(465, 474, 42, 'Fish and chips ', '14.00', '14.00', 'medium', 1, '[]', NULL, 'yes'),
(466, 474, 47, 'Blueberry pancakes ', '7.00', '20.00', 'large', 1, '[{"id":47,"name":"Whip cream","price":4,"company_item_id":47},{"id":48,"name":"Apple cuts","price":6,"company_item_id":47},{"id":49,"name":"Extra pancakes","price":3,"company_item_id":47}]', 'Small chips\n', 'yes'),
(467, 479, 73, 'Teat', '25.00', '25.00', 'none', 1, '[]', NULL, 'yes'),
(468, 481, 17, 'Vodka tonic', '3.00', '9.00', 'medium', 3, '[]', NULL, 'yes'),
(469, 481, 48, 'Tiramisu', '12.00', '36.00', 'none', 3, '[]', NULL, 'yes'),
(470, 481, 50, 'White wine', '7.00', '14.00', 'large', 2, '[]', NULL, 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `business_timings`
--

CREATE TABLE `business_timings` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `day` varchar(20) DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `type` enum('company','individual') NOT NULL DEFAULT 'company',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_timings`
--

INSERT INTO `business_timings` (`id`, `company_id`, `day`, `start_time`, `end_time`, `type`, `created`, `modified`) VALUES
(1, 2, 'Thursday', '09:30:00', '20:00:00', 'company', '2019-01-31 11:31:39', '2019-01-31 11:31:39'),
(2, 2, 'Friday', '09:30:00', '20:30:00', 'company', '2019-01-31 11:32:02', '2019-01-31 11:32:02'),
(3, 1, 'Monday', '10:00:00', '18:00:00', 'company', '2019-03-08 04:57:40', '2019-03-08 04:57:40'),
(4, 14, 'Wednesday', '09:30:00', '23:59:00', 'company', '2019-05-01 03:24:02', '2019-07-01 15:18:53'),
(6, 12, 'Wednesday', '09:00:00', '17:25:00', 'company', '2019-05-01 03:40:00', '2019-05-01 03:40:00'),
(7, 14, 'Monday', '10:29:00', '21:29:00', 'company', '2019-05-17 19:38:03', '2019-09-08 21:59:44'),
(8, 14, 'Thursday', '08:12:00', '18:12:00', 'company', '2019-05-17 19:38:37', '2019-05-17 19:38:37'),
(9, 21, 'Friday', '10:00:00', '17:58:00', 'company', '2019-05-17 19:58:07', '2019-05-17 19:58:07'),
(10, 21, 'Saturday', '10:00:00', '17:58:00', 'company', '2019-05-17 19:58:46', '2019-05-17 19:58:46'),
(16, 23, 'Wednesday', '08:00:00', '20:30:00', 'company', '2019-05-29 23:49:52', '2019-05-29 23:49:52'),
(17, 23, 'Thursday', '08:30:00', '21:00:00', 'company', '2019-05-29 23:50:20', '2019-05-29 23:50:20'),
(18, 23, 'Friday', '08:19:00', '21:19:00', 'company', '2019-05-29 23:50:47', '2019-05-29 23:50:47'),
(19, 27, 'wednesday', '07:35:00', '17:35:00', 'company', '2019-06-28 12:35:58', '2019-06-28 12:35:58'),
(20, 22, 'Saturday', '08:00:00', '21:05:00', 'company', '2019-06-28 22:05:37', '2019-06-28 22:05:37'),
(21, 22, 'Sunday', '08:00:00', '20:00:00', 'company', '2019-06-28 22:06:09', '2019-06-28 22:06:09'),
(22, 22, 'Monday', '08:06:00', '22:06:00', 'company', '2019-06-28 22:06:44', '2019-06-28 22:06:44'),
(23, 22, 'Tuesday', '07:26:00', '23:27:00', 'company', '2019-06-28 22:07:24', '2019-08-20 19:27:28'),
(24, 22, 'Friday', '08:08:00', '23:00:00', 'company', '2019-06-28 22:08:23', '2019-06-28 22:08:23'),
(25, 23, 'Saturday', '09:30:00', '21:25:00', 'company', '2019-06-29 09:25:09', '2019-06-29 09:25:09'),
(26, 23, 'Sunday', '08:30:00', '21:25:00', 'company', '2019-06-29 09:25:33', '2019-06-29 09:25:33'),
(27, 12, 'Monday', '08:00:00', '21:00:00', 'company', '2019-07-01 12:13:15', '2019-07-01 12:13:15'),
(28, 14, 'Tuesday', '08:18:00', '23:55:00', 'company', '2019-07-01 15:18:22', '2019-07-01 15:18:22'),
(29, 22, 'Wednesday', '05:38:00', '20:38:00', 'company', '2019-07-04 06:08:58', '2019-07-04 06:08:58'),
(30, 22, 'Thursday', '08:12:00', '18:12:00', 'company', '2019-07-04 12:12:52', '2019-07-04 12:12:52'),
(31, 12, 'Thursday', '00:00:00', '12:00:00', 'company', '2019-08-22 02:03:47', '2019-08-22 02:03:47'),
(32, 23, 'Monday', '07:51:00', '23:51:00', 'company', '2019-08-26 19:51:48', '2019-08-26 19:51:48'),
(33, 14, 'Friday', '10:30:00', '19:30:00', 'company', '2019-09-08 22:00:56', '2019-09-08 22:00:56');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `status`, `created`, `modified`) VALUES
(1, 'Salon', 'active', '2018-07-31 00:00:00', '2018-07-31 00:00:00'),
(2, 'Ball Rooms', 'active', '2018-07-31 10:12:36', '2018-07-31 10:12:36'),
(3, 'Meeting Rooms', 'inactive', '2018-07-31 10:12:36', '2018-07-31 10:12:36'),
(4, 'Gym', 'inactive', '2018-11-19 11:22:17', '2018-11-19 11:22:17'),
(5, 'Restaurant', 'active', '2018-11-29 21:50:33', '2018-11-29 21:50:33'),
(6, 'others', 'active', '2019-02-02 10:39:58', '2019-02-02 10:39:58'),
(7, 'nightclub/lounge', 'active', '2019-02-02 10:41:18', '2019-02-02 10:41:18'),
(8, 'Barbershop', 'active', '2019-02-02 10:41:43', '2019-02-02 10:41:43'),
(9, 'Food', 'inactive', '2019-04-24 09:53:19', '2019-04-24 09:53:19'),
(10, 'Groceries', 'inactive', '2019-04-24 09:53:59', '2019-04-24 09:53:59'),
(11, 'Fast Food', 'inactive', '2019-04-24 09:54:09', '2019-04-24 09:54:09'),
(12, 'Bakery', 'inactive', '2019-04-24 09:54:20', '2019-04-24 09:54:20'),
(13, 'Dessert', 'inactive', '2019-04-24 09:54:27', '2019-04-24 09:54:27'),
(14, 'Drink', 'inactive', '2019-04-24 09:54:46', '2019-04-24 09:54:46'),
(15, 'Coffee', 'inactive', '2019-04-24 09:54:59', '2019-04-24 09:54:59'),
(16, 'Stores', 'active', '2019-04-24 09:55:09', '2019-04-24 09:55:09'),
(17, 'Tea', 'inactive', '2019-04-24 09:55:21', '2019-04-24 09:55:21'),
(18, 'Juice', 'inactive', '2019-04-24 09:55:25', '2019-04-24 09:55:25'),
(19, 'Bar', 'inactive', '2019-04-24 09:55:33', '2019-04-24 09:55:33'),
(20, 'Beer', 'inactive', '2019-04-24 09:55:38', '2019-04-24 09:55:38'),
(21, 'Wine', 'inactive', '2019-04-24 09:55:43', '2019-04-24 09:55:43'),
(22, 'Shopping', 'inactive', '2019-04-24 09:55:51', '2019-04-24 09:55:51'),
(23, 'Shopping Center', 'inactive', '2019-04-24 09:56:51', '2019-04-24 09:56:51'),
(24, 'Apparel', 'inactive', '2019-04-24 09:56:57', '2019-04-24 09:56:57'),
(25, 'Home & office', 'inactive', '2019-04-24 09:57:05', '2019-04-24 09:57:05'),
(26, 'Sporting Goods', 'inactive', '2019-04-24 09:57:14', '2019-04-24 09:57:14'),
(27, 'Travel', 'inactive', '2019-04-24 09:57:22', '2019-04-24 09:57:22'),
(28, 'Airport ', 'inactive', '2019-04-24 09:57:30', '2019-04-24 09:57:30'),
(29, 'Museum ', 'inactive', '2019-04-24 09:57:36', '2019-04-24 09:57:36'),
(30, 'Hotel', 'inactive', '2019-04-24 09:57:41', '2019-04-24 09:57:41'),
(31, 'Landmark', 'inactive', '2019-04-24 09:57:52', '2019-04-24 09:57:52'),
(32, 'Service', 'active', '2019-04-24 09:57:58', '2019-04-24 09:57:58'),
(33, 'Beauty ', 'inactive', '2019-04-24 09:58:03', '2019-04-24 09:58:03'),
(34, 'Laundry ', 'inactive', '2019-04-24 09:58:11', '2019-04-24 09:58:11'),
(35, 'Bank ', 'inactive', '2019-04-24 09:58:27', '2019-04-24 09:58:27'),
(36, 'Home Service ', 'inactive', '2019-04-24 09:58:34', '2019-04-24 09:58:34'),
(37, 'Pet Service ', 'inactive', '2019-04-24 09:58:41', '2019-04-24 09:58:41'),
(38, 'Post', 'inactive', '2019-04-24 09:58:46', '2019-04-24 09:58:46'),
(39, 'Landscaping ', 'inactive', '2019-04-24 09:58:52', '2019-04-24 09:58:52'),
(40, 'DMV', 'inactive', '2019-04-24 09:58:58', '2019-04-24 09:58:58'),
(41, 'Fun', 'inactive', '2019-04-24 09:59:14', '2019-04-24 09:59:14'),
(42, 'Nightlife ', 'inactive', '2019-04-24 09:59:23', '2019-04-24 09:59:23'),
(43, 'lounge ', 'inactive', '2019-04-24 09:59:35', '2019-04-24 09:59:35'),
(44, 'Music & Drama ', 'inactive', '2019-04-24 09:59:47', '2019-04-24 09:59:47'),
(45, 'Park & Rec', 'inactive', '2019-04-24 10:00:11', '2019-04-24 10:00:11'),
(46, 'Movies ', 'inactive', '2019-04-24 10:00:18', '2019-04-24 10:00:18'),
(47, 'Sport', 'inactive', '2019-04-24 10:00:46', '2019-04-24 10:00:46'),
(48, 'Health', 'inactive', '2019-04-24 10:00:54', '2019-04-24 10:00:54'),
(49, 'Fitness', 'inactive', '2019-04-24 10:01:05', '2019-04-24 10:01:05'),
(50, 'Drug Store ', 'inactive', '2019-04-24 10:01:12', '2019-04-24 10:01:12'),
(51, 'Hospital ', 'inactive', '2019-04-24 10:01:18', '2019-04-24 10:01:18'),
(52, 'Park', 'inactive', '2019-04-24 10:01:42', '2019-04-26 03:15:26'),
(53, 'Therapy ', 'inactive', '2019-04-24 11:06:23', '2019-04-24 11:06:23'),
(54, 'Tattoo', 'inactive', '2019-04-24 11:06:31', '2019-04-24 11:06:31'),
(55, 'Fun & Game', 'inactive', '2019-04-24 11:06:51', '2019-04-24 11:06:51'),
(56, 'Auto Service ', 'inactive', '2019-04-24 11:07:14', '2019-04-24 11:07:14'),
(57, 'Car Rental ', 'inactive', '2019-04-24 11:07:21', '2019-04-24 11:07:21'),
(58, 'Realty', 'inactive', '2019-04-24 11:07:55', '2019-04-24 11:07:55'),
(59, 'Vegan', 'inactive', '2019-04-24 11:14:47', '2019-04-24 11:14:47'),
(60, 'Vegetarian ', 'inactive', '2019-04-24 11:15:44', '2019-04-24 11:15:44'),
(61, 'Spa', 'inactive', '2019-04-24 20:22:44', '2019-04-24 20:22:44'),
(62, 'Pizza', 'inactive', '2019-04-24 20:23:18', '2019-04-24 20:23:18'),
(63, 'Mexican food', 'inactive', '2019-04-24 20:24:49', '2019-04-24 20:24:49'),
(64, 'Chinese Food', 'inactive', '2019-04-24 23:57:59', '2019-04-24 23:57:59'),
(65, 'Indian Food', 'inactive', '2019-04-24 23:58:46', '2019-04-24 23:58:46'),
(66, 'Italian Food', 'inactive', '2019-04-24 23:59:14', '2019-04-24 23:59:14'),
(67, 'Swimming', 'inactive', '2019-04-24 23:59:40', '2019-04-24 23:59:40'),
(68, 'Plumber', 'inactive', '2019-04-25 00:02:04', '2019-04-25 00:02:04'),
(69, 'Tax', 'inactive', '2019-04-25 00:02:42', '2019-04-25 00:02:42'),
(70, 'Dentist', 'inactive', '2019-04-25 00:05:09', '2019-04-25 00:05:09'),
(71, 'Optometrist', 'inactive', '2019-04-25 00:06:00', '2019-04-25 00:06:00'),
(72, 'Fedex', 'inactive', '2019-04-25 00:06:23', '2019-04-25 00:06:23'),
(73, 'UPS', 'inactive', '2019-04-25 00:06:47', '2019-04-25 00:06:47'),
(74, 'Ice Cream ', 'inactive', '2019-04-28 21:49:40', '2019-04-28 21:49:40'),
(75, 'Phone', 'inactive', '2019-05-02 18:01:32', '2019-05-02 18:01:32'),
(76, 'Martial Arts ', 'inactive', '2019-05-02 21:08:51', '2019-05-02 21:08:51'),
(77, 'Kid Clubs ', 'inactive', '2019-05-02 21:09:05', '2019-05-02 21:09:05'),
(78, 'Boxing ', 'inactive', '2019-05-02 21:09:14', '2019-05-02 21:09:14'),
(79, 'Breakfast', 'inactive', '2019-05-11 11:35:44', '2019-05-11 11:35:44'),
(80, 'Lunch', 'inactive', '2019-05-11 11:35:53', '2019-05-11 11:35:53'),
(81, 'electrics', 'active', '2019-05-28 11:36:59', '2019-05-28 11:36:59'),
(82, 'Shop', 'active', '2019-05-28 11:40:41', '2019-05-28 11:42:27');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `company_name` varchar(45) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `address` varchar(125) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `latitude` decimal(10,8) DEFAULT NULL,
  `longitude` decimal(11,8) DEFAULT NULL,
  `about` text,
  `booking_amount` decimal(10,2) DEFAULT '0.00',
  `business_phone` varchar(16) DEFAULT '0',
  `offers` varchar(100) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `wait` enum('yes','no') NOT NULL DEFAULT 'yes',
  `type` enum('company','individual') NOT NULL DEFAULT 'company',
  `wait_status` enum('open','close') NOT NULL DEFAULT 'close',
  `delivery` varchar(6) NOT NULL DEFAULT 'no',
  `delivery_radius` decimal(10,2) DEFAULT NULL,
  `delivery_amount` decimal(10,2) DEFAULT '0.00',
  `tax` decimal(10,2) DEFAULT '0.00',
  `client_id` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `user_id`, `category_id`, `company_name`, `first_name`, `last_name`, `phone`, `email`, `address`, `city`, `state`, `country`, `zipcode`, `latitude`, `longitude`, `about`, `booking_amount`, `business_phone`, `offers`, `status`, `wait`, `type`, `wait_status`, `delivery`, `delivery_radius`, `delivery_amount`, `tax`, `client_id`, `created`, `modified`) VALUES
(1, 2, 1, 'Love.Hair.Growth', 'Erisi', 'Lino', '3238290494', '', '167 n market st', 'Inglewood', 'Ca', 'USA', '90302', '33.95908600', '-118.35190700', 'I am self employed at Really Raw salon.I believe in growth and rejuvenation of every type of hair. I also believe every type of hair is beautiful you just need to know what to do with it.', NULL, '', 'M-Th the shampoo and flat iron is $25.', 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-01-29 09:14:16', '2019-01-29 09:19:14'),
(2, 9, 3, 'The Corporate Park ', 'Ar', 'Sa', '7300820270', 'thecity@yopmail.com', 'Sarswati Vihar Ajabpur khurd ', 'Dehradun ', 'Uttarakhand ', 'India ', '248001', '30.33458200', '78.05378100', 'Open for personal and corporate meetings.', NULL, '', '5%', 'active', 'yes', 'company', 'open', 'no', NULL, NULL, NULL, NULL, '2019-01-31 10:46:41', '2019-01-31 11:26:18'),
(6, 12, 2, 'Holiday Inn Los Angeles Gateway Torrance', 'Benjamin', 'Pineda', '3108186648', NULL, '19800 S Vermont Ave', 'Torrance', 'Ca', 'Usa', '90502', '33.85079600', '-118.28932400', NULL, NULL, '', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-02-04 22:26:11', '2019-02-04 22:26:11'),
(7, 13, 8, 'Nadines barber shop', 'Mike', 'W', '3107663023', NULL, '4233 west century blvd', 'Inglewood', 'Ca', 'US', '90304', '33.94584400', '-118.34971000', NULL, NULL, '', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-02-08 06:35:47', '2019-02-08 06:35:47'),
(8, 17, 1, 'Neeba\'s House of Hair', 'Keneba', 'Nicholas', '4242077324', '', '13031 Ramona Ave', 'Hawthorne', 'CA', 'United States', '90250', '33.91278100', '-118.35203900', 'We make custom wigs, install weaves. Make up available upon request', NULL, '', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-02-20 12:51:54', '2019-02-20 12:56:15'),
(9, 25, 5, 'Kc soup', 'Vaib', 'Rana', '8126208888', NULL, 'Chandigarh', 'Chandigarh', 'Punjab', 'India', '248001', '30.72733500', '76.78916000', 'This is ...', NULL, '', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-03-31 23:16:03', '2019-03-31 23:16:03'),
(10, 27, 5, 'Test company', 'Test', 'Server', '8755715559', '', 'Shastradhara road', 'Rishikesh', 'Uttrakhand', 'India', '248001', '30.33458200', '78.05378100', 'Testing', NULL, '', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-04-03 23:14:02', '2019-06-04 14:41:50'),
(11, 31, 1, 'Hdhudh', 'Ydhdh', 'Ghjgghh', '3212022278', NULL, 'It park', 'Torrance', 'Los angeles', 'Usa', '90248', '33.86923100', '-118.31349900', 'Ighjb', NULL, '', 'Jdhdhdy', 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-04-04 00:03:40', '2019-04-04 00:03:40'),
(12, 35, 5, 'Yummy Restaurant', 'Sarabjeet', 'Kaur', '9627570278', '', '34/4 IG Marg niranjanpur', 'Dehradun', 'Uttarakhand', 'India', '248001', '30.30736100', '78.01701700', 'Fhffuffu cyfuu fydduduu hffuffu ufufuffu ufuffufu fufufuufu fuifufufuf hlhhj ududydf vjcvjcjjvjvvjvjvjvjvjcjvjcjvj bvvvvv bgh ccvvv cgvvbbbb', '6.00', '8888888888', '5%', 'active', 'yes', 'company', 'open', 'yes', '5.00', '10.00', '5.00', 'ATRWOgi8kiONFVYgyueLZyVU-MHzEkBTrcmaqZcV5lrlNfohkg5Yw16ZNdi2wLq2BqyQESEKE-wBn48M', '2019-04-25 03:38:09', '2019-08-21 00:16:25'),
(13, 37, 5, 'Tasty restaurant', 'Tanuja', 'Bhatt', '9897444444', NULL, 'Jsjsjsj', 'Dehradun', 'Uttarakhand', 'India', '248001', '30.33458200', '78.05378100', 'Ksjsjsj', NULL, '', '5%', 'active', 'yes', 'individual', 'close', 'no', NULL, NULL, NULL, NULL, '2019-04-25 03:58:06', '2019-04-25 03:58:06'),
(14, 38, 5, 'bob restaurant', 'Pankaj', 'Pant', '8126208713', 'pantpankaj@yopmail.com', 'It part', 'Dehradun nalooo', 'Uttarakhand', 'India', '248001', '30.36336400', '78.06878300', 'Evontech india restaurant in it park', '2.00', '3233232323', 'Offer text', 'active', 'yes', 'company', 'close', 'yes', '5.00', '28.27', '5.00', 'AVj9Ox3QPHXcVaAwFUts5PjdCS8Q79mSmW4N-K5TXm1tqkrqHf3OHhheXy30k5CDwagRo-qimwKzCHNQ', '2019-05-01 02:52:05', '2019-12-17 21:52:34'),
(15, 39, 5, 'Iejsj', 'Jdjdj', 'Jdjdjd', '9897333333', NULL, 'Hdhdjd', 'Dehradun', 'Uttarakhand', 'India', '94994499', '30.31649400', '78.03219200', '', NULL, '', 'Jsj', 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-05-01 03:45:42', '2019-05-01 03:45:42'),
(16, 40, 5, 'Mom kitchen', 'Mmmm', 'Nnn', '9897222222', NULL, 'Vhjj', 'Dehradun', 'Uttarakhand', 'India', '248001', '30.36011800', '78.07143700', 'Gfvjhbjk', '50.00', '9898999999', '5', 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-05-09 16:04:59', '2019-05-09 16:04:59'),
(19, 41, 1, 'Mbsjs', 'Sakab', 'Sjsbeb', '9897111111', NULL, 'Sbsbjw', 'Dehradun', 'Uttrakhand', 'India', '248001', '30.25679400', '78.10869700', 'Jssbb', '5.00', '987648649', '6', 'active', 'yes', 'individual', 'close', 'no', NULL, NULL, NULL, NULL, '2019-05-10 14:40:49', '2019-05-10 14:40:49'),
(21, 42, 5, 'New restaurant', 'Test name ', 'Test surname', '9898989898', NULL, 'Vishnupuram', 'Dehradun', 'Uttarakhand', 'India', '248001', '30.30023600', '78.06489300', 'Hello', '20.00', '3256565356', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-05-17 19:11:21', '2019-05-17 19:11:21'),
(22, 44, 5, 'The Waffle Spot', 'Mike ', 'Hyeit', '3107092343', NULL, '20909 anza ave ', 'Torrance ', 'Ca', 'United state ', '90503', '33.84032200', '-118.36296200', 'We provide the best waffle la had to offer ', '15.75', '3236985863', '20% discount ', 'active', 'yes', 'company', 'close', 'yes', '5.00', '7.00', '15.90', 'AVj9Ox3QPHXcVaAwFUts5PjdCS8Q79mSmW4N-K5TXm1tqkrqHf3OHhheXy30k5CDwagRo-qimwKzCHNQ', '2019-05-17 23:25:54', '2019-10-10 10:22:33'),
(23, 45, 1, 'Relax 101', 'Kenneth ', 'Wilson', '3109510778', '', '19800 south vermont', 'Torrance ', 'Ca', 'Usa', '90503', '33.79038500', '-118.30845900', 'Best place to unwind after a long day ', '10.00', '5637951350', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, 'AY69gxjhYYmG4q9w-sxeFlBelh50ZOL29avynbkylRG6jBnPivJsihDJ3FcY8Ar7wa21twKCz57z_FBI', '2019-05-19 05:37:27', '2019-08-26 21:06:58'),
(24, 49, 5, 'Qqq', 'Aaa', 'Sss', '9898222222', NULL, 'Fff', 'Dehradun', 'Uttarakhand', 'India', '248001', '30.31634700', '78.03355500', 'Ddd', '10.00', '9898333333', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-05-20 11:10:39', '2019-05-20 11:10:39'),
(25, 50, 5, 'Mmm', 'Cvvv', 'Jgbj', '9898333333', NULL, 'Sss', 'Dehradun', 'Uttarakhand', 'India', '248001', '30.31448200', '78.01096100', 'Hvbjk', '10.00', '9898444444', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-05-20 11:37:43', '2019-05-20 11:37:43'),
(26, 51, 5, 'Dg Restaurant', 'Stf', 'Ugb', '9898444444', '', 'Hhj', 'Dehradun', 'Uttarakhand', 'India', '248001', '30.33458200', '78.05378100', 'Bvh', '5.00', '9898555555', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-05-20 12:20:44', '2019-12-17 21:50:36'),
(27, 48, 1, 'SRK saloon', 'raju', 'chacha', '9595959595', '', 'Gggg', 'Ggg', 'Uttarakhand', 'India', '248001', '30.36336400', '78.06878300', 'Hhhhs', '23.30', '3232323232', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, 'AVj9Ox3QPHXcVaAwFUts5PjdCS8Q79mSmW4N-K5TXm1tqkrqHf3OHhheXy30k5CDwagRo-qimwKzCHNQ', '2019-05-20 12:46:11', '2019-06-28 12:56:30'),
(28, 52, 5, ' ABC restautant', 'Àaaaaaaa', 'Bbbbbbv', '9898555555', 'abc@gmail.com', 'Dddddd', 'Dehradun', 'Uttrakhand', 'India', '248001', '30.33458200', '78.05378100', 'Cccccc', '10.00', '9898666666', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-05-20 12:46:55', '2019-12-17 21:50:13'),
(29, 53, 5, 'night restaurant', 'Dgc', 'Ugvh', '9898666666', '', 'Gghh', 'Dehradun', 'Uttrakhand', 'India', '248001', '30.29448100', '78.05800200', 'Jgbh', '5.00', '9898777777', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-05-20 13:27:12', '2019-12-17 21:46:50'),
(30, 54, 5, 'Ishsj', 'Jjajsjs', 'Idjdjdj', '9898777777', '', 'Hshsj', 'Dehradun', 'Uttrakhand', 'India', '248001', '30.33458200', '78.05378100', 'Isjsjs', '4.00', '9898653599', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-05-20 13:40:24', '2019-05-20 13:40:24'),
(31, 58, 5, 'Guuugu', 'Vygg', 'Gugu', '9898989800', '', 'Ycycycy', 'Dehradun', 'Uttrakhand', 'India', '248009', '30.30837500', '78.05706800', 'Gygyfy', '68.00', '9693868899', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-05-20 14:01:11', '2019-05-20 14:01:11'),
(32, 61, 5, 'night restaurant', 'Ssyb', 'Igbj', '9595950000', '', 'Hgui', 'Dehradun', 'Uttarakhand', 'India', '248001', '30.36011800', '78.07143700', 'Iyhi', NULL, '9685698569', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-05-23 11:38:46', '2019-12-17 21:46:36'),
(33, 60, 5, 'G8hh', 'Ggui', 'Ihjj', '9595959500', '', 'Hgjo', 'Dehradun', 'Uttrakhand', 'India', '248001', '30.36011800', '78.07143700', 'Ufhjo', NULL, '9658325698', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-05-23 11:49:30', '2019-05-23 11:49:30'),
(34, 63, 5, 'Hijjj', 'Fughj', 'Yghjj', '9494949400', '', 'Gghhj', 'Dehradun', 'Uttrakhand', 'India', '248001', '30.33458200', '78.05378100', 'Tujuj', NULL, '8556696669', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, NULL, NULL, NULL, '2019-05-23 11:55:51', '2019-05-23 11:55:51'),
(35, 101, 1, 'Sarabjeet salon', 'Sarabjeet', 'Kaur', '9869869869', '', 'Clock tower', 'Dehradun', 'Uttarakhand', 'India', '248001', '30.32526400', '78.04129800', 'Chxhf', '5.00', '9589589589', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, '0.00', '0.00', NULL, '2019-07-22 05:20:20', '2019-07-23 05:32:36'),
(36, 102, 1, 'Srk hair salon', 'Srk', 'Khan', '9999888899', '', 'Mumbai', 'Mumbai', 'Mumbai', 'India', '248001', '18.92198400', '72.83465400', 'Best', '0.00', '3333333333', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, NULL, '0.00', NULL, '2019-07-23 05:16:15', '2019-07-23 05:16:15'),
(37, 109, 5, 'Gupta burger', 'Gupta', 'Ju', '9494949493', '', 'Dfff', 'Dehradun', 'Uk', 'India', '248001', '30.33458200', '78.05378100', 'Bargur wale', '0.00', '6666666666', NULL, 'active', 'yes', 'company', 'close', 'no', NULL, '0.00', '0.00', NULL, '2019-09-17 03:21:42', '2019-09-17 03:21:42'),
(38, 112, 2, 'room', 'raja', 'babu', '9876543210', 'raju@yopmail.com', 'dehradun', 'dehradun', 'uttarakhand', 'india', '248001', '30.33458200', '78.05378100', 'hahajsj', '0.00', '3636363636', 'vbbhh', 'active', 'yes', 'company', 'close', 'no', NULL, '0.00', '0.00', NULL, '2019-11-26 23:30:33', '2019-12-17 21:49:01');

-- --------------------------------------------------------

--
-- Table structure for table `company_category`
--

CREATE TABLE `company_category` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(75) NOT NULL,
  `restricted` varchar(10) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_category`
--

INSERT INTO `company_category` (`id`, `company_id`, `name`, `restricted`) VALUES
(1, 1, 'Breakfast  gdfgedfgdf', 'no'),
(2, 1, 'lunch', 'no'),
(3, 1, 'Breakfast', 'yes_21'),
(17, 14, 'Starter', 'no'),
(18, 14, 'Breakfast', 'no'),
(21, 14, 'Dinner', 'no'),
(22, 14, 'Lunch', 'no'),
(23, 14, 'Alcohol dangerous', 'yes_18'),
(24, 14, 'Indian food', 'no'),
(25, 22, 'Lunch', 'no'),
(26, 22, 'Liquor ', 'yes_21'),
(27, 29, 'Chinese', 'no'),
(28, 14, 'Breakfast', 'no'),
(29, 14, 'Dinner', 'no'),
(30, 33, 'Dinner', 'no'),
(31, 10, 'Breakfast', 'yes_21'),
(32, 22, 'Breakfast', 'no'),
(33, 22, 'Dessert ', 'no'),
(34, 22, 'Wine', 'yes_21'),
(40, 14, 'Kkkk', 'no'),
(41, 14, 'Chinese food', 'yes_21'),
(42, 14, 'Beer', 'yes_18'),
(50, 12, 'Italian', 'no'),
(51, 12, 'Breakfast', 'yes_18');

-- --------------------------------------------------------

--
-- Table structure for table `company_images`
--

CREATE TABLE `company_images` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(75) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_images`
--

INSERT INTO `company_images` (`id`, `company_id`, `name`, `created`, `modified`) VALUES
(1, 1, '15487334884_1.jpg', '2019-01-29 09:14:48', '2019-01-29 09:14:48'),
(2, 1, '154873351923_1.jpg', '2019-01-29 09:15:19', '2019-01-29 09:15:19'),
(3, 1, '154873353796_1.jpg', '2019-01-29 09:15:37', '2019-01-29 09:15:37'),
(4, 1, '154873356587_1.jpg', '2019-01-29 09:16:05', '2019-01-29 09:16:05'),
(5, 1, '154873360352_1.jpg', '2019-01-29 09:16:43', '2019-01-29 09:16:43'),
(6, 2, '154216939168_2.jpg', '2019-01-30 23:58:18', '2019-01-30 23:58:18'),
(7, 2, '154892389262_2.jpg', '2019-01-31 14:08:12', '2019-01-31 14:08:12'),
(8, 16, '155739811838_16.jpg', '2019-05-09 16:05:18', '2019-05-09 16:05:18'),
(10, 16, '155739853612_16.jpg', '2019-05-09 16:12:16', '2019-05-09 16:12:16'),
(11, 14, '155739973696_14.jpg', '2019-05-09 16:32:16', '2019-05-09 16:32:16'),
(12, 12, '15580739944_12.jpg', '2019-05-17 11:49:54', '2019-05-17 11:49:54'),
(14, 21, '155810049459_21.jpg', '2019-05-17 19:11:34', '2019-05-17 19:11:34'),
(15, 22, '155811641613_22.jpg', '2019-05-17 23:36:56', '2019-05-17 23:36:56'),
(16, 22, '155811642856_22.jpg', '2019-05-17 23:37:08', '2019-05-17 23:37:08'),
(17, 22, '155811643798_22.jpg', '2019-05-17 23:37:17', '2019-05-17 23:37:17'),
(18, 22, '155811660142_22.jpg', '2019-05-17 23:40:02', '2019-05-17 23:40:02'),
(19, 22, '155811661460_22.jpg', '2019-05-17 23:40:14', '2019-05-17 23:40:14'),
(20, 23, '155822560722_23.jpg', '2019-05-19 05:56:47', '2019-05-19 05:56:47'),
(22, 23, '155822562531_23.jpg', '2019-05-19 05:57:05', '2019-05-19 05:57:05'),
(23, 23, '155822563716_23.jpg', '2019-05-19 05:57:17', '2019-05-19 05:57:17'),
(24, 23, '155822564937_23.jpg', '2019-05-19 05:57:29', '2019-05-19 05:57:29'),
(25, 24, '155833226369_24.jpg', '2019-05-20 11:34:23', '2019-05-20 11:34:23'),
(26, 25, '155833257045_25.jpg', '2019-05-20 11:39:30', '2019-05-20 11:39:30'),
(27, 26, '155833508628_26.jpg', '2019-05-20 12:21:26', '2019-05-20 12:21:26'),
(28, 28, '15583366404_28.jpg', '2019-05-20 12:47:20', '2019-05-20 12:47:20'),
(29, 29, '155833905144_29.jpg', '2019-05-20 13:27:31', '2019-05-20 13:27:31'),
(30, 12, '155904465774_12.jpg', '2019-05-28 17:27:37', '2019-05-28 17:27:37'),
(31, 12, '155904467236_12.jpg', '2019-05-28 17:27:52', '2019-05-28 17:27:52'),
(32, 10, '155963909294_10.jpg', '2019-06-04 14:34:52', '2019-06-04 14:34:52'),
(33, 10, '155964046750_10.jpg', '2019-06-04 14:57:48', '2019-06-04 14:57:48'),
(34, 14, '156017163471_14.jpg', '2019-06-10 18:30:34', '2019-06-10 18:30:34'),
(35, 27, '156017310334_27.jpg', '2019-06-10 18:55:03', '2019-06-10 18:55:03'),
(36, 12, '156163322741_12.jpg', '2019-06-27 16:30:27', '2019-06-27 16:30:27'),
(37, 27, '156170553094_27.jpg', '2019-06-28 12:35:30', '2019-06-28 12:35:30'),
(38, 14, '157485783488_14.jpg', '2019-11-27 04:30:34', '2019-11-27 04:30:34'),
(39, 38, '157485843586_38.jpg', '2019-11-27 04:40:35', '2019-11-27 04:40:35');

-- --------------------------------------------------------

--
-- Table structure for table `company_items`
--

CREATE TABLE `company_items` (
  `id` int(11) NOT NULL,
  `company_category_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(125) NOT NULL,
  `price` int(11) DEFAULT NULL,
  `description` varchar(150) NOT NULL,
  `size` varchar(225) NOT NULL,
  `image` varchar(75) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_items`
--

INSERT INTO `company_items` (`id`, `company_category_id`, `company_id`, `name`, `price`, `description`, `size`, `image`) VALUES
(2, 18, 14, 'Noddles abcd lorem ispm torod', 56, 'Lorem hjfff jhffg ihgf jjhg ujjb uhhgg uhh uhhh hghh hggh ', 'null', '156163310531561633101371.jpg'),
(3, 18, 14, 'Mango', 56, 'Abcs', 'not_applicable', '155798440821557984269846.jpg'),
(4, 18, 14, 'Ghh', 23, 'Ghh', 'not_applicable', '155798448931557984486130.jpg'),
(11, 18, 14, 'Samosha', 2, 'Ancd', 'not_applicable', '155807365241558073509383.jpg'),
(14, 18, 14, 'Rose', 23, 'Gift', 'null', '155808634121558086300531.jpg'),
(15, 25, 22, 'Waffle ', 8, 'Enjoy our famous waffle  and whip cream ', '[{"size":"medium","price":3},{"size":"large","price":4},{"size":"small","price":2}]', '155815631611558156210184.jpg'),
(16, 26, 22, 'Vegan Drinks', 12, 'Enjoy our flavorful vodka fruit drinks ', '[{"size":"medium","price":3},{"size":"large","price":4},{"size":"small","price":2}]', '155820363121558203561716.jpg'),
(17, 26, 22, 'Vodka tonic', 16, 'Mint leaf and bodka', '[{"size":"medium","price":3},{"size":"large","price":4},{"size":"small","price":2}]', '155820748821558207451572.jpg'),
(18, 27, 29, 'Noodles', 2, 'Haka', '[{"size":"large","price":"7"},{"size":"medium","price":"5"},{"size":"small","price":"2"}][{"size":"medium","price":14},{"size":"large","price":17}]', '155833916601558339132974.jpg'),
(19, 30, 33, 'Hkk', 1, 'Igh', '[{"size":"medium","price":14},{"size":"large","price":17}]', NULL),
(20, 18, 14, 'Noodles', 23, 'Hhh', '[{"size":"medium","price":14},{"size":"large","price":17}]', NULL),
(23, 31, 10, 'Abcd', 12, 'Vvv', '[{"size":"medium","price":14},{"size":"large","price":17}]', '155963917311559639149157.jpg'),
(34, 17, 14, 'Test', NULL, 'Fgg', '[{"size":"medium","price":14},{"size":"large","price":17}]', NULL),
(35, 17, 14, 'Food', NULL, 'Hh', '[{"size":"medium","price":14},{"size":"large","price":17}]', NULL),
(36, 23, 14, 'Vine 1', NULL, 'Abcd', '[{"size":"medium","price":14},{"size":"large","price":17}]', NULL),
(38, 17, 14, 'New food', NULL, 'Vbbn', '[{"size":"medium","price":14},{"size":"large","price":17}]', '156169840631561698395348.jpg'),
(42, 25, 22, 'Fish and chips ', NULL, 'Fresh fish and potato chips ', '[{"size":"medium","price":14},{"size":"large","price":17}]', NULL),
(46, 25, 22, 'Avocado Burger ', NULL, 'Enjoy our home made avocado burger with american cheese ', '[{"size":"large","price":7},{"size":"medium","price":5},{"size":"small","price":2}]', '156225376101562253667821.jpg'),
(47, 32, 22, 'Blueberry pancakes ', NULL, 'Buttermilk pancake to with blueberry and cinnamon', '[{"size":"large","price":7},{"size":"medium","price":5},{"size":"small","price":2}]', '156225409101562253811421.jpg'),
(48, 33, 22, 'Tiramisu', NULL, 'Tiramisu top with cherry and cinnamon ', '[{"size":"none","price":12}]', '156225423921562254121693.jpg'),
(49, 34, 22, 'Grape wine', NULL, 'Old grape wine 100 years old', '[{"size":"large","price":27},{"size":"medium","price":23},{"size":"small","price":20}]', '156225528441562254280834.jpg'),
(50, 34, 22, 'White wine', NULL, '12 calories', '[{"size":"large","price":7},{"size":"medium","price":5},{"size":"small","price":2}]', '156225550111562255330059.jpg'),
(51, 25, 22, 'BBQ Pork', NULL, 'Garlic bbq pork  season to perfection ', '[{"size":"large","price":6},{"size":"medium","price":5},{"size":"small","price":2}]', '156225563821562255531500.jpg'),
(52, 32, 22, 'Toast and Eggs', NULL, 'Eggs and toast  with garnish tomato ', '[{"size":"large","price":7},{"size":"medium","price":5},{"size":"small","price":2}]', '156225573241562255671190.jpg'),
(53, 34, 22, 'Wine and cheese', NULL, 'Wine and cheese with smoke ham', '[{"size":"large","price":6},{"size":"medium","price":5},{"size":"small","price":2}]', '156225595241562255841915.jpg'),
(56, 42, 14, 'Test new', NULL, 'Fghh', '[{"size":"none","price":1.6},{"size":"medium","price":1.7}]', NULL),
(65, 17, 14, 'Abcd', NULL, 'Hhjj', '[{"size":"none","price":1.6}]', '156257957241562579562781.jpg'),
(66, 33, 22, 'Chocolate Sunday ', NULL, 'Rich chocolate tasty goodness', '[{"size":"none","price":12.5}]', '156238012141562380041901.jpg'),
(69, 41, 14, 'Darubaaz', NULL, 'Ggg', '[{"size":"none","price":2}]', NULL),
(72, 50, 12, 'Pasta', NULL, 'Cheese', '[{"size":"none","price":1.5}]', NULL),
(73, 42, 14, 'Teat', NULL, 'Ffff', '[{"size":"none","price":25}]', NULL),
(74, 51, 12, 'Upma', NULL, 'Rava upma', '[{"size":"small","price":1}]', '156922932141569229260759.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `company_items_options`
--

CREATE TABLE `company_items_options` (
  `id` int(11) NOT NULL,
  `company_item_id` int(11) NOT NULL,
  `name` varchar(75) NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_items_options`
--

INSERT INTO `company_items_options` (`id`, `company_item_id`, `name`, `price`) VALUES
(7, 2, 'Extra 3', '2.00'),
(8, 3, 'Base', '2.00'),
(9, 3, 'Cat', '1.00'),
(10, 4, 'Exta', '1.00'),
(13, 4, 'Extra 2', '23.00'),
(17, 11, 'With tea', '2.00'),
(19, 14, 'Extra 1', '2.00'),
(20, 15, 'Whip cream', '4.00'),
(21, 18, 'Vinegar', '1.00'),
(22, 14, 'Size large', '8.00'),
(23, 14, 'Size small', '5.00'),
(24, 14, 'Size medium', '6.00'),
(33, 38, 'Exta', '8.50'),
(35, 42, 'Salad ', '5.00'),
(36, 42, 'Milk', '4.00'),
(43, 35, 'Coke', '0.00'),
(45, 38, 'Coke', '2.50'),
(46, 38, 'Pepsi', '0.00'),
(47, 47, 'Whip cream', '4.00'),
(48, 47, 'Apple cuts', '6.00'),
(49, 47, 'Extra pancakes', '3.00'),
(50, 34, 'Coke', '2.00'),
(53, 56, 'Coke', '2.00'),
(54, 56, 'Pepsi', '0.00'),
(55, 69, 'Chakana', '2.00'),
(56, 71, 'bbbbb', '1.98'),
(57, 71, 'aaaa', '1.25'),
(58, 2, 'Extra 4', '2.00'),
(59, 73, 'Gghg', '25.00'),
(60, 74, 'Abc', '2.55');

-- --------------------------------------------------------

--
-- Table structure for table `company_subscriptions`
--

CREATE TABLE `company_subscriptions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `subs_type` enum('free','paid') NOT NULL DEFAULT 'free',
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `sortname` varchar(3) NOT NULL,
  `name` varchar(100) NOT NULL,
  `countrycode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `sortname`, `name`, `countrycode`) VALUES
(1, 'US', 'United States', 1),
(2, 'AL', 'Albania', 355),
(3, 'DZ', 'Algeria', 213),
(4, 'AS', 'American Samoa', 1684),
(5, 'AD', 'Andorra', 376),
(6, 'AO', 'Angola', 244),
(7, 'AI', 'Anguilla', 1264),
(8, 'AQ', 'Antarctica', 0),
(9, 'AG', 'Antigua And Barbuda', 1268),
(10, 'AR', 'Argentina', 54),
(11, 'AM', 'Armenia', 374),
(12, 'AW', 'Aruba', 297),
(13, 'AU', 'Australia', 61),
(14, 'AT', 'Austria', 43),
(15, 'AZ', 'Azerbaijan', 994),
(16, 'BS', 'Bahamas The', 1242),
(17, 'BH', 'Bahrain', 973),
(18, 'BD', 'Bangladesh', 880),
(19, 'BB', 'Barbados', 1246),
(20, 'BY', 'Belarus', 375),
(21, 'BE', 'Belgium', 32),
(22, 'BZ', 'Belize', 501),
(23, 'BJ', 'Benin', 229),
(24, 'BM', 'Bermuda', 1441),
(25, 'BT', 'Bhutan', 975),
(26, 'BO', 'Bolivia', 591),
(27, 'BA', 'Bosnia and Herzegovina', 387),
(28, 'BW', 'Botswana', 267),
(29, 'BV', 'Bouvet Island', 0),
(30, 'BR', 'Brazil', 55),
(31, 'IO', 'British Indian Ocean Territory', 246),
(32, 'BN', 'Brunei', 673),
(33, 'BG', 'Bulgaria', 359),
(34, 'BF', 'Burkina Faso', 226),
(35, 'BI', 'Burundi', 257),
(36, 'KH', 'Cambodia', 855),
(37, 'CM', 'Cameroon', 237),
(38, 'CA', 'Canada', 1),
(39, 'CV', 'Cape Verde', 238),
(40, 'KY', 'Cayman Islands', 1345),
(41, 'CF', 'Central African Republic', 236),
(42, 'TD', 'Chad', 235),
(43, 'CL', 'Chile', 56),
(44, 'CN', 'China', 86),
(45, 'CX', 'Christmas Island', 61),
(46, 'CC', 'Cocos (Keeling) Islands', 672),
(47, 'CO', 'Colombia', 57),
(48, 'KM', 'Comoros', 269),
(49, 'CG', 'Republic Of The Congo', 242),
(50, 'CD', 'Democratic Republic Of The Congo', 242),
(51, 'CK', 'Cook Islands', 682),
(52, 'CR', 'Costa Rica', 506),
(53, 'CI', 'Cote D\'Ivoire (Ivory Coast)', 225),
(54, 'HR', 'Croatia (Hrvatska)', 385),
(55, 'CU', 'Cuba', 53),
(56, 'CY', 'Cyprus', 357),
(57, 'CZ', 'Czech Republic', 420),
(58, 'DK', 'Denmark', 45),
(59, 'DJ', 'Djibouti', 253),
(60, 'DM', 'Dominica', 1767),
(61, 'DO', 'Dominican Republic', 1809),
(62, 'TP', 'East Timor', 670),
(63, 'EC', 'Ecuador', 593),
(64, 'EG', 'Egypt', 20),
(65, 'SV', 'El Salvador', 503),
(66, 'GQ', 'Equatorial Guinea', 240),
(67, 'ER', 'Eritrea', 291),
(68, 'EE', 'Estonia', 372),
(69, 'ET', 'Ethiopia', 251),
(70, 'XA', 'External Territories of Australia', 61),
(71, 'FK', 'Falkland Islands', 500),
(72, 'FO', 'Faroe Islands', 298),
(73, 'FJ', 'Fiji Islands', 679),
(74, 'FI', 'Finland', 358),
(75, 'FR', 'France', 33),
(76, 'GF', 'French Guiana', 594),
(77, 'PF', 'French Polynesia', 689),
(78, 'TF', 'French Southern Territories', 0),
(79, 'GA', 'Gabon', 241),
(80, 'GM', 'Gambia The', 220),
(81, 'GE', 'Georgia', 995),
(82, 'DE', 'Germany', 49),
(83, 'GH', 'Ghana', 233),
(84, 'GI', 'Gibraltar', 350),
(85, 'GR', 'Greece', 30),
(86, 'GL', 'Greenland', 299),
(87, 'GD', 'Grenada', 1473),
(88, 'GP', 'Guadeloupe', 590),
(89, 'GU', 'Guam', 1671),
(90, 'GT', 'Guatemala', 502),
(91, 'XU', 'Guernsey and Alderney', 44),
(92, 'GN', 'Guinea', 224),
(93, 'GW', 'Guinea-Bissau', 245),
(94, 'GY', 'Guyana', 592),
(95, 'HT', 'Haiti', 509),
(96, 'HM', 'Heard and McDonald Islands', 0),
(97, 'HN', 'Honduras', 504),
(98, 'HK', 'Hong Kong S.A.R.', 852),
(99, 'HU', 'Hungary', 36),
(100, 'IS', 'Iceland', 354),
(101, 'IN', 'India', 91),
(102, 'ID', 'Indonesia', 62),
(103, 'IR', 'Iran', 98),
(104, 'IQ', 'Iraq', 964),
(105, 'IE', 'Ireland', 353),
(106, 'IL', 'Israel', 972),
(107, 'IT', 'Italy', 39),
(108, 'JM', 'Jamaica', 1876),
(109, 'JP', 'Japan', 81),
(110, 'XJ', 'Jersey', 44),
(111, 'JO', 'Jordan', 962),
(112, 'KZ', 'Kazakhstan', 7),
(113, 'KE', 'Kenya', 254),
(114, 'KI', 'Kiribati', 686),
(115, 'KP', 'Korea North', 850),
(116, 'KR', 'Korea South', 82),
(117, 'KW', 'Kuwait', 965),
(118, 'KG', 'Kyrgyzstan', 996),
(119, 'LA', 'Laos', 856),
(120, 'LV', 'Latvia', 371),
(121, 'LB', 'Lebanon', 961),
(122, 'LS', 'Lesotho', 266),
(123, 'LR', 'Liberia', 231),
(124, 'LY', 'Libya', 218),
(125, 'LI', 'Liechtenstein', 423),
(126, 'LT', 'Lithuania', 370),
(127, 'LU', 'Luxembourg', 352),
(128, 'MO', 'Macau S.A.R.', 853),
(129, 'MK', 'Macedonia', 389),
(130, 'MG', 'Madagascar', 261),
(131, 'MW', 'Malawi', 265),
(132, 'MY', 'Malaysia', 60),
(133, 'MV', 'Maldives', 960),
(134, 'ML', 'Mali', 223),
(135, 'MT', 'Malta', 356),
(136, 'XM', 'Man (Isle of)', 44),
(137, 'MH', 'Marshall Islands', 692),
(138, 'MQ', 'Martinique', 596),
(139, 'MR', 'Mauritania', 222),
(140, 'MU', 'Mauritius', 230),
(141, 'YT', 'Mayotte', 269),
(142, 'MX', 'Mexico', 52),
(143, 'FM', 'Micronesia', 691),
(144, 'MD', 'Moldova', 373),
(145, 'MC', 'Monaco', 377),
(146, 'MN', 'Mongolia', 976),
(147, 'MS', 'Montserrat', 1664),
(148, 'MA', 'Morocco', 212),
(149, 'MZ', 'Mozambique', 258),
(150, 'MM', 'Myanmar', 95),
(151, 'NA', 'Namibia', 264),
(152, 'NR', 'Nauru', 674),
(153, 'NP', 'Nepal', 977),
(154, 'AN', 'Netherlands Antilles', 599),
(155, 'NL', 'Netherlands The', 31),
(156, 'NC', 'New Caledonia', 687),
(157, 'NZ', 'New Zealand', 64),
(158, 'NI', 'Nicaragua', 505),
(159, 'NE', 'Niger', 227),
(160, 'NG', 'Nigeria', 234),
(161, 'NU', 'Niue', 683),
(162, 'NF', 'Norfolk Island', 672),
(163, 'MP', 'Northern Mariana Islands', 1670),
(164, 'NO', 'Norway', 47),
(165, 'OM', 'Oman', 968),
(166, 'PK', 'Pakistan', 92),
(167, 'PW', 'Palau', 680),
(168, 'PS', 'Palestinian Territory Occupied', 970),
(169, 'PA', 'Panama', 507),
(170, 'PG', 'Papua new Guinea', 675),
(171, 'PY', 'Paraguay', 595),
(172, 'PE', 'Peru', 51),
(173, 'PH', 'Philippines', 63),
(174, 'PN', 'Pitcairn Island', 0),
(175, 'PL', 'Poland', 48),
(176, 'PT', 'Portugal', 351),
(177, 'PR', 'Puerto Rico', 1787),
(178, 'QA', 'Qatar', 974),
(179, 'RE', 'Reunion', 262),
(180, 'RO', 'Romania', 40),
(181, 'RU', 'Russia', 70),
(182, 'RW', 'Rwanda', 250),
(183, 'SH', 'Saint Helena', 290),
(184, 'KN', 'Saint Kitts And Nevis', 1869),
(185, 'LC', 'Saint Lucia', 1758),
(186, 'PM', 'Saint Pierre and Miquelon', 508),
(187, 'VC', 'Saint Vincent And The Grenadines', 1784),
(188, 'WS', 'Samoa', 684),
(189, 'SM', 'San Marino', 378),
(190, 'ST', 'Sao Tome and Principe', 239),
(191, 'SA', 'Saudi Arabia', 966),
(192, 'SN', 'Senegal', 221),
(193, 'RS', 'Serbia', 381),
(194, 'SC', 'Seychelles', 248),
(195, 'SL', 'Sierra Leone', 232),
(196, 'SG', 'Singapore', 65),
(197, 'SK', 'Slovakia', 421),
(198, 'SI', 'Slovenia', 386),
(199, 'XG', 'Smaller Territories of the UK', 44),
(200, 'SB', 'Solomon Islands', 677),
(201, 'SO', 'Somalia', 252),
(202, 'ZA', 'South Africa', 27),
(203, 'GS', 'South Georgia', 0),
(204, 'SS', 'South Sudan', 211),
(205, 'ES', 'Spain', 34),
(206, 'LK', 'Sri Lanka', 94),
(207, 'SD', 'Sudan', 249),
(208, 'SR', 'Suriname', 597),
(209, 'SJ', 'Svalbard And Jan Mayen Islands', 47),
(210, 'SZ', 'Swaziland', 268),
(211, 'SE', 'Sweden', 46),
(212, 'CH', 'Switzerland', 41),
(213, 'SY', 'Syria', 963),
(214, 'TW', 'Taiwan', 886),
(215, 'TJ', 'Tajikistan', 992),
(216, 'TZ', 'Tanzania', 255),
(217, 'TH', 'Thailand', 66),
(218, 'TG', 'Togo', 228),
(219, 'TK', 'Tokelau', 690),
(220, 'TO', 'Tonga', 676),
(221, 'TT', 'Trinidad And Tobago', 1868),
(222, 'TN', 'Tunisia', 216),
(223, 'TR', 'Turkey', 90),
(224, 'TM', 'Turkmenistan', 7370),
(225, 'TC', 'Turks And Caicos Islands', 1649),
(226, 'TV', 'Tuvalu', 688),
(227, 'UG', 'Uganda', 256),
(228, 'UA', 'Ukraine', 380),
(229, 'AE', 'United Arab Emirates', 971),
(230, 'GB', 'United Kingdom', 44),
(231, 'AF', 'Afghanistan', 93),
(232, 'UM', 'United States Minor Outlying Islands', 1),
(233, 'UY', 'Uruguay', 598),
(234, 'UZ', 'Uzbekistan', 998),
(235, 'VU', 'Vanuatu', 678),
(236, 'VA', 'Vatican City State (Holy See)', 39),
(237, 'VE', 'Venezuela', 58),
(238, 'VN', 'Vietnam', 84),
(239, 'VG', 'Virgin Islands (British)', 1284),
(240, 'VI', 'Virgin Islands (US)', 1340),
(241, 'WF', 'Wallis And Futuna Islands', 681),
(242, 'EH', 'Western Sahara', 212),
(243, 'YE', 'Yemen', 967),
(244, 'YU', 'Yugoslavia', 38),
(245, 'ZM', 'Zambia', 260),
(246, 'ZW', 'Zimbabwe', 263);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `email` varchar(75) DEFAULT NULL,
  `profile_pic` varchar(100) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `user_id`, `name`, `phone`, `email`, `profile_pic`, `created`, `modified`) VALUES
(1, 3, 'Juie', '7147091406', 'jhb4498@yahoo.com', '155348082553_1553480819871.jpg', '2019-01-30 00:15:40', '2019-03-25 07:57:05'),
(2, 4, 'Randall', '6264349058', 'rbrown2022@gmail.com', NULL, '2019-01-30 02:36:50', '2019-01-30 02:36:50'),
(3, 6, 'Taylor', '9094770833', 'tesparza212@gmail.com', NULL, '2019-01-30 08:40:48', '2019-01-30 08:40:48'),
(4, 7, 'J', '5202442220', 'jamalnicholas4@gmail.com', NULL, '2019-01-31 08:16:37', '2019-02-03 09:41:13'),
(5, 8, 'Arun Saini', '9808565240', 'arunksaini07@gmail.com', '154500833878.jpg', '2019-01-31 10:35:50', '2019-01-31 11:40:40'),
(6, 10, 'Deadloop', '5622827947', 'nestorcarrer25@gmail.com', NULL, '2019-01-31 20:20:32', '2019-01-31 20:20:32'),
(7, 14, 'Amber', '3106548496', 'a_tidmore15@yahoo.com', NULL, '2019-02-10 10:07:21', '2019-02-10 10:07:21'),
(8, 16, 'Aaron ', '3236081652', 'sponceredbynike@aol.com', NULL, '2019-02-19 09:18:53', '2019-02-19 09:18:53'),
(9, 19, 'Alexis', '5623558864', 'galarzaalexis96@yahoo.com', NULL, '2019-03-05 09:47:11', '2019-03-05 09:47:11'),
(10, 20, 'Sunita', '5624893791', 'sunitainxs@hotmail.com', NULL, '2019-03-10 09:44:34', '2019-03-10 09:44:34'),
(11, 21, 'Americus J', '8184225705', 'americusjones@rocketmail.com', '155235091372_1552350882403.jpg', '2019-03-12 06:05:13', '2019-03-12 06:05:13'),
(12, 22, 'Priyaa', '9760284879', 'priya1234@gmail.com', '157674631974_1576746311926.jpg', '2019-03-15 16:36:02', '2019-12-19 01:05:19'),
(14, 24, 'Sarabjeet', '8279842724', 'sarab@gmail.com', '156163577721_1561635772491.jpg', '2019-03-27 00:23:36', '2019-06-27 17:12:57'),
(15, 30, 'Demi', '9717916332', 'gchu@gmail.com', NULL, '2019-04-04 00:15:17', '2019-04-04 00:15:17'),
(16, 34, 'Kenbar', '3236950506', 'kenbarr43@yahoo.com', '157176782563_1571767821426.jpg', '2019-04-18 11:31:28', '2019-10-22 11:10:25'),
(17, 47, 'Justin Brown', '3109017784', 'kenbarr43@yahoo.com', '155823190297_1558231899469.jpg', '2019-05-19 07:41:42', '2019-07-04 12:05:44'),
(18, 59, 'Jvhjj', '9393939300', 'ggh@gh.com', NULL, '2019-05-20 14:04:47', '2019-05-20 14:04:47'),
(19, 85, 'Popu', '9696969696', 'popu@yopmail.com', NULL, '2019-06-03 16:39:46', '2019-06-03 16:41:27'),
(20, 100, 'Vaibhu', '8126208777', 'joshi@yopmail.com', '156257962484_1562579621175.jpg', '2019-07-08 02:33:31', '2019-07-08 02:53:44'),
(21, 111, 'Inder', '8755697925', 'inder@yopmail.com', NULL, '2019-09-25 05:18:28', '2019-09-25 05:18:28'),
(22, 113, 'pankaj', '9638527410', 'pant@yopmail.com', '157612802983_1576128026941.jpg', '2019-12-11 21:20:02', '2019-12-11 21:20:29');

-- --------------------------------------------------------

--
-- Table structure for table `customer_booking`
--

CREATE TABLE `customer_booking` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `slot_id` int(11) DEFAULT NULL,
  `status` enum('pending','accepted','rejected','completed') DEFAULT 'pending',
  `profile_type` varchar(75) NOT NULL DEFAULT 'company',
  `table_id` int(11) DEFAULT NULL,
  `no_of_guest` int(11) DEFAULT NULL,
  `comment` text,
  `rating` enum('yes','no') NOT NULL DEFAULT 'no',
  `type` enum('wait','other') NOT NULL DEFAULT 'other',
  `date` date DEFAULT NULL,
  `payment_status` varchar(5) NOT NULL DEFAULT 'no',
  `pay_more` varchar(5) NOT NULL DEFAULT 'no',
  `order_type` varchar(25) DEFAULT NULL,
  `delivery_amount` decimal(10,2) DEFAULT NULL,
  `booking_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `is_booking_item` varchar(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_booking`
--

INSERT INTO `customer_booking` (`id`, `employee_id`, `company_id`, `customer_id`, `slot_id`, `status`, `profile_type`, `table_id`, `no_of_guest`, `comment`, `rating`, `type`, `date`, `payment_status`, `pay_more`, `order_type`, `delivery_amount`, `booking_amount`, `is_booking_item`, `created`, `modified`) VALUES
(162, NULL, 23, 16, NULL, 'rejected', 'other', NULL, NULL, NULL, 'no', 'wait', NULL, 'no', 'no', NULL, NULL, '0.00', NULL, '2019-07-04 12:08:11', '2019-07-04 12:08:11'),
(243, NULL, 12, 20, NULL, 'pending', 'other', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'pick', '10.00', '0.00', NULL, '2019-07-25 22:36:36', '2019-07-31 22:36:36'),
(258, NULL, 12, 14, 65, 'pending', 'company', 7, 5, NULL, 'no', 'other', '2019-08-05', 'yes', 'yes', 'reservation', NULL, '0.00', NULL, '2019-07-28 22:38:29', '2019-07-28 22:38:29'),
(259, NULL, 12, 14, 66, 'pending', 'company', 7, 5, NULL, 'no', 'other', '2019-08-05', 'yes', 'yes', 'reservation', NULL, '0.00', NULL, '2019-07-28 23:07:02', '2019-07-28 23:07:02'),
(280, NULL, 14, 12, 80, 'pending', 'company', 27, 2, NULL, 'yes', 'other', '2019-10-12', 'yes', 'yes', 'reservation', '0.00', '12.50', NULL, '2019-08-04 23:14:45', '2019-08-04 23:14:45'),
(282, NULL, 22, 16, NULL, 'rejected', 'company', NULL, NULL, NULL, 'no', 'wait', NULL, 'no', 'no', NULL, NULL, '0.00', NULL, '2019-08-05 20:43:36', '2019-08-05 20:43:36'),
(283, NULL, 22, 17, NULL, 'rejected', 'company', NULL, NULL, NULL, 'no', 'wait', NULL, 'no', 'no', NULL, NULL, '0.00', NULL, '2019-08-05 20:48:21', '2019-08-05 20:48:21'),
(288, NULL, 14, 12, NULL, 'rejected', 'company', NULL, NULL, NULL, 'no', 'wait', NULL, 'no', 'no', NULL, NULL, '0.00', NULL, '2019-08-06 00:01:42', '2019-08-06 00:01:42'),
(289, NULL, 14, 12, NULL, 'rejected', 'company', NULL, NULL, NULL, 'no', 'wait', NULL, 'no', 'no', NULL, NULL, '0.00', NULL, '2019-08-06 05:18:11', '2019-08-06 05:18:11'),
(290, NULL, 14, 14, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-08-06 05:47:00', '2019-08-06 05:47:00'),
(291, NULL, 14, 14, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-08-06 06:08:21', '2019-08-06 06:08:21'),
(295, NULL, 12, 14, 65, 'pending', 'company', 7, 5, NULL, 'no', 'other', '2019-08-12', 'yes', 'yes', 'reservation', '0.00', '6.60', NULL, '2019-08-07 16:07:38', '2019-08-07 16:07:38'),
(304, NULL, 22, 14, 76, 'pending', 'company', 18, 5, NULL, 'no', 'other', '2019-08-09', 'yes', 'yes', 'reservation', '0.00', '0.00', NULL, '2019-08-08 17:09:09', '2019-08-08 17:09:09'),
(307, 33, NULL, 14, 64, 'pending', 'room_employee', NULL, NULL, NULL, 'no', 'other', '2019-08-11', 'yes', 'yes', 'reservation', '0.00', '10.00', NULL, '2019-08-08 23:02:19', '2019-08-08 23:02:19'),
(308, NULL, 22, 14, 33, 'pending', 'company', 17, 5, NULL, 'no', 'other', '2019-08-10', 'yes', 'yes', 'reservation', '0.00', '0.00', 'No', '2019-08-08 23:18:46', '2019-08-08 23:18:46'),
(309, NULL, 14, 12, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-08-09 05:38:10', '2019-08-09 05:38:10'),
(310, 33, NULL, 14, 63, 'pending', 'room_employee', NULL, NULL, NULL, 'no', 'other', '2019-08-11', 'yes', 'yes', 'reservation', '0.00', '10.00', NULL, '2019-08-09 05:38:27', '2019-08-09 05:38:27'),
(311, NULL, 22, 14, 79, 'pending', 'company', 17, 5, NULL, 'no', 'other', '2019-08-09', 'yes', 'yes', 'reservation', '0.00', '0.00', 'No', '2019-08-09 05:53:09', '2019-08-09 05:53:09'),
(316, NULL, 22, 14, 50, 'pending', 'company', 18, 5, NULL, 'no', 'other', '2019-08-18', 'yes', 'yes', 'reservation', '0.00', '0.00', 'Yes', '2019-08-11 23:21:37', '2019-08-11 23:21:37'),
(317, 33, NULL, 14, 63, 'pending', 'room_employee', NULL, NULL, NULL, 'no', 'other', '2019-08-18', 'yes', 'yes', 'reservation', '0.00', '10.00', NULL, '2019-08-11 23:29:05', '2019-08-11 23:29:05'),
(321, NULL, 14, 12, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-08-13 02:01:34', '2019-08-13 02:01:34'),
(322, NULL, 14, 12, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-08-13 02:56:20', '2019-08-13 02:56:20'),
(324, NULL, 14, 12, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-08-13 03:10:08', '2019-08-13 03:10:08'),
(325, NULL, 12, 14, 73, 'pending', 'company', 5, 6, NULL, 'no', 'other', '2019-08-15', 'yes', 'yes', 'reservation', '0.00', '6.60', NULL, '2019-08-13 03:26:07', '2019-08-13 03:26:07'),
(326, 59, NULL, 14, 43, 'pending', 'room_employee', NULL, NULL, NULL, 'no', 'other', '2019-08-13', 'yes', 'yes', 'reservation', '0.00', '23.30', NULL, '2019-08-13 03:30:26', '2019-08-13 03:30:26'),
(327, 59, NULL, 14, 43, 'pending', 'room_employee', NULL, NULL, NULL, 'no', 'other', '2019-08-20', 'yes', 'yes', 'reservation', '0.00', '23.30', NULL, '2019-08-13 03:47:47', '2019-08-13 03:47:47'),
(330, NULL, 14, 12, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'deliver', '28.27', '0.00', NULL, '2019-08-13 05:27:37', '2019-08-13 05:27:37'),
(332, NULL, 14, 12, NULL, 'pending', 'company', NULL, NULL, NULL, 'yes', 'other', NULL, 'yes', 'yes', 'deliver', '28.27', '0.00', NULL, '2019-08-14 00:13:09', '2019-08-14 00:13:09'),
(333, NULL, 14, 12, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'deliver', '28.27', '0.00', NULL, '2019-08-14 02:06:04', '2019-08-14 02:06:04'),
(334, NULL, 12, 14, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-08-14 02:33:47', '2019-08-14 02:33:47'),
(335, NULL, 12, 14, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'deliver', '10.00', '0.00', NULL, '2019-08-14 02:36:17', '2019-08-14 02:36:17'),
(344, NULL, 12, 14, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-08-20 23:55:02', '2019-08-20 23:55:02'),
(346, NULL, 22, 16, NULL, 'completed', 'company', NULL, NULL, NULL, 'no', 'wait', NULL, 'no', 'no', NULL, NULL, '0.00', NULL, '2019-08-21 13:34:36', '2019-08-21 13:34:36'),
(350, NULL, 14, 12, NULL, 'pending', 'company', NULL, NULL, NULL, 'yes', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-08-22 02:35:20', '2019-08-22 02:35:20'),
(363, NULL, 14, 12, NULL, 'pending', 'company', NULL, NULL, NULL, 'yes', 'other', NULL, 'yes', 'yes', 'deliver', '28.27', '0.00', NULL, '2019-08-22 05:04:08', '2019-11-27 05:04:08'),
(364, NULL, 14, 12, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-08-22 05:20:03', '2019-08-22 05:20:55'),
(365, NULL, 22, 16, 78, 'pending', 'company', 30, 5, NULL, 'yes', 'other', '2019-08-23', 'yes', 'yes', 'reservation', '0.00', '0.00', NULL, '2019-08-22 11:15:45', '2019-08-22 11:21:08'),
(366, NULL, 12, 14, 19, 'pending', 'company', 7, 5, NULL, 'yes', 'other', '2019-09-26', 'yes', 'yes', 'reservation', '0.00', '0.00', NULL, '2019-08-23 03:36:57', '2019-08-23 03:38:49'),
(367, NULL, 12, 12, 19, 'pending', 'company', 5, 2, NULL, 'yes', 'other', '2019-10-11', 'yes', 'yes', 'reservation', '0.00', '6.00', NULL, '2019-08-23 03:51:05', '2019-08-23 03:53:11'),
(368, NULL, 22, 16, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-08-26 19:47:35', '2019-08-26 19:49:44'),
(375, NULL, 22, 16, 82, 'pending', 'company', 29, 2, NULL, 'no', 'other', '2019-09-04', 'yes', 'yes', 'reservation', '0.00', '0.00', 'No', '2019-09-03 21:44:16', '2019-09-03 21:48:09'),
(376, NULL, 22, 16, 87, 'pending', 'company', 29, 5, NULL, 'yes', 'other', '2019-09-04', 'yes', 'yes', 'reservation', '0.00', '15.75', NULL, '2019-09-04 13:24:23', '2019-09-04 15:01:05'),
(377, NULL, 14, 12, NULL, 'pending', 'company', NULL, NULL, NULL, 'yes', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-09-08 21:18:07', '2019-09-24 23:29:27'),
(380, 33, NULL, 16, 63, 'pending', 'room_employee', NULL, NULL, NULL, 'no', 'other', '2019-09-22', 'yes', 'yes', 'reservation', '0.00', '10.00', NULL, '2019-09-22 09:53:01', '2019-09-22 09:53:30'),
(383, NULL, 12, 14, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-09-22 23:50:29', '2019-10-09 21:25:25'),
(384, NULL, 14, 12, NULL, 'pending', 'company', NULL, NULL, NULL, 'yes', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-09-23 03:22:42', '2019-10-05 21:50:33'),
(385, NULL, 22, 16, 85, 'pending', 'company', 29, 5, NULL, 'no', 'other', '2019-09-25', 'yes', 'yes', 'reservation', '0.00', '15.75', NULL, '2019-09-25 10:12:51', '2019-09-25 10:13:26'),
(386, NULL, 22, 16, 88, 'pending', 'company', 29, 8, NULL, 'no', 'other', '2019-09-25', 'yes', 'yes', 'reservation', '0.00', '15.75', NULL, '2019-09-25 12:10:12', '2019-09-25 12:11:01'),
(387, NULL, 22, 16, 79, 'pending', 'company', 29, 5, NULL, 'yes', 'other', '2019-09-27', 'yes', 'yes', 'reservation', '0.00', '0.00', 'No', '2019-09-25 23:01:06', '2019-09-25 23:05:18'),
(391, NULL, 22, 16, 76, 'pending', 'company', 29, 5, NULL, 'no', 'other', '2019-10-04', 'yes', 'yes', 'reservation', '0.00', '0.00', NULL, '2019-10-03 08:28:14', '2019-10-03 10:52:38'),
(392, 33, NULL, 16, 36, 'pending', 'room_employee', NULL, NULL, NULL, 'no', 'other', '2019-10-03', 'yes', 'yes', 'reservation', '0.00', '10.00', NULL, '2019-10-03 09:29:52', '2019-10-03 09:30:19'),
(394, 33, NULL, 16, 102, 'pending', 'room_employee', NULL, NULL, NULL, 'yes', 'other', '2019-10-03', 'yes', 'yes', 'reservation', '0.00', '10.00', NULL, '2019-10-03 13:22:49', '2019-10-03 13:23:26'),
(395, 33, NULL, 16, 105, 'pending', 'room_employee', NULL, NULL, NULL, 'no', 'other', '2019-10-03', 'yes', 'yes', 'reservation', '0.00', '10.00', NULL, '2019-10-03 15:30:52', '2019-10-03 15:31:07'),
(400, NULL, 22, 16, NULL, 'rejected', 'company', NULL, NULL, NULL, 'no', 'wait', NULL, 'no', 'no', NULL, NULL, '0.00', NULL, '2019-10-10 10:10:29', '2019-10-10 10:10:29'),
(401, NULL, 22, 16, 76, 'pending', 'company', 29, 2, NULL, 'no', 'other', '2019-10-11', 'yes', 'yes', 'reservation', '0.00', '0.00', NULL, '2019-10-10 10:11:53', '2019-10-10 10:13:37'),
(402, NULL, 22, 16, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'deliver', '7.00', '0.00', NULL, '2019-10-10 10:13:49', '2019-10-10 10:15:22'),
(403, NULL, 22, 16, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-10-10 10:15:39', '2019-10-10 10:15:52'),
(405, 33, NULL, 16, 101, 'pending', 'room_employee', NULL, NULL, NULL, 'no', 'other', '2019-10-10', 'yes', 'yes', 'reservation', '0.00', '10.00', NULL, '2019-10-10 10:16:20', '2019-10-10 10:16:36'),
(406, 33, NULL, 16, 105, 'pending', 'room_employee', NULL, NULL, NULL, 'no', 'other', '2019-10-10', 'yes', 'yes', 'reservation', '0.00', '10.00', NULL, '2019-10-10 15:27:37', '2019-10-10 15:28:12'),
(408, 33, NULL, 16, 108, 'pending', 'room_employee', NULL, NULL, NULL, 'no', 'other', '2019-10-10', 'yes', 'yes', 'reservation', '0.00', '10.00', NULL, '2019-10-10 19:40:30', '2019-10-10 19:40:44'),
(409, 33, NULL, 16, 109, 'pending', 'room_employee', NULL, NULL, NULL, 'no', 'other', '2019-10-10', 'yes', 'yes', 'reservation', '0.00', '10.00', NULL, '2019-10-10 20:28:49', '2019-10-10 20:29:02'),
(413, 33, NULL, 16, 103, 'pending', 'room_employee', NULL, NULL, NULL, 'no', 'other', '2019-10-17', 'yes', 'yes', 'reservation', '0.00', '10.00', NULL, '2019-10-17 09:24:58', '2019-10-17 09:28:59'),
(414, NULL, 22, 16, 78, 'pending', 'company', 34, 6, NULL, 'no', 'other', '2019-10-18', 'yes', 'yes', 'reservation', '0.00', '0.00', NULL, '2019-10-17 11:21:10', '2019-10-17 11:23:57'),
(415, 33, NULL, 16, 100, 'pending', 'room_employee', NULL, NULL, NULL, 'no', 'other', '2019-10-17', 'yes', 'yes', 'reservation', '0.00', '10.00', NULL, '2019-10-17 11:43:43', '2019-10-17 11:46:47'),
(417, NULL, 14, 12, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-10-18 06:13:46', '2019-11-28 06:14:20'),
(419, NULL, 22, 16, 26, 'pending', 'company', 18, 6, NULL, 'no', 'other', '2019-10-19', 'yes', 'yes', 'reservation', '0.00', '15.75', NULL, '2019-10-18 14:03:10', '2019-10-18 14:03:41'),
(421, NULL, 22, 17, 78, 'pending', 'company', 17, 5, NULL, 'no', 'other', '2019-10-25', 'yes', 'yes', 'reservation', '0.00', '15.75', NULL, '2019-10-24 09:41:00', '2019-10-24 09:41:38'),
(426, NULL, 22, 16, 50, 'pending', 'company', 29, 5, NULL, 'no', 'other', '2020-11-01', 'yes', 'yes', 'reservation', '0.00', '15.75', NULL, '2019-10-31 14:59:07', '2019-10-31 14:59:39'),
(427, NULL, 14, 12, NULL, 'rejected', 'company', NULL, NULL, NULL, 'no', 'wait', NULL, 'no', 'no', NULL, NULL, '0.00', NULL, '2019-11-01 02:18:40', '2019-11-01 02:18:40'),
(428, NULL, 14, 12, 13, 'pending', 'company', 27, 2, NULL, 'no', 'other', '2019-11-29', 'yes', 'yes', 'reservation', '0.00', '0.00', NULL, '2019-11-01 03:06:58', '2019-11-01 03:08:31'),
(429, NULL, 22, 17, 25, 'pending', 'company', 18, 5, NULL, 'no', 'other', '2019-11-02', 'yes', 'yes', 'reservation', '0.00', '15.75', NULL, '2019-11-01 13:16:43', '2019-11-01 13:17:26'),
(431, NULL, 22, 16, 57, 'pending', 'company', 18, 5, NULL, 'no', 'other', '2019-11-04', 'yes', 'yes', 'reservation', '0.00', '15.75', NULL, '2019-11-04 10:04:23', '2019-11-04 10:04:54'),
(432, 33, NULL, 17, 42, 'pending', 'room_employee', NULL, NULL, NULL, 'no', 'other', '2019-11-04', 'yes', 'yes', 'reservation', '0.00', '10.00', NULL, '2019-11-04 10:05:22', '2019-11-04 10:05:52'),
(433, NULL, 22, 17, 57, 'pending', 'company', 17, 5, NULL, 'no', 'other', '2019-11-04', 'yes', 'yes', 'reservation', '0.00', '15.75', NULL, '2019-11-04 10:06:27', '2019-11-04 10:06:42'),
(434, 33, NULL, 16, 94, 'pending', 'room_employee', NULL, NULL, NULL, 'no', 'other', '2019-11-04', 'yes', 'yes', 'reservation', '0.00', '10.00', NULL, '2019-11-04 10:07:22', '2019-11-04 10:07:37'),
(439, NULL, 22, 17, 61, 'pending', 'company', 29, 5, NULL, 'no', 'other', '2019-11-05', 'yes', 'yes', 'reservation', '0.00', '15.75', NULL, '2019-11-05 07:30:46', '2019-11-05 07:31:44'),
(440, NULL, 22, 17, 60, 'pending', 'company', 29, 5, NULL, 'no', 'other', '2019-11-05', 'yes', 'yes', 'reservation', '0.00', '15.75', NULL, '2019-11-05 07:34:07', '2019-11-05 07:34:23'),
(442, NULL, 14, 12, NULL, 'rejected', 'company', NULL, NULL, NULL, 'no', 'wait', NULL, 'no', 'no', NULL, NULL, '0.00', NULL, '2019-11-14 22:52:59', '2019-11-14 22:52:59'),
(443, NULL, 14, 12, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-11-29 04:45:26', '2019-11-29 04:48:50'),
(444, NULL, 14, 12, NULL, 'pending', 'company', NULL, NULL, NULL, 'yes', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-11-21 05:40:31', '2019-11-21 05:40:53'),
(445, NULL, 14, 12, 21, 'pending', 'company', 28, 2, NULL, 'yes', 'other', '2019-11-22', 'yes', 'yes', 'reservation', '0.00', '0.00', NULL, '2019-11-21 05:43:18', '2019-11-21 05:43:49'),
(455, 59, NULL, 12, 43, 'pending', 'company', NULL, NULL, NULL, 'yes', 'other', '2019-11-26', 'yes', 'yes', 'reservation', '0.00', '23.30', NULL, '2019-11-25 01:56:52', '2019-11-25 01:57:17'),
(457, NULL, 14, 12, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-11-25 02:18:17', '2019-11-27 02:42:35'),
(458, NULL, 14, 12, NULL, 'completed', 'company', NULL, NULL, NULL, 'no', 'wait', NULL, 'no', 'no', NULL, NULL, '0.00', NULL, '2019-11-25 22:15:30', '2019-11-25 22:15:30'),
(460, NULL, 14, 12, NULL, 'rejected', 'company', NULL, NULL, NULL, 'no', 'wait', NULL, 'no', 'no', NULL, NULL, '0.00', NULL, '2019-11-26 01:49:25', '2019-11-26 01:49:25'),
(466, NULL, 14, 12, 13, 'pending', 'company', 28, 2, 'birthday party', 'no', 'other', '2019-11-29', 'yes', 'yes', 'reservation', '0.00', '2.00', NULL, '2019-11-27 22:55:05', '2019-11-27 22:55:32'),
(469, NULL, 22, 17, 98, 'pending', 'company', 18, 5, 'Celebrating our 11the year anniversary \n', 'no', 'other', '2019-12-03', 'yes', 'yes', 'reservation', '0.00', '15.75', NULL, '2019-12-03 11:13:11', '2019-12-03 11:16:46'),
(471, 33, NULL, 17, 35, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', '2019-12-05', 'yes', 'yes', 'reservation', '0.00', '10.00', NULL, '2019-12-03 11:17:42', '2019-12-03 11:17:58'),
(472, NULL, 22, 17, 85, 'pending', 'company', 29, 5, NULL, 'no', 'other', '2019-12-04', 'yes', 'yes', 'reservation', '0.00', '15.75', NULL, '2019-12-04 09:56:35', '2019-12-04 09:57:03'),
(473, 33, NULL, 17, 103, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', '2019-12-05', 'yes', 'yes', 'reservation', '0.00', '10.00', NULL, '2019-12-04 12:55:21', '2019-12-04 12:55:58'),
(474, NULL, 22, 17, 88, 'pending', 'company', 29, 5, NULL, 'no', 'other', '2019-12-04', 'yes', 'yes', 'reservation', '0.00', '0.00', NULL, '2019-12-04 12:56:34', '2019-12-04 12:57:33'),
(475, NULL, 22, 17, 56, 'pending', 'company', 29, 8, 'Extra meat \n', 'no', 'other', '2019-12-09', 'yes', 'yes', 'reservation', '0.00', '15.75', NULL, '2019-12-08 14:36:59', '2019-12-08 14:37:27'),
(476, NULL, 22, 17, 27, 'pending', 'company', 29, 5, 'Hello\n', 'no', 'other', '2019-12-14', 'yes', 'yes', 'reservation', '0.00', '15.75', NULL, '2019-12-13 13:47:28', '2019-12-13 13:48:15'),
(478, NULL, 22, 17, 76, 'pending', 'company', 18, 45, NULL, 'no', 'other', '2019-12-27', 'yes', 'yes', 'reservation', '0.00', '15.75', NULL, '2019-12-25 18:18:28', '2019-12-25 18:19:00'),
(479, NULL, 14, 12, NULL, 'pending', 'company', NULL, NULL, NULL, 'no', 'other', NULL, 'yes', 'yes', 'pick up', '0.00', '0.00', NULL, '2019-12-26 21:03:47', '2019-12-26 21:05:23'),
(480, NULL, 22, 17, 77, 'pending', 'company', 18, 5, NULL, 'no', 'other', '2019-12-27', 'yes', 'yes', 'reservation', '0.00', '15.75', NULL, '2019-12-26 21:40:04', '2019-12-26 21:40:38'),
(481, NULL, 22, 17, 61, 'pending', 'company', 29, 8, NULL, 'no', 'other', '2020-01-07', 'yes', 'yes', 'reservation', '0.00', '0.00', NULL, '2020-01-06 12:35:18', '2020-01-06 12:36:42'),
(486, NULL, 22, 17, NULL, 'rejected', 'company', NULL, NULL, NULL, 'no', 'wait', NULL, 'no', 'no', NULL, NULL, '0.00', NULL, '2020-02-11 20:08:51', '2020-02-11 20:08:51'),
(497, NULL, 23, 17, NULL, 'rejected', 'company', NULL, NULL, 'Fhdjxjdu', 'no', 'wait', NULL, 'no', 'no', NULL, NULL, '0.00', NULL, '2020-03-12 20:18:33', '2020-03-12 20:18:44'),
(498, NULL, 22, 17, 60, 'pending', 'company', 29, 5, NULL, 'no', 'other', '2020-06-16', 'no', 'no', NULL, NULL, '0.00', NULL, '2020-05-16 18:56:55', '2020-05-16 18:56:55');

-- --------------------------------------------------------

--
-- Table structure for table `customer_delivery_address`
--

CREATE TABLE `customer_delivery_address` (
  `id` int(11) NOT NULL,
  `booking_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `address` varchar(125) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_delivery_address`
--

INSERT INTO `customer_delivery_address` (`id`, `booking_id`, `customer_id`, `address`, `created`, `modified`) VALUES
(9, 1, NULL, 'ekta vihar', '2019-07-12 02:28:06', '2019-07-12 02:28:06'),
(28, 190, 14, 'Gantaghar dehradun', '2019-07-15 05:07:03', '2019-07-15 05:07:03'),
(29, 217, 14, 'Clock tower dehradun', '2019-07-23 05:39:18', '2019-07-23 05:39:18'),
(33, 281, 12, 'Dehradun', '2019-08-05 03:02:13', '2019-08-05 03:02:13'),
(40, 291, 14, 'Clock tower,Dehradun,Uttarakhand', '2019-08-11 23:12:57', '2019-08-11 23:12:57'),
(42, 330, 12, 'It park,Dehradun,Uttarakhand', '2019-08-14 00:08:58', '2019-08-14 00:08:58'),
(43, 332, 12, 'Gantaghar,Dehradun,Uttarakhand', '2019-08-14 00:15:59', '2019-08-14 00:15:59'),
(44, 335, 14, 'Clocktower Dehradun Uttarakhand', '2019-08-14 02:42:24', '2019-08-14 02:42:24'),
(45, 333, 12, 'Gantaghar,Dehradun,Uttraranchal', '2019-08-14 18:31:22', '2019-08-14 18:31:22'),
(48, 363, 12, 'GantagharDunUttarakhand ', '2019-08-22 05:05:39', '2019-08-22 05:05:39'),
(49, 383, 14, 'Clock tower Dehradun 248001', '2019-09-26 06:01:43', '2019-09-26 06:01:43'),
(50, 402, 16, '24231 Crenshaw blvd Torrance 90505', '2019-10-10 10:15:07', '2019-10-10 10:15:07');

-- --------------------------------------------------------

--
-- Table structure for table `customer_payment`
--

CREATE TABLE `customer_payment` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `booking_id` int(11) DEFAULT NULL,
  `transaction_id` varchar(100) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `booking_amount` decimal(10,2) DEFAULT '0.00',
  `delivery_amount` decimal(10,2) DEFAULT NULL,
  `tax_amount` decimal(10,2) DEFAULT '0.00',
  `payment_status` varchar(25) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_payment`
--

INSERT INTO `customer_payment` (`id`, `customer_id`, `company_id`, `booking_id`, `transaction_id`, `amount`, `booking_amount`, `delivery_amount`, `tax_amount`, `payment_status`, `created`, `modified`) VALUES
(1, 14, 12, 187, 'PAY-4RT594400M804250BLUWBS5Y', '5.25', '0.00', '0.00', '0.00', 'yes', '2019-07-14 23:13:32', '2019-07-14 23:13:32'),
(2, 12, NULL, 201, 'PAY-5VG05145RP826910CLUYFHFA', '23.30', '0.00', '0.00', '0.00', 'yes', '2019-07-18 04:10:33', '2019-07-18 04:10:33'),
(3, 14, 12, 196, 'PAY-4LC654906Y1109055LUY3SXA', '2.10', '0.00', '0.00', '0.00', 'yes', '2019-07-19 05:37:05', '2019-07-19 05:37:05'),
(4, 14, NULL, 209, 'PAY-5J379086R3328561ULU22LJQ', '23.30', '0.00', '0.00', '0.00', 'yes', '2019-07-22 05:02:04', '2019-07-22 05:02:04'),
(5, 14, 12, 210, 'PAY-60E59531HX7494456LU22NJQ', '8.40', '0.00', '0.00', '0.00', 'yes', '2019-07-22 05:06:18', '2019-07-22 05:06:18'),
(6, 14, 12, 213, 'PAY-2FG53787E78222424LU3PPQQ', '4.20', '0.00', '0.00', '0.00', 'yes', '2019-07-23 05:04:40', '2019-07-23 05:04:40'),
(7, 14, 12, 214, 'PAY-6NR8769437308741MLU3PQJQ', '20.50', '0.00', '0.00', '0.00', 'yes', '2019-07-23 05:06:18', '2019-07-23 05:06:18'),
(8, 14, NULL, 215, 'PAY-40E14381619643318LU3P5SY', '5.00', '0.00', '0.00', '0.00', 'yes', '2019-07-23 05:34:37', '2019-07-23 05:34:37'),
(9, 14, 12, 205, 'PAY-6MR64749YD062305MLU3P6AQ', '1.05', '0.00', '0.00', '0.00', 'yes', '2019-07-23 05:35:33', '2019-07-23 05:35:33'),
(10, 14, 12, 216, 'PAY-82M57942AL0344915LU3P7AI', '5.25', '0.00', '0.00', '0.00', 'yes', '2019-07-23 05:37:41', '2019-07-23 05:37:41'),
(11, 14, 12, 217, 'PAY-7CL36514JC397734MLU3QAEY', '26.80', '0.00', '0.00', '0.00', 'yes', '2019-07-23 05:40:08', '2019-07-23 05:40:08'),
(12, 14, 12, 218, 'PAY-4WB275782H9429456LU3QHOA', '1.05', '0.00', '0.00', '0.00', 'yes', '2019-07-23 05:55:38', '2019-07-23 05:55:38'),
(13, 12, 14, 219, 'PAYID-LU37V6I6XE0859899762825V', '1.64', '0.00', '0.00', '0.00', 'yes', '2019-07-23 23:30:38', '2019-07-23 23:30:38'),
(14, 12, 14, 219, 'PAYID-LU37ZAQ8V964649WT5768224', '1.64', '0.00', '0.00', '0.00', 'yes', '2019-07-23 23:36:59', '2019-07-23 23:36:59'),
(15, 14, 14, 222, 'PAYID-LU4ALHI73257491097183837', '1.64', '0.00', '0.00', '0.00', 'yes', '2019-07-24 00:15:51', '2019-07-24 00:15:51'),
(16, 12, 14, 224, 'PAYID-LU4AODQ6RS22971GX082694H', '12.50', '0.00', '0.00', '0.00', 'yes', '2019-07-24 00:22:01', '2019-07-24 00:22:01'),
(17, 14, 12, 225, 'PAY-54K85539Y29481818LU4B3NQ', '2.10', '0.00', '0.00', '0.00', 'yes', '2019-07-24 01:58:50', '2019-07-24 01:58:50'),
(18, 14, 12, 227, 'PAY-3W391153TK203641DLU4B57Y', '2.10', '0.00', '0.00', '0.00', 'yes', '2019-07-24 02:04:21', '2019-07-24 02:04:21'),
(19, 12, 14, 229, 'PAYID-LU4TMRI7JW69946MN4221523', '12.50', '0.00', '0.00', '0.00', 'yes', '2019-07-24 21:55:41', '2019-07-24 21:55:41'),
(20, 12, 14, 229, 'PAYID-LU4TNNI8PL63467VJ928480T', '12.50', '0.00', '0.00', '0.00', 'yes', '2019-07-24 21:57:33', '2019-07-24 21:57:33'),
(21, 12, 14, 229, 'PAYID-LU4TOFQ7YW80881Y0875621T', '1.64', '0.00', '0.00', '0.00', 'yes', '2019-07-24 21:59:11', '2019-07-24 21:59:11'),
(22, 12, 14, 230, 'PAYID-LU4T2IY4RB746390C706122W', '5.65', '0.00', '0.00', '0.00', 'yes', '2019-07-24 22:24:59', '2019-07-24 22:24:59'),
(25, 12, 27, 231, 'PAYID-LU4VEYY6U564988PA280345Y', '23.30', '0.00', '0.00', '0.00', 'yes', '2019-07-24 23:55:38', '2019-07-24 23:55:38'),
(26, 12, 27, 231, '3ccfgfgvdf343434343434', '13.00', '0.00', '0.00', '0.00', 'yes', '2019-07-25 00:00:29', '2019-07-25 00:00:29'),
(27, 12, 27, 231, '3ccfgfgvdf343434343434', '13.00', '0.00', '0.00', '0.00', 'yes', '2019-07-25 00:01:21', '2019-07-25 00:01:21'),
(28, 12, 27, 231, '3ccfgfgvdf343434343434', '13.00', '0.00', '0.00', '0.00', 'yes', '2019-07-25 00:01:33', '2019-07-25 00:01:33'),
(29, 12, 27, 231, '3ccfgfgvdf343434343434', '13.00', '0.00', '0.00', '0.00', 'yes', '2019-07-25 00:04:57', '2019-07-25 00:04:57'),
(30, 12, 27, 231, '3ccfgfgvdf343434343434', '13.00', '0.00', '0.00', '0.00', 'yes', '2019-07-25 00:06:24', '2019-07-25 00:06:24'),
(31, 12, 27, 231, '3ccfgfgvdf343434343434', '13.00', '0.00', '0.00', '0.00', 'yes', '2019-07-25 00:07:34', '2019-07-25 00:07:34'),
(32, 12, 27, 231, '3ccfgfgvdf343434343434', '13.00', '0.00', '0.00', '0.00', 'yes', '2019-07-25 00:07:48', '2019-07-25 00:07:48'),
(33, 12, 27, 231, '3ccfgfgvdf343434343434', '13.00', '0.00', '0.00', '0.00', 'yes', '2019-07-25 00:09:01', '2019-07-25 00:09:01'),
(34, 12, 27, 231, '3ccfgfgvdf343434343434', '13.00', '0.00', '0.00', '0.00', 'yes', '2019-07-25 00:09:19', '2019-07-25 00:09:19'),
(35, 12, 27, 231, '3ccfgfgvdf343434343434', '13.00', '0.00', '0.00', '0.00', 'yes', '2019-07-25 00:09:38', '2019-07-25 00:09:38'),
(36, 12, 27, 231, '3ccfgfgvdf343434343434', '13.00', '0.00', '0.00', '0.00', 'yes', '2019-07-25 00:10:47', '2019-07-25 00:10:47'),
(37, 12, 14, 232, 'PAYID-LU4V6DY3SG5031950815425J', '12.50', '0.00', '0.00', '0.00', 'yes', '2019-07-25 00:49:43', '2019-07-25 00:49:43'),
(38, 12, 14, 190, 'PAYID-LU4XDAY1YW27889WK551270L', '5.75', '0.00', '0.00', '0.00', 'yes', '2019-07-25 02:08:27', '2019-07-25 02:08:27'),
(39, 12, 14, 233, 'PAYID-LU4XNBY57N485607T080470C', '1.64', '0.00', '0.00', '0.00', 'yes', '2019-07-25 02:30:11', '2019-07-25 02:30:11'),
(40, 12, 14, 234, 'PAYID-LU4XQHY7YS90083M28636026', '1.64', '0.00', '0.00', '0.00', 'yes', '2019-07-25 02:36:39', '2019-07-25 02:36:39'),
(49, 20, 12, 243, 'PAYID-LU5JDBI1MS2935133674380D', '1.58', '0.00', '0.00', '0.00', 'yes', '2019-07-25 22:37:17', '2019-07-25 22:37:17'),
(54, 12, 27, 248, 'PAYID-LU5NTMY3G9805680E180091A', '23.30', '0.00', '0.00', '0.00', 'yes', '2019-07-26 03:45:18', '2019-07-26 03:45:18'),
(55, 12, 27, 247, 'PAYID-LU5NU5I1VG99548S31170011', '23.30', '0.00', '0.00', '0.00', 'yes', '2019-07-26 03:48:31', '2019-07-26 03:48:31'),
(67, 14, 12, 258, 'PAY-3DY84910SA4331440LU7IXJA', '1.05', '0.00', '0.00', '0.00', 'yes', '2019-07-28 23:01:28', '2019-07-28 23:01:28'),
(68, 14, 12, 259, 'PAY-1KR51405M7209131VLU7I2FA', '6.60', '0.00', '0.00', '0.00', 'yes', '2019-07-28 23:07:37', '2019-07-28 23:07:37'),
(69, 14, 12, 259, 'PAY-0HN72887L8501213JLU7I2RA', '1.05', '0.00', '0.00', '0.00', 'yes', '2019-07-28 23:08:25', '2019-07-28 23:08:25'),
(88, 12, 14, 280, 'PAYID-LVD4S6Y29U24263XA344602S', '12.50', '12.50', NULL, '0.00', 'yes', '2019-08-04 23:15:34', '2019-08-04 23:15:34'),
(91, 14, 14, 290, 'PAY-7KJ586826U303714LLVEXQHA', '1.64', '0.00', NULL, '0.00', 'yes', '2019-08-06 05:53:05', '2019-08-06 05:53:05'),
(93, 14, 12, 295, 'PAY-2FW585668S328843CLVFKT7Y', '6.60', '6.60', NULL, '0.00', 'yes', '2019-08-07 16:08:12', '2019-08-07 16:08:12'),
(98, 14, 22, 304, 'PAY-69S053484R718203GLVGAYMQ', '8.11', '0.00', NULL, '0.00', 'yes', '2019-08-08 17:19:25', '2019-08-08 17:19:25'),
(100, 14, 23, 307, 'PAY-45H47703V9516083FLVGWOAA', '10.00', '10.00', NULL, '0.00', 'yes', '2019-08-09 05:29:10', '2019-08-09 05:29:10'),
(101, 14, 22, 308, 'PAY-6R227896PD810932GLVGWPCI', '15.75', '15.75', NULL, '0.00', 'yes', '2019-08-09 05:31:27', '2019-08-09 05:31:27'),
(102, 14, 23, 310, 'PAY-82725662TB694724DLVGWSUI', '10.00', '10.00', NULL, '0.00', 'yes', '2019-08-09 05:39:03', '2019-08-09 05:39:03'),
(103, 14, 22, 308, 'PAY-1G1857262N392452ULVGWUDQ', '6.27', '0.00', NULL, '0.00', 'yes', '2019-08-09 05:42:11', '2019-08-09 05:42:11'),
(104, 12, 14, 309, 'PAYID-LVGWYFA72S31385R2210323M', '1.64', '0.00', NULL, '0.00', 'yes', '2019-08-09 05:50:45', '2019-08-09 05:50:45'),
(105, 14, 22, 311, 'PAY-7TK22683F8679845CLVGWZ3Y', '15.75', '15.75', NULL, '0.00', 'yes', '2019-08-09 05:54:29', '2019-08-09 05:54:29'),
(106, 14, 22, 311, 'PAY-0RP54217U7152484GLVGW4MY', '1.63', '0.00', NULL, '0.00', 'yes', '2019-08-09 05:59:53', '2019-08-09 05:59:53'),
(108, 14, 22, 316, 'PAY-1KP74036HF3569901LVIQL4I', '3.48', '0.00', NULL, '0.00', 'yes', '2019-08-11 23:24:05', '2019-08-11 23:24:05'),
(109, 14, 22, 316, 'PAY-25Y98257EM149813LLVIQNOQ', '8.11', '0.00', NULL, '0.00', 'yes', '2019-08-11 23:27:24', '2019-08-11 23:27:24'),
(110, 14, 22, 316, 'PAY-2PS96700N2098840RLVIQOXY', '8.11', '0.00', NULL, '0.00', 'yes', '2019-08-11 23:30:09', '2019-08-11 23:30:09'),
(111, 14, 23, 317, 'PAY-4KC82391VD430492WLVIQPPQ', '10.00', '10.00', NULL, '0.00', 'yes', '2019-08-11 23:31:48', '2019-08-11 23:31:48'),
(112, 12, 14, 321, 'PAYID-LVJHZEI8S06678258812903L', '3.70', '0.00', NULL, '0.00', 'yes', '2019-08-13 02:02:17', '2019-08-13 02:02:17'),
(113, 12, 14, 322, 'PAYID-LVJISXY56T23336T6775535X', '1.64', '0.00', NULL, '0.00', 'yes', '2019-08-13 02:56:55', '2019-08-13 02:56:55'),
(114, 12, 14, 324, 'PAYID-LVJIZCA97L80614JU028491T', '8.21', '0.00', NULL, '0.00', 'yes', '2019-08-13 03:10:29', '2019-08-13 03:10:29'),
(115, 14, 12, 325, 'PAY-1WP21763A06407339LVJJAZY', '6.60', '6.60', NULL, '0.00', 'yes', '2019-08-13 03:27:08', '2019-08-13 03:27:08'),
(116, 14, 27, 326, 'PAY-39T46596GB357924JLVJJCXI', '23.30', '23.30', NULL, '0.00', 'yes', '2019-08-13 03:31:12', '2019-08-13 03:31:12'),
(117, 14, 27, 327, 'PAY-7H004060WU814710NLVJJOVI', '23.30', '23.30', NULL, '0.00', 'yes', '2019-08-13 03:56:44', '2019-08-13 03:56:44'),
(119, 12, 14, 330, 'PAYID-LVJ3HRQ2AE900832E8027415', '29.91', '0.00', '28.27', '0.00', 'yes', '2019-08-14 00:10:09', '2019-08-14 00:10:09'),
(120, 12, 14, 332, 'PAYID-LVJ3MCI7VS99920AT579352D', '29.91', '0.00', '28.27', '0.00', 'yes', '2019-08-14 00:19:47', '2019-08-14 00:19:47'),
(121, 14, 12, 334, 'PAY-4B737200EH158112RLVJ5LGI', '12.60', '0.00', NULL, '0.00', 'yes', '2019-08-14 02:34:39', '2019-08-14 02:34:39'),
(122, 14, 12, 335, 'PAY-3BE433494L187161LLVJ5PFQ', '25.75', '0.00', '10.00', '0.00', 'yes', '2019-08-14 02:43:07', '2019-08-14 02:43:07'),
(123, 12, 14, 333, 'PAYID-LVKLMFI1T8477057H277150A', '29.91', '0.00', '28.27', '0.00', 'yes', '2019-08-14 18:32:26', '2019-08-14 18:32:26'),
(128, 14, 12, 344, 'PAY-1C336319RJ314805DLVPCWPA', '7.88', '0.00', NULL, '0.00', 'yes', '2019-08-21 22:42:42', '2019-08-21 22:42:42'),
(129, 14, 14, 291, 'PAY-575000938D815270TLVPF5OI', '1.64', '0.00', NULL, '0.00', 'yes', '2019-08-22 02:22:24', '2019-08-22 02:22:24'),
(130, 12, 14, 350, 'PAYID-LVPGEMA8NN445449K030823W', '3.70', '0.00', NULL, '0.00', 'yes', '2019-08-22 02:37:10', '2019-08-22 02:37:10'),
(131, 12, 14, 363, 'PAYID-LVPIKJY69M32250K60418635', '29.91', '0.00', '28.27', '0.00', 'yes', '2019-08-22 05:06:07', '2019-08-22 05:06:07'),
(132, 12, 14, 364, 'PAYID-LVPIRGQ2HH246548A960540K', '2.05', '0.00', NULL, '0.00', 'yes', '2019-08-22 05:20:55', '2019-08-22 05:20:55'),
(133, 16, 22, 365, 'PAYID-LVPN2BI6X964548Y3226735E', '128.07', '0.00', NULL, '0.00', 'yes', '2019-08-22 11:21:08', '2019-08-22 11:21:08'),
(134, 14, 12, 366, 'PAY-6FJ8385439516993YLVP4EIY', '1.57', '0.00', NULL, '0.00', 'yes', '2019-08-23 03:38:49', '2019-08-23 03:38:49'),
(135, 12, 12, 367, 'PAYID-LVP4KSI2R729389N3189562E', '6.00', '6.00', NULL, '0.00', 'yes', '2019-08-23 03:53:11', '2019-08-23 03:53:11'),
(136, 16, 22, 368, 'PAYID-LVSJUPI19386565NS5845505', '28.98', '0.00', NULL, '0.00', 'yes', '2019-08-26 19:49:44', '2019-08-26 19:49:44'),
(137, 16, 22, 375, 'PAYID-LVXUDDI18302920X8132423P', '15.75', '15.75', NULL, '0.00', 'yes', '2019-09-03 21:46:15', '2019-09-03 21:46:15'),
(138, 16, 22, 375, 'PAYID-LVXUEAA99785827PL8445641', '0.48', '0.00', NULL, '0.00', 'yes', '2019-09-03 21:48:09', '2019-09-03 21:48:09'),
(139, 16, 22, 376, 'PAYID-LVYDICY2ME59408YD025130V', '15.75', '15.75', NULL, '0.00', 'yes', '2019-09-04 15:01:05', '2019-09-04 15:01:05'),
(140, 12, 14, 377, 'PAYID-LWBSAPY2A9212889X5422704', '29.87', '0.00', NULL, '0.00', 'yes', '2019-09-18 23:29:27', '2019-09-18 23:29:27'),
(141, 16, 23, 380, 'PAYID-LWD2N7Q0LA80156BC0190548', '10.00', '10.00', NULL, '0.00', 'yes', '2019-09-22 09:53:30', '2019-09-22 09:53:30'),
(142, 16, 22, 385, 'PAYID-LWF2AKQ1VX694691K564471H', '15.75', '15.75', NULL, '0.00', 'yes', '2019-09-25 10:13:26', '2019-09-25 10:13:26'),
(143, 16, 22, 386, 'PAYID-LWF3XPA5MT29358MX1056426', '15.75', '15.75', NULL, '0.00', 'yes', '2019-09-25 12:11:01', '2019-09-25 12:11:01'),
(144, 16, 22, 387, 'PAYID-LWGFIZY8XM87888C31311434', '15.75', '15.75', NULL, '0.00', 'yes', '2019-09-25 23:02:24', '2019-09-25 23:02:24'),
(145, 16, 22, 387, 'PAYID-LWGFKFQ4TB83978VP149352R', '1.63', '0.00', NULL, '0.00', 'yes', '2019-09-25 23:05:18', '2019-09-25 23:05:18'),
(147, 16, 23, 392, 'PAYID-LWLCEEA0V9991206X525014Y', '10.00', '10.00', NULL, '0.00', 'yes', '2019-10-03 09:30:19', '2019-10-03 09:30:19'),
(148, 16, 22, 391, 'PAYID-LWLDKVA7H1368406F052062X', '77.07', '0.00', NULL, '0.00', 'yes', '2019-10-03 10:52:38', '2019-10-03 10:52:38'),
(149, 16, 23, 394, 'PAYID-LWLFRNA4E189158L4697301S', '10.00', '10.00', NULL, '0.00', 'yes', '2019-10-03 13:23:26', '2019-10-03 13:23:26'),
(150, 16, 23, 395, 'PAYID-LWLHNIQ05G53947AL097624V', '10.00', '10.00', NULL, '0.00', 'yes', '2019-10-03 15:31:07', '2019-10-03 15:31:07'),
(151, 14, 12, 383, 'PAY-4L411295WW514044NLWPLFIQ', '10.61', '0.00', NULL, '0.00', 'yes', '2019-10-09 21:25:25', '2019-10-09 21:25:25'),
(152, 12, 14, 384, 'PAYID-LWPLRCY6YB82446851797932', '72.78', '0.00', NULL, '0.00', 'yes', '2019-10-09 21:50:33', '2019-10-09 21:50:33'),
(153, 16, 22, 401, 'PAYID-LWPWNOA54714864N2969493N', '49.26', '0.00', NULL, '0.00', 'yes', '2019-10-10 10:13:37', '2019-10-10 10:13:37'),
(154, 16, 22, 402, 'PAYID-LWPWOII1YS13151K5432244E', '20.91', '0.00', '7.00', '0.00', 'yes', '2019-10-10 10:15:22', '2019-10-10 10:15:22'),
(155, 16, 22, 403, 'PAYID-LWPWOQA3T828770DP2735323', '6.95', '0.00', NULL, '0.00', 'yes', '2019-10-10 10:15:52', '2019-10-10 10:15:52'),
(156, 16, 23, 405, 'PAYID-LWPWO3A8XL153006H3064013', '10.00', '10.00', NULL, '0.00', 'yes', '2019-10-10 10:16:36', '2019-10-10 10:16:36'),
(157, 16, 23, 406, 'PAYID-LWP3A4A059320961H893902C', '10.00', '10.00', NULL, '0.00', 'yes', '2019-10-10 15:28:12', '2019-10-10 15:28:12'),
(159, 16, 23, 408, 'PAYID-LWP6XJA9TL14930MA4787046', '10.00', '10.00', NULL, '0.00', 'yes', '2019-10-10 19:40:44', '2019-10-10 19:40:44'),
(160, 16, 23, 409, 'PAYID-LWP7N5Q69E144057C1766423', '10.00', '10.00', NULL, '0.00', 'yes', '2019-10-10 20:29:02', '2019-10-10 20:29:02'),
(161, 16, 23, 413, 'PAYID-LWUJNRA35N937224X780833C', '10.00', '10.00', NULL, '0.00', 'yes', '2019-10-17 09:28:59', '2019-10-17 09:28:59'),
(162, 16, 22, 414, 'PAYID-LWULDMY44N69690YU173135H', '77.65', '0.00', NULL, '0.00', 'yes', '2019-10-17 11:23:57', '2019-10-17 11:23:57'),
(163, 16, 23, 415, 'PAYID-LWULOEI27D83758EJ921012Y', '10.00', '10.00', NULL, '0.00', 'yes', '2019-10-17 11:46:47', '2019-10-17 11:46:47'),
(164, 12, 14, 417, 'PAYID-LWU3VJQ7U347530V09483905', '25.66', '0.00', NULL, '0.00', 'yes', '2019-10-18 06:14:20', '2019-10-18 06:14:20'),
(165, 16, 22, 419, 'PAYID-LWVCRJY9DL7688023780551D', '15.75', '15.75', NULL, '0.00', 'yes', '2019-10-18 14:03:41', '2019-10-18 14:03:41'),
(166, 17, 22, 421, 'PAYID-LWY5IOY4J906465SX8015824', '15.75', '15.75', NULL, '0.00', 'yes', '2019-10-24 09:41:38', '2019-10-24 09:41:38'),
(167, 16, 22, 426, 'PAYID-LW5VSQQ1MR37018MP2816221', '15.75', '15.75', NULL, '0.00', 'yes', '2019-10-31 14:59:39', '2019-10-31 14:59:39'),
(168, 12, 14, 428, 'PAYID-LW6AIGA1NN70663U17132639', '3.78', '0.00', NULL, '0.00', 'yes', '2019-11-01 03:08:31', '2019-11-01 03:08:31'),
(169, 17, 22, 429, 'PAYID-LW6JFSA70C81005R51286631', '15.75', '15.75', NULL, '0.00', 'yes', '2019-11-01 13:17:26', '2019-11-01 13:17:26'),
(170, 16, 22, 431, 'PAYID-LXAGQOY25T619704Y554363D', '15.75', '15.75', NULL, '0.00', 'yes', '2019-11-04 10:04:54', '2019-11-04 10:04:54'),
(171, 17, 23, 432, 'PAYID-LXAGQ6I0K732528U5965860K', '10.00', '10.00', NULL, '0.00', 'yes', '2019-11-04 10:05:52', '2019-11-04 10:05:52'),
(172, 17, 22, 433, 'PAYID-LXAGRLA0UD4719972782504R', '15.75', '15.75', NULL, '0.00', 'yes', '2019-11-04 10:06:42', '2019-11-04 10:06:42'),
(173, 16, 23, 434, 'PAYID-LXAGRYA3YL28861H5543700M', '10.00', '10.00', NULL, '0.00', 'yes', '2019-11-04 10:07:37', '2019-11-04 10:07:37'),
(174, 17, 22, 439, 'PAYID-LXAZLWI33D01401HK5501822', '15.75', '15.75', NULL, '0.00', 'yes', '2019-11-05 07:31:44', '2019-11-05 07:31:44'),
(175, 17, 22, 440, 'PAYID-LXAZM6I6LP44543JD9465711', '15.75', '15.75', NULL, '0.00', 'yes', '2019-11-05 07:34:23', '2019-11-05 07:34:23'),
(176, 12, 14, 443, 'PAYID-LXKTMKY09D33265L3997762W', '26.25', '0.00', NULL, '0.00', 'yes', '2019-11-20 04:48:50', '2019-11-20 04:48:50'),
(177, 12, 14, 444, 'PAYID-LXLJHXQ7NM69182UR496860K', '3.78', '0.00', NULL, '0.00', 'yes', '2019-11-21 05:40:53', '2019-11-21 05:40:53'),
(178, 12, 14, 445, 'PAYID-LXLJJDI6NU841793W221793S', '52.50', '0.00', NULL, '0.00', 'yes', '2019-11-21 05:43:49', '2019-11-21 05:43:49'),
(179, 12, 27, 448, 'PAYID-LXL3HDY4JU633978Y538432X', '23.30', '23.30', NULL, '0.00', 'yes', '2019-11-22 02:08:22', '2019-11-22 02:08:22'),
(180, 12, 27, 449, 'PAYID-LXL3QWQ7LE67060LV396682K', '23.30', '23.30', NULL, '0.00', 'yes', '2019-11-22 02:28:49', '2019-11-22 02:28:49'),
(181, 12, 27, 450, 'PAYID-LXNZYHI68R12363GW577043J', '23.30', '23.30', NULL, '0.00', 'yes', '2019-11-25 01:17:24', '2019-11-25 01:17:24'),
(182, 12, 27, 451, 'PAYID-LXNZ4QY7KM25813C3336424J', '23.30', '23.30', NULL, '0.00', 'yes', '2019-11-25 01:26:34', '2019-11-25 01:26:34'),
(183, 12, 27, 452, 'PAYID-LXN2D4A7G619970VL528493L', '23.30', '23.30', NULL, '0.00', 'yes', '2019-11-25 01:42:16', '2019-11-25 01:42:16'),
(184, 12, 27, 453, 'PAYID-LXN2FCI5SR08069TY9923012', '23.30', '23.30', NULL, '0.00', 'yes', '2019-11-25 01:44:47', '2019-11-25 01:44:47'),
(185, 12, 27, 454, 'PAYID-LXN2IUI2NL53448DF7341136', '23.30', '23.30', NULL, '0.00', 'yes', '2019-11-25 01:52:28', '2019-11-25 01:52:28'),
(186, 12, 27, 455, 'PAYID-LXN2K5Q4P805930VC5381903', '23.30', '23.30', NULL, '0.00', 'yes', '2019-11-25 01:57:17', '2019-11-25 01:57:17'),
(187, 12, 14, 457, 'PAYID-LXPFGEY67M062957S292211W', '5.88', '0.00', NULL, '0.00', 'yes', '2019-11-27 02:42:35', '2019-11-27 02:42:35'),
(188, 12, 14, 466, 'PAYID-LXPW6XI1EX99096A25051153', '2.00', '2.00', NULL, '0.00', 'yes', '2019-11-27 22:55:32', '2019-11-27 22:55:32'),
(189, 17, 22, 469, 'PAYID-LXTLIFA49R187594H0831057', '15.75', '15.75', NULL, '0.00', 'yes', '2019-12-03 11:16:46', '2019-12-03 11:16:46'),
(190, 17, 23, 471, 'PAYID-LXTLJYA26S51776BE902653C', '10.00', '10.00', NULL, '0.00', 'yes', '2019-12-03 11:17:58', '2019-12-03 11:17:58'),
(191, 17, 22, 472, 'PAYID-LXT7GZY3LM86176GJ034840T', '15.75', '15.75', NULL, '0.00', 'yes', '2019-12-04 09:57:03', '2019-12-04 09:57:03'),
(192, 17, 23, 473, 'PAYID-LXUB2VA3NW8323577049213C', '10.00', '10.00', NULL, '0.00', 'yes', '2019-12-04 12:55:58', '2019-12-04 12:55:58'),
(193, 17, 22, 474, 'PAYID-LXUB3NI49F30526G3949392D', '39.41', '0.00', NULL, '0.00', 'yes', '2019-12-04 12:57:33', '2019-12-04 12:57:33'),
(194, 17, 22, 475, 'PAYID-LXWXWII2CA50043N04803029', '15.75', '15.75', NULL, '0.00', 'yes', '2019-12-08 14:37:27', '2019-12-08 14:37:27'),
(195, 17, 22, 476, 'PAYID-LX2AOFY2L863678K11603510', '15.75', '15.75', NULL, '0.00', 'yes', '2019-12-13 13:48:15', '2019-12-13 13:48:15'),
(196, 17, 22, 478, 'PAYID-LYCBRCY45795609YE588851G', '15.75', '15.75', NULL, '0.00', 'yes', '2019-12-25 18:19:00', '2019-12-25 18:19:00'),
(197, 12, 14, 479, 'PAYID-LYCZCCI9SS3342513811220P', '26.25', '0.00', NULL, '0.00', 'yes', '2019-12-26 21:05:23', '2019-12-26 21:05:23'),
(198, 17, 22, 480, 'PAYID-LYCZSTY2EV43040WC408110U', '15.75', '15.75', NULL, '0.00', 'yes', '2019-12-26 21:40:38', '2019-12-26 21:40:38'),
(199, 17, 22, 481, 'PAYID-LYJZUUY2MX596979F5439443', '68.38', '0.00', NULL, '0.00', 'yes', '2020-01-06 12:36:42', '2020-01-06 12:36:42');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `about` text,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `profile_pic` varchar(100) DEFAULT NULL,
  `type` enum('employee','other') NOT NULL DEFAULT 'employee',
  `custom` enum('yes','no') NOT NULL DEFAULT 'no',
  `approve` varchar(5) NOT NULL DEFAULT 'yes',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `user_id`, `category_id`, `company_id`, `first_name`, `last_name`, `about`, `phone`, `email`, `profile_pic`, `type`, `custom`, `approve`, `created`, `modified`) VALUES
(3, 27, 5, 10, 'john', NULL, 'Hdudbdyd', NULL, NULL, NULL, 'other', 'yes', 'no', '2019-04-03 23:51:48', '2019-04-03 23:51:48'),
(33, 71, 1, 23, 'Ken', 'Akeem', 'Experience barber', '2094031507', '', '156020786066.jpg', 'employee', 'no', 'yes', '2019-05-29 07:45:37', '2019-06-11 04:34:20'),
(59, 97, 1, 27, 'rahul', 'tata', 'Vhvhhh', '9911991199', '', '156027506375.jpg', 'employee', 'no', 'yes', '2019-06-03 16:21:12', '2019-06-11 23:14:23'),
(60, 2, 1, 1, 'John', 'Albert', 'Exper barber', NULL, '', '155956755929.jpg', 'employee', 'yes', 'yes', '2019-06-03 18:42:39', '2019-06-03 18:42:39'),
(63, 98, 1, 23, 'Duval', 'Mariano', 'Talented artist ', '3107569826', '', '156022942761.jpg', 'employee', 'no', 'yes', '2019-06-11 10:33:47', '2019-06-11 10:37:07'),
(64, 48, 1, 27, 'Rastogi', 'simran', '', NULL, 'null', '156163401237.jpg', 'employee', 'yes', 'yes', '2019-06-18 18:23:41', '2019-11-26 23:59:07'),
(65, 101, 1, 35, 'Sarab 1', 'Kaur 1', 'Janaj', NULL, '', NULL, 'employee', 'yes', 'yes', '2019-07-22 05:22:00', '2019-07-22 05:22:00'),
(66, 112, 2, 38, 'room 1', NULL, '4 people', NULL, 'raju@yopmail.com', NULL, 'other', 'yes', 'yes', '2019-11-26 23:33:16', '2019-11-27 01:04:12'),
(67, 112, 2, 38, 'Jasvinder', 'Singh', 'null', NULL, 'raju@yopmail.com', NULL, 'employee', 'yes', 'yes', '2019-11-26 23:45:38', '2019-11-26 23:45:38'),
(70, 48, 1, 27, 'ABC', NULL, 'test', NULL, '', NULL, 'other', 'yes', 'yes', '2019-11-27 00:02:57', '2019-11-27 00:02:57'),
(71, 112, 2, 38, 'room 2', NULL, '', NULL, 'raju@yopmail.com', NULL, 'other', 'yes', 'yes', '2019-11-27 04:39:05', '2019-11-27 04:39:05'),
(72, 48, 1, 27, 'Jas', 'Singh', 'hair stylist', NULL, 'null', '157503136533.jpg', 'employee', 'yes', 'yes', '2019-11-28 05:45:25', '2019-11-29 04:43:25');

-- --------------------------------------------------------

--
-- Table structure for table `employee_images`
--

CREATE TABLE `employee_images` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `type` enum('employee','other') NOT NULL DEFAULT 'employee',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_images`
--

INSERT INTO `employee_images` (`id`, `employee_id`, `name`, `type`, `created`, `modified`) VALUES
(1, 71, '157485835728_71.jpg', 'other', '2019-11-27 04:39:17', '2019-11-27 04:39:17');

-- --------------------------------------------------------

--
-- Table structure for table `favorite`
--

CREATE TABLE `favorite` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `is_favorite` varchar(6) NOT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favorite`
--

INSERT INTO `favorite` (`id`, `customer_id`, `company_id`, `is_favorite`, `created`) VALUES
(1, 14, 10, 'no', '2019-04-16 05:39:05'),
(2, 14, 16, 'no', '2019-05-15 10:29:43'),
(3, 16, 22, 'yes', '2019-05-18 00:52:14'),
(4, 16, 23, 'yes', '2019-06-03 22:11:30'),
(5, 12, 14, 'yes', '2019-07-01 11:13:21'),
(6, 14, 12, 'no', '2019-07-01 12:15:02'),
(7, 12, 27, 'yes', '2019-07-03 12:35:13'),
(8, 12, 22, 'yes', '2019-07-04 22:08:19'),
(9, 20, 16, 'yes', '2019-07-08 03:07:18'),
(10, 20, 14, 'yes', '2019-07-08 03:07:33'),
(11, 20, 12, 'yes', '2019-07-08 03:07:38'),
(12, 12, 12, 'no', '2019-07-11 04:51:02'),
(13, 14, 14, 'yes', '2019-07-23 23:56:52'),
(14, 17, 22, 'yes', '2019-08-05 21:56:30'),
(15, 17, 23, 'yes', '2019-10-22 18:56:06');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `from_user_id` int(11) NOT NULL,
  `to_user_id` int(11) NOT NULL,
  `to_profile_id` int(11) DEFAULT NULL,
  `message` text,
  `type` varchar(50) DEFAULT NULL,
  `redirect_id` int(11) DEFAULT NULL,
  `mark_as` enum('read','unread') NOT NULL DEFAULT 'unread',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `from_user_id`, `to_user_id`, `to_profile_id`, `message`, `type`, `redirect_id`, `mark_as`, `created`, `modified`) VALUES
(1, 22, 48, NULL, 'reservation', 'room_employee', NULL, 'read', '2019-07-26 10:57:28', '2019-07-26 10:57:28'),
(3, 22, 48, NULL, 'reservation', 'room_employee', NULL, 'read', '2019-07-26 11:07:06', '2019-07-26 11:07:06'),
(4, 22, 35, NULL, 'reservation', 'company', NULL, 'read', '2019-07-26 11:15:55', '2019-07-26 11:15:55'),
(5, 22, 35, NULL, 'reservation', 'company', NULL, 'read', '2019-07-26 11:23:11', '2019-07-26 11:23:11'),
(6, 22, 48, NULL, 'reservation', 'room_employee', NULL, 'read', '2019-07-26 11:28:21', '2019-07-26 11:28:21'),
(7, 22, 48, NULL, 'reservation', 'room_employee', NULL, 'read', '2019-07-26 12:45:51', '2019-07-26 12:45:51'),
(8, 22, 48, NULL, 'reservation', 'room_employee', NULL, 'read', '2019-07-26 13:00:06', '2019-07-26 13:00:06'),
(10, 24, 35, NULL, 'reservation', 'company', NULL, 'read', '2019-07-29 06:01:28', '2019-07-29 06:01:28'),
(11, 24, 35, NULL, 'reservation', 'company', NULL, 'read', '2019-07-29 06:07:37', '2019-07-29 06:07:37'),
(12, 24, 35, NULL, 'reservation', 'company', NULL, 'read', '2019-07-29 06:08:25', '2019-07-29 06:08:25'),
(14, 34, 45, NULL, 'reservation', 'room_employee', NULL, 'read', '2019-07-29 16:55:29', '2019-07-29 16:55:29'),
(20, 22, 35, NULL, 'deliver', 'company', NULL, 'read', '2019-08-01 07:24:34', '2019-08-01 07:24:34'),
(32, 34, 44, NULL, 'Kenbar: Job rejected', 'company_waiting', NULL, 'read', '2019-08-06 04:54:06', '2019-08-06 04:54:06'),
(33, 34, 44, NULL, 'reservation', 'company', NULL, 'unread', '2019-08-06 04:54:58', '2019-08-06 04:54:58'),
(34, 47, 44, NULL, 'Justin Brown: Job rejected', 'company_waiting', NULL, 'unread', '2019-08-06 04:56:47', '2019-08-06 04:56:47'),
(43, 34, 45, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-08-07 16:27:32', '2019-08-07 16:27:32'),
(44, 34, 44, NULL, 'reservation', 'company', NULL, 'unread', '2019-08-07 16:42:51', '2019-08-07 16:42:51'),
(45, 34, 44, NULL, 'reservation', 'company', NULL, 'read', '2019-08-07 16:47:51', '2019-08-07 16:47:51'),
(47, 24, 44, NULL, 'reservation', 'company', NULL, 'unread', '2019-08-08 11:49:25', '2019-08-08 11:49:25'),
(48, 24, 44, NULL, 'reservation', 'company', NULL, 'read', '2019-08-09 06:11:20', '2019-08-09 06:11:20'),
(49, 24, 45, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-08-09 12:29:10', '2019-08-09 12:29:10'),
(50, 24, 44, NULL, 'reservation', 'company', NULL, 'read', '2019-08-09 12:31:27', '2019-08-09 12:31:27'),
(51, 24, 45, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-08-09 12:39:03', '2019-08-09 12:39:03'),
(52, 24, 44, NULL, 'reservation', 'company', NULL, 'read', '2019-08-09 12:42:11', '2019-08-09 12:42:11'),
(54, 24, 44, NULL, 'reservation', 'company', NULL, 'read', '2019-08-09 12:54:29', '2019-08-09 12:54:29'),
(55, 24, 44, NULL, 'reservation', 'company', NULL, 'read', '2019-08-09 12:59:53', '2019-08-09 12:59:53'),
(56, 34, 44, NULL, 'Waiting Request', 'waiting_request', NULL, 'read', '2019-08-10 03:19:37', '2019-08-10 03:19:37'),
(57, 34, 45, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-08-11 21:51:01', '2019-08-11 21:51:01'),
(58, 24, 44, NULL, 'reservation', 'company', NULL, 'read', '2019-08-12 06:24:05', '2019-08-12 06:24:05'),
(59, 24, 44, NULL, 'reservation', 'company', NULL, 'read', '2019-08-12 06:27:24', '2019-08-12 06:27:24'),
(60, 24, 44, NULL, 'reservation', 'company', NULL, 'read', '2019-08-12 06:30:09', '2019-08-12 06:30:09'),
(61, 24, 45, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-08-12 06:31:48', '2019-08-12 06:31:48'),
(62, 24, 35, NULL, 'Waiting Request', 'waiting_request', NULL, 'read', '2019-08-12 09:41:28', '2019-08-12 09:41:28'),
(69, 24, 48, NULL, 'reservation', 'room_employee', NULL, 'read', '2019-08-13 10:56:44', '2019-08-13 10:56:44'),
(70, 24, 97, NULL, 'reservation', 'room_employee', 327, 'read', '2019-08-13 10:56:44', '2019-08-13 10:56:44'),
(72, 34, 44, NULL, 'deliver', 'company', NULL, 'read', '2019-08-14 02:36:17', '2019-08-14 02:36:17'),
(74, 22, 38, NULL, 'deliver', 'company', 363, 'read', '2019-11-25 05:04:47', '2019-08-14 07:19:47'),
(75, 24, 35, NULL, 'pick up', 'company', NULL, 'read', '2019-08-14 09:34:39', '2019-08-14 09:34:39'),
(76, 24, 35, NULL, 'deliver', 'company', NULL, 'read', '2019-08-14 09:43:07', '2019-08-14 09:43:07'),
(78, 34, 44, NULL, 'deliver', 'company', NULL, 'read', '2019-08-15 18:23:44', '2019-08-15 18:23:44'),
(79, 34, 44, NULL, 'Waiting Request', 'waiting_request', NULL, 'read', '2019-08-15 19:30:06', '2019-08-15 19:30:06'),
(80, 47, 44, NULL, 'Waiting Request', 'waiting_request', NULL, 'read', '2019-08-15 20:13:52', '2019-08-15 20:13:52'),
(81, 34, 44, NULL, 'Waiting Request', 'waiting_request', NULL, 'read', '2019-08-21 02:27:58', '2019-08-21 02:27:58'),
(82, 34, 44, NULL, 'pick up', 'company', NULL, 'read', '2019-08-21 02:36:11', '2019-08-21 02:36:11'),
(83, 34, 44, NULL, 'deliver', 'company', NULL, 'read', '2019-08-21 02:39:23', '2019-08-21 02:39:23'),
(84, 34, 44, NULL, 'reservation', 'company', NULL, 'read', '2019-08-21 13:26:10', '2019-08-21 13:26:10'),
(85, 34, 44, NULL, 'Waiting Request', 'waiting_request', NULL, 'read', '2019-08-21 20:34:36', '2019-08-21 20:34:36'),
(86, 34, 45, NULL, 'Waiting Request', 'waiting_request', NULL, 'unread', '2019-08-21 20:34:45', '2019-08-21 20:34:45'),
(87, 34, 45, NULL, 'Wait list 5 need more milk bdbdbdb hdbdhd ', 'send_to_host', NULL, 'unread', '2019-08-21 20:36:01', '2019-08-21 20:36:01'),
(89, 47, 44, NULL, 'Waiting Request', 'waiting_request', NULL, 'read', '2019-08-21 20:44:05', '2019-08-21 20:44:05'),
(90, 44, 34, NULL, 'Mike : Job completed', 'guest_waiting', NULL, 'read', '2019-08-22 05:16:25', '2019-08-22 05:16:25'),
(91, 24, 35, NULL, 'pick up', 'company', NULL, 'read', '2019-08-22 05:42:42', '2019-08-22 05:42:42'),
(92, 22, 44, NULL, 'Waiting Request', 'waiting_request', NULL, 'read', '2019-08-22 07:16:10', '2019-08-22 07:16:10'),
(103, 34, 44, NULL, 'reservation', 'company', NULL, 'read', '2019-08-22 11:21:08', '2019-08-22 11:21:08'),
(104, 22, 35, NULL, 'reservation', 'company', NULL, 'read', '2019-08-23 03:53:11', '2019-08-23 03:53:11'),
(105, 34, 44, NULL, 'pick up', 'company', NULL, 'read', '2019-08-26 19:49:44', '2019-08-26 19:49:44'),
(106, 34, 45, NULL, 'Keep it cool people is a small potato for both male and female to be a like thank you for you', 'send_to_host', NULL, 'unread', '2019-08-26 19:53:22', '2019-08-26 19:53:22'),
(107, 34, 45, NULL, 'Waiting Request', 'waiting_request', NULL, 'unread', '2019-08-26 19:53:34', '2019-08-26 19:53:34'),
(108, 47, 45, NULL, 'Waiting Request', 'waiting_request', NULL, 'unread', '2019-08-26 21:04:51', '2019-08-26 21:04:51'),
(109, 34, 44, NULL, 'Waiting Request', 'waiting_request', NULL, 'read', '2019-08-29 17:58:47', '2019-08-29 17:58:47'),
(110, 34, 44, NULL, 'reservation', 'company', NULL, 'read', '2019-09-03 21:46:15', '2019-09-03 21:46:15'),
(111, 34, 44, NULL, 'reservation', 'company', NULL, 'read', '2019-09-03 21:48:09', '2019-09-03 21:48:09'),
(112, 34, 44, NULL, 'reservation', 'company', NULL, 'read', '2019-09-04 15:01:05', '2019-09-04 15:01:05'),
(114, 34, 45, NULL, 'Hello', 'send_to_host', NULL, 'unread', '2019-09-22 09:52:52', '2019-09-22 09:52:52'),
(115, 34, 45, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-09-22 09:53:30', '2019-09-22 09:53:30'),
(116, 34, 71, NULL, 'reservation', 'room_employee', NULL, 'read', '2019-09-22 09:53:30', '2019-09-22 09:53:30'),
(117, 34, 45, NULL, 'Waiting Request', 'waiting_request', NULL, 'unread', '2019-09-22 09:53:46', '2019-09-22 09:53:46'),
(118, 34, 44, NULL, 'reservation', 'company', NULL, 'read', '2019-09-25 10:13:26', '2019-09-25 10:13:26'),
(119, 34, 44, NULL, 'reservation', 'company', NULL, 'unread', '2019-09-25 12:11:01', '2019-09-25 12:11:01'),
(120, 34, 44, NULL, 'reservation', 'company', NULL, 'unread', '2019-09-25 23:02:24', '2019-09-25 23:02:24'),
(121, 34, 44, NULL, 'reservation', 'company', NULL, 'unread', '2019-09-25 23:05:18', '2019-09-25 23:05:18'),
(122, 24, 35, NULL, 'reservation', 'company', NULL, 'read', '2019-09-26 03:37:34', '2019-09-26 03:37:34'),
(123, 34, 45, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-10-03 09:30:19', '2019-10-03 09:30:19'),
(124, 34, 71, NULL, 'reservation', 'room_employee', NULL, 'read', '2019-10-03 09:30:19', '2019-10-03 09:30:19'),
(125, 34, 45, NULL, 'Waiting Request', 'waiting_request', NULL, 'unread', '2019-10-03 10:26:44', '2019-10-03 10:26:44'),
(126, 34, 44, NULL, 'reservation', 'company', NULL, 'unread', '2019-10-03 10:52:38', '2019-10-03 10:52:38'),
(127, 34, 45, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-10-03 13:23:26', '2019-10-03 13:23:26'),
(128, 34, 71, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-10-03 13:23:26', '2019-10-03 13:23:26'),
(129, 34, 45, NULL, 'reservation', 'room_employee', NULL, 'read', '2019-10-03 15:31:07', '2019-10-03 15:31:07'),
(130, 34, 71, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-10-03 15:31:07', '2019-10-03 15:31:07'),
(132, 34, 44, NULL, 'Waiting Request', 'waiting_request', NULL, 'unread', '2019-10-10 10:10:29', '2019-10-10 10:10:29'),
(133, 34, 44, NULL, 'Hello there ', 'send_to_host', NULL, 'unread', '2019-10-10 10:10:46', '2019-10-10 10:10:46'),
(134, 34, 44, NULL, 'Kenbar: Job rejected', 'company_waiting', NULL, 'unread', '2019-10-10 10:11:25', '2019-10-10 10:11:25'),
(135, 34, 44, NULL, 'reservation', 'company', NULL, 'read', '2019-10-10 10:13:37', '2019-10-10 10:13:37'),
(136, 34, 44, NULL, 'deliver', 'company', NULL, 'read', '2019-10-10 10:15:22', '2019-10-10 10:15:22'),
(137, 34, 44, NULL, 'pick up', 'company', NULL, 'read', '2019-10-10 10:15:52', '2019-10-10 10:15:52'),
(138, 34, 44, NULL, 'Waiting Request', 'waiting_request', NULL, 'read', '2019-10-10 10:15:58', '2019-10-10 10:15:58'),
(139, 34, 45, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-10-10 10:16:36', '2019-10-10 10:16:36'),
(140, 34, 71, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-10-10 10:16:37', '2019-10-10 10:16:37'),
(141, 34, 45, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-10-10 15:28:12', '2019-10-10 15:28:12'),
(142, 34, 71, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-10-10 15:28:12', '2019-10-10 15:28:12'),
(143, 34, 45, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-10-10 18:21:05', '2019-10-10 18:21:05'),
(144, 34, 71, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-10-10 18:21:05', '2019-10-10 18:21:05'),
(145, 34, 45, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-10-10 19:40:44', '2019-10-10 19:40:44'),
(146, 34, 71, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-10-10 19:40:44', '2019-10-10 19:40:44'),
(147, 34, 45, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-10-10 20:29:02', '2019-10-10 20:29:02'),
(148, 34, 71, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-10-10 20:29:02', '2019-10-10 20:29:02'),
(149, 34, 44, NULL, 'Waiting Request', 'waiting_request', NULL, 'read', '2019-10-13 15:49:59', '2019-10-13 15:49:59'),
(150, 34, 45, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-10-17 09:28:59', '2019-10-17 09:28:59'),
(151, 34, 71, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-10-17 09:28:59', '2019-10-17 09:28:59'),
(152, 34, 44, NULL, 'reservation', 'company', NULL, 'read', '2019-10-17 11:23:57', '2019-10-17 11:23:57'),
(153, 34, 45, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-10-17 11:46:47', '2019-10-17 11:46:47'),
(154, 34, 71, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-10-17 11:46:47', '2019-10-17 11:46:47'),
(155, 34, 44, NULL, 'Waiting Request', 'waiting_request', NULL, 'read', '2019-10-17 11:47:16', '2019-10-17 11:47:16'),
(156, 22, 38, NULL, 'pick up', 'company', 417, 'read', '2019-11-28 06:14:20', '2019-10-18 06:14:20'),
(157, 34, 71, NULL, 'Dissusysyeyeyyh dhdhdh f', 'send_to_host', NULL, 'unread', '2019-10-18 09:39:13', '2019-10-18 09:39:13'),
(160, 22, 38, NULL, 'reservation', 'company', 428, 'read', '2019-11-29 03:08:31', '2019-11-01 03:08:31'),
(161, 47, 45, NULL, 'reservation', 'room_employee', 432, 'unread', '2019-11-04 10:05:52', '2019-11-04 10:05:52'),
(162, 47, 71, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-11-04 10:05:52', '2019-11-04 10:05:52'),
(163, 34, 45, NULL, 'reservation', 'room_employee', 434, 'unread', '2019-11-04 10:07:37', '2019-11-04 10:07:37'),
(164, 34, 71, NULL, 'reservation', 'room_employee', NULL, 'unread', '2019-11-04 10:07:37', '2019-11-04 10:07:37'),
(165, 34, 45, NULL, 'Waiting Request', 'waiting_request', NULL, 'unread', '2019-11-04 10:07:41', '2019-11-04 10:07:41'),
(166, 47, 45, NULL, 'Waiting Request', 'waiting_request', NULL, 'unread', '2019-11-04 10:08:23', '2019-11-04 10:08:23'),
(167, 47, 44, NULL, 'reservation', 'company', 439, 'read', '2019-11-05 07:31:44', '2019-11-05 07:31:44'),
(168, 47, 44, NULL, 'reservation', 'company', 440, 'read', '2019-11-05 07:34:23', '2019-11-05 07:34:23'),
(169, 47, 44, NULL, 'Waiting Request', 'waiting_request', NULL, 'read', '2019-11-08 11:17:05', '2019-11-08 11:17:05'),
(174, 22, 38, NULL, 'pick up', 'company', 443, 'read', '2019-11-29 04:48:50', '2019-11-20 04:48:50'),
(175, 38, 22, NULL, 'pick up', 'guest_booking', 443, 'read', '2019-11-21 05:17:56', '2019-11-21 05:17:56'),
(176, 38, 22, NULL, 'reservation', 'guest_booking', 428, 'read', '2019-11-21 10:39:16', '2019-11-21 10:39:16'),
(177, 38, 22, NULL, 'deliver', 'guest_booking', 363, 'read', '2019-11-21 10:40:02', '2019-11-21 10:40:02'),
(183, 22, 38, NULL, 'pick up', 'company', 444, 'read', '2019-11-21 05:40:53', '2019-11-21 05:40:53'),
(184, 22, 38, NULL, 'reservation', 'company', 445, 'read', '2019-11-21 05:43:49', '2019-11-21 05:43:49'),
(185, 22, 48, NULL, 'reservation', 'company', 448, 'read', '2019-11-22 02:08:22', '2019-11-22 02:08:22'),
(186, 22, 97, NULL, 'reservation', 'company', 448, 'read', '2019-11-22 02:08:22', '2019-11-22 02:08:22'),
(187, 22, 48, NULL, 'reservation', 'company', 449, 'read', '2019-11-22 02:28:49', '2019-11-22 02:28:49'),
(197, 22, 48, NULL, 'reservation', 'company', 455, 'read', '2019-11-25 01:57:17', '2019-11-25 01:57:17'),
(198, 22, 97, NULL, 'reservation', 'company', 455, 'read', '2019-11-25 01:57:17', '2019-11-25 01:57:17'),
(199, 22, 38, NULL, 'Waiting Request', 'waiting_request', NULL, 'read', '2019-11-25 22:15:30', '2019-11-25 22:15:30'),
(200, 24, 38, NULL, 'Waiting Request', 'waiting_request', NULL, 'read', '2019-11-25 22:32:03', '2019-11-25 22:32:03'),
(202, 22, 38, NULL, 'Waiting Request', 'waiting_request', NULL, 'read', '2019-11-26 01:49:25', '2019-11-26 01:49:25'),
(204, 22, 38, NULL, 'pick up', 'company', 457, 'read', '2019-11-27 02:42:35', '2019-11-27 02:42:35'),
(205, 22, 38, NULL, 'reservation', 'company', 466, 'read', '2019-11-27 22:55:32', '2019-11-27 22:55:32'),
(206, 47, 44, NULL, 'reservation', 'company', 469, 'unread', '2019-12-03 11:16:46', '2019-12-03 11:16:46'),
(207, 47, 44, NULL, 'Waiting Request', 'waiting_request', NULL, 'read', '2019-12-03 11:17:16', '2019-12-03 11:17:16'),
(208, 47, 45, NULL, 'reservation', 'company', 471, 'unread', '2019-12-03 11:17:58', '2019-12-03 11:17:58'),
(209, 47, 71, NULL, 'reservation', 'company', 471, 'unread', '2019-12-03 11:17:58', '2019-12-03 11:17:58'),
(210, 47, 44, NULL, 'reservation', 'company', 472, 'read', '2019-12-04 09:57:03', '2019-12-04 09:57:03'),
(211, 47, 45, NULL, 'reservation', 'company', 473, 'unread', '2019-12-04 12:55:58', '2019-12-04 12:55:58'),
(212, 47, 71, NULL, 'reservation', 'company', 473, 'unread', '2019-12-04 12:55:58', '2019-12-04 12:55:58'),
(213, 47, 44, NULL, 'reservation', 'company', 474, 'read', '2019-12-04 12:57:33', '2019-12-04 12:57:33'),
(214, 47, 44, NULL, 'reservation', 'company', 475, 'read', '2019-12-08 14:37:27', '2019-12-08 14:37:27'),
(215, 47, 44, NULL, 'Hello how are you doing today. My name is George and I was wondering if I could get some money to purchase and house. Lol ', 'send_to_host', NULL, 'read', '2019-12-13 13:45:47', '2019-12-13 13:45:47'),
(216, 47, 44, NULL, 'Hi', 'send_to_host', NULL, 'unread', '2019-12-13 13:46:15', '2019-12-13 13:46:15'),
(217, 47, 44, NULL, 'reservation', 'company', 476, 'unread', '2019-12-13 13:48:15', '2019-12-13 13:48:15'),
(218, 47, 44, NULL, 'Waiting Request', 'waiting_request', NULL, 'unread', '2019-12-13 13:48:27', '2019-12-13 13:48:27'),
(219, 47, 44, NULL, 'reservation', 'company', 478, 'unread', '2019-12-25 18:19:00', '2019-12-25 18:19:00'),
(220, 22, 38, NULL, 'pick up', 'company', 479, 'read', '2019-12-26 21:05:23', '2019-12-26 21:05:23'),
(221, 47, 44, NULL, 'reservation', 'company', 480, 'unread', '2019-12-26 21:40:38', '2019-12-26 21:40:38'),
(222, 47, 44, NULL, 'reservation', 'company', 481, 'unread', '2020-01-06 12:36:42', '2020-01-06 12:36:42'),
(223, 47, 45, NULL, 'Waiting Request', 'waiting_request', NULL, 'unread', '2020-02-05 17:46:27', '2020-02-05 17:46:27'),
(224, 47, 44, NULL, 'Waiting Request', 'waiting_request', NULL, 'unread', '2020-02-11 20:08:51', '2020-02-11 20:08:51'),
(225, 47, 44, NULL, 'Justin Brown: Job rejected', 'company_waiting', NULL, 'unread', '2020-02-11 20:09:36', '2020-02-11 20:09:36'),
(226, 47, 45, NULL, 'Waiting Request', 'waiting_request', NULL, 'unread', '2020-03-02 21:19:40', '2020-03-02 21:19:40'),
(227, 47, 45, NULL, 'Waiting Request', 'waiting_request', NULL, 'unread', '2020-03-12 20:18:33', '2020-03-12 20:18:33'),
(228, 47, 45, NULL, 'Justin Brown: Job rejected', 'company_waiting', NULL, 'unread', '2020-03-12 20:20:08', '2020-03-12 20:20:08'),
(229, 47, 45, NULL, 'Waiting Request', 'waiting_request', NULL, 'unread', '2020-05-16 19:09:52', '2020-05-16 19:09:52'),
(230, 47, 44, NULL, 'Waiting Request', 'waiting_request', NULL, 'unread', '2020-05-16 19:10:36', '2020-05-16 19:10:36');

-- --------------------------------------------------------

--
-- Table structure for table `payment_details`
--

CREATE TABLE `payment_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `adam_id` int(4) DEFAULT NULL,
  `app_item_id` int(4) DEFAULT NULL,
  `bundle_id` varchar(50) DEFAULT NULL,
  `version_external_identifier` varchar(10) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `quantity` varchar(10) DEFAULT NULL,
  `product_id` varchar(100) DEFAULT NULL,
  `transaction_id` text NOT NULL,
  `original_transaction_id` varchar(100) DEFAULT NULL,
  `purchase_date` datetime DEFAULT NULL,
  `original_purchase_date` datetime DEFAULT NULL,
  `expires_date` datetime DEFAULT NULL,
  `web_order_line_item_id` varchar(100) DEFAULT NULL,
  `is_trial_period` tinyint(1) DEFAULT NULL,
  `receipt` text,
  `expiration_intent` varchar(10) DEFAULT NULL,
  `auto_renew_product_id` varchar(100) DEFAULT NULL,
  `is_in_billing_retry_period` tinyint(1) DEFAULT NULL,
  `auto_renew_status` int(4) DEFAULT NULL,
  `device_type` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_details`
--

INSERT INTO `payment_details` (`id`, `user_id`, `company_id`, `adam_id`, `app_item_id`, `bundle_id`, `version_external_identifier`, `amount`, `quantity`, `product_id`, `transaction_id`, `original_transaction_id`, `purchase_date`, `original_purchase_date`, `expires_date`, `web_order_line_item_id`, `is_trial_period`, `receipt`, `expiration_intent`, `auto_renew_product_id`, `is_in_billing_retry_period`, `auto_renew_status`, `device_type`, `created`, `modified`) VALUES
(1, 38, 14, NULL, NULL, 'com.kenbar.mynus2', NULL, '9.99', NULL, 'com.mynustwo.subscription', '1000000573129705', NULL, NULL, NULL, NULL, NULL, NULL, 'MIJoogYJKoZIhvcNAQcCoIJokzCCaI8CAQExCzAJBgUrDgMCGgUAMIJYQwYJKoZIhvcNAQcBoIJYNASCWDAxglgsMAoCAQgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgELAgEBBAMCAQAwCwIBDwIBAQQDAgEAMAsCARACAQEEAwIBADALAgEZAgEBBAMCAQMwDAIBCgIBAQQEFgI0KzAMAgEOAgEBBAQCAgC9MA0CAQ0CAQEEBQIDAdUkMA0CARMCAQEEBQwDMS4wMA4CAQkCAQEEBgIEUDI1MzAPAgEDAgEBBAcMBTAuMC4zMBgCAQQCAQIEEHL5+NzrVmNoBH6SU2UkCAIwGwIBAAIBAQQTDBFQcm9kdWN0aW9uU2FuZGJveDAbAgECAgEBBBMMEWNvbS5rZW5iYXIubXludXMyMBwCAQUCAQEEFGq/g7kiKVZXHamLVlAdg3iYZtdYMB4CAQwCAQEEFhYUMjAxOS0wOS0yN1QwNTozNDozNlowHgIBEgIBAQQWFhQyMDEzLTA4LTAxVDA3OjAwOjAwWjBFAgEGAgEBBD3n9Zl2Dui0LMtZv6WJrvafjZOKjDgnMIwklXQ1FTcbv/orNVKk4aPigl5CTlNq8CXoU8UasnACJthxLnFXME0CAQcCAQEERaOx9Y6j4TuHVgPu6DJOThCKxtBnvqVDokcbMltYgVSQdaHOd2pWgbbjQmBcL2JQVSN3hyifpzLIThIckMbDxdUEF5KKyzCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoPOAwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTExNzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMDY6NTU6MTRaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMDc6MTA6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKDzhMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMzMwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDA3OjEwOjE0WjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDA3OjI1OjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyg92TAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTMyMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QwNzoyNToxNFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QwNzo0MDoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoPsMwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTA5NjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMDc6NDA6MTRaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMDc6NTU6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKD+4MBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMTAwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDA3OjU1OjE0WjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDA4OjEwOjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyhAxTAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTIyMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QwODoxMDoxNFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QwODoyNToxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoQdwwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTExODAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMTI6MjI6NTFaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMTI6Mzc6NTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKFbKMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMjQwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDEyOjM3OjUxWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDEyOjUyOjUxWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyhXzjAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTM1MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QxMjo1Mjo1MVowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QxMzowNzo1MVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoWNYwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTA5OTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMTM6MDc6NTFaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMTM6MjI6NTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKFn1MBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMDMwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDEzOjIyOjUxWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDEzOjM3OjUxWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyhbAzAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTE1MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QxMzozNzo1MVowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QxMzo1Mjo1MVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoW/swGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTExMjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjJUMTA6NTg6MTRaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjJUMTE6MTM6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKTVXMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMzYwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTIyVDExOjEzOjE0WjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTIyVDExOjI4OjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyk2WDAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MDk1MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yMlQxMToyODoxNFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0yMlQxMTo0MzoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpN3MwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTA5NzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjJUMTE6NDM6MTRaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjJUMTE6NTg6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKThXMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMTYwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTIyVDExOjU4OjE0WjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTIyVDEyOjEzOjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyk5ZjAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTAwMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yMlQxMjoxMzoxNFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0yMlQxMjoyODoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpOlcwGwICBqcCAQEEEgwQMTAwMDAwMDQzNTE4MjcyNTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjRUMDU6NTQ6MTBaMB8CAgaqAgEBBBYWFDIwMTgtMDgtMjRUMDk6NTA6MzFaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjRUMDY6MDk6MTBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKbdcMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMzAwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI0VDA2OjA5OjEwWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI0VDA2OjI0OjEwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pym36zAbAgIGpwIBAQQSDBAxMDAwMDAwNDM1MDQ1MzU2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yNFQwNjoyNDoxMFowHwICBqoCAQEEFhYUMjAxOC0wOC0yNFQwNjoyMzoyM1owHwICBqwCAQEEFhYUMjAxOC0wOC0yNFQwNjozOToxMFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpuKMwGwICBqcCAQEEEgwQMTAwMDAwMDQzNTA1Mjg1MzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjRUMDY6Mzk6MTBaMB8CAgaqAgEBBBYWFDIwMTgtMDgtMjRUMDY6Mzg6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjRUMDY6NTQ6MTBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKbleMBsCAganAgEBBBIMEDEwMDAwMDA0MzUwNjA4MTQwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI0VDA2OjU0OjEwWjAfAgIGqgIBAQQWFhQyMDE4LTA4LTI0VDA2OjUzOjEyWjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI0VDA3OjA5OjEwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pym6EzAbAgIGpwIBAQQSDBAxMDAwMDAwNDM1MDY4NjU0MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yNFQwNzowOToxMFowHwICBqoCAQEEFhYUMjAxOC0wOC0yNFQwNzowODozMlowHwICBqwCAQEEFhYUMjAxOC0wOC0yNFQwNzoyNDoxMFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpuwYwGwICBqcCAQEEEgwQMTAwMDAwMDQzNTE4MjcyMTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjRUMDk6NTA6MjlaMB8CAgaqAgEBBBYWFDIwMTgtMDgtMjRUMDk6NTA6MjlaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjRUMTA6MDU6MjlaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKcgvMBsCAganAgEBBBIMEDEwMDAwMDA0MzU2NDQ2NzcwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI3VDA1OjM4OjU4WjAfAgIGqgIBAQQWFhQyMDE4LTA4LTI3VDA1OjM4OjU5WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI3VDA1OjUzOjU4WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyobJzAbAgIGpwIBAQQSDBAxMDAwMDAwNDM1NjQ5Njc1MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yN1QwNTo1Mzo1OFowHwICBqoCAQEEFhYUMjAxOC0wOC0yN1QwNTo1MzowMlowHwICBqwCAQEEFhYUMjAxOC0wOC0yN1QwNjowODo1OFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqG6wwGwICBqcCAQEEEgwQMTAwMDAwMDQzNTY1MzEwMDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjdUMDY6MDg6NThaMB8CAgaqAgEBBBYWFDIwMTgtMDgtMjdUMDY6MDg6MDJaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjdUMDY6MjM6NThaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKhxPMBsCAganAgEBBBIMEDEwMDAwMDA0MzU2NjAzOTMwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI3VDA2OjIzOjU4WjAfAgIGqgIBAQQWFhQyMDE4LTA4LTI3VDA2OjIzOjA1WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI3VDA2OjM4OjU4WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyodCzAbAgIGpwIBAQQSDBAxMDAwMDAwNDM1NjY3NTkyMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yN1QwNjozODo1OFowHwICBqoCAQEEFhYUMjAxOC0wOC0yN1QwNjozODoxMVowHwICBqwCAQEEFhYUMjAxOC0wOC0yN1QwNjo1Mzo1OFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqHeIwGwICBqcCAQEEEgwQMTAwMDAwMDQzNTY3NDI3OTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjdUMDY6NTM6NThaMB8CAgaqAgEBBBYWFDIwMTgtMDgtMjdUMDY6NTM6MzdaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjdUMDc6MDg6NThaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKh7dMBsCAganAgEBBBIMEDEwMDAwMDA0MzY0Mzc4NjEwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI4VDEyOjQyOjExWjAfAgIGqgIBAQQWFhQyMDE4LTA4LTI4VDEyOjQyOjEyWjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI4VDEyOjU3OjExWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyp4SzAbAgIGpwIBAQQSDBAxMDAwMDAwNDM2NDQyNDM1MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yOFQxMjo1NzoxMlowHwICBqoCAQEEFhYUMjAxOC0wOC0yOFQxMjo1NzoxM1owHwICBqwCAQEEFhYUMjAxOC0wOC0yOFQxMzoxMjoxMlowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqeVQwGwICBqcCAQEEEgwQMTAwMDAwMDQzNjQ1MTcyMTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjhUMTM6MTI6MTJaMB8CAgaqAgEBBBYWFDIwMTgtMDgtMjhUMTM6MTE6MzhaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjhUMTM6Mjc6MTJaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKnqSMBsCAganAgEBBBIMEDEwMDAwMDA0MzY0NjEzODQwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI4VDEzOjI3OjEyWjAfAgIGqgIBAQQWFhQyMDE4LTA4LTI4VDEzOjI2OjMwWjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI4VDEzOjQyOjEyWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyp7xTAbAgIGpwIBAQQSDBAxMDAwMDAwNDM2NDY3MDI5MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yOFQxMzo0MjoxMlowHwICBqoCAQEEFhYUMjAxOC0wOC0yOFQxMzo0MTo0NVowHwICBqwCAQEEFhYUMjAxOC0wOC0yOFQxMzo1NzoxMlowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqfQQwGwICBqcCAQEEEgwQMTAwMDAwMDQzNjQ3MjUzNzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjhUMTM6NTc6MTJaMB8CAgaqAgEBBBYWFDIwMTgtMDgtMjhUMTM6NTY6MjhaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjhUMTQ6MTI6MTJaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKn4iMBsCAganAgEBBBIMEDEwMDAwMDA0NDA2MjQyOTkwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA5LTA2VDEzOjQyOjExWjAfAgIGqgIBAQQWFhQyMDE4LTA5LTA2VDEzOjQyOjEyWjAfAgIGrAIBAQQWFhQyMDE4LTA5LTA2VDEzOjU3OjExWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyySjjAbAgIGpwIBAQQSDBAxMDAwMDAwNDQwNjMzOTcwMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOS0wNlQxMzo1NzoxMVowHwICBqoCAQEEFhYUMjAxOC0wOS0wNlQxMzo1NjozNFowHwICBqwCAQEEFhYUMjAxOC0wOS0wNlQxNDoxMjoxMVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcsk6wwGwICBqcCAQEEEgwQMTAwMDAwMDQ0MDY0MTQ4MjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDktMDZUMTQ6MTI6MTFaMB8CAgaqAgEBBBYWFDIwMTgtMDktMDZUMTQ6MTE6MjVaMB8CAgasAgEBBBYWFDIwMTgtMDktMDZUMTQ6Mjc6MTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nLJT+MBsCAganAgEBBBIMEDEwMDAwMDA0NDA2NDkzMzMwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA5LTA2VDE0OjI3OjExWjAfAgIGqgIBAQQWFhQyMDE4LTA5LTA2VDE0OjI2OjIyWjAfAgIGrAIBAQQWFhQyMDE4LTA5LTA2VDE0OjQyOjExWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyyWYzAbAgIGpwIBAQQSDBAxMDAwMDAwNDQwNjU3NjAzMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOS0wNlQxNDo0MjoxMVowHwICBqoCAQEEFhYUMjAxOC0wOS0wNlQxNDo0MToyNFowHwICBqwCAQEEFhYUMjAxOC0wOS0wNlQxNDo1NzoxMVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcsl9EwGwICBqcCAQEEEgwQMTAwMDAwMDQ0MDY2NTM0OTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDktMDZUMTQ6NTc6MTFaMB8CAgaqAgEBBBYWFDIwMTgtMDktMDZUMTQ6NTc6MDBaMB8CAgasAgEBBBYWFDIwMTgtMDktMDZUMTU6MTI6MTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nLJkSMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkwNDUwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjQwWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjQzWjAfAgIGrAIBAQQWFhQyMDE4LTExLTI4VDA2OjExOjQwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p0AC8jAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDY0NzI4MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0xMS0yOFQwNjoxMTo0MFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNjoxMDo0NFowHwICBqwCAQEEFhYUMjAxOC0xMS0yOFQwNjoyNjo0MFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqdAA6IwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA3MzgxNzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMTEtMjhUMDY6MjY6NDBaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDY6MjY6NDFaMB8CAgasAgEBBBYWFDIwMTgtMTEtMjhUMDY6NDE6NDBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nQASSMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNzk3OTEwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTExLTI4VDA2OjQxOjQwWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA2OjQwOjUzWjAfAgIGrAIBAQQWFhQyMDE4LTExLTI4VDA2OjU2OjQwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p0AFgzAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDg4NDMyMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0xMS0yOFQwNjo1Njo0MFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNjo1NjozMFowHwICBqwCAQEEFhYUMjAxOC0xMS0yOFQwNzoxMTo0MFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqdABo0wGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA5NzgzNjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMTEtMjhUMDc6MTE6NDBaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDc6MTE6MTZaMB8CAgasAgEBBBYWFDIwMTgtMTEtMjhUMDc6MjY6NDBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nQAd+MBsCAganAgEBBBIMEDEwMDAwMDA1NzE2MDExODkwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI0VDA2OjQ0OjA3WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI0VDA2OjQ0OjA4WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI0VDA2OjU5OjA3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5U9AjAbAgIGpwIBAQQSDBAxMDAwMDAwNTcxNjA4NzY5MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yNFQwNjo1OTowN1owHwICBqoCAQEEFhYUMjAxOS0wOS0yNFQwNjo1ODoyMFowHwICBqwCAQEEFhYUMjAxOS0wOS0yNFQwNzoxNDowN1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeVPlQwGwICBqcCAQEEEgwQMTAwMDAwMDU3MTYxNzQ2MTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjRUMDc6MTQ6MDdaMB8CAgaqAgEBBBYWFDIwMTktMDktMjRUMDc6MTM6MDlaMB8CAgasAgEBBBYWFDIwMTktMDktMjRUMDc6Mjk6MDdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlT/hMBsCAganAgEBBBIMEDEwMDAwMDA1NzE2MjU1NDIwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI0VDA3OjI5OjA3WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI0VDA3OjI4OjA4WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI0VDA3OjQ0OjA3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5VBVTAbAgIGpwIBAQQSDBAxMDAwMDAwNTcxNjMxMzQwMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yNFQwNzo0NDowN1owHwICBqoCAQEEFhYUMjAxOS0wOS0yNFQwNzo0MzoxNFowHwICBqwCAQEEFhYUMjAxOS0wOS0yNFQwNzo1OTowN1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeVQtgwGwICBqcCAQEEEgwQMTAwMDAwMDU3MTYzODM4MTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjRUMDc6NTk6MDdaMB8CAgaqAgEBBBYWFDIwMTktMDktMjRUMDc6NTg6MTlaMB8CAgasAgEBBBYWFDIwMTktMDktMjRUMDg6MTQ6MDdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlUTOMBsCAganAgEBBBIMEDEwMDAwMDA1NzMxMjk3MDUwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI3VDA1OjM0OjM1WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI3VDA1OjM0OjM2WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI3VDA1OjQ5OjM1WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uoIIOZTCCBXwwggRkoAMCAQICCA7rV4fnngmNMA0GCSqGSIb3DQEBBQUAMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MTExMzAyMTUwOVoXDTIzMDIwNzIxNDg0N1owgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKXPgf0looFb1oftI9ozHI7iI8ClxCbLPcaf7EoNVYb/pALXl8o5VG19f7JUGJ3ELFJxjmR7gs6JuknWCOW0iHHPP1tGLsbEHbgDqViiBD4heNXbt9COEo2DTFsqaDeTwvK9HsTSoQxKWFKrEuPt3R+YFZA1LcLMEsqNSIH3WHhUa+iMMTYfSgYMR1TzN5C4spKJfV+khUrhwJzguqS7gpdj9CuTwf0+b8rB9Typj1IawCUKdg7e/pn+/8Jr9VterHNRSQhWicxDkMyOgQLQoJe2XLGhaWmHkBBoJiY5uB0Qc7AKXcVz0N92O9gt2Yge4+wHz+KO0NP6JlWB7+IDSSMCAwEAAaOCAdcwggHTMD8GCCsGAQUFBwEBBDMwMTAvBggrBgEFBQcwAYYjaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy13d2RyMDQwHQYDVR0OBBYEFJGknPzEdrefoIr0TfWPNl3tKwSFMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUiCcXCam2GGCL7Ou69kdZxVJUo7cwggEeBgNVHSAEggEVMIIBETCCAQ0GCiqGSIb3Y2QFBgEwgf4wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNgYIKwYBBQUHAgEWKmh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5LzAOBgNVHQ8BAf8EBAMCB4AwEAYKKoZIhvdjZAYLAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAA2mG9MuPeNbKwduQpZs0+iMQzCCX+Bc0Y2+vQ+9GvwlktuMhcOAWd/j4tcuBRSsDdu2uP78NS58y60Xa45/H+R3ubFnlbQTXqYZhnb4WiCV52OMD3P86O3GH66Z+GVIXKDgKDrAEDctuaAEOR9zucgF/fLefxoqKm4rAfygIFzZ630npjP49ZjgvkTbsUxn/G4KT8niBqjSl/OnjmtRolqEdWXRFgRi48Ff9Qipz2jZkgDJwYyz+I0AZLpYYMB8r491ymm5WyrWHWhumEL1TKc3GZvMOxx6GUPzo22/SGAGDDaSK+zeGLUR2i0j0I78oGmcFxuegHs5R0UwYS/HE6gwggQiMIIDCqADAgECAggB3rzEOW2gEDANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMTMwMjA3MjE0ODQ3WhcNMjMwMjA3MjE0ODQ3WjCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMo4VKbLVqrIJDlI6Yzu7F+4fyaRvDRTes58Y4Bhd2RepQcjtjn+UC0VVlhwLX7EbsFKhT4v8N6EGqFXya97GP9q+hUSSRUIGayq2yoy7ZZjaFIVPYyK7L9rGJXgA6wBfZcFZ84OhZU3au0Jtq5nzVFkn8Zc0bxXbmc1gHY2pIeBbjiP2CsVTnsl2Fq/ToPBjdKT1RpxtWCcnTNOVfkSWAyGuBYNweV3RY1QSLorLeSUheHoxJ3GaKWwo/xnfnC6AllLd0KRObn1zeFM78A7SIym5SFd/Wpqu6cWNWDS5q3zRinJ6MOL6XnAamFnFbLw/eVovGJfbs+Z3e8bY/6SZasCAwEAAaOBpjCBozAdBgNVHQ4EFgQUiCcXCam2GGCL7Ou69kdZxVJUo7cwDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAOBgNVHQ8BAf8EBAMCAYYwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAE/P71m+LPWybC+P7hOHMugFNahui33JaQy52Re8dyzUZ+L9mm06WVzfgwG9sq4qYXKxr83DRTCPo4MNzh1HtPGTiqN0m6TDmHKHOz6vRQuSVLkyu5AYU2sKThC22R1QbCGAColOV4xrWzw9pv3e9w0jHQtKJoc/upGSTKQZEhltV/V6WId7aIrkhoxK6+JJFKql3VUAqa67SzCu4aCxvCmA5gl35b40ogHKf9ziCuY7uLvsumKV8wVjQYLNDzsdTJWk26v5yZXpT+RN5yaZgem8+bQp0gF6ZuEujPYhisX4eOGBrr/TkJ2prfOv/TgalmcwHFGlXOxxioK0bA8MFR8wggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4++NwzeajTEV+H0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz/2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcNgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIDutXh+eeCY0wCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQCY8VfRvDn3rWdVg64lwpq8/TvNNu7X0iRDz9CCCJtBXeAgtriJJHQtPm6ZoKTbvDmnaj18P26aEm3LDxXa/PMgoOzLbOMm/jmP+8qhjI6G2Olh/VOgzZzvPap6HLwtEZ4vj8sMC23SoglGDK28LsuGSYeHG45UAWuN4W4YXy+lR3CNsZV8GEkgBlfMAW4qYu5zrRN59s5Z6/LYx5sQ7AUjha6QGaNNOPe92aRuLWPEoh7rKo1SJb3LurHCMVEPtrgS+43HrLXyrfob+WMhg+nubKPfaTtnJP97jOCid6s44dA1pDRuX6NjgU2IlYrl/AWfz2fRO4Ub4W8I2wJCCFUY', NULL, NULL, NULL, NULL, 'ios', '2019-09-26 22:34:37', '2019-09-26 22:34:37');
INSERT INTO `payment_details` (`id`, `user_id`, `company_id`, `adam_id`, `app_item_id`, `bundle_id`, `version_external_identifier`, `amount`, `quantity`, `product_id`, `transaction_id`, `original_transaction_id`, `purchase_date`, `original_purchase_date`, `expires_date`, `web_order_line_item_id`, `is_trial_period`, `receipt`, `expiration_intent`, `auto_renew_product_id`, `is_in_billing_retry_period`, `auto_renew_status`, `device_type`, `created`, `modified`) VALUES
(2, 45, 23, NULL, NULL, 'com.kenbar.mynus2', NULL, '9.99', NULL, 'com.mynustwo.subscription', '1000000576150620', NULL, NULL, NULL, NULL, NULL, NULL, 'MIJx4gYJKoZIhvcNAQcCoIJx0zCCcc8CAQExCzAJBgUrDgMCGgUAMIJhgwYJKoZIhvcNAQcBoIJhdASCYXAxgmFsMAoCAQgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgELAgEBBAMCAQAwCwIBDwIBAQQDAgEAMAsCARACAQEEAwIBADALAgEZAgEBBAMCAQMwDAIBCgIBAQQEFgI0KzAMAgEOAgEBBAQCAgCOMA0CAQ0CAQEEBQIDAdZQMA0CARMCAQEEBQwDMS4wMA4CAQkCAQEEBgIEUDI1MzAPAgEDAgEBBAcMBTAuMC4zMBgCAQQCAQIEECx7oOCabwXyrx6TLkorlLYwGwIBAAIBAQQTDBFQcm9kdWN0aW9uU2FuZGJveDAbAgECAgEBBBMMEWNvbS5rZW5iYXIubXludXMyMBwCAQUCAQEEFPzGp8uIyI9dKIGEyUeNM7OXDPnnMB4CAQwCAQEEFhYUMjAxOS0xMC0wN1QwMjoxOTo1OVowHgIBEgIBAQQWFhQyMDEzLTA4LTAxVDA3OjAwOjAwWjBJAgEHAgEBBEHRFx8dFq1CxO6MH3FgvN78ErKbnbxs2J+rc0OJdwjPAef6zAVGLWTvl5Y2Sd2rJMW3DQClwtIMvnTwk1SmUCge1TBNAgEGAgEBBEXye+HT/0KzELspenUI13UQY5exYPzXOKhE8kgSqfJdm+CXoe56G0stdTVuCV2dI8w7yiBgvUblrkeCYLrHrkRAsxEHqh4wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKDzgMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMTcwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDA2OjU1OjE0WjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDA3OjEwOjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyg84TAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTMzMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QwNzoxMDoxNFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QwNzoyNToxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoPdkwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTEzMjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMDc6MjU6MTRaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMDc6NDA6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKD7DMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkwOTYwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDA3OjQwOjE0WjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDA3OjU1OjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyg/uDAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTEwMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QwNzo1NToxNFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QwODoxMDoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoQMUwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTEyMjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMDg6MTA6MTRaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMDg6MjU6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKEHcMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMTgwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDEyOjIyOjUxWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDEyOjM3OjUxWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyhWyjAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTI0MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QxMjozNzo1MVowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QxMjo1Mjo1MVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoV84wGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTEzNTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMTI6NTI6NTFaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMTM6MDc6NTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKFjWMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkwOTkwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDEzOjA3OjUxWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDEzOjIyOjUxWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyhZ9TAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTAzMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QxMzoyMjo1MVowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QxMzozNzo1MVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoWwMwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTExNTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMTM6Mzc6NTFaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMTM6NTI6NTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKFv7MBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMTIwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTIyVDEwOjU4OjE0WjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTIyVDExOjEzOjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyk1VzAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTM2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yMlQxMToxMzoxNFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0yMlQxMToyODoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpNlgwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTA5NTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjJUMTE6Mjg6MTRaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjJUMTE6NDM6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKTdzMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkwOTcwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTIyVDExOjQzOjE0WjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTIyVDExOjU4OjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyk4VzAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTE2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yMlQxMTo1ODoxNFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0yMlQxMjoxMzoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpOWYwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTEwMDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjJUMTI6MTM6MTRaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjJUMTI6Mjg6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKTpXMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMDEwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI0VDA1OjU0OjEwWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI0VDA2OjA5OjEwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pym3XDAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTMwMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yNFQwNjowOToxMFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0yNFQwNjoyNDoxMFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpt+swGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTExNDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjRUMDY6MjQ6MTBaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjRUMDY6Mzk6MTBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKbijMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMzEwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI0VDA2OjM5OjEwWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI0VDA2OjU0OjEwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pym5XjAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTA4MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yNFQwNjo1NDoxMFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0yNFQwNzowOToxMFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpuhMwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTEzNDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjRUMDc6MDk6MTBaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjRUMDc6MjQ6MTBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKbsGMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMDYwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI0VDA5OjUwOjI5WjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI0VDEwOjA1OjI5WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pynILzAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTM3MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yN1QwNTozODo1OFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0yN1QwNTo1Mzo1OFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqGycwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTEyNzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjdUMDU6NTM6NThaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjdUMDY6MDg6NThaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKhusMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMDIwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI3VDA2OjA4OjU4WjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI3VDA2OjIzOjU4WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyocTzAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTI1MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yN1QwNjoyMzo1OFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0yN1QwNjozODo1OFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqHQswGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTEyMTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjdUMDY6Mzg6NThaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjdUMDY6NTM6NThaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKh3iMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMDcwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI3VDA2OjUzOjU4WjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI3VDA3OjA4OjU4WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyoe3TAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTIzMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yOFQxMjo0MjoxMVowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0yOFQxMjo1NzoxMVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqeEswGwICBqcCAQEEEgwQMTAwMDAwMDQzNjQ0MjQzNTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjhUMTI6NTc6MTJaMB8CAgaqAgEBBBYWFDIwMTgtMDgtMjhUMTI6NTc6MTNaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjhUMTM6MTI6MTJaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKnlUMBsCAganAgEBBBIMEDEwMDAwMDA0MzY0NTE3MjEwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI4VDEzOjEyOjEyWjAfAgIGqgIBAQQWFhQyMDE4LTA4LTI4VDEzOjExOjM4WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI4VDEzOjI3OjEyWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyp6kjAbAgIGpwIBAQQSDBAxMDAwMDAwNDM2NDYxMzg0MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yOFQxMzoyNzoxMlowHwICBqoCAQEEFhYUMjAxOC0wOC0yOFQxMzoyNjozMFowHwICBqwCAQEEFhYUMjAxOC0wOC0yOFQxMzo0MjoxMlowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqe8UwGwICBqcCAQEEEgwQMTAwMDAwMDQzNjQ2NzAyOTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjhUMTM6NDI6MTJaMB8CAgaqAgEBBBYWFDIwMTgtMDgtMjhUMTM6NDE6NDVaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjhUMTM6NTc6MTJaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKn0EMBsCAganAgEBBBIMEDEwMDAwMDA0MzY0NzI1MzcwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI4VDEzOjU3OjEyWjAfAgIGqgIBAQQWFhQyMDE4LTA4LTI4VDEzOjU2OjI4WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI4VDE0OjEyOjEyWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyp+IjAbAgIGpwIBAQQSDBAxMDAwMDAwNDQwNjI0Mjk5MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOS0wNlQxMzo0MjoxMVowHwICBqoCAQEEFhYUMjAxOC0wOS0wNlQxMzo0MjoxMlowHwICBqwCAQEEFhYUMjAxOC0wOS0wNlQxMzo1NzoxMVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcsko4wGwICBqcCAQEEEgwQMTAwMDAwMDQ0MDYzMzk3MDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDktMDZUMTM6NTc6MTFaMB8CAgaqAgEBBBYWFDIwMTgtMDktMDZUMTM6NTY6MzRaMB8CAgasAgEBBBYWFDIwMTgtMDktMDZUMTQ6MTI6MTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nLJOsMBsCAganAgEBBBIMEDEwMDAwMDA0NDA2NDE0ODIwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA5LTA2VDE0OjEyOjExWjAfAgIGqgIBAQQWFhQyMDE4LTA5LTA2VDE0OjExOjI1WjAfAgIGrAIBAQQWFhQyMDE4LTA5LTA2VDE0OjI3OjExWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyyU/jAbAgIGpwIBAQQSDBAxMDAwMDAwNDQwNjQ5MzMzMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOS0wNlQxNDoyNzoxMVowHwICBqoCAQEEFhYUMjAxOC0wOS0wNlQxNDoyNjoyMlowHwICBqwCAQEEFhYUMjAxOC0wOS0wNlQxNDo0MjoxMVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcslmMwGwICBqcCAQEEEgwQMTAwMDAwMDQ0MDY1NzYwMzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDktMDZUMTQ6NDI6MTFaMB8CAgaqAgEBBBYWFDIwMTgtMDktMDZUMTQ6NDE6MjRaMB8CAgasAgEBBBYWFDIwMTgtMDktMDZUMTQ6NTc6MTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nLJfRMBsCAganAgEBBBIMEDEwMDAwMDA0NDA2NjUzNDkwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA5LTA2VDE0OjU3OjExWjAfAgIGqgIBAQQWFhQyMDE4LTA5LTA2VDE0OjU3OjAwWjAfAgIGrAIBAQQWFhQyMDE4LTA5LTA2VDE1OjEyOjExWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyyZEjAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MDQ1MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo0MFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo0M1owHwICBqwCAQEEFhYUMjAxOC0xMS0yOFQwNjoxMTo0MFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqdAAvIwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA2NDcyODAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMTEtMjhUMDY6MTE6NDBaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDY6MTA6NDRaMB8CAgasAgEBBBYWFDIwMTgtMTEtMjhUMDY6MjY6NDBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nQAOiMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNzM4MTcwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTExLTI4VDA2OjI2OjQwWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA2OjI2OjQxWjAfAgIGrAIBAQQWFhQyMDE4LTExLTI4VDA2OjQxOjQwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p0AEkjAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDc5NzkxMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0xMS0yOFQwNjo0MTo0MFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNjo0MDo1M1owHwICBqwCAQEEFhYUMjAxOC0xMS0yOFQwNjo1Njo0MFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqdABYMwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA4ODQzMjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMTEtMjhUMDY6NTY6NDBaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDY6NTY6MzBaMB8CAgasAgEBBBYWFDIwMTgtMTEtMjhUMDc6MTE6NDBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nQAaNMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwOTc4MzYwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTExLTI4VDA3OjExOjQwWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA3OjExOjE2WjAfAgIGrAIBAQQWFhQyMDE4LTExLTI4VDA3OjI2OjQwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p0AHfjAbAgIGpwIBAQQSDBAxMDAwMDAwNTcxNjAxMTg5MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yNFQwNjo0NDowN1owHwICBqoCAQEEFhYUMjAxOS0wOS0yNFQwNjo0NDowOFowHwICBqwCAQEEFhYUMjAxOS0wOS0yNFQwNjo1OTowN1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeVPQIwGwICBqcCAQEEEgwQMTAwMDAwMDU3MTYwODc2OTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjRUMDY6NTk6MDdaMB8CAgaqAgEBBBYWFDIwMTktMDktMjRUMDY6NTg6MjBaMB8CAgasAgEBBBYWFDIwMTktMDktMjRUMDc6MTQ6MDdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlT5UMBsCAganAgEBBBIMEDEwMDAwMDA1NzE2MTc0NjEwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI0VDA3OjE0OjA3WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI0VDA3OjEzOjA5WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI0VDA3OjI5OjA3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5U/4TAbAgIGpwIBAQQSDBAxMDAwMDAwNTcxNjI1NTQyMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yNFQwNzoyOTowN1owHwICBqoCAQEEFhYUMjAxOS0wOS0yNFQwNzoyODowOFowHwICBqwCAQEEFhYUMjAxOS0wOS0yNFQwNzo0NDowN1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeVQVUwGwICBqcCAQEEEgwQMTAwMDAwMDU3MTYzMTM0MDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjRUMDc6NDQ6MDdaMB8CAgaqAgEBBBYWFDIwMTktMDktMjRUMDc6NDM6MTRaMB8CAgasAgEBBBYWFDIwMTktMDktMjRUMDc6NTk6MDdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlULYMBsCAganAgEBBBIMEDEwMDAwMDA1NzE2MzgzODEwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI0VDA3OjU5OjA3WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI0VDA3OjU4OjE5WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI0VDA4OjE0OjA3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5VEzjAbAgIGpwIBAQQSDBAxMDAwMDAwNTczMTI5NzA2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yN1QwNTozNDozNVowHwICBqoCAQEEFhYUMjAxOS0wOS0yN1QwNTozNDozNlowHwICBqwCAQEEFhYUMjAxOS0wOS0yN1QwNTo0OTozNVowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeWkxIwGwICBqcCAQEEEgwQMTAwMDAwMDU3MzEzNzcwNjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjdUMDU6NDk6MzVaMB8CAgaqAgEBBBYWFDIwMTktMDktMjdUMDU6NDg6MzdaMB8CAgasAgEBBBYWFDIwMTktMDktMjdUMDY6MDQ6MzVaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlpP9MBsCAganAgEBBBIMEDEwMDAwMDA1NzMxNDUzMzYwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI3VDA2OjA0OjM1WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI3VDA2OjAzOjQ1WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI3VDA2OjE5OjM1WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5aVOjAbAgIGpwIBAQQSDBAxMDAwMDAwNTczMTU0Nzk1MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yN1QwNjoxOTozNVowHwICBqoCAQEEFhYUMjAxOS0wOS0yN1QwNjoxODo0MFowHwICBqwCAQEEFhYUMjAxOS0wOS0yN1QwNjozNDozNVowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeWlpEwGwICBqcCAQEEEgwQMTAwMDAwMDU3MzE2MzYxNDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjdUMDY6MzQ6MzVaMB8CAgaqAgEBBBYWFDIwMTktMDktMjdUMDY6MzM6NDBaMB8CAgasAgEBBBYWFDIwMTktMDktMjdUMDY6NDk6MzVaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlpfgMBsCAganAgEBBBIMEDEwMDAwMDA1NzMxNzU3MDkwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI3VDA2OjQ5OjM1WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI3VDA2OjQ4OjUxWjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI3VDA3OjA0OjM1WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5aZRzAbAgIGpwIBAQQSDBAxMDAwMDAwNTc2MTUwNjIwMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMC0wN1QwMjoxOTo1N1owHwICBqoCAQEEFhYUMjAxOS0xMC0wN1QwMjoxOTo1OVowHwICBqwCAQEEFhYUMjAxOS0xMC0wN1QwMjozNDo1N1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbqCCDmUwggV8MIIEZKADAgECAggO61eH554JjTANBgkqhkiG9w0BAQUFADCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTAeFw0xNTExMTMwMjE1MDlaFw0yMzAyMDcyMTQ4NDdaMIGJMTcwNQYDVQQDDC5NYWMgQXBwIFN0b3JlIGFuZCBpVHVuZXMgU3RvcmUgUmVjZWlwdCBTaWduaW5nMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQClz4H9JaKBW9aH7SPaMxyO4iPApcQmyz3Gn+xKDVWG/6QC15fKOVRtfX+yVBidxCxScY5ke4LOibpJ1gjltIhxzz9bRi7GxB24A6lYogQ+IXjV27fQjhKNg0xbKmg3k8LyvR7E0qEMSlhSqxLj7d0fmBWQNS3CzBLKjUiB91h4VGvojDE2H0oGDEdU8zeQuLKSiX1fpIVK4cCc4Lqku4KXY/Qrk8H9Pm/KwfU8qY9SGsAlCnYO3v6Z/v/Ca/VbXqxzUUkIVonMQ5DMjoEC0KCXtlyxoWlph5AQaCYmObgdEHOwCl3Fc9DfdjvYLdmIHuPsB8/ijtDT+iZVge/iA0kjAgMBAAGjggHXMIIB0zA/BggrBgEFBQcBAQQzMDEwLwYIKwYBBQUHMAGGI2h0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDMtd3dkcjA0MB0GA1UdDgQWBBSRpJz8xHa3n6CK9E31jzZd7SsEhTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFIgnFwmpthhgi+zruvZHWcVSVKO3MIIBHgYDVR0gBIIBFTCCAREwggENBgoqhkiG92NkBQYBMIH+MIHDBggrBgEFBQcCAjCBtgyBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMDYGCCsGAQUFBwIBFipodHRwOi8vd3d3LmFwcGxlLmNvbS9jZXJ0aWZpY2F0ZWF1dGhvcml0eS8wDgYDVR0PAQH/BAQDAgeAMBAGCiqGSIb3Y2QGCwEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQANphvTLj3jWysHbkKWbNPojEMwgl/gXNGNvr0PvRr8JZLbjIXDgFnf4+LXLgUUrA3btrj+/DUufMutF2uOfx/kd7mxZ5W0E16mGYZ2+FogledjjA9z/Ojtxh+umfhlSFyg4Cg6wBA3LbmgBDkfc7nIBf3y3n8aKipuKwH8oCBc2et9J6Yz+PWY4L5E27FMZ/xuCk/J4gao0pfzp45rUaJahHVl0RYEYuPBX/UIqc9o2ZIAycGMs/iNAGS6WGDAfK+PdcppuVsq1h1obphC9UynNxmbzDscehlD86Ntv0hgBgw2kivs3hi1EdotI9CO/KBpnBcbnoB7OUdFMGEvxxOoMIIEIjCCAwqgAwIBAgIIAd68xDltoBAwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTEzMDIwNzIxNDg0N1oXDTIzMDIwNzIxNDg0N1owgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDKOFSmy1aqyCQ5SOmM7uxfuH8mkbw0U3rOfGOAYXdkXqUHI7Y5/lAtFVZYcC1+xG7BSoU+L/DehBqhV8mvexj/avoVEkkVCBmsqtsqMu2WY2hSFT2Miuy/axiV4AOsAX2XBWfODoWVN2rtCbauZ81RZJ/GXNG8V25nNYB2NqSHgW44j9grFU57Jdhav06DwY3Sk9UacbVgnJ0zTlX5ElgMhrgWDcHld0WNUEi6Ky3klIXh6MSdxmilsKP8Z35wugJZS3dCkTm59c3hTO/AO0iMpuUhXf1qarunFjVg0uat80YpyejDi+l5wGphZxWy8P3laLxiX27Pmd3vG2P+kmWrAgMBAAGjgaYwgaMwHQYDVR0OBBYEFIgnFwmpthhgi+zruvZHWcVSVKO3MA8GA1UdEwEB/wQFMAMBAf8wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wLgYDVR0fBCcwJTAjoCGgH4YdaHR0cDovL2NybC5hcHBsZS5jb20vcm9vdC5jcmwwDgYDVR0PAQH/BAQDAgGGMBAGCiqGSIb3Y2QGAgEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQBPz+9Zviz1smwvj+4ThzLoBTWobot9yWkMudkXvHcs1Gfi/ZptOllc34MBvbKuKmFysa/Nw0Uwj6ODDc4dR7Txk4qjdJukw5hyhzs+r0ULklS5MruQGFNrCk4QttkdUGwhgAqJTleMa1s8Pab93vcNIx0LSiaHP7qRkkykGRIZbVf1eliHe2iK5IaMSuviSRSqpd1VAKmuu0swruGgsbwpgOYJd+W+NKIByn/c4grmO7i77LpilfMFY0GCzQ87HUyVpNur+cmV6U/kTecmmYHpvPm0KdIBembhLoz2IYrF+Hjhga6/05Cdqa3zr/04GpZnMBxRpVzscYqCtGwPDBUfMIIEuzCCA6OgAwIBAgIBAjANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMDYwNDI1MjE0MDM2WhcNMzUwMjA5MjE0MDM2WjBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDkkakJH5HbHkdQ6wXtXnmELes2oldMVeyLGYne+Uts9QerIjAC6Bg++FAJ039BqJj50cpmnCRrEdCju+QbKsMflZ56DKRHi1vUFjczy8QPTc4UadHJGXL1XQ7Vf1+b8iUDulWPTV0N8WQ1IxVLFVkds5T39pyez1C6wVhQZ48ItCD3y6wsIG9wtj8BMIy3Q88PnT3zK0koGsj+zrW5DtleHNbLPbU6rfQPDgCSC7EhFi501TwN22IWq6NxkkdTVcGvL0Gz+PvjcM3mo0xFfh9Ma1CWQYnEdGILEINBhzOKgbEwWOxaBDKMaLOPHd5lc/9nXmW8Sdh2nzMUZaF3lMktAgMBAAGjggF6MIIBdjAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUK9BpR5R2Cf70a40uQKb3R01/CF4wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wggERBgNVHSAEggEIMIIBBDCCAQAGCSqGSIb3Y2QFATCB8jAqBggrBgEFBQcCARYeaHR0cHM6Ly93d3cuYXBwbGUuY29tL2FwcGxlY2EvMIHDBggrBgEFBQcCAjCBthqBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMA0GCSqGSIb3DQEBBQUAA4IBAQBcNplMLXi37Yyb3PN3m/J20ncwT8EfhYOFG5k9RzfyqZtAjizUsZAS2L70c5vu0mQPy3lPNNiiPvl4/2vIB+x9OYOLUyDTOMSxv5pPCmv/K/xZpwUJfBdAVhEedNO3iyM7R6PVbyTi69G3cN8PReEnyvFteO3ntRcXqNx+IjXKJdXZD9Zr1KIkIxH3oayPc4FgxhtbCS+SsvhESPBgOJ4V9T0mZyCKM2r3DYLP3uujL/lTaltkwGMzd/c6ByxW69oPIQ7aunMZT7XZNn/Bh1XZp5m5MkL72NVxnn6hUrcbvZNCJBIqxw8dtk2cXmPIS4AXUKqK1drk/NAJBzewdXUhMYIByzCCAccCAQEwgaMwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkCCA7rV4fnngmNMAkGBSsOAwIaBQAwDQYJKoZIhvcNAQEBBQAEggEARKLZuaxIeyPbjC7znjQ2D/LFKazDH0fqoH0Mq67qDAr1o6VH2gf/cvAgKjJjxzOO1Be84NL9om/83FTArFGYh52ifq+C/0mGbmF8C6l3q3x7FTTm6EgAuNrP/nUGsAI5JzK5w0elrXTIaS5HowPVL2KjH6QFYqjjmyct/pu/xIv7nEggQzjUgGMqyYkUWnOZ3MpSlyQzrMr+jiKnDEFoioaEvVH3kaZFlpIvvQzjm9/2I/M3RQgCpswhebACW/Umk9kmvBOlC20A1YIXUtPYHuyrsUepFPCckQuWG/DuCX8odPrnwvzzaXJB/86Gmfc7b9H1IBaLepvvhdrbNjJQGQ==', NULL, NULL, NULL, NULL, 'ios', '2019-10-06 19:20:00', '2019-10-06 19:20:00');
INSERT INTO `payment_details` (`id`, `user_id`, `company_id`, `adam_id`, `app_item_id`, `bundle_id`, `version_external_identifier`, `amount`, `quantity`, `product_id`, `transaction_id`, `original_transaction_id`, `purchase_date`, `original_purchase_date`, `expires_date`, `web_order_line_item_id`, `is_trial_period`, `receipt`, `expiration_intent`, `auto_renew_product_id`, `is_in_billing_retry_period`, `auto_renew_status`, `device_type`, `created`, `modified`) VALUES
(3, 44, 22, NULL, NULL, 'com.kenbar.mynus2', NULL, '9.99', NULL, 'com.mynustwo.subscription', '1000000590035313', NULL, NULL, NULL, NULL, NULL, NULL, 'MIJ7MQYJKoZIhvcNAQcCoIJ7IjCCex4CAQExCzAJBgUrDgMCGgUAMIJq0gYJKoZIhvcNAQcBoIJqwwSCar8xgmq7MAoCAQgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgELAgEBBAMCAQAwCwIBDwIBAQQDAgEAMAsCARACAQEEAwIBADALAgEZAgEBBAMCAQMwDAIBCgIBAQQEFgI0KzAMAgEOAgEBBAQCAgCOMA0CAQ0CAQEEBQIDAdZQMA0CARMCAQEEBQwDMS4wMA4CAQkCAQEEBgIEUDI1MzAPAgEDAgEBBAcMBTAuMC4zMBgCAQQCAQIEEAxldVJTKuCLyCNlmLc+kHowGwIBAAIBAQQTDBFQcm9kdWN0aW9uU2FuZGJveDAbAgECAgEBBBMMEWNvbS5rZW5iYXIubXludXMyMBwCAQUCAQEEFJZ9gLDizyfagdMbf9W1VTn0BdplMB4CAQwCAQEEFhYUMjAxOS0xMS0wOFQxODowNzoxNVowHgIBEgIBAQQWFhQyMDEzLTA4LTAxVDA3OjAwOjAwWjBHAgEHAgEBBD/omZ/y+NkrPrDZQz1VPhHKCaGPOIRuP5Hkq5GpjwUgS+EUcMHPIiZdOh4tkcBwv+PDT4V0UPobjidAeAaw58QwYgIBBgIBAQRaVBpVjJ69bT7bGGG1AVhN+wgCmpU5uZeHRX/X2x1qrQVSlaerYcCxyXWCCEW1pVtIzY3nTotHwgmz6PjcrnpoeKw6sWz9q1E8khF03h41l2PjhpRjJVRZeaRSMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyg84DAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTE3MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QwNjo1NToxNFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QwNzoxMDoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoPOEwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTEzMzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMDc6MTA6MTRaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMDc6MjU6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKD3ZMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMzIwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDA3OjI1OjE0WjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDA3OjQwOjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyg+wzAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MDk2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QwNzo0MDoxNFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QwNzo1NToxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoP7gwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTExMDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMDc6NTU6MTRaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMDg6MTA6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKEDFMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMjIwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDA4OjEwOjE0WjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDA4OjI1OjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyhB3DAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTE4MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QxMjoyMjo1MVowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QxMjozNzo1MVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoVsowGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTEyNDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMTI6Mzc6NTFaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMTI6NTI6NTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKFfOMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMzUwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDEyOjUyOjUxWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDEzOjA3OjUxWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyhY1jAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MDk5MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QxMzowNzo1MVowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QxMzoyMjo1MVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoWfUwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTEwMzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMTM6MjI6NTFaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMTM6Mzc6NTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKFsDMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMTUwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDEzOjM3OjUxWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDEzOjUyOjUxWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyhb+zAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTEyMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yMlQxMDo1ODoxNFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0yMlQxMToxMzoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpNVcwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTEzNjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjJUMTE6MTM6MTRaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjJUMTE6Mjg6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKTZYMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkwOTUwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTIyVDExOjI4OjE0WjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTIyVDExOjQzOjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyk3czAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MDk3MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yMlQxMTo0MzoxNFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0yMlQxMTo1ODoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpOFcwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTExNjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjJUMTE6NTg6MTRaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjJUMTI6MTM6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKTlmMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMDAwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTIyVDEyOjEzOjE0WjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTIyVDEyOjI4OjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyk6VzAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTAxMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yNFQwNTo1NDoxMFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0yNFQwNjowOToxMFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpt1wwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTEzMDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjRUMDY6MDk6MTBaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjRUMDY6MjQ6MTBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKbfrMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMTQwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI0VDA2OjI0OjEwWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI0VDA2OjM5OjEwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pym4ozAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTMxMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yNFQwNjozOToxMFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0yNFQwNjo1NDoxMFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpuV4wGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTEwODAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjRUMDY6NTQ6MTBaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjRUMDc6MDk6MTBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKboTMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMzQwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI0VDA3OjA5OjEwWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI0VDA3OjI0OjEwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pym7BjAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTA2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yNFQwOTo1MDoyOVowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0yNFQxMDowNToyOVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpyC8wGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTEzNzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjdUMDU6Mzg6NThaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjdUMDU6NTM6NThaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKhsnMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMjcwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI3VDA1OjUzOjU4WjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI3VDA2OjA4OjU4WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyobrDAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTAyMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yN1QwNjowODo1OFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0yN1QwNjoyMzo1OFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqHE8wGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTEyNTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjdUMDY6MjM6NThaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjdUMDY6Mzg6NThaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKh0LMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMjEwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI3VDA2OjM4OjU4WjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI3VDA2OjUzOjU4WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyod4jAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTA3MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yN1QwNjo1Mzo1OFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0yN1QwNzowODo1OFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqHt0wGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTEyMzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjhUMTI6NDI6MTFaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjhUMTI6NTc6MTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKnhLMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMzgwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI4VDEyOjU3OjEyWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI4VDEzOjEyOjEyWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyp5VDAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTI4MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yOFQxMzoxMjoxMlowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0yOFQxMzoyNzoxMlowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqepIwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTEyMDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjhUMTM6Mjc6MTJaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjhUMTM6NDI6MTJaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKnvFMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMjYwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI4VDEzOjQyOjEyWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI4VDEzOjU3OjEyWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyp9BDAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTI5MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yOFQxMzo1NzoxMlowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOC0yOFQxNDoxMjoxMlowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqfiIwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTExOTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDktMDZUMTM6NDI6MTFaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDktMDZUMTM6NTc6MTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nLJKOMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkwOTgwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA5LTA2VDEzOjU3OjExWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA5LTA2VDE0OjEyOjExWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyyTrDAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTExMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOS0wNlQxNDoxMjoxMVowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOS0wNlQxNDoyNzoxMVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcslP4wGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTEwOTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDktMDZUMTQ6Mjc6MTFaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NTZaMB8CAgasAgEBBBYWFDIwMTgtMDktMDZUMTQ6NDI6MTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nLJZjMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMDUwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA5LTA2VDE0OjQyOjExWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA5LTA2VDE0OjU3OjExWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyyX0TAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTEzMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOS0wNlQxNDo1NzoxMVowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOS0wNlQxNToxMjoxMVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcsmRIwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA1OTA0ODAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NDBaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NDNaMB8CAgasAgEBBBYWFDIwMTgtMTEtMjhUMDY6MTE6NDBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nQALyMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNjQ3MjgwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTExLTI4VDA2OjExOjQwWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA2OjEwOjQ0WjAfAgIGrAIBAQQWFhQyMDE4LTExLTI4VDA2OjI2OjQwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p0ADojAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDczODE3MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0xMS0yOFQwNjoyNjo0MFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNjoyNjo0MVowHwICBqwCAQEEFhYUMjAxOC0xMS0yOFQwNjo0MTo0MFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqdABJIwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA3OTc5MTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMTEtMjhUMDY6NDE6NDBaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDY6NDA6NTNaMB8CAgasAgEBBBYWFDIwMTgtMTEtMjhUMDY6NTY6NDBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nQAWDMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwODg0MzIwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTExLTI4VDA2OjU2OjQwWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA2OjU2OjMwWjAfAgIGrAIBAQQWFhQyMDE4LTExLTI4VDA3OjExOjQwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p0AGjTAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDk3ODM2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0xMS0yOFQwNzoxMTo0MFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNzoxMToxNlowHwICBqwCAQEEFhYUMjAxOC0xMS0yOFQwNzoyNjo0MFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqdAB34wGwICBqcCAQEEEgwQMTAwMDAwMDU3MTYwMTE4OTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjRUMDY6NDQ6MDdaMB8CAgaqAgEBBBYWFDIwMTktMDktMjRUMDY6NDQ6MDhaMB8CAgasAgEBBBYWFDIwMTktMDktMjRUMDY6NTk6MDdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlT0CMBsCAganAgEBBBIMEDEwMDAwMDA1NzE2MDg3NjkwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI0VDA2OjU5OjA3WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI0VDA2OjU4OjIwWjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI0VDA3OjE0OjA3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5U+VDAbAgIGpwIBAQQSDBAxMDAwMDAwNTcxNjE3NDYxMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yNFQwNzoxNDowN1owHwICBqoCAQEEFhYUMjAxOS0wOS0yNFQwNzoxMzowOVowHwICBqwCAQEEFhYUMjAxOS0wOS0yNFQwNzoyOTowN1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeVP+EwGwICBqcCAQEEEgwQMTAwMDAwMDU3MTYyNTU0MjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjRUMDc6Mjk6MDdaMB8CAgaqAgEBBBYWFDIwMTktMDktMjRUMDc6Mjg6MDhaMB8CAgasAgEBBBYWFDIwMTktMDktMjRUMDc6NDQ6MDdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlUFVMBsCAganAgEBBBIMEDEwMDAwMDA1NzE2MzEzNDAwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI0VDA3OjQ0OjA3WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI0VDA3OjQzOjE0WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI0VDA3OjU5OjA3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5VC2DAbAgIGpwIBAQQSDBAxMDAwMDAwNTcxNjM4MzgxMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yNFQwNzo1OTowN1owHwICBqoCAQEEFhYUMjAxOS0wOS0yNFQwNzo1ODoxOVowHwICBqwCAQEEFhYUMjAxOS0wOS0yNFQwODoxNDowN1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeVRM4wGwICBqcCAQEEEgwQMTAwMDAwMDU3MzEyOTcwNjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjdUMDU6MzQ6MzVaMB8CAgaqAgEBBBYWFDIwMTktMDktMjdUMDU6MzQ6MzZaMB8CAgasAgEBBBYWFDIwMTktMDktMjdUMDU6NDk6MzVaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlpMSMBsCAganAgEBBBIMEDEwMDAwMDA1NzMxMzc3MDYwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI3VDA1OjQ5OjM1WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI3VDA1OjQ4OjM3WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI3VDA2OjA0OjM1WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5aT/TAbAgIGpwIBAQQSDBAxMDAwMDAwNTczMTQ1MzM2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yN1QwNjowNDozNVowHwICBqoCAQEEFhYUMjAxOS0wOS0yN1QwNjowMzo0NVowHwICBqwCAQEEFhYUMjAxOS0wOS0yN1QwNjoxOTozNVowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeWlTowGwICBqcCAQEEEgwQMTAwMDAwMDU3MzE1NDc5NTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjdUMDY6MTk6MzVaMB8CAgaqAgEBBBYWFDIwMTktMDktMjdUMDY6MTg6NDBaMB8CAgasAgEBBBYWFDIwMTktMDktMjdUMDY6MzQ6MzVaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlpaRMBsCAganAgEBBBIMEDEwMDAwMDA1NzMxNjM2MTQwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI3VDA2OjM0OjM1WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI3VDA2OjMzOjQwWjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI3VDA2OjQ5OjM1WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5aX4DAbAgIGpwIBAQQSDBAxMDAwMDAwNTczMTc1NzA5MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yN1QwNjo0OTozNVowHwICBqoCAQEEFhYUMjAxOS0wOS0yN1QwNjo0ODo1MVowHwICBqwCAQEEFhYUMjAxOS0wOS0yN1QwNzowNDozNVowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeWmUcwGwICBqcCAQEEEgwQMTAwMDAwMDU3NjE1MDYyMTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTAtMDdUMDI6MTk6NTdaMB8CAgaqAgEBBBYWFDIwMTktMTAtMDdUMDI6MTk6NTlaMB8CAgasAgEBBBYWFDIwMTktMTAtMDdUMDI6MzQ6NTdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nmUjsMBsCAganAgEBBBIMEDEwMDAwMDA1NzYxNTI0ODgwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTEwLTA3VDAyOjM0OjU3WjAfAgIGqgIBAQQWFhQyMDE5LTEwLTA3VDAyOjM0OjA0WjAfAgIGrAIBAQQWFhQyMDE5LTEwLTA3VDAyOjQ5OjU3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5lJRTAbAgIGpwIBAQQSDBAxMDAwMDAwNTc2MTUzODk2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMC0wN1QwMjo0OTo1N1owHwICBqoCAQEEFhYUMjAxOS0xMC0wN1QwMjo0OTowOFowHwICBqwCAQEEFhYUMjAxOS0xMC0wN1QwMzowNDo1N1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeZSZYwGwICBqcCAQEEEgwQMTAwMDAwMDU3NjE1NTU0NjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTAtMDdUMDM6MDQ6NTdaMB8CAgaqAgEBBBYWFDIwMTktMTAtMDdUMDM6MDQ6MDBaMB8CAgasAgEBBBYWFDIwMTktMTAtMDdUMDM6MTk6NTdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nmUn7MBsCAganAgEBBBIMEDEwMDAwMDA1NzYxNTY4MzcwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTEwLTA3VDAzOjE5OjU3WjAfAgIGqgIBAQQWFhQyMDE5LTEwLTA3VDAzOjE5OjAwWjAfAgIGrAIBAQQWFhQyMDE5LTEwLTA3VDAzOjM0OjU3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5lKUTAbAgIGpwIBAQQSDBAxMDAwMDAwNTc2MTU4NTg2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMC0wN1QwMzozNDo1N1owHwICBqoCAQEEFhYUMjAxOS0xMC0wN1QwMzozNDowMFowHwICBqwCAQEEFhYUMjAxOS0xMC0wN1QwMzo0OTo1N1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeZSq4wGwICBqcCAQEEEgwQMTAwMDAwMDU5MDAzNTMxMzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTEtMDhUMTg6MDc6MTRaMB8CAgaqAgEBBBYWFDIwMTktMTEtMDhUMTg6MDc6MTVaMB8CAgasAgEBBBYWFDIwMTktMTEtMDhUMTg6MjI6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb26ggg5lMIIFfDCCBGSgAwIBAgIIDutXh+eeCY0wDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTUxMTEzMDIxNTA5WhcNMjMwMjA3MjE0ODQ3WjCBiTE3MDUGA1UEAwwuTWFjIEFwcCBTdG9yZSBhbmQgaVR1bmVzIFN0b3JlIFJlY2VpcHQgU2lnbmluZzEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApc+B/SWigVvWh+0j2jMcjuIjwKXEJss9xp/sSg1Vhv+kAteXyjlUbX1/slQYncQsUnGOZHuCzom6SdYI5bSIcc8/W0YuxsQduAOpWKIEPiF41du30I4SjYNMWypoN5PC8r0exNKhDEpYUqsS4+3dH5gVkDUtwswSyo1IgfdYeFRr6IwxNh9KBgxHVPM3kLiykol9X6SFSuHAnOC6pLuCl2P0K5PB/T5vysH1PKmPUhrAJQp2Dt7+mf7/wmv1W16sc1FJCFaJzEOQzI6BAtCgl7ZcsaFpaYeQEGgmJjm4HRBzsApdxXPQ33Y72C3ZiB7j7AfP4o7Q0/omVYHv4gNJIwIDAQABo4IB1zCCAdMwPwYIKwYBBQUHAQEEMzAxMC8GCCsGAQUFBzABhiNodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLXd3ZHIwNDAdBgNVHQ4EFgQUkaSc/MR2t5+givRN9Y82Xe0rBIUwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSIJxcJqbYYYIvs67r2R1nFUlSjtzCCAR4GA1UdIASCARUwggERMIIBDQYKKoZIhvdjZAUGATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMA4GA1UdDwEB/wQEAwIHgDAQBgoqhkiG92NkBgsBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEADaYb0y4941srB25ClmzT6IxDMIJf4FzRjb69D70a/CWS24yFw4BZ3+Pi1y4FFKwN27a4/vw1LnzLrRdrjn8f5He5sWeVtBNephmGdvhaIJXnY4wPc/zo7cYfrpn4ZUhcoOAoOsAQNy25oAQ5H3O5yAX98t5/GioqbisB/KAgXNnrfSemM/j1mOC+RNuxTGf8bgpPyeIGqNKX86eOa1GiWoR1ZdEWBGLjwV/1CKnPaNmSAMnBjLP4jQBkulhgwHyvj3XKablbKtYdaG6YQvVMpzcZm8w7HHoZQ/Ojbb9IYAYMNpIr7N4YtRHaLSPQjvygaZwXG56AezlHRTBhL8cTqDCCBCIwggMKoAMCAQICCAHevMQ5baAQMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0xMzAyMDcyMTQ4NDdaFw0yMzAyMDcyMTQ4NDdaMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjhUpstWqsgkOUjpjO7sX7h/JpG8NFN6znxjgGF3ZF6lByO2Of5QLRVWWHAtfsRuwUqFPi/w3oQaoVfJr3sY/2r6FRJJFQgZrKrbKjLtlmNoUhU9jIrsv2sYleADrAF9lwVnzg6FlTdq7Qm2rmfNUWSfxlzRvFduZzWAdjakh4FuOI/YKxVOeyXYWr9Og8GN0pPVGnG1YJydM05V+RJYDIa4Fg3B5XdFjVBIuist5JSF4ejEncZopbCj/Gd+cLoCWUt3QpE5ufXN4UzvwDtIjKblIV39amq7pxY1YNLmrfNGKcnow4vpecBqYWcVsvD95Wi8Yl9uz5nd7xtj/pJlqwIDAQABo4GmMIGjMB0GA1UdDgQWBBSIJxcJqbYYYIvs67r2R1nFUlSjtzAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMC4GA1UdHwQnMCUwI6AhoB+GHWh0dHA6Ly9jcmwuYXBwbGUuY29tL3Jvb3QuY3JsMA4GA1UdDwEB/wQEAwIBhjAQBgoqhkiG92NkBgIBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAT8/vWb4s9bJsL4/uE4cy6AU1qG6LfclpDLnZF7x3LNRn4v2abTpZXN+DAb2yriphcrGvzcNFMI+jgw3OHUe08ZOKo3SbpMOYcoc7Pq9FC5JUuTK7kBhTawpOELbZHVBsIYAKiU5XjGtbPD2m/d73DSMdC0omhz+6kZJMpBkSGW1X9XpYh3toiuSGjErr4kkUqqXdVQCprrtLMK7hoLG8KYDmCXflvjSiAcp/3OIK5ju4u+y6YpXzBWNBgs0POx1MlaTbq/nJlelP5E3nJpmB6bz5tCnSAXpm4S6M9iGKxfh44YGuv9OQnamt86/9OBqWZzAcUaVc7HGKgrRsDwwVHzCCBLswggOjoAMCAQICAQIwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTA2MDQyNTIxNDAzNloXDTM1MDIwOTIxNDAzNlowYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5JGpCR+R2x5HUOsF7V55hC3rNqJXTFXsixmJ3vlLbPUHqyIwAugYPvhQCdN/QaiY+dHKZpwkaxHQo7vkGyrDH5WeegykR4tb1BY3M8vED03OFGnRyRly9V0O1X9fm/IlA7pVj01dDfFkNSMVSxVZHbOU9/acns9QusFYUGePCLQg98usLCBvcLY/ATCMt0PPD5098ytJKBrI/s61uQ7ZXhzWyz21Oq30Dw4AkguxIRYudNU8DdtiFqujcZJHU1XBry9Bs/j743DN5qNMRX4fTGtQlkGJxHRiCxCDQYczioGxMFjsWgQyjGizjx3eZXP/Z15lvEnYdp8zFGWhd5TJLQIDAQABo4IBejCCAXYwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFCvQaUeUdgn+9GuNLkCm90dNfwheMB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMIIBEQYDVR0gBIIBCDCCAQQwggEABgkqhkiG92NkBQEwgfIwKgYIKwYBBQUHAgEWHmh0dHBzOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzCBwwYIKwYBBQUHAgIwgbYagbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjANBgkqhkiG9w0BAQUFAAOCAQEAXDaZTC14t+2Mm9zzd5vydtJ3ME/BH4WDhRuZPUc38qmbQI4s1LGQEti+9HOb7tJkD8t5TzTYoj75eP9ryAfsfTmDi1Mg0zjEsb+aTwpr/yv8WacFCXwXQFYRHnTTt4sjO0ej1W8k4uvRt3DfD0XhJ8rxbXjt57UXF6jcfiI1yiXV2Q/Wa9SiJCMR96Gsj3OBYMYbWwkvkrL4REjwYDieFfU9JmcgijNq9w2Cz97roy/5U2pbZMBjM3f3OgcsVuvaDyEO2rpzGU+12TZ/wYdV2aeZuTJC+9jVcZ5+oVK3G72TQiQSKscPHbZNnF5jyEuAF1CqitXa5PzQCQc3sHV1ITGCAcswggHHAgEBMIGjMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5AggO61eH554JjTAJBgUrDgMCGgUAMA0GCSqGSIb3DQEBAQUABIIBAE2tiKqg4B9qmMtafssntuNE4pa9Ly8d6EzWo02Hj3S++1kpsn/x6L21f5ekXSMnJJqS2DlVQ6+sHQOuU6mJEeiAZzB/cLdPiVpzBP7v+65awwP51RnAqyuNY+jyOvGdU8rTpHQNxDTPn55ZpGqiuC/pRDLt9FVi09cnpI9ATURSQe56g/S72ua65guK2lSCqEdVcdltMJEMHyEhri5w5jG7V9L96TuBqb9db3nRV6o4yt01FgzRtuRO+8dvGGyuCV+fw9MYez3SGOlFhncUtwwoDsTv5rNDwjxCf/NOeHBNhqKsZ5nNivOzVt0pNnoBi1zClxSWutD+5zpyKx3H3RM=', NULL, NULL, NULL, NULL, 'ios', '2019-11-08 10:07:16', '2019-11-08 10:07:16');
INSERT INTO `payment_details` (`id`, `user_id`, `company_id`, `adam_id`, `app_item_id`, `bundle_id`, `version_external_identifier`, `amount`, `quantity`, `product_id`, `transaction_id`, `original_transaction_id`, `purchase_date`, `original_purchase_date`, `expires_date`, `web_order_line_item_id`, `is_trial_period`, `receipt`, `expiration_intent`, `auto_renew_product_id`, `is_in_billing_retry_period`, `auto_renew_status`, `device_type`, `created`, `modified`) VALUES
(4, 38, 14, NULL, NULL, 'com.kenbar.mynus2', NULL, '9.99', NULL, 'com.mynustwo.subscription', '1000000599127594', NULL, NULL, NULL, NULL, NULL, NULL, 'MIJWKwYJKoZIhvcNAQcCoIJWHDCCVhgCAQExCzAJBgUrDgMCGgUAMIJFzAYJKoZIhvcNAQcBoIJFvQSCRbkxgkW1MAoCAQgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgEDAgEBBAMMATYwCwIBCwIBAQQDAgEAMAsCAQ8CAQEEAwIBADALAgEQAgEBBAMCAQAwCwIBGQIBAQQDAgEDMAwCAQoCAQEEBBYCNCswDAIBDgIBAQQEAgIAvTANAgENAgEBBAUCAwHVJDANAgETAgEBBAUMAzEuMDAOAgEJAgEBBAYCBFAyNTMwGAIBBAIBAgQQFP6Zy59poZbkAD3QKMWcBDAbAgEAAgEBBBMMEVByb2R1Y3Rpb25TYW5kYm94MBsCAQICAQEEEwwRY29tLmtlbmJhci5teW51czIwHAIBBQIBAQQUR+mBjKbVhY9D6fsWHxS8uD0QzWcwHgIBDAIBAQQWFhQyMDE5LTExLTI5VDEyOjA5OjAzWjAeAgESAgEBBBYWFDIwMTMtMDgtMDFUMDc6MDA6MDBaMD8CAQcCAQEEN47nGhtoC44sXfkIO9To20OgV9AxurC62g1phA3/y0Ie2rH7diYTTSSEJ7qRpylqCQgpXq20kwswWAIBBgIBAQRQoHZ++aEz+zzTggGcfgLqCta2wHVjA7AqKOKj3mB4LLofhGXDzEDILYt7qWJ4LUhRuGJlhwGKaSEviDtKTeNsTfrXM1VrTo42ILdKFciNgYcwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKDzgMBsCAganAgEBBBIMEDEwMDAwMDA1NzE2MDIzNDEwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDA2OjU1OjE0WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI0VDA2OjQ2OjE4WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDA3OjEwOjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyg84TAbAgIGpwIBAQQSDBAxMDAwMDAwNTcxNjAyMzM3MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QwNzoxMDoxNFowHwICBqoCAQEEFhYUMjAxOS0wOS0yNFQwNjo0NjoxOFowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QwNzoyNToxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoPdkwGwICBqcCAQEEEgwQMTAwMDAwMDU3MTYwMjM0NTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMDc6MjU6MTRaMB8CAgaqAgEBBBYWFDIwMTktMDktMjRUMDY6NDY6MThaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMDc6NDA6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKD7DMBsCAganAgEBBBIMEDEwMDAwMDA1NzE2MDIzMzYwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDA3OjQwOjE0WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI0VDA2OjQ2OjE4WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDA3OjU1OjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyg/uDAbAgIGpwIBAQQSDBAxMDAwMDAwNTcxNjAyMzQ0MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QwNzo1NToxNFowHwICBqoCAQEEFhYUMjAxOS0wOS0yNFQwNjo0NjoxOFowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QwODoxMDoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoQMUwGwICBqcCAQEEEgwQMTAwMDAwMDU3MTYwMjMzNDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMDg6MTA6MTRaMB8CAgaqAgEBBBYWFDIwMTktMDktMjRUMDY6NDY6MThaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMDg6MjU6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKEHcMBsCAganAgEBBBIMEDEwMDAwMDA1NzE2MDIzNDAwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDEyOjIyOjUxWjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI0VDA2OjQ2OjE4WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDEyOjM3OjUxWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyhWyjAbAgIGpwIBAQQSDBAxMDAwMDAwNTcxNjAyMzM4MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QxMjozNzo1MVowHwICBqoCAQEEFhYUMjAxOS0wOS0yNFQwNjo0NjoxOFowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QxMjo1Mjo1MVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoV84wGwICBqcCAQEEEgwQMTAwMDAwMDU3MTYwMjM0MzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMTI6NTI6NTFaMB8CAgaqAgEBBBYWFDIwMTktMDktMjRUMDY6NDY6MThaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMTM6MDc6NTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKFjWMBsCAganAgEBBBIMEDEwMDAwMDA1NzE2MDIzNDIwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDEzOjA3OjUxWjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI0VDA2OjQ2OjE4WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDEzOjIyOjUxWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyhZ9TAbAgIGpwIBAQQSDBAxMDAwMDAwNTcxNjAyMzQ2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QxMzoyMjo1MVowHwICBqoCAQEEFhYUMjAxOS0wOS0yNFQwNjo0NjoxOFowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QxMzozNzo1MVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoWwMwGwICBqcCAQEEEgwQMTAwMDAwMDU3MTYwMjMzOTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMTM6Mzc6NTFaMB8CAgaqAgEBBBYWFDIwMTktMDktMjRUMDY6NDY6MThaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMTM6NTI6NTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nLJOsMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNTkxMTEwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA5LTA2VDE0OjEyOjExWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA1OjU2OjU2WjAfAgIGrAIBAQQWFhQyMDE4LTA5LTA2VDE0OjI3OjExWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyyWYzAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDU5MTA1MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOS0wNlQxNDo0MjoxMVowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNTo1Njo1NlowHwICBqwCAQEEFhYUMjAxOC0wOS0wNlQxNDo1NzoxMVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqdAAvIwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA2NDcyODAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMTEtMjhUMDY6MTE6NDBaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDY6MTA6NDRaMB8CAgasAgEBBBYWFDIwMTgtMTEtMjhUMDY6MjY6NDBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nQAOiMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwNzM4MTcwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTExLTI4VDA2OjI2OjQwWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA2OjI2OjQxWjAfAgIGrAIBAQQWFhQyMDE4LTExLTI4VDA2OjQxOjQwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p0AEkjAbAgIGpwIBAQQSDBAxMDAwMDAwNDc5MDc5NzkxMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0xMS0yOFQwNjo0MTo0MFowHwICBqoCAQEEFhYUMjAxOC0xMS0yOFQwNjo0MDo1M1owHwICBqwCAQEEFhYUMjAxOC0xMS0yOFQwNjo1Njo0MFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqdABYMwGwICBqcCAQEEEgwQMTAwMDAwMDQ3OTA4ODQzMjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMTEtMjhUMDY6NTY6NDBaMB8CAgaqAgEBBBYWFDIwMTgtMTEtMjhUMDY6NTY6MzBaMB8CAgasAgEBBBYWFDIwMTgtMTEtMjhUMDc6MTE6NDBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nQAaNMBsCAganAgEBBBIMEDEwMDAwMDA0NzkwOTc4MzYwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTExLTI4VDA3OjExOjQwWjAfAgIGqgIBAQQWFhQyMDE4LTExLTI4VDA3OjExOjE2WjAfAgIGrAIBAQQWFhQyMDE4LTExLTI4VDA3OjI2OjQwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p0AHfjAbAgIGpwIBAQQSDBAxMDAwMDAwNTcxNjAxMTg5MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yNFQwNjo0NDowN1owHwICBqoCAQEEFhYUMjAxOS0wOS0yNFQwNjo0NDowOFowHwICBqwCAQEEFhYUMjAxOS0wOS0yNFQwNjo1OTowN1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeVPQIwGwICBqcCAQEEEgwQMTAwMDAwMDU3MTYwODc2OTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjRUMDY6NTk6MDdaMB8CAgaqAgEBBBYWFDIwMTktMDktMjRUMDY6NTg6MjBaMB8CAgasAgEBBBYWFDIwMTktMDktMjRUMDc6MTQ6MDdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlT5UMBsCAganAgEBBBIMEDEwMDAwMDA1NzE2MTc0NjEwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI0VDA3OjE0OjA3WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI0VDA3OjEzOjA5WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI0VDA3OjI5OjA3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5U/4TAbAgIGpwIBAQQSDBAxMDAwMDAwNTcxNjI1NTQyMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yNFQwNzoyOTowN1owHwICBqoCAQEEFhYUMjAxOS0wOS0yNFQwNzoyODowOFowHwICBqwCAQEEFhYUMjAxOS0wOS0yNFQwNzo0NDowN1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeVQVUwGwICBqcCAQEEEgwQMTAwMDAwMDU3MTYzMTM0MDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjRUMDc6NDQ6MDdaMB8CAgaqAgEBBBYWFDIwMTktMDktMjRUMDc6NDM6MTRaMB8CAgasAgEBBBYWFDIwMTktMDktMjRUMDc6NTk6MDdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlULYMBsCAganAgEBBBIMEDEwMDAwMDA1NzE2MzgzODEwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI0VDA3OjU5OjA3WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI0VDA3OjU4OjE5WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI0VDA4OjE0OjA3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5VEzjAbAgIGpwIBAQQSDBAxMDAwMDAwNTczMTI5NzA2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yN1QwNTozNDozNVowHwICBqoCAQEEFhYUMjAxOS0wOS0yN1QwNTozNDozNlowHwICBqwCAQEEFhYUMjAxOS0wOS0yN1QwNTo0OTozNVowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeWkxIwGwICBqcCAQEEEgwQMTAwMDAwMDU3MzEzNzcwNjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjdUMDU6NDk6MzVaMB8CAgaqAgEBBBYWFDIwMTktMDktMjdUMDU6NDg6MzdaMB8CAgasAgEBBBYWFDIwMTktMDktMjdUMDY6MDQ6MzVaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlpP9MBsCAganAgEBBBIMEDEwMDAwMDA1NzMxNDUzMzYwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI3VDA2OjA0OjM1WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI3VDA2OjAzOjQ1WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI3VDA2OjE5OjM1WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5aVOjAbAgIGpwIBAQQSDBAxMDAwMDAwNTczMTU0Nzk1MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yN1QwNjoxOTozNVowHwICBqoCAQEEFhYUMjAxOS0wOS0yN1QwNjoxODo0MFowHwICBqwCAQEEFhYUMjAxOS0wOS0yN1QwNjozNDozNVowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeWlpEwGwICBqcCAQEEEgwQMTAwMDAwMDU3MzE2MzYxNDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjdUMDY6MzQ6MzVaMB8CAgaqAgEBBBYWFDIwMTktMDktMjdUMDY6MzM6NDBaMB8CAgasAgEBBBYWFDIwMTktMDktMjdUMDY6NDk6MzVaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlpfgMBsCAganAgEBBBIMEDEwMDAwMDA1NzMxNzU3MDkwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI3VDA2OjQ5OjM1WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI3VDA2OjQ4OjUxWjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI3VDA3OjA0OjM1WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5aZRzAbAgIGpwIBAQQSDBAxMDAwMDAwNTc2MTUwNjIxMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMC0wN1QwMjoxOTo1N1owHwICBqoCAQEEFhYUMjAxOS0xMC0wN1QwMjoxOTo1OVowHwICBqwCAQEEFhYUMjAxOS0xMC0wN1QwMjozNDo1N1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeZSOwwGwICBqcCAQEEEgwQMTAwMDAwMDU3NjE1MjQ4ODAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTAtMDdUMDI6MzQ6NTdaMB8CAgaqAgEBBBYWFDIwMTktMTAtMDdUMDI6MzQ6MDRaMB8CAgasAgEBBBYWFDIwMTktMTAtMDdUMDI6NDk6NTdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nmUlFMBsCAganAgEBBBIMEDEwMDAwMDA1NzYxNTM4OTYwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTEwLTA3VDAyOjQ5OjU3WjAfAgIGqgIBAQQWFhQyMDE5LTEwLTA3VDAyOjQ5OjA4WjAfAgIGrAIBAQQWFhQyMDE5LTEwLTA3VDAzOjA0OjU3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5lJljAbAgIGpwIBAQQSDBAxMDAwMDAwNTc2MTU1NTQ2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMC0wN1QwMzowNDo1N1owHwICBqoCAQEEFhYUMjAxOS0xMC0wN1QwMzowNDowMFowHwICBqwCAQEEFhYUMjAxOS0xMC0wN1QwMzoxOTo1N1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeZSfswGwICBqcCAQEEEgwQMTAwMDAwMDU3NjE1NjgzNzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTAtMDdUMDM6MTk6NTdaMB8CAgaqAgEBBBYWFDIwMTktMTAtMDdUMDM6MTk6MDBaMB8CAgasAgEBBBYWFDIwMTktMTAtMDdUMDM6MzQ6NTdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nmUpRMBsCAganAgEBBBIMEDEwMDAwMDA1NzYxNTg1ODYwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTEwLTA3VDAzOjM0OjU3WjAfAgIGqgIBAQQWFhQyMDE5LTEwLTA3VDAzOjM0OjAwWjAfAgIGrAIBAQQWFhQyMDE5LTEwLTA3VDAzOjQ5OjU3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5lKrjAbAgIGpwIBAQQSDBAxMDAwMDAwNTkwMDM1MzE0MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMS0wOFQxODowNzoxNFowHwICBqoCAQEEFhYUMjAxOS0xMS0wOFQxODowNzoxNVowHwICBqwCAQEEFhYUMjAxOS0xMS0wOFQxODoyMjoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqekvR8wGwICBqcCAQEEEgwQMTAwMDAwMDU5MDAzOTk4NzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTEtMDhUMTg6MjI6MTRaMB8CAgaqAgEBBBYWFDIwMTktMTEtMDhUMTg6MjE6MTZaMB8CAgasAgEBBBYWFDIwMTktMTEtMDhUMTg6Mzc6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6npL3dMBsCAganAgEBBBIMEDEwMDAwMDA1OTAwNDMwMjcwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTExLTA4VDE4OjM3OjE0WjAfAgIGqgIBAQQWFhQyMDE5LTExLTA4VDE4OjM2OjIwWjAfAgIGrAIBAQQWFhQyMDE5LTExLTA4VDE4OjUyOjE0WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p6S+0zAbAgIGpwIBAQQSDBAxMDAwMDAwNTkwMDQ1NTYyMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMS0wOFQxODo1MjoxNFowHwICBqoCAQEEFhYUMjAxOS0xMS0wOFQxODo1MToyMFowHwICBqwCAQEEFhYUMjAxOS0xMS0wOFQxOTowNzoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqekv7owGwICBqcCAQEEEgwQMTAwMDAwMDU5MDA0NzQzNTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTEtMDhUMTk6MDc6MTRaMB8CAgaqAgEBBBYWFDIwMTktMTEtMDhUMTk6MDY6MTlaMB8CAgasAgEBBBYWFDIwMTktMTEtMDhUMTk6MjI6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6npMCEMBsCAganAgEBBBIMEDEwMDAwMDA1OTAwNDg3NzQwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTExLTA4VDE5OjIyOjE0WjAfAgIGqgIBAQQWFhQyMDE5LTExLTA4VDE5OjIxOjIyWjAfAgIGrAIBAQQWFhQyMDE5LTExLTA4VDE5OjM3OjE0WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p6TBWTAbAgIGpwIBAQQSDBAxMDAwMDAwNTk5MTI3NTk0MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMS0yOVQxMjowOTowMlowHwICBqoCAQEEFhYUMjAxOS0xMS0yOVQxMjowOTowM1owHwICBqwCAQEEFhYUMjAxOS0xMS0yOVQxMjoyNDowMlowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbqCCDmUwggV8MIIEZKADAgECAggO61eH554JjTANBgkqhkiG9w0BAQUFADCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTAeFw0xNTExMTMwMjE1MDlaFw0yMzAyMDcyMTQ4NDdaMIGJMTcwNQYDVQQDDC5NYWMgQXBwIFN0b3JlIGFuZCBpVHVuZXMgU3RvcmUgUmVjZWlwdCBTaWduaW5nMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczETMBEGA1UECgwKQXBwbGUgSW5jLjELMAkGA1UEBhMCVVMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQClz4H9JaKBW9aH7SPaMxyO4iPApcQmyz3Gn+xKDVWG/6QC15fKOVRtfX+yVBidxCxScY5ke4LOibpJ1gjltIhxzz9bRi7GxB24A6lYogQ+IXjV27fQjhKNg0xbKmg3k8LyvR7E0qEMSlhSqxLj7d0fmBWQNS3CzBLKjUiB91h4VGvojDE2H0oGDEdU8zeQuLKSiX1fpIVK4cCc4Lqku4KXY/Qrk8H9Pm/KwfU8qY9SGsAlCnYO3v6Z/v/Ca/VbXqxzUUkIVonMQ5DMjoEC0KCXtlyxoWlph5AQaCYmObgdEHOwCl3Fc9DfdjvYLdmIHuPsB8/ijtDT+iZVge/iA0kjAgMBAAGjggHXMIIB0zA/BggrBgEFBQcBAQQzMDEwLwYIKwYBBQUHMAGGI2h0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDMtd3dkcjA0MB0GA1UdDgQWBBSRpJz8xHa3n6CK9E31jzZd7SsEhTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFIgnFwmpthhgi+zruvZHWcVSVKO3MIIBHgYDVR0gBIIBFTCCAREwggENBgoqhkiG92NkBQYBMIH+MIHDBggrBgEFBQcCAjCBtgyBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMDYGCCsGAQUFBwIBFipodHRwOi8vd3d3LmFwcGxlLmNvbS9jZXJ0aWZpY2F0ZWF1dGhvcml0eS8wDgYDVR0PAQH/BAQDAgeAMBAGCiqGSIb3Y2QGCwEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQANphvTLj3jWysHbkKWbNPojEMwgl/gXNGNvr0PvRr8JZLbjIXDgFnf4+LXLgUUrA3btrj+/DUufMutF2uOfx/kd7mxZ5W0E16mGYZ2+FogledjjA9z/Ojtxh+umfhlSFyg4Cg6wBA3LbmgBDkfc7nIBf3y3n8aKipuKwH8oCBc2et9J6Yz+PWY4L5E27FMZ/xuCk/J4gao0pfzp45rUaJahHVl0RYEYuPBX/UIqc9o2ZIAycGMs/iNAGS6WGDAfK+PdcppuVsq1h1obphC9UynNxmbzDscehlD86Ntv0hgBgw2kivs3hi1EdotI9CO/KBpnBcbnoB7OUdFMGEvxxOoMIIEIjCCAwqgAwIBAgIIAd68xDltoBAwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTEzMDIwNzIxNDg0N1oXDTIzMDIwNzIxNDg0N1owgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDKOFSmy1aqyCQ5SOmM7uxfuH8mkbw0U3rOfGOAYXdkXqUHI7Y5/lAtFVZYcC1+xG7BSoU+L/DehBqhV8mvexj/avoVEkkVCBmsqtsqMu2WY2hSFT2Miuy/axiV4AOsAX2XBWfODoWVN2rtCbauZ81RZJ/GXNG8V25nNYB2NqSHgW44j9grFU57Jdhav06DwY3Sk9UacbVgnJ0zTlX5ElgMhrgWDcHld0WNUEi6Ky3klIXh6MSdxmilsKP8Z35wugJZS3dCkTm59c3hTO/AO0iMpuUhXf1qarunFjVg0uat80YpyejDi+l5wGphZxWy8P3laLxiX27Pmd3vG2P+kmWrAgMBAAGjgaYwgaMwHQYDVR0OBBYEFIgnFwmpthhgi+zruvZHWcVSVKO3MA8GA1UdEwEB/wQFMAMBAf8wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wLgYDVR0fBCcwJTAjoCGgH4YdaHR0cDovL2NybC5hcHBsZS5jb20vcm9vdC5jcmwwDgYDVR0PAQH/BAQDAgGGMBAGCiqGSIb3Y2QGAgEEAgUAMA0GCSqGSIb3DQEBBQUAA4IBAQBPz+9Zviz1smwvj+4ThzLoBTWobot9yWkMudkXvHcs1Gfi/ZptOllc34MBvbKuKmFysa/Nw0Uwj6ODDc4dR7Txk4qjdJukw5hyhzs+r0ULklS5MruQGFNrCk4QttkdUGwhgAqJTleMa1s8Pab93vcNIx0LSiaHP7qRkkykGRIZbVf1eliHe2iK5IaMSuviSRSqpd1VAKmuu0swruGgsbwpgOYJd+W+NKIByn/c4grmO7i77LpilfMFY0GCzQ87HUyVpNur+cmV6U/kTecmmYHpvPm0KdIBembhLoz2IYrF+Hjhga6/05Cdqa3zr/04GpZnMBxRpVzscYqCtGwPDBUfMIIEuzCCA6OgAwIBAgIBAjANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMDYwNDI1MjE0MDM2WhcNMzUwMjA5MjE0MDM2WjBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDkkakJH5HbHkdQ6wXtXnmELes2oldMVeyLGYne+Uts9QerIjAC6Bg++FAJ039BqJj50cpmnCRrEdCju+QbKsMflZ56DKRHi1vUFjczy8QPTc4UadHJGXL1XQ7Vf1+b8iUDulWPTV0N8WQ1IxVLFVkds5T39pyez1C6wVhQZ48ItCD3y6wsIG9wtj8BMIy3Q88PnT3zK0koGsj+zrW5DtleHNbLPbU6rfQPDgCSC7EhFi501TwN22IWq6NxkkdTVcGvL0Gz+PvjcM3mo0xFfh9Ma1CWQYnEdGILEINBhzOKgbEwWOxaBDKMaLOPHd5lc/9nXmW8Sdh2nzMUZaF3lMktAgMBAAGjggF6MIIBdjAOBgNVHQ8BAf8EBAMCAQYwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUK9BpR5R2Cf70a40uQKb3R01/CF4wHwYDVR0jBBgwFoAUK9BpR5R2Cf70a40uQKb3R01/CF4wggERBgNVHSAEggEIMIIBBDCCAQAGCSqGSIb3Y2QFATCB8jAqBggrBgEFBQcCARYeaHR0cHM6Ly93d3cuYXBwbGUuY29tL2FwcGxlY2EvMIHDBggrBgEFBQcCAjCBthqBs1JlbGlhbmNlIG9uIHRoaXMgY2VydGlmaWNhdGUgYnkgYW55IHBhcnR5IGFzc3VtZXMgYWNjZXB0YW5jZSBvZiB0aGUgdGhlbiBhcHBsaWNhYmxlIHN0YW5kYXJkIHRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZSwgY2VydGlmaWNhdGUgcG9saWN5IGFuZCBjZXJ0aWZpY2F0aW9uIHByYWN0aWNlIHN0YXRlbWVudHMuMA0GCSqGSIb3DQEBBQUAA4IBAQBcNplMLXi37Yyb3PN3m/J20ncwT8EfhYOFG5k9RzfyqZtAjizUsZAS2L70c5vu0mQPy3lPNNiiPvl4/2vIB+x9OYOLUyDTOMSxv5pPCmv/K/xZpwUJfBdAVhEedNO3iyM7R6PVbyTi69G3cN8PReEnyvFteO3ntRcXqNx+IjXKJdXZD9Zr1KIkIxH3oayPc4FgxhtbCS+SsvhESPBgOJ4V9T0mZyCKM2r3DYLP3uujL/lTaltkwGMzd/c6ByxW69oPIQ7aunMZT7XZNn/Bh1XZp5m5MkL72NVxnn6hUrcbvZNCJBIqxw8dtk2cXmPIS4AXUKqK1drk/NAJBzewdXUhMYIByzCCAccCAQEwgaMwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkCCA7rV4fnngmNMAkGBSsOAwIaBQAwDQYJKoZIhvcNAQEBBQAEggEAa2uMsJBMTTLSW2IAAHAeSEBIOGtb7zPld0mwZMjF3AgTxXFqdCby7kfO/uGFUf910esGCUyMRTHkEYJh5f05FWuM2nk+aqqQ4vV0WD+ndHeUnZskVixldZKtKLk9oyQXnDX8Hco0uBnMzysDI70aGhSIghcJ3FsNCE7kRt8DuTZxxuEpEkwaqeqClyJcKXHHarYDcjg7VgxeE6tyeF95fB8WyktODfTTZkshk6KkrU089zNArvPtupb0ZgOSTb9b2CwXBBNrk33rRRZPM6beJWuY69QqJeT/9sJ33XPZ3lajCyFnBsa2WbcsZKBe4n4vw3o4dtZqCb07A/cspGphTg==', NULL, NULL, NULL, NULL, 'ios', '2019-11-29 04:09:04', '2019-11-29 04:09:04');
INSERT INTO `payment_details` (`id`, `user_id`, `company_id`, `adam_id`, `app_item_id`, `bundle_id`, `version_external_identifier`, `amount`, `quantity`, `product_id`, `transaction_id`, `original_transaction_id`, `purchase_date`, `original_purchase_date`, `expires_date`, `web_order_line_item_id`, `is_trial_period`, `receipt`, `expiration_intent`, `auto_renew_product_id`, `is_in_billing_retry_period`, `auto_renew_status`, `device_type`, `created`, `modified`) VALUES
(5, 44, 22, NULL, NULL, 'com.kenbar.mynus2', NULL, '9.99', NULL, 'com.mynustwo.subscription', '1000000609262206', NULL, NULL, NULL, NULL, NULL, NULL, 'MIJCQQYJKoZIhvcNAQcCoIJCMjCCQi4CAQExCzAJBgUrDgMCGgUAMIIx4gYJKoZIhvcNAQcBoIIx0wSCMc8xgjHLMAoCAQgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgELAgEBBAMCAQAwCwIBDwIBAQQDAgEAMAsCARACAQEEAwIBADALAgEZAgEBBAMCAQMwDAIBAwIBAQQEDAIxMDAMAgEKAgEBBAQWAjQrMAwCAQ4CAQEEBAICAI4wDQIBDQIBAQQFAgMB1lAwDQIBEwIBAQQFDAMxLjAwDgIBCQIBAQQGAgRQMjUzMBgCAQQCAQIEEM0kGa1hvPOYw6e6YYpHo2AwGwIBAAIBAQQTDBFQcm9kdWN0aW9uU2FuZGJveDAbAgECAgEBBBMMEWNvbS5rZW5iYXIubXludXMyMBwCAQUCAQEEFPQMoYkIXU1tUpRo32VR2c6YT288MB4CAQwCAQEEFhYUMjAxOS0xMi0yN1QwNTozMDoyN1owHgIBEgIBAQQWFhQyMDEzLTA4LTAxVDA3OjAwOjAwWjBQAgEHAgEBBEh0dpRws9eTROHbKx04AqAiZP4LrBPBCaPn/ddSdjuHbNtFe7eSAaLuwfAXsixvtMXQDTmguLFPvXXOuT16q777fDmYoiCN9PAwXgIBBgIBAQRWvoH5NVtB7oARJ11zCKBskMd3Nzr64FjG9mvJ8r3wr5u10tQoUQGxexRsffVvAov+UUhYXYWmJtfaShGH7TdasAVpucL543i2VDAtAOrtleJiVWTDQCUwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nQAd+MBsCAganAgEBBBIMEDEwMDAwMDA1NzE2MDkyMDYwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI0VDA2OjQ0OjA3WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI0VDA2OjU5OjI1WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI0VDA2OjU5OjA3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5U9AjAbAgIGpwIBAQQSDBAxMDAwMDAwNTcxNjA4NzY5MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yNFQwNjo1OTowN1owHwICBqoCAQEEFhYUMjAxOS0wOS0yNFQwNjo1ODoyMFowHwICBqwCAQEEFhYUMjAxOS0wOS0yNFQwNzoxNDowN1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeVPlQwGwICBqcCAQEEEgwQMTAwMDAwMDU3MTYxNzQ2MTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjRUMDc6MTQ6MDdaMB8CAgaqAgEBBBYWFDIwMTktMDktMjRUMDc6MTM6MDlaMB8CAgasAgEBBBYWFDIwMTktMDktMjRUMDc6Mjk6MDdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlT/hMBsCAganAgEBBBIMEDEwMDAwMDA1NzE2MjU1NDIwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI0VDA3OjI5OjA3WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI0VDA3OjI4OjA4WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI0VDA3OjQ0OjA3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5VBVTAbAgIGpwIBAQQSDBAxMDAwMDAwNTcxNjMxMzQwMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yNFQwNzo0NDowN1owHwICBqoCAQEEFhYUMjAxOS0wOS0yNFQwNzo0MzoxNFowHwICBqwCAQEEFhYUMjAxOS0wOS0yNFQwNzo1OTowN1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeVQtgwGwICBqcCAQEEEgwQMTAwMDAwMDU3MTYzODM4MTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjRUMDc6NTk6MDdaMB8CAgaqAgEBBBYWFDIwMTktMDktMjRUMDc6NTg6MTlaMB8CAgasAgEBBBYWFDIwMTktMDktMjRUMDg6MTQ6MDdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlUTOMBsCAganAgEBBBIMEDEwMDAwMDA1NzMxMjk3MDYwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI3VDA1OjM0OjM1WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI3VDA1OjM0OjM2WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI3VDA1OjQ5OjM1WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5aTEjAbAgIGpwIBAQQSDBAxMDAwMDAwNTczMTM3NzA2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yN1QwNTo0OTozNVowHwICBqoCAQEEFhYUMjAxOS0wOS0yN1QwNTo0ODozN1owHwICBqwCAQEEFhYUMjAxOS0wOS0yN1QwNjowNDozNVowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeWk/0wGwICBqcCAQEEEgwQMTAwMDAwMDU3MzE0NTMzNjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjdUMDY6MDQ6MzVaMB8CAgaqAgEBBBYWFDIwMTktMDktMjdUMDY6MDM6NDVaMB8CAgasAgEBBBYWFDIwMTktMDktMjdUMDY6MTk6MzVaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlpU6MBsCAganAgEBBBIMEDEwMDAwMDA1NzMxNTQ3OTUwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI3VDA2OjE5OjM1WjAfAgIGqgIBAQQWFhQyMDE5LTA5LTI3VDA2OjE4OjQwWjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI3VDA2OjM0OjM1WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5aWkTAbAgIGpwIBAQQSDBAxMDAwMDAwNTczMTYzNjE0MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yN1QwNjozNDozNVowHwICBqoCAQEEFhYUMjAxOS0wOS0yN1QwNjozMzo0MFowHwICBqwCAQEEFhYUMjAxOS0wOS0yN1QwNjo0OTozNVowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeWl+AwGwICBqcCAQEEEgwQMTAwMDAwMDU3MzE3NTcwOTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjdUMDY6NDk6MzVaMB8CAgaqAgEBBBYWFDIwMTktMDktMjdUMDY6NDg6NTFaMB8CAgasAgEBBBYWFDIwMTktMDktMjdUMDc6MDQ6MzVaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlplHMBsCAganAgEBBBIMEDEwMDAwMDA1NzYxNTA2MjEwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTEwLTA3VDAyOjE5OjU3WjAfAgIGqgIBAQQWFhQyMDE5LTEwLTA3VDAyOjE5OjU5WjAfAgIGrAIBAQQWFhQyMDE5LTEwLTA3VDAyOjM0OjU3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5lI7DAbAgIGpwIBAQQSDBAxMDAwMDAwNTc2MTUyNDg4MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMC0wN1QwMjozNDo1N1owHwICBqoCAQEEFhYUMjAxOS0xMC0wN1QwMjozNDowNFowHwICBqwCAQEEFhYUMjAxOS0xMC0wN1QwMjo0OTo1N1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeZSUUwGwICBqcCAQEEEgwQMTAwMDAwMDU3NjE1Mzg5NjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTAtMDdUMDI6NDk6NTdaMB8CAgaqAgEBBBYWFDIwMTktMTAtMDdUMDI6NDk6MDhaMB8CAgasAgEBBBYWFDIwMTktMTAtMDdUMDM6MDQ6NTdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nmUmWMBsCAganAgEBBBIMEDEwMDAwMDA1NzYxNTU1NDYwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTEwLTA3VDAzOjA0OjU3WjAfAgIGqgIBAQQWFhQyMDE5LTEwLTA3VDAzOjA0OjAwWjAfAgIGrAIBAQQWFhQyMDE5LTEwLTA3VDAzOjE5OjU3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5lJ+zAbAgIGpwIBAQQSDBAxMDAwMDAwNTc2MTU2ODM3MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMC0wN1QwMzoxOTo1N1owHwICBqoCAQEEFhYUMjAxOS0xMC0wN1QwMzoxOTowMFowHwICBqwCAQEEFhYUMjAxOS0xMC0wN1QwMzozNDo1N1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeZSlEwGwICBqcCAQEEEgwQMTAwMDAwMDU3NjE1ODU4NjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTAtMDdUMDM6MzQ6NTdaMB8CAgaqAgEBBBYWFDIwMTktMTAtMDdUMDM6MzQ6MDBaMB8CAgasAgEBBBYWFDIwMTktMTAtMDdUMDM6NDk6NTdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nmUquMBsCAganAgEBBBIMEDEwMDAwMDA1OTAwMzUzMTQwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTExLTA4VDE4OjA3OjE0WjAfAgIGqgIBAQQWFhQyMDE5LTExLTA4VDE4OjA3OjE1WjAfAgIGrAIBAQQWFhQyMDE5LTExLTA4VDE4OjIyOjE0WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p6S9HzAbAgIGpwIBAQQSDBAxMDAwMDAwNTkwMDM5OTg3MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMS0wOFQxODoyMjoxNFowHwICBqoCAQEEFhYUMjAxOS0xMS0wOFQxODoyMToxNlowHwICBqwCAQEEFhYUMjAxOS0xMS0wOFQxODozNzoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqekvd0wGwICBqcCAQEEEgwQMTAwMDAwMDU5MDA0MzAyNzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTEtMDhUMTg6Mzc6MTRaMB8CAgaqAgEBBBYWFDIwMTktMTEtMDhUMTg6MzY6MjBaMB8CAgasAgEBBBYWFDIwMTktMTEtMDhUMTg6NTI6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6npL7TMBsCAganAgEBBBIMEDEwMDAwMDA1OTAwNDU1NjIwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTExLTA4VDE4OjUyOjE0WjAfAgIGqgIBAQQWFhQyMDE5LTExLTA4VDE4OjUxOjIwWjAfAgIGrAIBAQQWFhQyMDE5LTExLTA4VDE5OjA3OjE0WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p6S/ujAbAgIGpwIBAQQSDBAxMDAwMDAwNTkwMDQ3NDM1MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMS0wOFQxOTowNzoxNFowHwICBqoCAQEEFhYUMjAxOS0xMS0wOFQxOTowNjoxOVowHwICBqwCAQEEFhYUMjAxOS0xMS0wOFQxOToyMjoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqekwIQwGwICBqcCAQEEEgwQMTAwMDAwMDU5MDA0ODc3NDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTEtMDhUMTk6MjI6MTRaMB8CAgaqAgEBBBYWFDIwMTktMTEtMDhUMTk6MjE6MjJaMB8CAgasAgEBBBYWFDIwMTktMTEtMDhUMTk6Mzc6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6npMFZMBsCAganAgEBBBIMEDEwMDAwMDA1OTkxMjc1OTUwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTExLTI5VDEyOjA5OjAyWjAfAgIGqgIBAQQWFhQyMDE5LTExLTI5VDEyOjA5OjAzWjAfAgIGrAIBAQQWFhQyMDE5LTExLTI5VDEyOjI0OjAyWjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p6yNvzAbAgIGpwIBAQQSDBAxMDAwMDAwNTk5MTM0Mzg4MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMS0yOVQxMjoyNDowMlowHwICBqoCAQEEFhYUMjAxOS0xMS0yOVQxMjoyMzowM1owHwICBqwCAQEEFhYUMjAxOS0xMS0yOVQxMjozOTowMlowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqesjzUwGwICBqcCAQEEEgwQMTAwMDAwMDU5OTE0MTAyMDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTEtMjlUMTI6Mzk6MDJaMB8CAgaqAgEBBBYWFDIwMTktMTEtMjlUMTI6Mzg6MThaMB8CAgasAgEBBBYWFDIwMTktMTEtMjlUMTI6NTQ6MDJaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nrJDdMBsCAganAgEBBBIMEDEwMDAwMDA1OTkxNDU0NDAwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTExLTI5VDEyOjU0OjAyWjAfAgIGqgIBAQQWFhQyMDE5LTExLTI5VDEyOjUzOjE0WjAfAgIGrAIBAQQWFhQyMDE5LTExLTI5VDEzOjA5OjAyWjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p6ySmjAbAgIGpwIBAQQSDBAxMDAwMDAwNTk5MTUxNDIzMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMS0yOVQxMzowOTowMlowHwICBqoCAQEEFhYUMjAxOS0xMS0yOVQxMzowODoxOFowHwICBqwCAQEEFhYUMjAxOS0xMS0yOVQxMzoyNDowMlowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeslE4wGwICBqcCAQEEEgwQMTAwMDAwMDU5OTE1NzkzMzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTEtMjlUMTM6MjQ6MDJaMB8CAgaqAgEBBBYWFDIwMTktMTEtMjlUMTM6MjM6MzZaMB8CAgasAgEBBBYWFDIwMTktMTEtMjlUMTM6Mzk6MDJaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nrJYDMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjIyMDYwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTEyLTI3VDA1OjMwOjI2WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjMwOjI3WjAfAgIGrAIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjI2WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uoIIOZTCCBXwwggRkoAMCAQICCA7rV4fnngmNMA0GCSqGSIb3DQEBBQUAMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MTExMzAyMTUwOVoXDTIzMDIwNzIxNDg0N1owgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKXPgf0looFb1oftI9ozHI7iI8ClxCbLPcaf7EoNVYb/pALXl8o5VG19f7JUGJ3ELFJxjmR7gs6JuknWCOW0iHHPP1tGLsbEHbgDqViiBD4heNXbt9COEo2DTFsqaDeTwvK9HsTSoQxKWFKrEuPt3R+YFZA1LcLMEsqNSIH3WHhUa+iMMTYfSgYMR1TzN5C4spKJfV+khUrhwJzguqS7gpdj9CuTwf0+b8rB9Typj1IawCUKdg7e/pn+/8Jr9VterHNRSQhWicxDkMyOgQLQoJe2XLGhaWmHkBBoJiY5uB0Qc7AKXcVz0N92O9gt2Yge4+wHz+KO0NP6JlWB7+IDSSMCAwEAAaOCAdcwggHTMD8GCCsGAQUFBwEBBDMwMTAvBggrBgEFBQcwAYYjaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy13d2RyMDQwHQYDVR0OBBYEFJGknPzEdrefoIr0TfWPNl3tKwSFMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUiCcXCam2GGCL7Ou69kdZxVJUo7cwggEeBgNVHSAEggEVMIIBETCCAQ0GCiqGSIb3Y2QFBgEwgf4wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNgYIKwYBBQUHAgEWKmh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5LzAOBgNVHQ8BAf8EBAMCB4AwEAYKKoZIhvdjZAYLAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAA2mG9MuPeNbKwduQpZs0+iMQzCCX+Bc0Y2+vQ+9GvwlktuMhcOAWd/j4tcuBRSsDdu2uP78NS58y60Xa45/H+R3ubFnlbQTXqYZhnb4WiCV52OMD3P86O3GH66Z+GVIXKDgKDrAEDctuaAEOR9zucgF/fLefxoqKm4rAfygIFzZ630npjP49ZjgvkTbsUxn/G4KT8niBqjSl/OnjmtRolqEdWXRFgRi48Ff9Qipz2jZkgDJwYyz+I0AZLpYYMB8r491ymm5WyrWHWhumEL1TKc3GZvMOxx6GUPzo22/SGAGDDaSK+zeGLUR2i0j0I78oGmcFxuegHs5R0UwYS/HE6gwggQiMIIDCqADAgECAggB3rzEOW2gEDANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMTMwMjA3MjE0ODQ3WhcNMjMwMjA3MjE0ODQ3WjCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMo4VKbLVqrIJDlI6Yzu7F+4fyaRvDRTes58Y4Bhd2RepQcjtjn+UC0VVlhwLX7EbsFKhT4v8N6EGqFXya97GP9q+hUSSRUIGayq2yoy7ZZjaFIVPYyK7L9rGJXgA6wBfZcFZ84OhZU3au0Jtq5nzVFkn8Zc0bxXbmc1gHY2pIeBbjiP2CsVTnsl2Fq/ToPBjdKT1RpxtWCcnTNOVfkSWAyGuBYNweV3RY1QSLorLeSUheHoxJ3GaKWwo/xnfnC6AllLd0KRObn1zeFM78A7SIym5SFd/Wpqu6cWNWDS5q3zRinJ6MOL6XnAamFnFbLw/eVovGJfbs+Z3e8bY/6SZasCAwEAAaOBpjCBozAdBgNVHQ4EFgQUiCcXCam2GGCL7Ou69kdZxVJUo7cwDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAOBgNVHQ8BAf8EBAMCAYYwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAE/P71m+LPWybC+P7hOHMugFNahui33JaQy52Re8dyzUZ+L9mm06WVzfgwG9sq4qYXKxr83DRTCPo4MNzh1HtPGTiqN0m6TDmHKHOz6vRQuSVLkyu5AYU2sKThC22R1QbCGAColOV4xrWzw9pv3e9w0jHQtKJoc/upGSTKQZEhltV/V6WId7aIrkhoxK6+JJFKql3VUAqa67SzCu4aCxvCmA5gl35b40ogHKf9ziCuY7uLvsumKV8wVjQYLNDzsdTJWk26v5yZXpT+RN5yaZgem8+bQp0gF6ZuEujPYhisX4eOGBrr/TkJ2prfOv/TgalmcwHFGlXOxxioK0bA8MFR8wggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4++NwzeajTEV+H0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz/2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcNgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIDutXh+eeCY0wCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQBv41V+dEFHG27w0bOyXDWamwY4cKV8aDEaiXaiPjyGyTZGMzOk4WA4OlRjJTkEsEvZe99Oo9AFP54LIkuRm1OVFnCb/nO1PludUmMavUkZwVurtPHskuv+zZuSSKG7wFqET7gtrlUSfPo7lXF2k+YSSspCnRQAi5ZCdk0ixLtGTwrZc1Hp3ks/M+1ts7skXAnQGTsqpJhIsz7/f56S+F4CQC6z5nDFojKLoZbq5hUTbmslCLNbNeqa+p/YvXxfuoNGUTMa98fTSlvIZXgkNeUGAIhI+8vegMZtd5XkveaZ6O25G0cp0wwdSwFM5eK68o440rKslGxDhtfN4ZHAj/yh', NULL, NULL, NULL, NULL, 'ios', '2019-12-26 21:30:28', '2019-12-26 21:30:28');
INSERT INTO `payment_details` (`id`, `user_id`, `company_id`, `adam_id`, `app_item_id`, `bundle_id`, `version_external_identifier`, `amount`, `quantity`, `product_id`, `transaction_id`, `original_transaction_id`, `purchase_date`, `original_purchase_date`, `expires_date`, `web_order_line_item_id`, `is_trial_period`, `receipt`, `expiration_intent`, `auto_renew_product_id`, `is_in_billing_retry_period`, `auto_renew_status`, `device_type`, `created`, `modified`) VALUES
(6, 44, 22, NULL, NULL, 'com.kenbar.mynus2', NULL, '9.99', NULL, 'com.mynustwo.subscription', '1000000609264811', NULL, NULL, NULL, NULL, NULL, NULL, 'MIKPFQYJKoZIhvcNAQcCoIKPBjCCjwICAQExCzAJBgUrDgMCGgUAMIJ+tgYJKoZIhvcNAQcBoIJ+pwSCfqMxgn6fMAoCAQgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgELAgEBBAMCAQAwCwIBDwIBAQQDAgEAMAsCARACAQEEAwIBADALAgEZAgEBBAMCAQMwDAIBAwIBAQQEDAIxMDAMAgEKAgEBBAQWAjQrMAwCAQ4CAQEEBAICAI4wDQIBDQIBAQQFAgMB1lAwDQIBEwIBAQQFDAMxLjAwDgIBCQIBAQQGAgRQMjUzMBgCAQQCAQIEEITHp46DMf66ndLYfVN6likwGwIBAAIBAQQTDBFQcm9kdWN0aW9uU2FuZGJveDAbAgECAgEBBBMMEWNvbS5rZW5iYXIubXludXMyMBwCAQUCAQEEFO0x9Qd0waSgJ4OgUa8UpVKZN7sQMB4CAQwCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHgIBEgIBAQQWFhQyMDEzLTA4LTAxVDA3OjAwOjAwWjBCAgEHAgEBBDrEycibHeZ4roZ3te5Mr3nd8jHhupzaj7hUSG/npRbqoRmGQI3eqMCuI8VYGSV9ayxMbXVxU7fQ84BnMEwCAQYCAQEERD+OvTdSo637LVgk5dv3eGZHRiENCFfNBsbCvxjV3RMuNDhf8kxEtWNd2sGeZACKSNgNQ1ap1nK4cKlBbav6fcnE782yMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyg84DAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODIxMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QwNjo1NToxNFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QwNzoxMDoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoPOEwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg2NTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMDc6MTA6MTRaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMDc6MjU6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKD3ZMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NDQwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDA3OjI1OjE0WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDA3OjQwOjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyg+wzAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODQ2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QwNzo0MDoxNFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QwNzo1NToxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoP7gwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg0NTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMDc6NTU6MTRaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMDg6MTA6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKEDFMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MTQwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDA4OjEwOjE0WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDA4OjI1OjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyhB3DAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODI2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QxMjoyMjo1MVowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QxMjozNzo1MVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoVsowGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgzNjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMTI6Mzc6NTFaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMTI6NTI6NTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKFfOMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MTIwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDEyOjUyOjUxWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDEzOjA3OjUxWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyhY1jAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODI5MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QxMzowNzo1MVowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QxMzoyMjo1MVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoWfUwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgyNDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMTM6MjI6NTFaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMTM6Mzc6NTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKFsDMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NjgwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDEzOjM3OjUxWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDEzOjUyOjUxWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyhb+zAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODY5MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yMlQxMDo1ODoxNFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0yMlQxMToxMzoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpNVcwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg2MTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjJUMTE6MTM6MTRaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjJUMTE6Mjg6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKTZYMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MTMwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTIyVDExOjI4OjE0WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTIyVDExOjQzOjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyk3czAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODI3MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yMlQxMTo0MzoxNFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0yMlQxMTo1ODoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpOFcwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgzMDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjJUMTE6NTg6MTRaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjJUMTI6MTM6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKTlmMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NTMwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTIyVDEyOjEzOjE0WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTIyVDEyOjI4OjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyk6VzAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODU0MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yNFQwNTo1NDoxMFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0yNFQwNjowOToxMFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpt1wwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgyMjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjRUMDY6MDk6MTBaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjRUMDY6MjQ6MTBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKbfrMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NTkwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI0VDA2OjI0OjEwWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI0VDA2OjM5OjEwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pym4ozAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODUxMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yNFQwNjozOToxMFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0yNFQwNjo1NDoxMFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpuV4wGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgzOTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjRUMDY6NTQ6MTBaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjRUMDc6MDk6MTBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKboTMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NDEwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI0VDA3OjA5OjEwWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI0VDA3OjI0OjEwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pym7BjAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODUwMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yNFQwOTo1MDoyOVowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0yNFQxMDowNToyOVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpyC8wGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgxOTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjdUMDU6Mzg6NThaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjdUMDU6NTM6NThaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKhsnMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NjMwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI3VDA1OjUzOjU4WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI3VDA2OjA4OjU4WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyobrDAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODU3MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yN1QwNjowODo1OFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0yN1QwNjoyMzo1OFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqHE8wGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg2NDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjdUMDY6MjM6NThaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjdUMDY6Mzg6NThaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKh0LMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MjgwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI3VDA2OjM4OjU4WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI3VDA2OjUzOjU4WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyod4jAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODIwMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yN1QwNjo1Mzo1OFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0yN1QwNzowODo1OFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqHt0wGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg2NzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjhUMTI6NDI6MTFaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjhUMTI6NTc6MTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKnhLMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NTIwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI4VDEyOjU3OjEyWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI4VDEzOjEyOjEyWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyp5VDAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODE1MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yOFQxMzoxMjoxMlowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0yOFQxMzoyNzoxMlowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqepIwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgzMzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjhUMTM6Mjc6MTJaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjhUMTM6NDI6MTJaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKnvFMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MzUwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI4VDEzOjQyOjEyWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI4VDEzOjU3OjEyWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyp9BDAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODE3MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yOFQxMzo1NzoxMlowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0yOFQxNDoxMjoxMlowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqfiIwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg1NjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDktMDZUMTM6NDI6MTFaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDktMDZUMTM6NTc6MTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nLJKOMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MjMwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA5LTA2VDEzOjU3OjExWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA5LTA2VDE0OjEyOjExWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyyTrDAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODE4MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOS0wNlQxNDoxMjoxMVowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOS0wNlQxNDoyNzoxMVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcslP4wGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgwNzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDktMDZUMTQ6Mjc6MTFaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDktMDZUMTQ6NDI6MTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nLJZjMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NTUwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA5LTA2VDE0OjQyOjExWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA5LTA2VDE0OjU3OjExWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyyX0TAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODMxMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOS0wNlQxNDo1NzoxMVowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOS0wNlQxNToxMjoxMVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcsmRIwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgxNjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NDBaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMTEtMjhUMDY6MTE6NDBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nQALyMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MzQwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTExLTI4VDA2OjExOjQwWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTExLTI4VDA2OjI2OjQwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p0ADojAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODcwMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0xMS0yOFQwNjoyNjo0MFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0xMS0yOFQwNjo0MTo0MFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqdABJIwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg0MDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMTEtMjhUMDY6NDE6NDBaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMTEtMjhUMDY6NTY6NDBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nQAWDMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MDkwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTExLTI4VDA2OjU2OjQwWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTExLTI4VDA3OjExOjQwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p0AGjTAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODQzMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0xMS0yOFQwNzoxMTo0MFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0xMS0yOFQwNzoyNjo0MFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqdAB34wGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgxMDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjRUMDY6NDQ6MDdaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTktMDktMjRUMDY6NTk6MDdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlT0CMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NDcwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI0VDA2OjU5OjA3WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI0VDA3OjE0OjA3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5U+VDAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODA4MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yNFQwNzoxNDowN1owHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOS0wOS0yNFQwNzoyOTowN1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeVP+EwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg1ODAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjRUMDc6Mjk6MDdaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTktMDktMjRUMDc6NDQ6MDdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlUFVMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MzgwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI0VDA3OjQ0OjA3WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI0VDA3OjU5OjA3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5VC2DAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODI1MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yNFQwNzo1OTowN1owHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOS0wOS0yNFQwODoxNDowN1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeVRM4wGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgzNzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjdUMDU6MzQ6MzVaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTktMDktMjdUMDU6NDk6MzVaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlpMSMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NDIwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI3VDA1OjQ5OjM1WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI3VDA2OjA0OjM1WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5aT/TAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODYyMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yN1QwNjowNDozNVowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOS0wOS0yN1QwNjoxOTozNVowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeWlTowGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg0ODAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjdUMDY6MTk6MzVaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTktMDktMjdUMDY6MzQ6MzVaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlpaRMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MzIwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI3VDA2OjM0OjM1WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI3VDA2OjQ5OjM1WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5aX4DAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODQ5MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yN1QwNjo0OTozNVowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOS0wOS0yN1QwNzowNDozNVowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeWmUcwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg2MDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTAtMDdUMDI6MTk6NTdaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTktMTAtMDdUMDI6MzQ6NTdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nmUjsMBsCAganAgEBBBIMEDEwMDAwMDA1NzYxNTI0OTMwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTEwLTA3VDAyOjM0OjU3WjAfAgIGqgIBAQQWFhQyMDE5LTEwLTA3VDAyOjM0OjA0WjAfAgIGrAIBAQQWFhQyMDE5LTEwLTA3VDAyOjQ5OjU3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5lJRTAbAgIGpwIBAQQSDBAxMDAwMDAwNTc2MTUzODk2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMC0wN1QwMjo0OTo1N1owHwICBqoCAQEEFhYUMjAxOS0xMC0wN1QwMjo0OTowOFowHwICBqwCAQEEFhYUMjAxOS0xMC0wN1QwMzowNDo1N1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeZSZYwGwICBqcCAQEEEgwQMTAwMDAwMDU3NjE1NTU0NjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTAtMDdUMDM6MDQ6NTdaMB8CAgaqAgEBBBYWFDIwMTktMTAtMDdUMDM6MDQ6MDBaMB8CAgasAgEBBBYWFDIwMTktMTAtMDdUMDM6MTk6NTdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nmUn7MBsCAganAgEBBBIMEDEwMDAwMDA1NzYxNTY4MzcwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTEwLTA3VDAzOjE5OjU3WjAfAgIGqgIBAQQWFhQyMDE5LTEwLTA3VDAzOjE5OjAwWjAfAgIGrAIBAQQWFhQyMDE5LTEwLTA3VDAzOjM0OjU3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5lKUTAbAgIGpwIBAQQSDBAxMDAwMDAwNTc2MTU4NTg2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMC0wN1QwMzozNDo1N1owHwICBqoCAQEEFhYUMjAxOS0xMC0wN1QwMzozNDowMFowHwICBqwCAQEEFhYUMjAxOS0xMC0wN1QwMzo0OTo1N1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeZSq4wGwICBqcCAQEEEgwQMTAwMDAwMDU5MDAzNTMxNDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTEtMDhUMTg6MDc6MTRaMB8CAgaqAgEBBBYWFDIwMTktMTEtMDhUMTg6MDc6MTVaMB8CAgasAgEBBBYWFDIwMTktMTEtMDhUMTg6MjI6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6npL0fMBsCAganAgEBBBIMEDEwMDAwMDA1OTAwMzk5ODcwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTExLTA4VDE4OjIyOjE0WjAfAgIGqgIBAQQWFhQyMDE5LTExLTA4VDE4OjIxOjE2WjAfAgIGrAIBAQQWFhQyMDE5LTExLTA4VDE4OjM3OjE0WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p6S93TAbAgIGpwIBAQQSDBAxMDAwMDAwNTkwMDQzMDI3MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMS0wOFQxODozNzoxNFowHwICBqoCAQEEFhYUMjAxOS0xMS0wOFQxODozNjoyMFowHwICBqwCAQEEFhYUMjAxOS0xMS0wOFQxODo1MjoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqekvtMwGwICBqcCAQEEEgwQMTAwMDAwMDU5MDA0NTU2MjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTEtMDhUMTg6NTI6MTRaMB8CAgaqAgEBBBYWFDIwMTktMTEtMDhUMTg6NTE6MjBaMB8CAgasAgEBBBYWFDIwMTktMTEtMDhUMTk6MDc6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6npL+6MBsCAganAgEBBBIMEDEwMDAwMDA1OTAwNDc0MzUwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTExLTA4VDE5OjA3OjE0WjAfAgIGqgIBAQQWFhQyMDE5LTExLTA4VDE5OjA2OjE5WjAfAgIGrAIBAQQWFhQyMDE5LTExLTA4VDE5OjIyOjE0WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p6TAhDAbAgIGpwIBAQQSDBAxMDAwMDAwNTkwMDQ4Nzc0MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMS0wOFQxOToyMjoxNFowHwICBqoCAQEEFhYUMjAxOS0xMS0wOFQxOToyMToyMlowHwICBqwCAQEEFhYUMjAxOS0xMS0wOFQxOTozNzoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqekwVkwGwICBqcCAQEEEgwQMTAwMDAwMDU5OTEyNzU5NTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTEtMjlUMTI6MDk6MDJaMB8CAgaqAgEBBBYWFDIwMTktMTEtMjlUMTI6MDk6MDNaMB8CAgasAgEBBBYWFDIwMTktMTEtMjlUMTI6MjQ6MDJaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nrI2/MBsCAganAgEBBBIMEDEwMDAwMDA1OTkxMzQzODgwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTExLTI5VDEyOjI0OjAyWjAfAgIGqgIBAQQWFhQyMDE5LTExLTI5VDEyOjIzOjAzWjAfAgIGrAIBAQQWFhQyMDE5LTExLTI5VDEyOjM5OjAyWjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p6yPNTAbAgIGpwIBAQQSDBAxMDAwMDAwNTk5MTQxMDIwMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMS0yOVQxMjozOTowMlowHwICBqoCAQEEFhYUMjAxOS0xMS0yOVQxMjozODoxOFowHwICBqwCAQEEFhYUMjAxOS0xMS0yOVQxMjo1NDowMlowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeskN0wGwICBqcCAQEEEgwQMTAwMDAwMDU5OTE0NTQ0MDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTEtMjlUMTI6NTQ6MDJaMB8CAgaqAgEBBBYWFDIwMTktMTEtMjlUMTI6NTM6MTRaMB8CAgasAgEBBBYWFDIwMTktMTEtMjlUMTM6MDk6MDJaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nrJKaMBsCAganAgEBBBIMEDEwMDAwMDA1OTkxNTE0MjMwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTExLTI5VDEzOjA5OjAyWjAfAgIGqgIBAQQWFhQyMDE5LTExLTI5VDEzOjA4OjE4WjAfAgIGrAIBAQQWFhQyMDE5LTExLTI5VDEzOjI0OjAyWjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p6yUTjAbAgIGpwIBAQQSDBAxMDAwMDAwNTk5MTU3OTMzMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMS0yOVQxMzoyNDowMlowHwICBqoCAQEEFhYUMjAxOS0xMS0yOVQxMzoyMzozNlowHwICBqwCAQEEFhYUMjAxOS0xMS0yOVQxMzozOTowMlowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeslgMwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2MjIwNzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTItMjdUMDU6MzA6MjZaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6MzA6MjdaMB8CAgasAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MjZaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nteR4MBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ3NTgwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjI2WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ0OjQ3WjAfAgIGrAIBAQQWFhQyMDE5LTEyLTI3VDA2OjAwOjI2WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uoIIOZTCCBXwwggRkoAMCAQICCA7rV4fnngmNMA0GCSqGSIb3DQEBBQUAMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MTExMzAyMTUwOVoXDTIzMDIwNzIxNDg0N1owgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKXPgf0looFb1oftI9ozHI7iI8ClxCbLPcaf7EoNVYb/pALXl8o5VG19f7JUGJ3ELFJxjmR7gs6JuknWCOW0iHHPP1tGLsbEHbgDqViiBD4heNXbt9COEo2DTFsqaDeTwvK9HsTSoQxKWFKrEuPt3R+YFZA1LcLMEsqNSIH3WHhUa+iMMTYfSgYMR1TzN5C4spKJfV+khUrhwJzguqS7gpdj9CuTwf0+b8rB9Typj1IawCUKdg7e/pn+/8Jr9VterHNRSQhWicxDkMyOgQLQoJe2XLGhaWmHkBBoJiY5uB0Qc7AKXcVz0N92O9gt2Yge4+wHz+KO0NP6JlWB7+IDSSMCAwEAAaOCAdcwggHTMD8GCCsGAQUFBwEBBDMwMTAvBggrBgEFBQcwAYYjaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy13d2RyMDQwHQYDVR0OBBYEFJGknPzEdrefoIr0TfWPNl3tKwSFMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUiCcXCam2GGCL7Ou69kdZxVJUo7cwggEeBgNVHSAEggEVMIIBETCCAQ0GCiqGSIb3Y2QFBgEwgf4wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNgYIKwYBBQUHAgEWKmh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5LzAOBgNVHQ8BAf8EBAMCB4AwEAYKKoZIhvdjZAYLAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAA2mG9MuPeNbKwduQpZs0+iMQzCCX+Bc0Y2+vQ+9GvwlktuMhcOAWd/j4tcuBRSsDdu2uP78NS58y60Xa45/H+R3ubFnlbQTXqYZhnb4WiCV52OMD3P86O3GH66Z+GVIXKDgKDrAEDctuaAEOR9zucgF/fLefxoqKm4rAfygIFzZ630npjP49ZjgvkTbsUxn/G4KT8niBqjSl/OnjmtRolqEdWXRFgRi48Ff9Qipz2jZkgDJwYyz+I0AZLpYYMB8r491ymm5WyrWHWhumEL1TKc3GZvMOxx6GUPzo22/SGAGDDaSK+zeGLUR2i0j0I78oGmcFxuegHs5R0UwYS/HE6gwggQiMIIDCqADAgECAggB3rzEOW2gEDANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMTMwMjA3MjE0ODQ3WhcNMjMwMjA3MjE0ODQ3WjCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMo4VKbLVqrIJDlI6Yzu7F+4fyaRvDRTes58Y4Bhd2RepQcjtjn+UC0VVlhwLX7EbsFKhT4v8N6EGqFXya97GP9q+hUSSRUIGayq2yoy7ZZjaFIVPYyK7L9rGJXgA6wBfZcFZ84OhZU3au0Jtq5nzVFkn8Zc0bxXbmc1gHY2pIeBbjiP2CsVTnsl2Fq/ToPBjdKT1RpxtWCcnTNOVfkSWAyGuBYNweV3RY1QSLorLeSUheHoxJ3GaKWwo/xnfnC6AllLd0KRObn1zeFM78A7SIym5SFd/Wpqu6cWNWDS5q3zRinJ6MOL6XnAamFnFbLw/eVovGJfbs+Z3e8bY/6SZasCAwEAAaOBpjCBozAdBgNVHQ4EFgQUiCcXCam2GGCL7Ou69kdZxVJUo7cwDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAOBgNVHQ8BAf8EBAMCAYYwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAE/P71m+LPWybC+P7hOHMugFNahui33JaQy52Re8dyzUZ+L9mm06WVzfgwG9sq4qYXKxr83DRTCPo4MNzh1HtPGTiqN0m6TDmHKHOz6vRQuSVLkyu5AYU2sKThC22R1QbCGAColOV4xrWzw9pv3e9w0jHQtKJoc/upGSTKQZEhltV/V6WId7aIrkhoxK6+JJFKql3VUAqa67SzCu4aCxvCmA5gl35b40ogHKf9ziCuY7uLvsumKV8wVjQYLNDzsdTJWk26v5yZXpT+RN5yaZgem8+bQp0gF6ZuEujPYhisX4eOGBrr/TkJ2prfOv/TgalmcwHFGlXOxxioK0bA8MFR8wggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4++NwzeajTEV+H0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz/2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcNgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIDutXh+eeCY0wCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQB6re7Kb7ZU0Wj944Lo+aIQLwpKw9VTeNv+LnXj95hEKddVQJFMGb9E5Y6x+gAh6QH+U7dX+vGLWdoNaSNTYu7hcIfimNWXXV4AglWeYxyOfGels5+V047pw2g0cujEs4MpAvxCxCFpGZwqOEY0p+7ulehOeQ9Y/GXndm2lGxBFxkhzuliTa4K482PrAPuFx4eDaoTPSWuoNGzd/eLREkENQFvb6wWna+FxiOHDNboOjbzPxtl4kI/u3ogOKtygLC5CanuJWb+jKPtmB4NxEkKkaVLd8bI3ZIkh9tyNAYO55LMC6TU5X2ghcIspAKx6+1lFR9AbZVnB0K+KIe+FbIiG', NULL, NULL, NULL, NULL, 'ios', '2019-12-26 21:45:15', '2019-12-26 21:45:15');
INSERT INTO `payment_details` (`id`, `user_id`, `company_id`, `adam_id`, `app_item_id`, `bundle_id`, `version_external_identifier`, `amount`, `quantity`, `product_id`, `transaction_id`, `original_transaction_id`, `purchase_date`, `original_purchase_date`, `expires_date`, `web_order_line_item_id`, `is_trial_period`, `receipt`, `expiration_intent`, `auto_renew_product_id`, `is_in_billing_retry_period`, `auto_renew_status`, `device_type`, `created`, `modified`) VALUES
(7, 44, 22, NULL, NULL, 'com.kenbar.mynus2', NULL, '9.99', NULL, 'com.mynustwo.subscription', '1000000609267377', NULL, NULL, NULL, NULL, NULL, NULL, 'MIKPIQYJKoZIhvcNAQcCoIKPEjCCjw4CAQExCzAJBgUrDgMCGgUAMIJ+wgYJKoZIhvcNAQcBoIJ+swSCfq8xgn6rMAoCAQgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgELAgEBBAMCAQAwCwIBDwIBAQQDAgEAMAsCARACAQEEAwIBADALAgEZAgEBBAMCAQMwDAIBAwIBAQQEDAIxMDAMAgEKAgEBBAQWAjQrMAwCAQ4CAQEEBAICAI4wDQIBDQIBAQQFAgMB1lAwDQIBEwIBAQQFDAMxLjAwDgIBCQIBAQQGAgRQMjUzMBgCAQQCAQIEEPffhcXTnII/5sJAHMzkmLkwGwIBAAIBAQQTDBFQcm9kdWN0aW9uU2FuZGJveDAbAgECAgEBBBMMEWNvbS5rZW5iYXIubXludXMyMBwCAQUCAQEEFNEhayNlexPSlPvP8zzyINGRuxB4MB4CAQwCAQEEFhYUMjAxOS0xMi0yN1QwNTo1OTo1M1owHgIBEgIBAQQWFhQyMDEzLTA4LTAxVDA3OjAwOjAwWjA7AgEHAgEBBDN51ZKObs9yCyDjAYIiD6AfhHonNyT+FOBNHBZ7HMWQIiJu9q3u3ke9RSs04u/W4Asaxt4wXwIBBgIBAQRXkuveKUbV0VJj38dM6pz3QUOATczSBbpt0xMbICmLwtqtm8v3y8NSsZAgLmOHahuA5OmvXPCPKtR2psWH1qlHdDjThkDG2a6Nvb9RH4JcRiRy2pKqwnQ1MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyg84DAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODIxMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QwNjo1NToxNFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QwNzoxMDoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoPOEwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg2NTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMDc6MTA6MTRaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMDc6MjU6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKD3ZMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NDQwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDA3OjI1OjE0WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDA3OjQwOjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyg+wzAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODQ2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QwNzo0MDoxNFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QwNzo1NToxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoP7gwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg0NTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMDc6NTU6MTRaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMDg6MTA6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKEDFMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MTQwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDA4OjEwOjE0WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDA4OjI1OjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyhB3DAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODI2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QxMjoyMjo1MVowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QxMjozNzo1MVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoVsowGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgzNjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMTI6Mzc6NTFaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMTI6NTI6NTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKFfOMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MTIwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDEyOjUyOjUxWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDEzOjA3OjUxWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyhY1jAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODI5MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0xN1QxMzowNzo1MVowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0xN1QxMzoyMjo1MVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcoWfUwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgyNDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMTdUMTM6MjI6NTFaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMTdUMTM6Mzc6NTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKFsDMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NjgwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTE3VDEzOjM3OjUxWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTE3VDEzOjUyOjUxWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyhb+zAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODY5MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yMlQxMDo1ODoxNFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0yMlQxMToxMzoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpNVcwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg2MTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjJUMTE6MTM6MTRaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjJUMTE6Mjg6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKTZYMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MTMwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTIyVDExOjI4OjE0WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTIyVDExOjQzOjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyk3czAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODI3MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yMlQxMTo0MzoxNFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0yMlQxMTo1ODoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpOFcwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgzMDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjJUMTE6NTg6MTRaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjJUMTI6MTM6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKTlmMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NTMwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTIyVDEyOjEzOjE0WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTIyVDEyOjI4OjE0WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyk6VzAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODU0MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yNFQwNTo1NDoxMFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0yNFQwNjowOToxMFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpt1wwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgyMjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjRUMDY6MDk6MTBaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjRUMDY6MjQ6MTBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKbfrMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NTkwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI0VDA2OjI0OjEwWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI0VDA2OjM5OjEwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pym4ozAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODUxMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yNFQwNjozOToxMFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0yNFQwNjo1NDoxMFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpuV4wGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgzOTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjRUMDY6NTQ6MTBaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjRUMDc6MDk6MTBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKboTMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NDEwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI0VDA3OjA5OjEwWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI0VDA3OjI0OjEwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pym7BjAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODUwMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yNFQwOTo1MDoyOVowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0yNFQxMDowNToyOVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcpyC8wGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgxOTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjdUMDU6Mzg6NThaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjdUMDU6NTM6NThaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKhsnMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NjMwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI3VDA1OjUzOjU4WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI3VDA2OjA4OjU4WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyobrDAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODU3MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yN1QwNjowODo1OFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0yN1QwNjoyMzo1OFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqHE8wGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg2NDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjdUMDY6MjM6NThaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjdUMDY6Mzg6NThaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKh0LMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MjgwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI3VDA2OjM4OjU4WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI3VDA2OjUzOjU4WjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyod4jAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODIwMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yN1QwNjo1Mzo1OFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0yN1QwNzowODo1OFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqHt0wGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg2NzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjhUMTI6NDI6MTFaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjhUMTI6NTc6MTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKnhLMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NTIwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI4VDEyOjU3OjEyWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI4VDEzOjEyOjEyWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyp5VDAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODE1MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yOFQxMzoxMjoxMlowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0yOFQxMzoyNzoxMlowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqepIwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgzMzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDgtMjhUMTM6Mjc6MTJaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDgtMjhUMTM6NDI6MTJaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nKnvFMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MzUwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA4LTI4VDEzOjQyOjEyWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA4LTI4VDEzOjU3OjEyWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyp9BDAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODE3MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOC0yOFQxMzo1NzoxMlowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOC0yOFQxNDoxMjoxMlowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcqfiIwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg1NjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDktMDZUMTM6NDI6MTFaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDktMDZUMTM6NTc6MTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nLJKOMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MjMwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA5LTA2VDEzOjU3OjExWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA5LTA2VDE0OjEyOjExWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyyTrDAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODE4MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOS0wNlQxNDoxMjoxMVowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOS0wNlQxNDoyNzoxMVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcslP4wGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgwNzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMDktMDZUMTQ6Mjc6MTFaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMDktMDZUMTQ6NDI6MTFaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nLJZjMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NTUwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTA5LTA2VDE0OjQyOjExWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTA5LTA2VDE0OjU3OjExWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+pyyX0TAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODMxMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0wOS0wNlQxNDo1NzoxMVowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0wOS0wNlQxNToxMjoxMVowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqcsmRIwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgxNjAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMTEtMjhUMDU6NTY6NDBaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMTEtMjhUMDY6MTE6NDBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nQALyMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MzQwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTExLTI4VDA2OjExOjQwWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTExLTI4VDA2OjI2OjQwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p0ADojAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODcwMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0xMS0yOFQwNjoyNjo0MFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0xMS0yOFQwNjo0MTo0MFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqdABJIwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg0MDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTgtMTEtMjhUMDY6NDE6NDBaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTgtMTEtMjhUMDY6NTY6NDBaMCQCAgamAgEBBBsMGWNvbS5teW51czIuc3Vic2NyaXB0aW9uOTkwggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nQAWDMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MDkwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE4LTExLTI4VDA2OjU2OjQwWjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE4LTExLTI4VDA3OjExOjQwWjAkAgIGpgIBAQQbDBljb20ubXludXMyLnN1YnNjcmlwdGlvbjk5MIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p0AGjTAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODQzMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOC0xMS0yOFQwNzoxMTo0MFowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOC0xMS0yOFQwNzoyNjo0MFowJAICBqYCAQEEGwwZY29tLm15bnVzMi5zdWJzY3JpcHRpb245OTCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqdAB34wGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgxMDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjRUMDY6NDQ6MDdaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTktMDktMjRUMDY6NTk6MDdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlT0CMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NDcwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI0VDA2OjU5OjA3WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI0VDA3OjE0OjA3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5U+VDAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODA4MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yNFQwNzoxNDowN1owHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOS0wOS0yNFQwNzoyOTowN1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeVP+EwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg1ODAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjRUMDc6Mjk6MDdaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTktMDktMjRUMDc6NDQ6MDdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlUFVMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MzgwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI0VDA3OjQ0OjA3WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI0VDA3OjU5OjA3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5VC2DAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODI1MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yNFQwNzo1OTowN1owHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOS0wOS0yNFQwODoxNDowN1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeVRM4wGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDgzNzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjdUMDU6MzQ6MzVaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTktMDktMjdUMDU6NDk6MzVaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlpMSMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4NDIwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI3VDA1OjQ5OjM1WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI3VDA2OjA0OjM1WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5aT/TAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODYyMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yN1QwNjowNDozNVowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOS0wOS0yN1QwNjoxOTozNVowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeWlTowGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg0ODAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMDktMjdUMDY6MTk6MzVaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTktMDktMjdUMDY6MzQ6MzVaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nlpaRMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjQ4MzIwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTA5LTI3VDA2OjM0OjM1WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjQ1OjE0WjAfAgIGrAIBAQQWFhQyMDE5LTA5LTI3VDA2OjQ5OjM1WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5aX4DAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjY0ODQ5MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0wOS0yN1QwNjo0OTozNVowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToxNFowHwICBqwCAQEEFhYUMjAxOS0wOS0yN1QwNzowNDozNVowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeWmUcwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDg2MDAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTAtMDdUMDI6MTk6NTdaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MTRaMB8CAgasAgEBBBYWFDIwMTktMTAtMDdUMDI6MzQ6NTdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nmUlFMBsCAganAgEBBBIMEDEwMDAwMDA1NzYxNTM4OTcwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTEwLTA3VDAyOjQ5OjU3WjAfAgIGqgIBAQQWFhQyMDE5LTEwLTA3VDAyOjQ5OjA4WjAfAgIGrAIBAQQWFhQyMDE5LTEwLTA3VDAzOjA0OjU3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5lJljAbAgIGpwIBAQQSDBAxMDAwMDAwNTc2MTU1NTQ2MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMC0wN1QwMzowNDo1N1owHwICBqoCAQEEFhYUMjAxOS0xMC0wN1QwMzowNDowMFowHwICBqwCAQEEFhYUMjAxOS0xMC0wN1QwMzoxOTo1N1owJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeZSfswGwICBqcCAQEEEgwQMTAwMDAwMDU3NjE1NjgzNzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTAtMDdUMDM6MTk6NTdaMB8CAgaqAgEBBBYWFDIwMTktMTAtMDdUMDM6MTk6MDBaMB8CAgasAgEBBBYWFDIwMTktMTAtMDdUMDM6MzQ6NTdaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nmUpRMBsCAganAgEBBBIMEDEwMDAwMDA1NzYxNTg1ODYwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTEwLTA3VDAzOjM0OjU3WjAfAgIGqgIBAQQWFhQyMDE5LTEwLTA3VDAzOjM0OjAwWjAfAgIGrAIBAQQWFhQyMDE5LTEwLTA3VDAzOjQ5OjU3WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p5lKrjAbAgIGpwIBAQQSDBAxMDAwMDAwNTkwMDM1MzE0MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMS0wOFQxODowNzoxNFowHwICBqoCAQEEFhYUMjAxOS0xMS0wOFQxODowNzoxNVowHwICBqwCAQEEFhYUMjAxOS0xMS0wOFQxODoyMjoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqekvR8wGwICBqcCAQEEEgwQMTAwMDAwMDU5MDAzOTk4NzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTEtMDhUMTg6MjI6MTRaMB8CAgaqAgEBBBYWFDIwMTktMTEtMDhUMTg6MjE6MTZaMB8CAgasAgEBBBYWFDIwMTktMTEtMDhUMTg6Mzc6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6npL3dMBsCAganAgEBBBIMEDEwMDAwMDA1OTAwNDMwMjcwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTExLTA4VDE4OjM3OjE0WjAfAgIGqgIBAQQWFhQyMDE5LTExLTA4VDE4OjM2OjIwWjAfAgIGrAIBAQQWFhQyMDE5LTExLTA4VDE4OjUyOjE0WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p6S+0zAbAgIGpwIBAQQSDBAxMDAwMDAwNTkwMDQ1NTYyMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMS0wOFQxODo1MjoxNFowHwICBqoCAQEEFhYUMjAxOS0xMS0wOFQxODo1MToyMFowHwICBqwCAQEEFhYUMjAxOS0xMS0wOFQxOTowNzoxNFowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqekv7owGwICBqcCAQEEEgwQMTAwMDAwMDU5MDA0NzQzNTAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTEtMDhUMTk6MDc6MTRaMB8CAgaqAgEBBBYWFDIwMTktMTEtMDhUMTk6MDY6MTlaMB8CAgasAgEBBBYWFDIwMTktMTEtMDhUMTk6MjI6MTRaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6npMCEMBsCAganAgEBBBIMEDEwMDAwMDA1OTAwNDg3NzQwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTExLTA4VDE5OjIyOjE0WjAfAgIGqgIBAQQWFhQyMDE5LTExLTA4VDE5OjIxOjIyWjAfAgIGrAIBAQQWFhQyMDE5LTExLTA4VDE5OjM3OjE0WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p6TBWTAbAgIGpwIBAQQSDBAxMDAwMDAwNTk5MTI3NTk1MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMS0yOVQxMjowOTowMlowHwICBqoCAQEEFhYUMjAxOS0xMS0yOVQxMjowOTowM1owHwICBqwCAQEEFhYUMjAxOS0xMS0yOVQxMjoyNDowMlowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqesjb8wGwICBqcCAQEEEgwQMTAwMDAwMDU5OTEzNDM4ODAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTEtMjlUMTI6MjQ6MDJaMB8CAgaqAgEBBBYWFDIwMTktMTEtMjlUMTI6MjM6MDNaMB8CAgasAgEBBBYWFDIwMTktMTEtMjlUMTI6Mzk6MDJaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nrI81MBsCAganAgEBBBIMEDEwMDAwMDA1OTkxNDEwMjAwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTExLTI5VDEyOjM5OjAyWjAfAgIGqgIBAQQWFhQyMDE5LTExLTI5VDEyOjM4OjE4WjAfAgIGrAIBAQQWFhQyMDE5LTExLTI5VDEyOjU0OjAyWjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p6yQ3TAbAgIGpwIBAQQSDBAxMDAwMDAwNTk5MTQ1NDQwMBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMS0yOVQxMjo1NDowMlowHwICBqoCAQEEFhYUMjAxOS0xMS0yOVQxMjo1MzoxNFowHwICBqwCAQEEFhYUMjAxOS0xMS0yOVQxMzowOTowMlowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqeskpowGwICBqcCAQEEEgwQMTAwMDAwMDU5OTE1MTQyMzAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTEtMjlUMTM6MDk6MDJaMB8CAgaqAgEBBBYWFDIwMTktMTEtMjlUMTM6MDg6MThaMB8CAgasAgEBBBYWFDIwMTktMTEtMjlUMTM6MjQ6MDJaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nrJROMBsCAganAgEBBBIMEDEwMDAwMDA1OTkxNTc5MzMwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTExLTI5VDEzOjI0OjAyWjAfAgIGqgIBAQQWFhQyMDE5LTExLTI5VDEzOjIzOjM2WjAfAgIGrAIBAQQWFhQyMDE5LTExLTI5VDEzOjM5OjAyWjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uMIIBhgIBEQIBAQSCAXwxggF4MAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQMwDAICBq4CAQEEAwIBADAMAgIGsQIBAQQDAgEAMAwCAga3AgEBBAMCAQAwEgICBq8CAQEECQIHA41+p6yWAzAbAgIGpwIBAQQSDBAxMDAwMDAwNjA5MjYyMjA3MBsCAgapAgEBBBIMEDEwMDAwMDA0MzIzNjMxOTcwHwICBqgCAQEEFhYUMjAxOS0xMi0yN1QwNTozMDoyNlowHwICBqoCAQEEFhYUMjAxOS0xMi0yN1QwNTozMDoyN1owHwICBqwCAQEEFhYUMjAxOS0xMi0yN1QwNTo0NToyNlowJAICBqYCAQEEGwwZY29tLm15bnVzdHdvLnN1YnNjcmlwdGlvbjCCAYYCARECAQEEggF8MYIBeDALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEDMAwCAgauAgEBBAMCAQAwDAICBrECAQEEAwIBADAMAgIGtwIBAQQDAgEAMBICAgavAgEBBAkCBwONfqe15HgwGwICBqcCAQEEEgwQMTAwMDAwMDYwOTI2NDc1ODAbAgIGqQIBAQQSDBAxMDAwMDAwNDMyMzYzMTk3MB8CAgaoAgEBBBYWFDIwMTktMTItMjdUMDU6NDU6MjZaMB8CAgaqAgEBBBYWFDIwMTktMTItMjdUMDU6NDQ6NDdaMB8CAgasAgEBBBYWFDIwMTktMTItMjdUMDY6MDA6MjZaMCQCAgamAgEBBBsMGWNvbS5teW51c3R3by5zdWJzY3JpcHRpb24wggGGAgERAgEBBIIBfDGCAXgwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAzAMAgIGrgIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwDAICBrcCAQEEAwIBADASAgIGrwIBAQQJAgcDjX6nteUVMBsCAganAgEBBBIMEDEwMDAwMDA2MDkyNjczNzUwGwICBqkCAQEEEgwQMTAwMDAwMDQzMjM2MzE5NzAfAgIGqAIBAQQWFhQyMDE5LTEyLTI3VDA2OjAwOjI2WjAfAgIGqgIBAQQWFhQyMDE5LTEyLTI3VDA1OjU5OjM5WjAfAgIGrAIBAQQWFhQyMDE5LTEyLTI3VDA2OjE1OjI2WjAkAgIGpgIBAQQbDBljb20ubXludXN0d28uc3Vic2NyaXB0aW9uoIIOZTCCBXwwggRkoAMCAQICCA7rV4fnngmNMA0GCSqGSIb3DQEBBQUAMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MTExMzAyMTUwOVoXDTIzMDIwNzIxNDg0N1owgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKXPgf0looFb1oftI9ozHI7iI8ClxCbLPcaf7EoNVYb/pALXl8o5VG19f7JUGJ3ELFJxjmR7gs6JuknWCOW0iHHPP1tGLsbEHbgDqViiBD4heNXbt9COEo2DTFsqaDeTwvK9HsTSoQxKWFKrEuPt3R+YFZA1LcLMEsqNSIH3WHhUa+iMMTYfSgYMR1TzN5C4spKJfV+khUrhwJzguqS7gpdj9CuTwf0+b8rB9Typj1IawCUKdg7e/pn+/8Jr9VterHNRSQhWicxDkMyOgQLQoJe2XLGhaWmHkBBoJiY5uB0Qc7AKXcVz0N92O9gt2Yge4+wHz+KO0NP6JlWB7+IDSSMCAwEAAaOCAdcwggHTMD8GCCsGAQUFBwEBBDMwMTAvBggrBgEFBQcwAYYjaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy13d2RyMDQwHQYDVR0OBBYEFJGknPzEdrefoIr0TfWPNl3tKwSFMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUiCcXCam2GGCL7Ou69kdZxVJUo7cwggEeBgNVHSAEggEVMIIBETCCAQ0GCiqGSIb3Y2QFBgEwgf4wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNgYIKwYBBQUHAgEWKmh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5LzAOBgNVHQ8BAf8EBAMCB4AwEAYKKoZIhvdjZAYLAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAA2mG9MuPeNbKwduQpZs0+iMQzCCX+Bc0Y2+vQ+9GvwlktuMhcOAWd/j4tcuBRSsDdu2uP78NS58y60Xa45/H+R3ubFnlbQTXqYZhnb4WiCV52OMD3P86O3GH66Z+GVIXKDgKDrAEDctuaAEOR9zucgF/fLefxoqKm4rAfygIFzZ630npjP49ZjgvkTbsUxn/G4KT8niBqjSl/OnjmtRolqEdWXRFgRi48Ff9Qipz2jZkgDJwYyz+I0AZLpYYMB8r491ymm5WyrWHWhumEL1TKc3GZvMOxx6GUPzo22/SGAGDDaSK+zeGLUR2i0j0I78oGmcFxuegHs5R0UwYS/HE6gwggQiMIIDCqADAgECAggB3rzEOW2gEDANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMTMwMjA3MjE0ODQ3WhcNMjMwMjA3MjE0ODQ3WjCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMo4VKbLVqrIJDlI6Yzu7F+4fyaRvDRTes58Y4Bhd2RepQcjtjn+UC0VVlhwLX7EbsFKhT4v8N6EGqFXya97GP9q+hUSSRUIGayq2yoy7ZZjaFIVPYyK7L9rGJXgA6wBfZcFZ84OhZU3au0Jtq5nzVFkn8Zc0bxXbmc1gHY2pIeBbjiP2CsVTnsl2Fq/ToPBjdKT1RpxtWCcnTNOVfkSWAyGuBYNweV3RY1QSLorLeSUheHoxJ3GaKWwo/xnfnC6AllLd0KRObn1zeFM78A7SIym5SFd/Wpqu6cWNWDS5q3zRinJ6MOL6XnAamFnFbLw/eVovGJfbs+Z3e8bY/6SZasCAwEAAaOBpjCBozAdBgNVHQ4EFgQUiCcXCam2GGCL7Ou69kdZxVJUo7cwDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAOBgNVHQ8BAf8EBAMCAYYwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAE/P71m+LPWybC+P7hOHMugFNahui33JaQy52Re8dyzUZ+L9mm06WVzfgwG9sq4qYXKxr83DRTCPo4MNzh1HtPGTiqN0m6TDmHKHOz6vRQuSVLkyu5AYU2sKThC22R1QbCGAColOV4xrWzw9pv3e9w0jHQtKJoc/upGSTKQZEhltV/V6WId7aIrkhoxK6+JJFKql3VUAqa67SzCu4aCxvCmA5gl35b40ogHKf9ziCuY7uLvsumKV8wVjQYLNDzsdTJWk26v5yZXpT+RN5yaZgem8+bQp0gF6ZuEujPYhisX4eOGBrr/TkJ2prfOv/TgalmcwHFGlXOxxioK0bA8MFR8wggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4++NwzeajTEV+H0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz/2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcNgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIDutXh+eeCY0wCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQCczLnFk5UW+PsMQu6FKpOGr4X5a053soDiM9vevY/UUwmE1bzU3EHQZZjrTpWbK5Qvk6wQd7Uowwl70b/UrbHMCtTFoyh1KsCCK6MiLjaF78lZ7uHgpnTO4kc9pxwrMawVWtSwE6ZpnEJWeUZlVPUDZiAqXwZR95ygmvOYOzIZHpmM/sS8AVr+qiqEUPhkSBVt+WjJ/0DOcWosou5IsF03s1yF3IalM0JNZqh1j3x5FYI1awEFg2PILYMJUbc/juxA87WAsI1LTfM+VTOUDn5Gvdt9N7YAMnC0DYkIgz/En+/oVvYIQTldgqOe1eEUGCg8ElB2LatrwjWn90/issXZ', NULL, NULL, NULL, NULL, 'ios', '2019-12-26 21:59:54', '2019-12-26 21:59:54');

-- --------------------------------------------------------

--
-- Table structure for table `phones`
--

CREATE TABLE `phones` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `phone` varchar(16) NOT NULL,
  `otp` varchar(4) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `phones`
--

INSERT INTO `phones` (`id`, `user_id`, `phone`, `otp`, `created`, `modified`) VALUES
(1, 38, '8126208888', '4050', '2019-05-07 23:07:15', '2019-05-07 23:07:15'),
(2, 38, '8126208888', '4280', '2019-05-07 23:11:41', '2019-05-07 23:11:41'),
(3, 38, '8758748667', '7549', '2019-05-07 23:13:11', '2019-05-07 23:13:11'),
(4, 38, '8126208713', '3070', '2019-05-07 23:14:34', '2019-05-07 23:14:34'),
(5, 38, '8126208714', '4640', '2019-05-09 23:33:14', '2019-05-09 23:33:14'),
(6, 24, '8279842725', '8019', '2019-06-18 18:02:56', '2019-06-18 18:02:56'),
(7, 34, '3205868543', '5390', '2019-10-03 13:33:39', '2019-10-03 13:33:39');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `value` int(11) DEFAULT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'employee',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`id`, `customer_id`, `employee_id`, `value`, `type`, `created`, `modified`) VALUES
(1, 16, 33, 5, 'employee', '2019-06-30 10:42:29', '2019-06-30 10:42:29'),
(3, 12, 59, 5, 'employee', '2019-07-26 02:55:50', '2019-11-27 02:39:40'),
(4, 16, 22, 5, 'company', '2019-08-04 15:55:18', '2019-08-04 15:55:18'),
(12, 12, 64, 3, 'employee', '2019-08-09 01:52:41', '2019-08-09 01:52:41'),
(24, 12, 14, 5, 'company', '2019-08-09 02:25:20', '2019-11-27 02:39:35'),
(25, 14, 14, 1, 'company', NULL, NULL),
(26, 12, 12, 3, 'company', '2019-09-10 05:52:56', '2019-09-10 05:52:56'),
(27, 14, 12, 3, 'company', '2019-09-19 01:44:30', '2019-09-19 01:44:30');

-- --------------------------------------------------------

--
-- Table structure for table `restaurant_arrangement`
--

CREATE TABLE `restaurant_arrangement` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `capacity` int(11) NOT NULL,
  `description` varchar(125) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `restaurant_arrangement`
--

INSERT INTO `restaurant_arrangement` (`id`, `company_id`, `name`, `capacity`, `description`) VALUES
(1, 9, 'table no one', 4, 'pankaj pant'),
(2, 9, 'table no 2', 5, 'xyz'),
(3, 9, 'table no 5', 4, 'pankaj pant'),
(5, 12, 'Cgychcy', 2, 'Ycycycycy'),
(7, 12, 'Round', 2, 'Fgcg'),
(12, 12, 'Square', 6, 'Gugg'),
(14, 12, 'Frcrc', 5, 'Dcdcd'),
(16, 21, 'Table 1', 4, 'It is round table with seating capacity 4'),
(17, 22, 'Ocean view ', 6, 'Enjoy the best view in the house overlooking the beach '),
(18, 22, 'Table 5', 3, 'Seat 3 '),
(21, 29, 'Yyhh', 6, 'Ghvb'),
(25, 32, 'Vjnn', 5, 'Gvhj'),
(26, 33, 'Fug', 666, 'Hgjjk'),
(27, 14, 'Table 1', 2, 'Vhh'),
(28, 14, 'Table 2 ', 3, 'Gahsh'),
(29, 22, 'Table 4', 8, 'Best seat in the house '),
(30, 22, 'Table 3', 8, 'Near exit door '),
(31, 12, 'Bkbkbi', 5, 'Cucucucufufu'),
(32, 12, 'Hgcc', 8, 'Hlk'),
(33, 12, 'Fbij', 5, 'Moc'),
(34, 22, 'Table 12 ', 8, 'Between exit and bathroom ');

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `days` varchar(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `employee_id`, `company_id`, `date`, `days`, `created`) VALUES
(3, 3, NULL, NULL, 'monday', '2019-05-07 22:23:34'),
(4, NULL, 14, NULL, 'Tuesday', '2019-05-08 00:18:38'),
(5, NULL, 12, NULL, 'Monday', '2019-05-08 02:14:33'),
(6, NULL, 12, NULL, 'Thursday', '2019-05-08 23:32:40'),
(7, NULL, 14, NULL, 'Friday ', '2019-05-09 16:33:52'),
(8, NULL, 14, NULL, 'Monday ', '2019-05-13 10:58:58'),
(9, NULL, 12, NULL, 'Friday ', '2019-05-17 10:08:14'),
(10, NULL, 14, NULL, 'Sunday', '2019-05-17 19:40:19'),
(11, NULL, 21, NULL, 'Friday ', '2019-05-17 19:59:50'),
(12, NULL, 22, NULL, 'Saturday ', '2019-05-18 10:38:12'),
(13, NULL, 21, NULL, 'Saturday ', '2019-05-18 13:34:29'),
(14, NULL, 26, NULL, 'Wednesday ', '2019-05-20 12:35:37'),
(16, NULL, 12, NULL, 'Wednesday ', '2019-05-22 12:27:27'),
(17, 33, NULL, NULL, 'Thursday ', '2019-05-29 21:55:01'),
(19, 33, NULL, NULL, 'Monday ', '2019-06-03 22:10:43'),
(20, 59, NULL, NULL, 'Tuesday ', '2019-06-04 15:24:01'),
(21, 63, NULL, NULL, 'Tuesday ', '2019-06-11 10:35:08'),
(22, 64, NULL, NULL, 'Wednesday ', '2019-06-18 18:24:15'),
(23, NULL, 22, NULL, 'Sunday ', '2019-06-28 10:21:41'),
(24, NULL, 22, NULL, 'Monday ', '2019-06-28 10:22:56'),
(25, NULL, 22, NULL, 'Tuesday ', '2019-06-28 10:23:46'),
(26, 33, NULL, NULL, 'Sunday ', '2019-06-30 10:35:05'),
(27, 65, NULL, NULL, 'Tuesday ', '2019-07-22 05:24:07'),
(28, 64, NULL, NULL, 'Thursday ', '2019-07-24 23:34:04'),
(29, 59, NULL, NULL, 'Friday ', '2019-07-25 05:27:26'),
(30, NULL, 22, NULL, 'Friday ', '2019-08-01 20:01:15'),
(31, NULL, 22, NULL, 'Wednesday ', '2019-08-07 09:33:53');

-- --------------------------------------------------------

--
-- Table structure for table `slots`
--

CREATE TABLE `slots` (
  `id` int(11) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slots`
--

INSERT INTO `slots` (`id`, `schedule_id`, `created`, `start_time`, `end_time`) VALUES
(5, 4, '2019-05-08 00:18:38', '14:30:00', '20:30:00'),
(6, 4, '2019-05-08 00:18:38', '12:30:00', '15:30:00'),
(7, 4, '2019-05-08 00:37:23', '12:07:00', '13:07:00'),
(8, 4, '2019-05-08 02:02:05', '18:05:00', '18:31:00'),
(9, 5, '2019-05-08 02:14:33', '06:00:00', '07:00:00'),
(10, 6, '2019-05-08 23:32:40', '04:00:00', '05:00:00'),
(11, 6, '2019-05-08 23:32:40', '07:00:00', '08:00:00'),
(12, 6, '2019-05-09 14:43:14', '09:00:00', '10:00:00'),
(13, 7, '2019-05-09 16:33:52', '10:00:00', '11:00:00'),
(14, 8, '2019-05-13 10:58:58', '13:58:00', '14:58:00'),
(15, 6, '2019-05-16 11:24:55', '18:00:00', '19:00:00'),
(16, 6, '2019-05-16 00:00:00', '19:00:00', '20:00:00'),
(17, 6, '2019-05-16 18:18:48', '19:00:00', '20:00:00'),
(18, 6, '2019-05-16 18:18:48', '20:18:00', '21:18:00'),
(19, 9, '2019-05-17 10:08:14', '11:00:00', '12:00:00'),
(20, 9, '2019-05-17 10:08:14', '18:00:00', '18:30:00'),
(21, 7, '2019-05-17 11:03:34', '18:00:00', '19:00:00'),
(22, 10, '2019-05-17 19:40:19', '11:00:00', '12:00:00'),
(23, 11, '2019-05-17 19:59:50', '12:00:00', '13:00:00'),
(24, 11, '2019-05-17 20:00:26', '15:00:00', '16:00:00'),
(25, 12, '2019-05-18 10:38:12', '08:00:00', '10:00:00'),
(26, 12, '2019-05-18 10:38:12', '10:00:00', '12:00:00'),
(27, 12, '2019-05-18 10:38:12', '12:00:00', '14:00:00'),
(28, 13, '2019-05-18 13:34:29', '13:00:00', '15:00:00'),
(29, 10, '2019-05-19 00:31:05', '14:00:00', '16:00:00'),
(30, 14, '2019-05-20 12:35:37', '06:00:00', '07:00:00'),
(33, 12, '2019-05-24 11:28:28', '13:58:00', '14:58:00'),
(34, 17, '2019-05-29 21:55:01', '09:24:00', '10:24:00'),
(35, 17, '2019-05-29 21:55:01', '10:24:00', '11:24:00'),
(36, 17, '2019-05-29 21:55:01', '12:24:00', '13:24:00'),
(39, 19, '2019-06-03 22:10:43', '09:40:00', '10:40:00'),
(40, 19, '2019-06-03 22:10:43', '10:40:00', '11:40:00'),
(41, 19, '2019-06-03 22:10:43', '11:40:00', '12:40:00'),
(42, 19, '2019-06-03 22:10:43', '12:40:00', '13:40:00'),
(43, 20, '2019-06-04 15:24:01', '17:23:00', '18:23:00'),
(44, 21, '2019-06-11 10:35:08', '08:04:00', '09:04:00'),
(45, 21, '2019-06-11 10:35:08', '10:04:00', '11:04:00'),
(46, 21, '2019-06-11 10:35:08', '13:04:00', '14:05:00'),
(47, 4, '2019-06-18 08:03:34', '09:00:00', '10:00:00'),
(48, 22, '2019-06-18 18:24:15', '11:23:00', '11:24:00'),
(49, 23, '2019-06-28 10:21:41', '10:21:00', '11:21:00'),
(50, 23, '2019-06-28 10:21:41', '11:21:00', '12:21:00'),
(51, 23, '2019-06-28 10:21:41', '12:21:00', '13:21:00'),
(52, 23, '2019-06-28 10:21:41', '13:21:00', '14:21:00'),
(53, 24, '2019-06-28 10:22:56', '08:21:00', '09:21:00'),
(54, 24, '2019-06-28 10:22:56', '09:22:00', '10:22:00'),
(55, 24, '2019-06-28 10:22:56', '10:22:00', '11:22:00'),
(56, 24, '2019-06-28 10:22:56', '11:21:00', '12:22:00'),
(57, 24, '2019-06-28 10:22:56', '12:22:00', '13:22:00'),
(58, 25, '2019-06-28 10:23:46', '08:23:00', '09:23:00'),
(59, 25, '2019-06-28 10:23:46', '09:23:00', '10:23:00'),
(60, 25, '2019-06-28 10:23:46', '10:23:00', '11:23:00'),
(61, 25, '2019-06-28 10:23:46', '11:23:00', '12:23:00'),
(62, 26, '2019-06-30 10:35:05', '08:30:00', '09:30:00'),
(63, 26, '2019-06-30 10:35:05', '10:34:00', '11:34:00'),
(64, 26, '2019-06-30 10:35:05', '11:34:00', '12:35:00'),
(65, 5, '2019-07-04 22:28:42', '10:58:00', '11:58:00'),
(66, 5, '2019-07-04 22:28:42', '11:58:00', '12:58:00'),
(67, 27, '2019-07-22 05:24:07', '06:00:00', '07:00:00'),
(68, 28, '2019-07-24 23:34:04', '15:03:00', '16:03:00'),
(69, 28, '2019-07-24 23:34:04', '16:03:00', '17:07:00'),
(70, 29, '2019-07-25 05:27:26', '17:57:00', '18:57:00'),
(72, 6, '2019-07-31 21:53:40', '18:00:00', '19:00:00'),
(73, 6, '2019-07-31 21:53:40', '19:00:00', '20:00:00'),
(74, 6, '2019-07-31 21:53:40', '20:00:00', '21:00:00'),
(75, 30, '2019-08-01 20:01:15', '08:00:00', '09:00:00'),
(76, 30, '2019-08-01 20:01:15', '10:00:00', '11:00:00'),
(77, 30, '2019-08-01 20:01:15', '10:00:00', '11:00:00'),
(78, 30, '2019-08-01 20:01:15', '11:00:00', '12:00:00'),
(79, 30, '2019-08-01 20:01:15', '08:00:00', '09:00:00'),
(80, 8, '2019-08-04 23:13:29', '23:30:00', '23:41:00'),
(81, 31, '2019-08-07 09:33:53', '09:32:00', '10:32:00'),
(82, 31, '2019-08-07 09:33:53', '10:32:00', '11:32:00'),
(83, 31, '2019-08-07 09:33:53', '11:32:00', '12:32:00'),
(84, 31, '2019-08-07 09:33:53', '12:32:00', '13:32:00'),
(85, 31, '2019-08-07 09:33:53', '13:32:00', '14:32:00'),
(86, 31, '2019-08-07 09:33:53', '14:33:00', '15:33:00'),
(87, 31, '2019-08-07 09:33:53', '15:33:00', '16:33:00'),
(88, 31, '2019-08-07 09:33:53', '16:33:00', '17:33:00'),
(89, 19, '2019-08-19 15:36:37', '08:34:00', '09:34:00'),
(90, 19, '2019-08-19 15:36:37', '09:35:00', '10:35:00'),
(91, 19, '2019-08-19 15:36:37', '10:35:00', '11:35:00'),
(92, 19, '2019-08-19 15:36:37', '11:35:00', '12:35:00'),
(93, 19, '2019-08-19 15:36:37', '12:35:00', '13:36:00'),
(94, 19, '2019-08-19 15:36:37', '13:36:00', '14:36:00'),
(95, 19, '2019-08-19 15:36:37', '14:36:00', '15:36:00'),
(96, 19, '2019-08-19 15:36:37', '15:36:00', '16:36:00'),
(97, 19, '2019-08-19 15:36:37', '16:36:00', '17:36:00'),
(98, 25, '2019-09-03 21:42:00', '21:41:00', '22:41:00'),
(99, 17, '2019-10-03 13:12:03', '13:11:00', '14:11:00'),
(100, 17, '2019-10-03 13:12:03', '14:11:00', '15:11:00'),
(101, 17, '2019-10-03 13:12:03', '15:11:00', '16:11:00'),
(102, 17, '2019-10-03 13:12:03', '16:11:00', '17:11:00'),
(103, 17, '2019-10-03 15:30:21', '15:29:00', '16:29:00'),
(104, 17, '2019-10-03 15:30:21', '17:29:00', '18:29:00'),
(105, 17, '2019-10-03 15:30:21', '18:29:00', '19:30:00'),
(106, 17, '2019-10-03 15:30:21', '19:30:00', '20:30:00'),
(107, 17, '2019-10-03 15:30:21', '20:30:00', '21:30:00'),
(108, 17, '2019-10-10 19:39:57', '20:39:00', '21:39:00'),
(109, 17, '2019-10-10 20:28:29', '21:32:00', '22:32:00');

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `amount`, `created`, `modified`) VALUES
(1, '9.99', '2018-08-17 13:22:06', '2018-08-17 13:22:06');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `otp` varchar(4) NOT NULL,
  `access_token` text NOT NULL,
  `type` enum('admin','host','guest') DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_otp_verified` varchar(15) NOT NULL DEFAULT 'no',
  `visibility` enum('yes','no') DEFAULT 'yes',
  `payment_status` enum('yes','no','free') DEFAULT 'no',
  `device_token` text,
  `device_type` varchar(25) DEFAULT NULL,
  `badge_count` int(3) NOT NULL DEFAULT '0',
  `noti_alert` enum('on','off') NOT NULL DEFAULT 'on',
  `country` varchar(75) DEFAULT NULL,
  `countrycode` int(5) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `phone`, `password`, `otp`, `access_token`, `type`, `status`, `is_otp_verified`, `visibility`, `payment_status`, `device_token`, `device_type`, `badge_count`, `noti_alert`, `country`, `countrycode`, `created`, `modified`) VALUES
(1, 'admin@gmail.com', '$2y$10$yDAoPreKfn90Mi4d6ch3w.d5VVxZJmOUzX6B.9yXRgikOz0op/h/C', '6789', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE1LCJpZCI6MTUsImV4cCI6MTUzMTI5NzI1M30.Xj1iGEgyagtCoG5Onur9CSaQevJDYKghliHFI1zmzog', 'admin', 'active', 'no', 'yes', 'no', NULL, NULL, 0, 'on', NULL, NULL, NULL, '2019-02-03 08:34:30'),
(2, '3399339933', '$2y$10$tf0.XVt7AaT9JitbuDRFWeutx0n969s1y06Tz3794UC1QeybUmzbK', '0871', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIsImlkIjoyLCJleHAiOjE1NTk1NzEwOTJ9.lucRsD8waoe3cga-6OR84THnNvRFMzqzZexlMU4lrEw', 'host', 'active', 'yes', 'yes', 'free', 'e2NSpx_AD4Y:APA91bE-P_kXdZXltEbyj8xmwvozsc67xT0jeIB-KFqXBzGw0sxhHKqR8Ce4_OvRucCY9LE4JDB_bHTzKXL383K4VwUy5NMMGyYXMatTqjmBEI7Nd8HOWMIUVIWMg8j4jvQEkJMuTf-l', 'android', 0, 'on', NULL, NULL, '2019-01-29 09:05:41', '2019-01-29 09:05:41'),
(3, '7147091406', '$2y$10$vc4Qtp254hqRvdzbca5wh.sozSShbF9nC1B37ENjIvcmUABNHa85u', '4304', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMsImlkIjozLCJleHAiOjE1NTM0ODQyODl9.m78blji5VkMalM_4aVFMzq8l6fXHizJrgc-BrFKmXEc', 'guest', 'active', 'yes', 'yes', 'no', 'd17gdIAXJNI:APA91bFX7THsggiIUNXp0vSa3MGpyiUJF-Te9WHZBmyRPKk6gHkravf-MeMe-JvuB0wxt3F665qihyK52cxglJ5pbbuCqSkNVr9QlLaeJpkb5eW1e0pOeCssamiM_QfPqwfGyccFz_of', 'android', 0, 'on', NULL, NULL, '2019-01-30 00:14:08', '2019-01-30 00:14:08'),
(4, '6264349058', '$2y$10$CpoJPND0qgVk0pj6YM4jD.vlCig3xaKjzo6RSZPbdwgdhf4FBoVva', '8587', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQsImlkIjo0LCJleHAiOjE1NDg3OTk1OTN9.EaFNCronijEFt1ybeScEXNQGV0K2CJX3on7hzFgYuow', 'guest', 'active', 'yes', 'yes', 'no', '', 'android', 0, 'on', NULL, NULL, '2019-01-30 02:35:07', '2019-01-30 02:35:07'),
(5, '9092246073', '$2y$10$dpDTuMfh/3RnB/cJGQLft.fVqGRYSBPo.tg.akC5WZ3jfajG6FqBC', '2469', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOm51bGwsImlkIjpudWxsLCJleHAiOjE1NDg4MDY0ODZ9.TWjy0VXP5gKce9qqa69VO15q_-iXa7ovyK98taGbxKM', 'guest', 'active', 'yes', 'yes', 'no', 'cr1FkeXeIq8:APA91bGiPmKA8wMFj8qL49IgZwWS4Ej4i0x_0LwKQt5JXB39rP8sxtWIz0sCoHe2NUXGpHF26SuJfqGmuWDitbrI3MpGkQkkHhRp97JGiSvEeagqFGipQSDPDxx8UG3_cJaMQnAcIRvA', 'ios', 0, 'on', NULL, NULL, '2019-01-30 04:31:27', '2019-01-30 04:31:27'),
(6, '9094770833', '$2y$10$kCb/YLd1mP8iIl65.nGme.AY/3Npe5G60uWKKTTYCNLsRNRlRHIpS', '8774', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjYsImlkIjo2LCJleHAiOjE1NDg4MjE0MzF9.KXmBRuqC_j1uJ_YqkngQyom2xhB1jUWv2YdrrEQC5XM', 'guest', 'active', 'yes', 'yes', 'no', 'erBbKGcSWFQ:APA91bH0StdrguOaXrUVeYkoMD7H-Sam8811K6Hbf_4m1TynCsQOV2jEQ33HTzCgPSpLtWTVGOVB-PbtJyvMsX86ruGTXuHO5wfLQMXK4n3xc2vKTwWBf5NzckttMsVf4-VJOZeUhs96', 'ios', 0, 'on', NULL, NULL, '2019-01-30 08:40:07', '2019-01-30 08:40:07'),
(7, '5202442220', '$2y$10$qZavCIsEZyrYlE9vIlLNle5vTomAnGwWsN3KXE9PWX05PvggYc7CS', '4253', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjcsImlkIjo3LCJleHAiOjE1NDg5MDYzMzh9.PTGRXl0wGHgfQcXihr2QpYhcQ9-GLdCYO0ZmZ0WsBYo', 'guest', 'active', 'yes', 'yes', 'no', 'd4O0qsxzsV4:APA91bGuy0ZB_7mqfrPUHNP4ZSRDUninkr1oRy13UN7bpxRaTj2m0iJgZQMcWPevKmSJrC-nj1FGdbRtawOsccTa85-cZYvAG-dqatkCnxpW1nW-czuN2Plb2iz__x2X4Zo_C-J1Ipno', 'android', 0, 'on', NULL, NULL, '2019-01-31 08:14:38', '2019-02-03 09:41:13'),
(8, '9808565240', '$2y$10$rzKLRrUnSlRta7N0WzwEJ.ZOJhMxcNlW65JtNlJDvBN2AmL.wZQYK', '3024', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjgsImlkIjo4LCJleHAiOjQ3MjgyNzc5MjV9.K6aTlVuyf3FPrwQCsPm6YGCeFHqiAP9QEXBp5I3ZDEo', 'guest', 'active', 'yes', 'no', 'no', 'cLoscYbsgjs:APA91bEeQZ8-Zv2xlCkCaBpbzlD5UbIV-rdt5GimlnVHz9iOapG1LnJUku5zo0RaW5z_EvlAmaQbwQweDHpSS1CJfFOmdqA0YpSy6xXhsgRgTUhOqg_Ul1xtwQs4R9RIcLQhOhd48Q7I', 'android', 0, 'on', 'India', 91, '2019-01-31 10:34:45', '2019-01-31 10:34:45'),
(9, '7300820270', '$2y$10$Fkcf7UjAT8.JYcTgGwUrAeCCsDhVdWH7yH8h9ME8wkCQeOt/eFVDO', '2232', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjksImlkIjo5LCJleHAiOjQ3Mzk0MzMxMjJ9.Mf5tQ62wZYndDa8HNwTmPl6KDND8Oo3xd2IP0IU-QhE', 'host', 'active', 'yes', 'yes', 'free', 'f2I5Czcsivw:APA91bHbPTDOWgSDy3OLMf-rWy8ydO73mv97Af_C0pH2JL-QMag1qcc3cPLncmw3OAXkesdZXCVfIV-N5fSI9GakFacBfNAzLdmikiODPPwd60_2exbJ5Mi84EQ6vqS2W8zsQW3L3-uV', 'android', 0, 'on', 'India', 91, '2019-01-31 10:36:44', '2019-01-31 10:55:51'),
(10, '5622827947', '$2y$10$eA35.tItn7PQRBCQOHIH5ekVnYf5GJ5iwjA8NByst5jIK4C9tAm8O', '9576', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpZCI6MTAsImV4cCI6MTU0ODk0OTgxN30.69cuSXouh48rRlLO1xYWxT9ubSiWM0kW__GhIMbIFV4', 'guest', 'active', 'yes', 'yes', 'no', 'cw2YgECIFu4:APA91bGM89_j3fCARVgYEPoMX75asDw6Xl-h2YHMx8tnkomWSftTKBrdVbtMPEFhwlIjK_oyez-qdAX2ZONReO22YPRpEMJc29Tsu-sPL3ys--X--H-y2VcMHiCoeKvalAGdQ0O7liMu', 'ios', 0, 'on', NULL, NULL, '2019-01-31 20:19:26', '2019-01-31 20:19:26'),
(11, '3107046680', '$2y$10$9LtVgtGUamQFtjdh0qj3meKpXHDnP1JjUPEymJ0r6BprZLv8A..1a', '2664', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjExLCJpZCI6MTEsImV4cCI6MTU0OTIxOTkzOX0.kgOQbWFic8PN4DOeujkkqZYP2PTbsgblMI2vCKQTtO8', 'host', 'active', 'yes', 'yes', 'no', 'dDiUEChi0ik:APA91bGy9RPRiGN58bDCls1chVPNYB3TpfUtwWhzbuHyEUN-KoInKhDaD-Dn79Edl9rFkzpPKkXx3oU6TMvb41TtQiwtqgXNZBqNHPZkC6Vjs-o9na5k8rU96ybxMQ8_frm3CbFg2G-b', 'ios', 0, 'on', NULL, NULL, '2019-02-03 23:21:37', '2019-02-03 23:21:37'),
(12, '3108186648', '$2y$10$ygZ0Z1u63w82QMvLlhpRsOCKWQYv0VswUe/36Y6AI/PzBkMcc4tc.', '9732', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyLCJpZCI6MTIsImV4cCI6MTU0OTM5MDkwNX0.Hh-_67gPT-jeg1y9XfcE6_JRWac8gKCFDy8JtSmL1yc', 'host', 'active', 'yes', 'no', 'free', '(null)', 'ios', 0, 'on', NULL, NULL, '2019-02-04 22:22:56', '2019-02-04 22:22:56'),
(13, '3107663023', '$2y$10$s8Qg1Xz0B0eZEwWMka5lz.co.yrfk7BerxuWOO6/r5AFcB4/z/A8y', '5413', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEzLCJpZCI6MTMsImV4cCI6MTU0OTU5MTQxN30.9XZq20SIEDl3OLehfc-NyIKKfLGXIqmCJ3_3cD0v40g', 'host', 'active', 'yes', 'yes', 'free', 'd8zSZ8iBPqs:APA91bG1GznMlRpTJwQJdiBm_PlEKpS-eiOagjU_YgBP6w7iKiWaOhBQxFUR-gTCQapen4qkDCh1dQZGaaTdf5ZpcPVpAY_06bs8-57vjhHmbDnpUW0LDLn0RARl8Fu5ysSbgXrvjSrv', 'android', 0, 'on', NULL, NULL, '2019-02-08 06:32:30', '2019-02-08 06:32:30'),
(14, '3106548496', '$2y$10$2IBC8MF.GOqTgwsPFXaKn.WlyVYSuc5Uaxy.B/kYY5HtG6C1/zrfm', '1502', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE0LCJpZCI6MTQsImV4cCI6MTU0OTg1NTQ1MH0.a2kJPNyBZJ0xKj2K-nhq2d3YpOfuDAWTaKJkgkeM_To', 'guest', 'active', 'yes', 'yes', 'no', 'cEybPGhQEmU:APA91bEOH9zSBTF7R-6fNPhUKo1PoZVAsH8xO9Yz26aaaVjt3CXYReaep4-0H2S5gM3hkg-irJ5lDdZcCkBpu0RJ1g11ofv98bY_ng6uK3RlB7xHPODOopzE80-bDrrmaxYQP2QVEBeE', 'ios', 0, 'on', NULL, NULL, '2019-02-10 10:06:25', '2019-02-10 10:06:25'),
(15, '8828335810', '$2y$10$S34mvTualtQE1yK1JJYXauyIz/6Ny4K9uX03yYa3fvsuYr/L/oROm', '4059', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOm51bGwsImlkIjpudWxsLCJleHAiOjE1NTAwNDYwMDR9.nYDiBrooLSvtnIVZbbuCUrNzidki9nZu39u3kkrV2zY', 'guest', 'active', 'no', 'yes', 'no', '', 'android', 0, 'on', NULL, NULL, '2019-02-13 12:50:04', '2019-02-13 12:50:04'),
(16, '3236081652', '$2y$10$vnz3YTTuKOEWqL8IS5oZuu2nk8Kp.ngo6wqugfbEtHLysS2qHjgCq', '4906', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE2LCJpZCI6MTYsImV4cCI6MTU1MDU1MTcxNX0.aD9tZ0r1K-3JxkJgnIIDILnGUO4PM8wTxKCma71ktNw', 'guest', 'active', 'yes', 'yes', 'no', 'fNJ-V8BFJ2c:APA91bFpntcJKXJ9P1owp2u-Pk_oa7U6Sbd31qCGgEWd0_S7JgD7UsjHTwcDmDernHb46FslT8cyxtuZcvn9xUCbHA_3URTGQo69894uepGugLtVO1sc4CkHS8tm1lcTktl7VApesqEF', 'ios', 0, 'on', NULL, NULL, '2019-02-19 09:18:13', '2019-02-19 09:18:13'),
(17, '4242077324', '$2y$10$lD8r0o/BMX19lOQJvvHKvuj0FvaR9GdD40N7K4JyI/ls6DKeN6w7y', '5809', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE3LCJpZCI6MTcsImV4cCI6MTU1MDY1MDc0N30.q5h1VzouugL-G_V32JIe-hHBdc0n4LRxMRJuzpFXQvQ', 'host', 'active', 'yes', 'yes', 'free', 'cW5iwjSMzFk:APA91bEx-itNLP-5Lbo0GridOhg1At7w3SJ9WtPLC-y92D-OdXs-YNsvNzkqnsqVycsTxcV3Y6kA6uXwqcNcs954d8__XVghl-ctcX10lf0i1lYN9-ct4DRjSW2Ot7bf98-lE03r8XMX', 'android', 0, 'on', NULL, NULL, '2019-02-20 12:48:18', '2019-02-20 12:48:18'),
(18, '3472256645', '$2y$10$1KasaEPYgQSxQcQG7ryQ2eoKpHkMTXVS0gFJQZZDB8lWPqlmL4CfO', '1158', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE4LCJpZCI6MTgsImV4cCI6MTU1MjY4NTY5MH0.EAD3N-PBK2oCJaAfUd38Zy5TKkwKnN7dkDrCkTBlfnY', 'guest', 'active', 'yes', 'yes', 'no', 'exgPMKNM0ro:APA91bGO-UoJKhHFB-PbsdKHR0uz5VhJXt2zkFjg1rwti4UIpChHstUf2YnIUfjzlzVZrSkJErA_3-eNVj6HAq6AzZ-1H8Yq59G8sXi0QiYZtsS_NvnEUm_h1cl0CBM8RjTw74bEGLep', 'android', 0, 'on', NULL, NULL, '2019-03-01 20:15:57', '2019-03-01 20:15:57'),
(19, '5623558864', '$2y$10$hsPFs4t6SaR.haiHKvNRo.OVmsFtrWe8YLWF2wgMqBrQSQ4vcJHhq', '2418', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE5LCJpZCI6MTksImV4cCI6MTU1MTc2MzAwOH0.hFYUmjOtwDVVteSRcl_8_DkCknOsuCzftT0a1a_9O1k', 'guest', 'active', 'yes', 'yes', 'no', 'cnzhaIPcw8M:APA91bGd0hfygrm69R9Zmx4dbPESk7a924HXx8VVjVbrcv-VEGJQNAVRNlQ9gPt96GSPxO0wiMdiLOGlxlOvhK_JsJehtvxNQpUSfDnvd56llB17qIvbw4lV7APRAt4Z8TBLICCutDX_', 'ios', 0, 'on', NULL, NULL, '2019-03-05 09:45:54', '2019-03-05 09:45:54'),
(20, '5624893791', '$2y$10$G/xwOWQGlSFWE2NWf8kpfeCbKeXqFpbjaBf6xPwMG9fpjEpXygvZW', '1322', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIwLCJpZCI6MjAsImV4cCI6MTU1MjE5NTE4Nn0.rf2QAGGW4ssppkQGPkiCjeAPLAlj_KRytQbLhZyY-Xk', 'guest', 'active', 'yes', 'yes', 'no', 'eIklR2Y9V4o:APA91bH1mTqQLXdsNwAx7w2YjhJVx_vKBxmx0LK3X4xqwWrwmnnvZo_gm_1MXF0kuW8BjZcmDkI9XbawJzBaI7dX1Sq29781K1WzxG0z_U2p7PNeuLnufxS2Y7Redtr-_eiJbLMvLSOF', 'ios', 0, 'on', NULL, NULL, '2019-03-10 09:43:38', '2019-03-10 09:43:38'),
(21, '8184225705', '$2y$10$XSonMpJrZHqPxKb95aXCoejl6A6OMXXADRLg33e..4m0zo7wvTVcS', '8340', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIxLCJpZCI6MjEsImV4cCI6MTU1MjM1NDQ0MH0.10ZK9rDctdBEo30k0YxvUfL7JmXxh_XaQo7dQumn-Hw', 'guest', 'active', 'yes', 'yes', 'no', 'duUw6D8CDrs:APA91bHRRMpwUtVwVW_mML3G9FQklI6JiYZ4hh_jHwhoEteO5Fwd6auSFKif0x-G4khywm6s_zXCQ78kmxD-eCfwykTTEtj1nFThXBNzc2O_iYJQdpuN0CXgZoJ0hPK_CtvwcykgDQPg', 'ios', 0, 'on', NULL, NULL, '2019-03-12 06:03:29', '2019-03-12 06:03:29'),
(22, '9760284879', '$2y$10$oLdktHZ.JxY.oQvO1gBcpeZfQPDYbfNmXDidDTmmQ.YpIxbEtmeQm', '2074', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIyLCJpZCI6MjIsImV4cCI6NDc0MDk2ODY5MH0.8HVWVRTXZSpSfhPP0ThZXiq8mpNQPQtnNrWNKX7Hqdw', 'guest', 'active', 'yes', 'yes', 'no', 'f2I5Czcsivw:APA91bHbPTDOWgSDy3OLMf-rWy8ydO73mv97Af_C0pH2JL-QMag1qcc3cPLncmw3OAXkesdZXCVfIV-N5fSI9GakFacBfNAzLdmikiODPPwd60_2exbJ5Mi84EQ6vqS2W8zsQW3L3-uV', 'android', 0, 'on', NULL, NULL, '2019-03-15 16:34:44', '2019-03-15 16:34:44'),
(24, '8279842724', '$2y$10$51l1a0vKgMNNS0u7jcw3V.DIFTg4.r9GIIGfeMVozSYO1K9Iv3/j6', '4895', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjI0LCJpZCI6MjQsImV4cCI6NDczMDQ1NDYxN30.4CuqGFvaXWHp8QmmYUZQ3GDjVfGg2bq46i8AxIchZF8', 'guest', 'active', 'yes', 'yes', 'no', '', 'ios', 0, 'on', 'India', 91, '2019-03-27 00:12:09', '2019-03-27 00:12:09'),
(25, '8126208888', '$2y$10$fJ33NJl6WGNCRlxUJ93RmOvSB2Jgjf/vxnM47iYecdslj1D.3Rtky', '1451', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjI1LCJpZCI6MjUsImV4cCI6MTU1NzM5ODg1OH0.3tii63KfnsMKZxC15nPBm998-4UMVyd29eJNFjcAPjI', 'host', 'active', 'yes', 'yes', 'free', 'fKDLkwrMlfY:APA91bEXQwCGxAYoqWHKrnAhomFegwwp2R1EnSH7o6-cgbzhj5kIn-Zsn3M3aSujlniV1tN4bq1UbFtQfeTOB_fRYhv611weVUXDBBQsIun3E3-2whLlwWp1kmyz7xIQ_T-j2aFE1Fjt', 'android', 0, 'on', '', NULL, '2019-03-31 23:11:58', '2019-03-31 23:11:58'),
(26, '8755715552', '$2y$10$6fVXZPfkWgL5oiB0cLwntuJmyzFs0xzwsfF/k0BH5TvTY416xxgaG', '1354', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOm51bGwsImlkIjpudWxsLCJleHAiOjE1NTQzNjE5MTh9.iLs4YUGBgIyHLIhfaRcDVSDm6o7uI7vzh96q4z6XDIQ', 'host', 'active', 'no', 'yes', 'no', 'fYvxO0dwKJo:APA91bEe_IlS2k9hWcNMBcooFFSqECgbWx3yQAeyZCC9Rvd0rIAxncr2Z6w5mj8FTLkjhsfJ3S8fkY0n9vSUR6W-apljvVWKDoHm4z-bJOJyS4zkElAfkrvQtT0SPDrb5Hwt6KD1gHTw', 'android', 0, 'on', 'India', 91, '2019-04-03 23:11:58', '2019-04-03 23:11:58'),
(27, '8755715559', '$2y$10$tf0.XVt7AaT9JitbuDRFWeutx0n969s1y06Tz3794UC1QeybUmzbK', '8171', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjI3LCJpZCI6MjcsImV4cCI6MTU1OTY0MjgyM30.CqdOyo_AjrNeT6gT0N8LTyOP5-J4L6vjX1-PkDRcKUw', 'host', 'active', 'yes', 'yes', 'free', 'eqKF7HTt2Ao:APA91bEfCBMF2rPXxTuJpGmNU-0J8Mnz3NxkKubKHFC2_NRlMscUZby5z0o8kVWb-bua9XXwxYoIhw52b1EN6s0HhzGO7nMaK2qhAYBmibXS8WxSnZR-L925Gy2PlOWnPHSW-4eVK1YQ', 'android', 0, 'on', 'India', 91, '2019-04-03 23:12:16', '2019-04-03 23:12:16'),
(28, '7500784905', '$2y$10$YEYR45JduC3zB7CPnwmzkeqK9Jwecrm36.lGP8B5jWdsfr5iEuLJi', '5722', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOm51bGwsImlkIjpudWxsLCJleHAiOjE1NTQzNjI2NzB9.N3G8TWEzmqPszAoPzh-4V5AfuwGhXlcaRIQLOPpnOh0', 'guest', 'active', 'no', 'yes', 'no', 'fYvxO0dwKJo:APA91bEe_IlS2k9hWcNMBcooFFSqECgbWx3yQAeyZCC9Rvd0rIAxncr2Z6w5mj8FTLkjhsfJ3S8fkY0n9vSUR6W-apljvVWKDoHm4z-bJOJyS4zkElAfkrvQtT0SPDrb5Hwt6KD1gHTw', 'android', 0, 'on', 'India', 91, '2019-04-03 23:24:30', '2019-04-03 23:24:30'),
(30, '9717916332', '$2y$10$BypAhwjMQYpVo.uUN/tviu/ucqkG1bYFr3ZT5lNYYnse8Ho0lpazS', '4875', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMwLCJpZCI6MzAsImV4cCI6MTU1NDM3Njg0N30.ta-4jlIGkcit0pt_0N-8nKEZ_W3V8MyCy5c0STN0N1Q', 'guest', 'active', 'yes', 'yes', 'no', 'fYvxO0dwKJo:APA91bEe_IlS2k9hWcNMBcooFFSqECgbWx3yQAeyZCC9Rvd0rIAxncr2Z6w5mj8FTLkjhsfJ3S8fkY0n9vSUR6W-apljvVWKDoHm4z-bJOJyS4zkElAfkrvQtT0SPDrb5Hwt6KD1gHTw', 'android', 0, 'on', 'India', 91, '2019-04-03 23:32:11', '2019-04-03 23:32:11'),
(31, '3212022278', '$2y$10$GeSb/wf/XSuG/9c2PJdJ/e.KVHEb9j9EwmPFxc4bNeNPZBOZHEI9O', '3019', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMxLCJpZCI6MzEsImV4cCI6MTU1NDM2NDg0OH0.AizyRBP8cB5jluvcWmY8BWFKpduv-4xT9PP3T3rTpWU', 'host', 'active', 'yes', 'yes', 'free', 'fYvxO0dwKJo:APA91bEe_IlS2k9hWcNMBcooFFSqECgbWx3yQAeyZCC9Rvd0rIAxncr2Z6w5mj8FTLkjhsfJ3S8fkY0n9vSUR6W-apljvVWKDoHm4z-bJOJyS4zkElAfkrvQtT0SPDrb5Hwt6KD1gHTw', 'android', 0, 'on', 'United States', 1, '2019-04-04 00:00:09', '2019-04-04 00:00:09'),
(32, '2064572648', '$2y$10$3C26i/llIwtA90roqTbPO./07dKmfzlv6trKLPplA1Xm/8yXSWE6O', '7753', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMyLCJpZCI6MzIsImV4cCI6NDczOTQzNDQyNn0.bxjN9YANzcBLNf6BPdITla_gmVTVrZ8GDJ8N9UzOn-U', 'guest', 'active', 'no', 'yes', 'no', 'fYvxO0dwKJo:APA91bEe_IlS2k9hWcNMBcooFFSqECgbWx3yQAeyZCC9Rvd0rIAxncr2Z6w5mj8FTLkjhsfJ3S8fkY0n9vSUR6W-apljvVWKDoHm4z-bJOJyS4zkElAfkrvQtT0SPDrb5Hwt6KD1gHTw', 'android', 0, 'on', 'United States', 1, '2019-04-04 00:09:39', '2019-04-04 00:09:39'),
(33, '8755697926', '$2y$10$AbyqOLdiVj/HGOUJm.FioOKHuWmpzqmhOtYM9UmdjE09BO5WhbFWW', '3164', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjMzLCJpZCI6MzMsImV4cCI6MTU1NTU4NDI3Nn0.xH7OV3vYFCT67LV4YUmluwa97ZxPzOh3B5UanFpLG5I', 'host', 'active', 'yes', 'yes', 'no', 'dmuRqRmt4Q8:APA91bHY40b6boG9ZpWgpLVitFHaF0usgSXAofD-zCNQpAXoRH9Y-o-0FT_a-enPAUETjrtOA1NoliXEA23UZviSHVb7cETHsvupNqO0EozlfpEDHENUutSmlcb6jvG9Sg0eVIx06uue', 'android', 0, 'on', 'India', 91, '2019-04-17 23:56:23', '2019-04-17 23:56:23'),
(34, '3236950506', '$2y$10$Hmwe2uzRxSopG/J3Jvivw.EPFF07qBH2fWLlkPdBgyY6PANj0ShzC', '4480', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjM0LCJpZCI6MzQsImV4cCI6NDczOTQzNDU5NX0.6MnCsVFdTD1r-P61pse1UX47iOuxdVRZdfWRpkWoLDc', 'guest', 'active', 'yes', 'no', 'no', 'equf-NU4yZY:APA91bFlM9igEA02OeTmT9djYrjbhc8IjQhON48Yc_NYHqwjLyszm7wMXKk5tZ0ty6cO4m_cnQyjC70PP9G1xPnlyxgJ4szYHFZsfD-UH8WQGQpf-mVxieC9dH-mErslePErOWtnkgVX', 'android', 0, 'on', 'United States', 1, '2019-04-18 11:30:35', '2019-04-18 11:30:35'),
(35, '9627570278', '$2y$10$fJ33NJl6WGNCRlxUJ93RmOvSB2Jgjf/vxnM47iYecdslj1D.3Rtky', '1012', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjM1LCJpZCI6MzUsImV4cCI6NDcyOTkyNDUyNH0.kSm9ol1qwmbmdEhCxZ_o5IeM70EUTgY8UcLOpqndaMs', 'host', 'active', 'yes', 'no', 'yes', '', 'android', 0, 'on', 'India', 91, '2019-04-25 03:22:46', '2019-04-25 03:22:46'),
(36, '9897555555', '$2y$10$fCAALkTTsZyz6Jloc3v4KO6mNCVq3re/4fOWqU42pdThfPKUHtsCS', '3698', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjM2LCJpZCI6MzYsImV4cCI6MTU1NjY5ODk5OH0.kRGjOYtBJFkN6o7qsM9tqT_5l-_vjxmMwnoj2inxtXw', 'host', 'active', 'yes', 'yes', 'no', '', 'ios', 0, 'on', 'India', 91, '2019-04-25 03:52:09', '2019-04-25 03:52:09'),
(37, '9897444444', '$2y$10$eIZopBiLw8GMklPr9Hn3oOY.nRvs9xj5mVV9V6MAx/KB7OinYqA2S', '4821', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjM3LCJpZCI6MzcsImV4cCI6MTU1NjcxMDAwNX0.BoYGvYZZxAZv94sOnfw1UpdB1mhICG3cVCUZ_SvNe2E', 'host', 'active', 'yes', 'yes', 'free', 'fXhKEr_eSrQ:APA91bGSh2vGPkeZn-pik93w3V2-84sal9k4eugslg8NiWwOgg0oc7JNh2b15_t1--whR9z7tSFxI36npyhlgq8fToctobyVcokVn6c0VpYdBAUK8DiDYH0GVfT4RvvhSm9VpI3W_d9M', 'android', 0, 'on', 'India', 91, '2019-04-25 03:56:36', '2019-04-25 03:56:36'),
(38, '8126208713', '$2y$10$8tq5LV9mvfEf0PYtlQ.dJuBWN7lTaMNn8kcvL.HK3nkZKTj2C3qXe', '3079', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjM4LCJpZCI6MzgsImV4cCI6NDc0MDk2ODcwMn0.b0eOFxg1uvc5nEwSJHSbo0Uv3i-Sopav7EsqQZJrFsQ', 'host', 'active', 'yes', 'no', 'no', 'f2I5Czcsivw:APA91bHbPTDOWgSDy3OLMf-rWy8ydO73mv97Af_C0pH2JL-QMag1qcc3cPLncmw3OAXkesdZXCVfIV-N5fSI9GakFacBfNAzLdmikiODPPwd60_2exbJ5Mi84EQ6vqS2W8zsQW3L3-uV', 'android', 0, 'on', 'India', 91, '2019-05-01 02:42:29', '2020-01-01 22:05:52'),
(39, '9897333333', '$2y$10$o9dKkbWAiAf87IeTFMVbTunWgV2zpVY4UWGl34fpyUwK2oxo3zanW', '0603', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjM5LCJpZCI6MzksImV4cCI6MTU1NjcxMTA0MX0.d_S1lydQF_-jM4JEZl-cLPxcX2PHax6tihLWwEsSYOQ', 'host', 'active', 'yes', 'yes', 'free', 'cGd7wSDN1UA:APA91bFAbdvYC5lLad3NRF6Q0P12Sx3h89M6JZTN0KuKGWqtpYns2Ub0Owv9fl9uNnkuxI-j992rOuJH-RgoiIlqyjDSwbM__VPKp9JhDpaMOCxKD4jAcqWN-3yRBQ3u8LfXZTbYTP6L', 'android', 0, 'on', 'India', 91, '2019-05-01 03:43:34', '2019-05-01 03:43:34'),
(40, '9897222222', '$2y$10$ZuevnYLeWKgoEir5Gaq3qukhba0MAtRWXL7DZT3mWODBvQz8aDiTK', '6682', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQwLCJpZCI6NDAsImV4cCI6MTU1NzQ3NTU0OH0.qLUK4knWet6uAZ3rIffXY0YD-Z5b81Myx2yn_Pcgwkw', 'host', 'active', 'yes', 'yes', 'free', 'cRipe0vIKJI:APA91bFeycXnzTDMu6ha0T-6qJsUWIOctIF-teAlI4URS3X_OUcoN07Cy_sAZThvqICW1yXrFXl0HDZQkk0vdO6ZkywubeJL8CXcgJLdvdSMTyV7ND8oXh2GHBb1zEhaAOYaOFqxMaRp', 'android', 0, 'on', 'India', 91, '2019-05-09 15:58:30', '2019-05-09 15:58:30'),
(41, '9897111111', '$2y$10$SpfyQ/lPmm4fo7pfB7eiWu2EGeA6XBBEc7P64cEsbwSxbblD20F8G', '0951', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQxLCJpZCI6NDEsImV4cCI6MTU1NzQ4MzIwOX0.zDoPUXF_qQfT1KScqCodHnnYKqWpWFAWvvF27e-8Ja8', 'host', 'active', 'yes', 'yes', 'free', 'f9iAmO7Z5ns:APA91bG2LITZ02ZA4cs9rV5IeIZo3sgTOWMJv74VRtKO9XhFWwpdOLj77Z0GSEawsiXfAPMChhpUwEwNw4ETISgKFJ-I7yg2rvjWOcmZ7x6Q43WXaBz4J40UgWCZFJrlgDlz7I6_d0rH', 'android', 0, 'on', 'India', 91, '2019-05-10 14:33:26', '2019-05-10 14:33:26'),
(42, '9898989898', '$2y$10$fS1JGHUAP/EihXAlaeE1zegyYIkKPoaGr0i9C/eLG52n4gjBXHGDi', '3830', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQyLCJpZCI6NDIsImV4cCI6MTU2NDA2MTczOH0.5aqHV5uuDAOFMr8K16vFI3cwxN6Uq4b0-Rg0LXlAIQQ', 'host', 'active', 'yes', 'yes', 'free', 'cM68uW_UfPU:APA91bHHw9UXvCYS66HaTNjeQ4y74iSpErLq6SpeioviUcJgMab7wqxq60d-5ZCSwZ5ARd126XSMGcEYPxvqilnGcc5Px4ngkCiOM1_-8Z6_ePIGHSYd5_HUrk_PomBen_P2iShcROeh', 'android', 0, 'on', 'India', 91, '2019-05-10 15:16:52', '2019-05-10 15:16:52'),
(44, '3107092343', '$2y$10$uJdOw9yILoL0etVEf.1QqO/tqSuqd8hiyUldSAomTWzpzXudoaRhm', '1979', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQ0LCJpZCI6NDQsImV4cCI6NDczOTQzNDEyM30.G0x-g3EoUhDucyYGyXnF8ThTtODX_-DqiZP8EZ9YiiQ', 'host', 'active', 'yes', 'yes', 'yes', 'f2I5Czcsivw:APA91bHbPTDOWgSDy3OLMf-rWy8ydO73mv97Af_C0pH2JL-QMag1qcc3cPLncmw3OAXkesdZXCVfIV-N5fSI9GakFacBfNAzLdmikiODPPwd60_2exbJ5Mi84EQ6vqS2W8zsQW3L3-uV', 'ios', 21, 'on', 'United States', 1, '2019-05-17 21:40:30', '2019-05-17 21:40:30'),
(45, '3109510778', '$2y$10$dVtjLVUm.s.6TSFMU2gTves49WSv/lTNV4kP0CMlBLR4vlSds4Bci', '6057', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQ1LCJpZCI6NDUsImV4cCI6NDczOTQzMzYzNn0.3dlMtuor_F1bOVhbanpuNl6V4KK9TaSO00Fz_5Gldpw', 'host', 'active', 'yes', 'no', 'yes', 'f2I5Czcsivw:APA91bHbPTDOWgSDy3OLMf-rWy8ydO73mv97Af_C0pH2JL-QMag1qcc3cPLncmw3OAXkesdZXCVfIV-N5fSI9GakFacBfNAzLdmikiODPPwd60_2exbJ5Mi84EQ6vqS2W8zsQW3L3-uV', 'ios', 34, 'on', 'United States', 1, '2019-05-19 05:32:17', '2019-05-19 05:32:17'),
(47, '3109017784', '$2y$10$ZMMZV4Vo0lh8L.Npz.X7o..oR3jounGfKkjkq1NA7E5Xr/0i8gXxe', '2222', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQ3LCJpZCI6NDcsImV4cCI6NDczOTQzNDM3OX0.K1GoKZ4e5WzA3CaCUGJNKVrDjgD5-2M23jvaqIbsppg', 'guest', 'active', 'yes', 'yes', 'no', 'flkh9uiiOQQ:APA91bFwTCK1Qg3nWeLA4YNQi7DDkdmpY32KovLbI3_S1P2rYnR3LeI26FDM4MwaSILsYik_mIqbGJcoZX8l2ySQNOCk_fWCygEJ6ArVIJjheSPjSydSwIPeH-ghmciOxT9lmF5wn4ix', 'android', 0, 'on', 'United States', 1, '2019-05-19 07:37:42', '2019-05-19 07:37:42'),
(48, '9595959595', '$2y$10$oLdktHZ.JxY.oQvO1gBcpeZfQPDYbfNmXDidDTmmQ.YpIxbEtmeQm', '0030', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQ4LCJpZCI6NDgsImV4cCI6NDczMDcwNDY5MH0.G7gbB8_Snc58_LdArE8ySbmxua_lD661YpGnXnRjBaA', 'host', 'active', 'yes', 'yes', 'free', 'f4H4Unl3M1A:APA91bEZHwgwO8CkDejyRmekJ2WGNmaa-JLFKGuTDxSDHbr46y8bg_o32Cz5blevCnXtEmnBMjbK6zQGgf1_2ZHWR_HBrk2HcZU8pbCRmvXY562__hID2Snl_aluQ_xb1hkBihmgv96b', 'android', 0, 'on', 'India', 91, '2019-05-20 10:46:09', '2020-01-01 21:47:22'),
(49, '9898222222', '$2y$10$BzQFEC8Uo1RJ7dYbb2QZN.NgyiJxDGDBfPBktGJG9p5Oy1ETyxmaW', '4242', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQ5LCJpZCI6NDksImV4cCI6MTU1ODMzNTQ3OX0.cKlsmUAZvs5ZM4l2ZOKl061d-b8uTp2XAkKSx9nn4Vo', 'host', 'active', 'yes', 'yes', 'free', 'fNqyMeGxHeE:APA91bGdNsQwdDgbvuwve7m9Cb0KImnx6XxLHdA2x2uxJDha1cXHZpgVmI1eJsTwxUVEybhmazho59tvem4UNZPizu95bz5K775zAVYGhOZ4qpIlKLMCS2DEmZrQ3Qh-AQxzGqylCb1F', 'android', 0, 'on', 'India', 91, '2019-05-20 10:56:56', '2019-05-20 10:56:56'),
(50, '9898333333', '$2y$10$Ao2RScqw9kRBrjvgeT6xZujpdVNbGluoJu9TzK/nZlJoUF7BVZUMq', '6624', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjUwLCJpZCI6NTAsImV4cCI6MTU1ODMzNTk5M30.0F_DOpH0kU0_x-aWGUTSW0JEhSalt37iC5rmeTDVJS4', 'host', 'active', 'yes', 'yes', 'free', 'fNqyMeGxHeE:APA91bGdNsQwdDgbvuwve7m9Cb0KImnx6XxLHdA2x2uxJDha1cXHZpgVmI1eJsTwxUVEybhmazho59tvem4UNZPizu95bz5K775zAVYGhOZ4qpIlKLMCS2DEmZrQ3Qh-AQxzGqylCb1F', 'android', 0, 'on', 'India', 91, '2019-05-20 11:35:24', '2019-05-20 11:35:24'),
(51, '9898444444', '$2y$10$1sjLX1GwbvixW.t0mAg.EO9veBO6HbYbNJnUlQkZl4RCygPPgCAka', '0727', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjUxLCJpZCI6NTEsImV4cCI6MTU1ODMzODU5OX0.Q94fWOxNSr3ZSkJvIoCKrUVEBrLz5gIOvKEN5C6qBYo', 'host', 'active', 'yes', 'yes', 'free', 'fNqyMeGxHeE:APA91bGdNsQwdDgbvuwve7m9Cb0KImnx6XxLHdA2x2uxJDha1cXHZpgVmI1eJsTwxUVEybhmazho59tvem4UNZPizu95bz5K775zAVYGhOZ4qpIlKLMCS2DEmZrQ3Qh-AQxzGqylCb1F', 'android', 0, 'on', 'India', 91, '2019-05-20 12:18:51', '2019-12-18 21:39:43'),
(52, '9898555555', '$2y$10$4v6AE/W/ROBgV5KooGHgHOCjG1CmPK9r9UFjul0YaWGnES2FO0Rnu', '1714', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjUyLCJpZCI6NTIsImV4cCI6MTU1ODM0MDE0MH0.eh-GWHJxm10BdaKRKtutaKcHljYOrej3JyeFqa5B2Xg', 'host', 'active', 'yes', 'yes', 'free', 'fNqyMeGxHeE:APA91bGdNsQwdDgbvuwve7m9Cb0KImnx6XxLHdA2x2uxJDha1cXHZpgVmI1eJsTwxUVEybhmazho59tvem4UNZPizu95bz5K775zAVYGhOZ4qpIlKLMCS2DEmZrQ3Qh-AQxzGqylCb1F', 'android', 0, 'on', 'India', 91, '2019-05-20 12:44:55', '2019-12-17 21:50:13'),
(53, '9898666666', '$2y$10$H8NQnKdRvxSh9m8qMZSX7.zphN8QU1vmXJiPgPKQt51CQM04DuPkS', '5651', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjUzLCJpZCI6NTMsImV4cCI6MTU1ODM0MjU4NX0.nJiMKQ2PazKtivkU8isQIp3HsozkHYCh_OaFqYWUyJA', 'host', 'active', 'yes', 'yes', 'no', 'fNqyMeGxHeE:APA91bGdNsQwdDgbvuwve7m9Cb0KImnx6XxLHdA2x2uxJDha1cXHZpgVmI1eJsTwxUVEybhmazho59tvem4UNZPizu95bz5K775zAVYGhOZ4qpIlKLMCS2DEmZrQ3Qh-AQxzGqylCb1F', 'android', 0, 'on', 'India', 91, '2019-05-20 13:24:53', '2019-12-17 21:46:50'),
(54, '9898777777', '$2y$10$rGTZPt6H0IxgYtHf/PQ7VuoHa7szBA.4mxdjgzLIBh3kwhLQTeuP.', '4921', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjU0LCJpZCI6NTQsImV4cCI6MTU1ODM0NDE5OH0.j79P6_i-wM4a0C_j5pU8B80lAqsIPuCBKBNmn5Ce1g0', 'host', 'active', 'yes', 'yes', 'free', 'fNqyMeGxHeE:APA91bGdNsQwdDgbvuwve7m9Cb0KImnx6XxLHdA2x2uxJDha1cXHZpgVmI1eJsTwxUVEybhmazho59tvem4UNZPizu95bz5K775zAVYGhOZ4qpIlKLMCS2DEmZrQ3Qh-AQxzGqylCb1F', 'android', 0, 'on', 'India', 91, '2019-05-20 13:38:55', '2019-05-20 13:38:55'),
(57, '9000052200', '$2y$10$5ibDqmaO6ro4rzPLdcwSVOmu52wGYsyITj5uja1CRzf3WizDEDyJ.', '3108', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOm51bGwsImlkIjpudWxsLCJleHAiOjE1NTgzNDQ1Mzl9.weiln-SH4tEwTN9d7NreYREvuc5R0-GvFy09mHZ_QRg', 'guest', 'active', 'no', 'yes', 'no', '', '', 0, 'on', 'india', 91, '2019-05-20 13:58:59', '2019-05-20 13:58:59'),
(58, '9898989800', '$2y$10$BVv4WlZXbUDL6pcf1mlNbOyqxb56dc5s.HWokg..bU/7OGB0.ZKdG', '3495', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjU4LCJpZCI6NTgsImV4cCI6MTU1ODM0NDkxMH0.LvFj6pLDJ6BY6baUsC_9p4CIBpXMNxELSWtjV6z5sq4', 'host', 'active', 'yes', 'yes', 'free', 'fNqyMeGxHeE:APA91bGdNsQwdDgbvuwve7m9Cb0KImnx6XxLHdA2x2uxJDha1cXHZpgVmI1eJsTwxUVEybhmazho59tvem4UNZPizu95bz5K775zAVYGhOZ4qpIlKLMCS2DEmZrQ3Qh-AQxzGqylCb1F', 'android', 0, 'on', 'India', 91, '2019-05-20 14:00:03', '2019-05-20 14:00:03'),
(59, '9393939300', '$2y$10$qi.ed2VoaHiWCNqnFKEWnes4iZu6/L7ACS8noFXcsRLNTnS.0b2m6', '2847', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjU5LCJpZCI6NTksImV4cCI6MTU1ODM0NDg3OH0.0Wrg2MwWbDjlBLPzJ_qllIq6QGckSRRBYBLWEO8yQN8', 'guest', 'active', 'yes', 'yes', 'no', 'fNqyMeGxHeE:APA91bGdNsQwdDgbvuwve7m9Cb0KImnx6XxLHdA2x2uxJDha1cXHZpgVmI1eJsTwxUVEybhmazho59tvem4UNZPizu95bz5K775zAVYGhOZ4qpIlKLMCS2DEmZrQ3Qh-AQxzGqylCb1F', 'android', 0, 'on', 'India', 91, '2019-05-20 14:04:15', '2019-05-20 14:04:15'),
(60, '9595959500', '$2y$10$ocFV8LydW.R6LbpKJ.4eoOoc2.VncH5qOIY/z4vyngEgcCUYaPTXK', '2231', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjYwLCJpZCI6NjAsImV4cCI6MTU1ODU5NjE5NH0.l0D88JhO6twkUdF1u-t8vFrfyMT_tmKWHFtIx95V730', 'host', 'active', 'yes', 'yes', 'free', 'fYQVBF1rRUU:APA91bFu13fAYQ2YI25dCWgY_m16nJ4Vw_TrqF7MkqN7yp81AbplftjkGkTLzI2hrm3yxHVrKl6UfJSIv62UHOojRYieUCCPHBbKxVsgxUbJ2RyQcrCftqdjViv1Waw35xk_WBNp_5sh', 'android', 0, 'on', 'India', 91, '2019-05-21 14:34:56', '2019-05-21 14:34:56'),
(61, '9595950000', '$2y$10$pRn5elEiEuHvsm3pkihBS.zzs.37uHWvlHhScT934ns6S4K3Pbxae', '9268', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjYxLCJpZCI6NjEsImV4cCI6MTU2OTUwNzMwMn0.s_l28tJhorCLqVZmTqwy3yTR8wwkXmNvcy4XFLNhNoU', 'host', 'active', 'yes', 'yes', 'free', 'd_7TW8-OyFc:APA91bEzlCE-b8jOtcDRge5GgYjr0vMQv-xBfXldFPH0PaULupujFv_pjgwjVoWiFp_pR9rlROBJBawnUw00XYENZ91jVtU7JVW4M0YE4nS_r2aSkPvUZT_sNJzc17EshfffB3apbuba', 'android', 0, 'on', 'India', 91, '2019-05-21 14:49:58', '2019-12-17 21:46:36'),
(63, '9494949400', '$2y$10$ZUh1bwKLjhcWmkJwOjbFiOWOHx4f5BIWyvxfUfq.gXDNdzW0IwmBi', '3355', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjYzLCJpZCI6NjMsImV4cCI6MTU1ODU5NjMwN30.JQ9rac0gS-wK6Ah84WimA2ZIj3s3iPzcE7_-PkQ6Kz0', 'host', 'active', 'yes', 'yes', 'free', 'fYQVBF1rRUU:APA91bFu13fAYQ2YI25dCWgY_m16nJ4Vw_TrqF7MkqN7yp81AbplftjkGkTLzI2hrm3yxHVrKl6UfJSIv62UHOojRYieUCCPHBbKxVsgxUbJ2RyQcrCftqdjViv1Waw35xk_WBNp_5sh', 'android', 0, 'on', 'India', 91, '2019-05-23 11:54:44', '2019-05-23 11:54:44'),
(69, '8826810345', '$2y$10$BgYMaDsY2VEjFGjt5dBM0udgLlyMvySQ14INDY99q6zOVS3taLH7K', '8376', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjY5LCJpZCI6NjksImV4cCI6MTU1OTAzMDc0Mn0.5gRMx_enVDz_Utr5C3_zZejQrflRbZ8Kfl0U_zToWHg', 'host', 'active', 'yes', 'no', 'no', 'fub76E0ahdU:APA91bFzH3AGUq3nVOLIQ82KSZUovEQuQczxGa4NGkWskFCV9TzBPvRiic1j60E-p9aLkuU3BOmu6ou4_rFIw9DmJE_BT9RPJXVNoZuDcgyDoG1amjzd1c4UwP4THaiuUwn1Teh3y9pD', 'android', 0, 'on', 'India', 91, '2019-05-27 18:29:58', '2019-05-27 18:29:58'),
(71, '2094031507', '$2y$10$nxgnHDw8HOyjEOEzdVEqTuY87et.P0wDoLB3KTKxxvIEAHu0UM16q', '8820', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjcxLCJpZCI6NzEsImV4cCI6NDczOTQzNDA1NX0.-hnl7DLPMWyvVCA82yT7Q7XnnfR0JEnk_JxNthOix40', 'host', 'active', 'yes', 'no', 'no', 'f2I5Czcsivw:APA91bHbPTDOWgSDy3OLMf-rWy8ydO73mv97Af_C0pH2JL-QMag1qcc3cPLncmw3OAXkesdZXCVfIV-N5fSI9GakFacBfNAzLdmikiODPPwd60_2exbJ5Mi84EQ6vqS2W8zsQW3L3-uV', 'ios', 14, 'on', 'United States', 1, '2019-05-28 11:18:53', '2019-05-28 11:18:53'),
(85, '9696969696', '$2y$10$nv1Jvis1vrWPhRoWUWDBuui8ueU6FKj2FHfN7FRf7taa.TuBgDBiq', '5393', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjg1LCJpZCI6ODUsImV4cCI6MTU2OTQ5NzQ0Mn0.0St-GGpXv1nL1e-yc8Ha81xTOVAj23jZ2ruSM-DQgd4', 'guest', 'active', 'yes', 'yes', 'no', '', 'ios', 0, 'on', 'India', 91, '2019-05-29 16:15:55', '2019-05-29 16:15:55'),
(91, '9933993399', '$2y$10$IVjmf1QmvyH5Erb4NX871e/k9klCejHd264CVbWxip3FgiYYF/PfG', '9482', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOm51bGwsImlkIjpudWxsLCJleHAiOjE1NTkzMDIyNTd9.Xe76ibXM1awvr61If_uSc3af4VAQSzkmDXiFDUxo7Qc', 'host', 'active', 'no', 'yes', 'no', 'dIFT8bpp8YE:APA91bEMpXX3zjCxOJhYw1Ijqu46RntmDone6ZhUXJE1plmbFTU05DGC1zwP30c2msWvOjKolIWKLJ4HTiDPZNcuYrzApZgFizTrTtM7Z89w7L4xEgbcxLRIhn7uUUvKldziJ_24CdLg', 'ios', 0, 'on', 'India', 91, '2019-05-31 16:00:57', '2019-05-31 16:00:57'),
(92, '9922992299', '$2y$10$mGsx1Ngy0bfRj8ApPrgQLefn9a4Or5LVzOtaq2MyZchM635HpnpNK', '5311', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOm51bGwsImlkIjpudWxsLCJleHAiOjE1NTkzMDIzMzN9.zBEKuEleu-_3fRtrES2wRf60PFWg6SmKbng35PhZypE', 'host', 'active', 'no', 'yes', 'no', 'dIFT8bpp8YE:APA91bEMpXX3zjCxOJhYw1Ijqu46RntmDone6ZhUXJE1plmbFTU05DGC1zwP30c2msWvOjKolIWKLJ4HTiDPZNcuYrzApZgFizTrTtM7Z89w7L4xEgbcxLRIhn7uUUvKldziJ_24CdLg', 'ios', 0, 'on', 'India', 91, '2019-05-31 16:02:13', '2019-05-31 16:02:13'),
(97, '9911991199', '$2y$10$tf0.XVt7AaT9JitbuDRFWeutx0n969s1y06Tz3794UC1QeybUmzbK', '6345', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjk3LCJpZCI6OTcsImV4cCI6NDczMDM0OTQ2N30.PdZ4y8TFKGJ5Wjx5npMXNqjiffuKZZ4JXQAbuOmzHkY', 'host', 'active', 'yes', 'yes', 'no', 'c6GATN3SgYA:APA91bGNullZRYqmeBsWPzipNLiGMJl4JXF-anLcHThVV7ZnqsUlrkhH_yA7q9NuPqs91crO9JbsrMkWid-i_rDI307NKTnXRSCN7h3nMhOfwN9WA7DXyylvmLujL6TciJEWZzWW9Rx-', 'android', 0, 'on', 'India', 91, '2019-05-31 17:03:12', '2019-05-31 17:03:12'),
(98, '3107569826', '$2y$10$4ZE2K5IoNINnxBx7vhnHoOyYlrfJ064X5g6t3pfBmMdqMksKs4GVa', '3329', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjk4LCJpZCI6OTgsImV4cCI6MTU2MDIzMjk0M30.ACebgnWfwBSDV--T1Jc_2MjLt0kZLsJ84iZWRoh-arg', 'host', 'active', 'yes', 'yes', 'no', 'dLAVncJMFA8:APA91bE4aF8zrEJhSbJIM7Kw5FV0OygGSIA-6wtR1lWfGKjd1TTjX8TWLqrf9TsgFWlwwqIHTvK3GSyo4mXkc-d1wD-LY_xH3_tQ07jMJmJyO0XZI0j6mUlBARPmL1Gb73B9sIp6IqXB', 'ios', 0, 'on', 'United States', 1, '2019-06-11 10:29:43', '2019-06-11 10:29:43'),
(99, '3108773707', '$2y$10$zFzYC2sAemcXVYKCuUsiA.xTSelinnBsauCL6sGbwMndn7IC7N/2W', '8863', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjk5LCJpZCI6OTksImV4cCI6MTU2MDU2MTk4Nn0.IUxqzcyD8bSsNyeeY1PIGKc9V0w79W3kgijs84uOGzY', 'host', 'active', 'yes', 'yes', 'no', 'dLAVncJMFA8:APA91bE4aF8zrEJhSbJIM7Kw5FV0OygGSIA-6wtR1lWfGKjd1TTjX8TWLqrf9TsgFWlwwqIHTvK3GSyo4mXkc-d1wD-LY_xH3_tQ07jMJmJyO0XZI0j6mUlBARPmL1Gb73B9sIp6IqXB', 'ios', 0, 'on', 'United States', 1, '2019-06-15 01:05:42', '2019-06-15 01:05:42'),
(100, '8126208777', '$2y$10$dSj/GQSi3dnZTIaZ22q5zO5I4S07AXlzWHbxDjcOh.3.LC7Bx1RPG', '3334', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwMCwiaWQiOjEwMCwiZXhwIjoxNTY0MTI0MzE5fQ.7ntRT3dDs8mbDljSsW0TbyH3JAQdDXvnDFcVv9i8psY', 'guest', 'active', 'yes', 'yes', 'no', 'cr_VhVrG_O8:APA91bFPKM2QuTUglNsgnIe372rvtiDgwLiuo_jHQn2J03Fl9jkEo41AZBjQZixvOkEtjzEP_Fjnmx8Kp7KWlUUm4hijz71sNmRrB9nU45e66uZW3aLW1o0Eq5mi4x7d6SXceYeQNYCw', 'android', 0, 'on', 'India', 91, '2019-07-08 02:32:02', '2019-07-08 02:32:02'),
(101, '9869869869', '$2y$10$V6kGnYRAYW1fmxfl4TbuUuqs376rTxU/KVFZHqfNRq5YYuaDvItdW', '9218', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwMSwiaWQiOjEwMSwiZXhwIjoxNTY4MTg3MjMyfQ.00JZJZBB6uWfshI1BfCWpR9-xz91XTgSOktaqOWQmT8', 'host', 'active', 'yes', 'yes', 'free', 'c8DPwIjH0jY:APA91bHtH2QYc_tSqu_1iTEM0kJsjEqVDlgXr7uuJka4HnHrDgh6o0LDkbPj3m5RXdwhQ7BiAd2BZh-lxhLx9BhovPkjzJr2huC7NgWG3EfIugWCZT2qADbjG4z6qZ2dasR7S5VQy8AS', 'android', 0, 'on', 'India', 91, '2019-07-22 05:18:25', '2019-07-22 05:18:25'),
(102, '9999888899', '$2y$10$kx1MGFHzu4F4BW.TsWW.4ucpZPt4Zie1AYpnn2ZXxzS4jC9XB5qbO', '9561', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwMiwiaWQiOjEwMiwiZXhwIjoxNTY4NjI5NDAwfQ.QRT021zQ7zn_jnHgalgrFlSanu8QZppV0YQjZSs2_Dk', 'host', 'active', 'yes', 'yes', 'free', 'f_k0J6Rz8cU:APA91bHmCHcGAgycBoO-nzeo4m4v-mvXX2qLwno_v5910Pcow7G3MEcVxTlu2RaHuSeYIE56ummui-O9HxQXM6SCjgWvI1Mk-gSCQe6YiQhUBqXOzl4xrVDz7WQcK8DqFkfVlTFZBcZS', 'android', 0, 'on', 'India', 91, '2019-07-23 05:14:03', '2019-07-23 05:14:03'),
(103, '9252525252', '$2y$10$2VwBOscmzhIXRQfui804wuWCHiKPnJ.Vx9GwjKtKohrt1v5hKB.Zu', '8648', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOm51bGwsImlkIjpudWxsLCJleHAiOjE1NjUxNjAwODJ9.iatLxoMQLQgOHFcLn1VzcSU2m5zMUZIUFDLNgXuwpZQ', 'guest', 'active', 'yes', 'yes', 'no', '', 'android', 0, 'on', 'India', 91, '2019-08-06 22:41:22', '2019-08-06 22:41:22'),
(104, '9685794655', '$2y$10$l5xav0Hh/P1WrYAFUwRWyOgsZ370cKIVmbLJmiJsSgoJhn7XO8NIS', '1916', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOm51bGwsImlkIjpudWxsLCJleHAiOjE1NjUxNjAzNjR9.kkOQDCk51UPdE2ncn8bYzVltPudDkkJG9jA3vO_KU4E', 'guest', 'active', 'yes', 'yes', 'no', '', 'android', 0, 'on', 'India', 91, '2019-08-06 22:46:04', '2019-08-06 22:46:04'),
(105, '9846854545', '$2y$10$z6M93U7PRg1wy6eK7X7vpe38C86M8eAfoZN/7j2xSz4IlelKCWVke', '3250', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOm51bGwsImlkIjpudWxsLCJleHAiOjE1NjUxNjA0NzZ9.59MIiFPcYymaJtAPe7Unr5AYDrDahukOuuHyRFgwrd8', 'guest', 'active', 'yes', 'yes', 'no', '', 'android', 0, 'on', 'India', 91, '2019-08-06 22:47:56', '2019-08-06 22:47:56'),
(106, '9562356989', '$2y$10$rzvvzVOy6ejpjxBT3kECLOoAS/4AqQ6rW1Rb.UtofOexw7QL5xbJC', '3238', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOm51bGwsImlkIjpudWxsLCJleHAiOjE1NjUxNjA1MDd9.8VhPArk54XkKzzmXNGPSkK_aSTAJzZsnmGwVPkvwdwI', 'host', 'active', 'yes', 'yes', 'no', '', 'android', 0, 'on', 'India', 91, '2019-08-06 22:48:27', '2019-08-06 22:48:27'),
(107, '9393939393', '$2y$10$mgc.7loxtJ7g3oJ2aujGe.JKcNuAYFYB3jJdMaQCdP.ch1ll9/OfK', '1962', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOm51bGwsImlkIjpudWxsLCJleHAiOjE1NjU3ODI5MzZ9.Bm52qq1Z2k459D8F2lajmr8rhNWdXy_tgfkcdrKchjI', 'host', 'active', 'no', 'yes', 'no', '', 'ios', 0, 'on', 'India', 91, '2019-08-14 03:42:16', '2019-08-14 03:42:16'),
(108, '9494949494', '$2y$10$AEcBOHqgKF4uVjt4xqCjAO0o3XAUitfdsj.k486FSy9rodEflZ.cS', '4457', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOm51bGwsImlkIjpudWxsLCJleHAiOjE1NjU3ODMwMDJ9.pdXY-ZI1NdXkp1Uq9HANvSNdM_cb8twvTYQSbCm5v4Q', 'guest', 'active', 'no', 'yes', 'no', '', 'ios', 0, 'on', 'India', 91, '2019-08-14 03:43:22', '2019-08-14 03:43:22'),
(109, '9494949493', '$2y$10$Z6.JhswnEhDtNA.p7e.0w.Jd6dwGLqPOWDIz8ah27FQB7VLCegaQm', '1011', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwOSwiaWQiOjEwOSwiZXhwIjoxNTY4NzE5MTY2fQ.zbI_QZ8pxMrTtKX4kvumB7oshZNxlb3bzbXhwXvwIkk', 'host', 'active', 'yes', 'yes', 'free', 'f_k0J6Rz8cU:APA91bHmCHcGAgycBoO-nzeo4m4v-mvXX2qLwno_v5910Pcow7G3MEcVxTlu2RaHuSeYIE56ummui-O9HxQXM6SCjgWvI1Mk-gSCQe6YiQhUBqXOzl4xrVDz7WQcK8DqFkfVlTFZBcZS', 'android', 0, 'on', 'India', 91, '2019-09-17 02:51:28', '2019-09-26 05:17:09'),
(110, '9759677100', '$2y$10$iwcXfK3oSMHWZP6Ywup1XOLVN5VPw6V1iDhVvTKn0ssZu6A/sCPLi', '6531', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOm51bGwsImlkIjpudWxsLCJleHAiOjE1Njk0MTYyNTN9.zAkoAoQk8NhU9BtmPKJI8wMs4Nx3JH0-Ypb2J9wn_hg', 'guest', 'active', 'no', 'yes', 'no', '', 'android', 0, 'on', 'India', 91, '2019-09-25 04:57:33', '2019-09-25 04:57:33'),
(111, '8755697925', '$2y$10$8vVRMOqIWNJJmB4iEA07zuq8dVywFI2ihUEHlby/VSdy5gLfRmQS6', '1852', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjExMSwiaWQiOjExMSwiZXhwIjoxNTY5NDE3NDcyfQ.DB7N3FVKz-AsCuv_-XDyW5SmohX5HT_Lu1M5i0gb5Pg', 'host', 'active', 'yes', 'yes', 'no', 'd_7TW8-OyFc:APA91bEzlCE-b8jOtcDRge5GgYjr0vMQv-xBfXldFPH0PaULupujFv_pjgwjVoWiFp_pR9rlROBJBawnUw00XYENZ91jVtU7JVW4M0YE4nS_r2aSkPvUZT_sNJzc17EshfffB3apbuba', 'android', 0, 'on', 'India', 91, '2019-09-25 05:13:28', '2019-09-25 05:13:28'),
(112, '9876543210', '$2y$10$yDAoPreKfn90Mi4d6ch3w.d5VVxZJmOUzX6B.9yXRgikOz0op/h/C', '0190', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjExMiwiaWQiOjExMiwiZXhwIjo0NzMwNTMxOTE5fQ.T_i6Z0WsHoIAUeRLhQYLftoPxTu9x4G_lbN5UWwPD4E', 'host', 'active', 'yes', 'yes', 'yes', 'ebZP0pO54bE:APA91bHNABSP2bPutPs1J3HxtTxbJOX7BbPwdDnT2019T_7rCityQE11M3XAf5o6bE_IO_wmF61xQEgBGrtxzR3BJAIlDDY4N3nF8mhdwN1_5rRWLWR6tWFVwTVtLsMM---r83ScBRBZ', 'ios', 0, 'on', 'India', 91, '2019-11-26 23:28:16', '2019-12-17 21:49:01'),
(113, '9638527410', '$2y$10$m15F8xVwsvXTwkghtBiiGOHATPwzfMYjNadO0GbuBoojZ0f8NToHK', '4992', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjExMywiaWQiOjExMywiZXhwIjo0NzMxODAxNTQzfQ.-cKaXT9u1sNGOtsgQty7Jv6hfntniPJ-BOrmWF6D3xQ', 'guest', 'active', 'yes', 'yes', 'no', 'f2I5Czcsivw:APA91bHbPTDOWgSDy3OLMf-rWy8ydO73mv97Af_C0pH2JL-QMag1qcc3cPLncmw3OAXkesdZXCVfIV-N5fSI9GakFacBfNAzLdmikiODPPwd60_2exbJ5Mi84EQ6vqS2W8zsQW3L3-uV', 'android', 0, 'on', 'India', 91, '2019-12-03 02:30:34', '2019-12-03 02:30:34'),
(114, 'Kenbar4330@gmail.com', '$2y$10$3euduAoApMs03/K6lHJLpOKyYBsSp5tsSLBnZtDsTosrtDaAkbm0a', '', '', 'admin', 'active', 'no', 'yes', 'no', NULL, NULL, 0, 'on', NULL, NULL, NULL, '2020-01-20 01:58:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking_items`
--
ALTER TABLE `booking_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business_timings`
--
ALTER TABLE `business_timings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_business_timings_companies` (`company_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`,`user_id`,`category_id`),
  ADD KEY `fk_companies_categories1_idx` (`category_id`),
  ADD KEY `fk_companies_users1_idx` (`user_id`);

--
-- Indexes for table `company_category`
--
ALTER TABLE `company_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_company_category_companies` (`company_id`);

--
-- Indexes for table `company_images`
--
ALTER TABLE `company_images`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_company_images_companies1_idx` (`company_id`);

--
-- Indexes for table `company_items`
--
ALTER TABLE `company_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_company_items_companies` (`company_id`);

--
-- Indexes for table `company_items_options`
--
ALTER TABLE `company_items_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_subscriptions`
--
ALTER TABLE `company_subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`,`user_id`),
  ADD KEY `fk_customers_users1_idx` (`user_id`);

--
-- Indexes for table `customer_booking`
--
ALTER TABLE `customer_booking`
  ADD PRIMARY KEY (`id`,`customer_id`) USING BTREE,
  ADD KEY `fk_booking_customers1_idx` (`customer_id`),
  ADD KEY `fk_booking_slots1_idx` (`slot_id`);

--
-- Indexes for table `customer_delivery_address`
--
ALTER TABLE `customer_delivery_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_payment`
--
ALTER TABLE `customer_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_employees_categories1_idx` (`category_id`),
  ADD KEY `fk_employees_companies1_idx` (`company_id`),
  ADD KEY `fk_employees_users1_idx` (`user_id`);

--
-- Indexes for table `employee_images`
--
ALTER TABLE `employee_images`
  ADD PRIMARY KEY (`id`,`employee_id`),
  ADD KEY `fk_employee_images_employees1_idx` (`employee_id`);

--
-- Indexes for table `favorite`
--
ALTER TABLE `favorite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_favorite_customers` (`customer_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_details`
--
ALTER TABLE `payment_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phones`
--
ALTER TABLE `phones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`,`customer_id`) USING BTREE,
  ADD KEY `fk_ratings_customers1_idx` (`customer_id`);

--
-- Indexes for table `restaurant_arrangement`
--
ALTER TABLE `restaurant_arrangement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_restaurant_arrangement_companies` (`company_id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_schedule_employees1_idx` (`employee_id`) USING BTREE;

--
-- Indexes for table `slots`
--
ALTER TABLE `slots`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `fk_slots_schedule1_idx` (`schedule_id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking_items`
--
ALTER TABLE `booking_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=471;
--
-- AUTO_INCREMENT for table `business_timings`
--
ALTER TABLE `business_timings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `company_category`
--
ALTER TABLE `company_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `company_images`
--
ALTER TABLE `company_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `company_items`
--
ALTER TABLE `company_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `company_items_options`
--
ALTER TABLE `company_items_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `company_subscriptions`
--
ALTER TABLE `company_subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `customer_booking`
--
ALTER TABLE `customer_booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=501;
--
-- AUTO_INCREMENT for table `customer_delivery_address`
--
ALTER TABLE `customer_delivery_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `customer_payment`
--
ALTER TABLE `customer_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `employee_images`
--
ALTER TABLE `employee_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `favorite`
--
ALTER TABLE `favorite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=231;
--
-- AUTO_INCREMENT for table `payment_details`
--
ALTER TABLE `payment_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `phones`
--
ALTER TABLE `phones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `restaurant_arrangement`
--
ALTER TABLE `restaurant_arrangement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `slots`
--
ALTER TABLE `slots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `business_timings`
--
ALTER TABLE `business_timings`
  ADD CONSTRAINT `fk_business_timings_companies` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `companies`
--
ALTER TABLE `companies`
  ADD CONSTRAINT `fk_companies_categories1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_companies_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `company_category`
--
ALTER TABLE `company_category`
  ADD CONSTRAINT `fk_company_category_companies` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `company_images`
--
ALTER TABLE `company_images`
  ADD CONSTRAINT `fk_company_images_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `company_items`
--
ALTER TABLE `company_items`
  ADD CONSTRAINT `fk_company_items_companies` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `fk_customers_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `customer_booking`
--
ALTER TABLE `customer_booking`
  ADD CONSTRAINT `fk_booking_customers1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_booking_slots1` FOREIGN KEY (`slot_id`) REFERENCES `slots` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `fk_employees_categories1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_employees_companies1` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_employees_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `employee_images`
--
ALTER TABLE `employee_images`
  ADD CONSTRAINT `fk_employee_images_employees1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `favorite`
--
ALTER TABLE `favorite`
  ADD CONSTRAINT `fk_favorite_customers` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`);

--
-- Constraints for table `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `fk_ratings_customers1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `restaurant_arrangement`
--
ALTER TABLE `restaurant_arrangement`
  ADD CONSTRAINT `fk_restaurant_arrangement_companies` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`);

--
-- Constraints for table `schedules`
--
ALTER TABLE `schedules`
  ADD CONSTRAINT `fk_schedule_employees1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `slots`
--
ALTER TABLE `slots`
  ADD CONSTRAINT `fk_slots_schedule1` FOREIGN KEY (`schedule_id`) REFERENCES `schedules` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
