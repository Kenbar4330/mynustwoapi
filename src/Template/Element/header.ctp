<header class="main-header">
  <!-- Logo -->
  <a href="/kenbar/admin/adminDashboard" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>MT</b></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>MYNUSTWO</b> Admin</span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">    
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <span class="hidden-xs">Admin</span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-footer">
              <div class="pull-left">
                <a href="<?= $this->Url->build('/admin/logout', true); ?>" >Sign out</a>
              </div>
            </li>
            <li class="user-footer">
              <div class="pull-left">
                <a href="<?= $this->Url->build('/admin/updatePassword', true); ?>" >Update Password</a>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>
<?php
$pLink = $this->request->params['action'];
?>
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
      </div>
    </div>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      
      <li class="<?php if($pLink == 'adminDashboard'){ echo 'active';} ?> treeview">
        <a href="<?= $this->Url->build('/admin/adminDashboard', true); ?>">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      
      <li class="<?php if(in_array($pLink, ['allCategories', 'addCategory', 'updateCategory'])){ echo 'active';} ?> treeview">
        <a href="#">
          <i class="fa fa-share"></i>
          <span>Categories</span>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if(in_array($pLink, ['allCategories', 'updateCategory'])){ echo 'active';} ?>">
            <a href="<?= $this->Url->build('/admin/allCategories', true); ?>"><i class="fa fa-fw fa-list"></i> All Categories</a>
          </li>
          <li class="<?php if($pLink == 'addCategory'){ echo 'active';} ?>">
            <a href="<?= $this->Url->build('/admin/addCategory', true); ?>"><i class="fa fa-edit"></i> Add New Category</a>
          </li>
        </ul>
      </li>
      
      <li class="<?php if(in_array($pLink, ['hosts', 'guests', 'viewHostDetails', 'updateHostDetails', 'viewHostEmployees', 'updateGuestDetails'])){ echo 'active';} ?> treeview">
        <a href="#">
          <i class="fa fa-users"></i>
          <span>All Users</span>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if(in_array($pLink, ['hosts', 'viewHostDetails', 'updateHostDetails', 'viewHostEmployees'])){ echo 'active';} ?>">
            <a href="<?= $this->Url->build('/admin/hosts', true); ?>"><i class="fa fa-briefcase"></i>Hosts Listing</a>
          </li>
          <li class="<?php if(in_array($pLink, ['guests', 'updateGuestDetails'])){ echo 'active';} ?>">
            <a href="<?= $this->Url->build('/admin/guests', true); ?>"><i class="fa fa-user"></i>Guests Listing</a>
          </li>
        </ul>
      </li>
      
      <li class="<?php if($pLink == 'manageSubscription'){ echo 'active';} ?> treeview">
        <a href="<?= $this->Url->build('/admin/manageSubscription', true); ?>">
          <i class="fa fa-usd"></i> <span>Manage Subscription</span>
        </a>
      </li>

      <li class="<?php if($pLink == 'paymentDetails'){ echo 'active';} ?> treeview">
        <a href="<?= $this->Url->build('/admin/paymentDetails', true); ?>">
          <i class="far fa-credit-card"></i> <span>Payment Details</span>
        </a>
      </li>

      <li class="<?php if($pLink == 'sendMessage'){ echo 'active';} ?> treeview">
        <a href="<?= $this->Url->build('/admin/sendMessage', true); ?>">
          <i class="fa fa-envelope"></i> <span>Send Message</span>
        </a>
      </li>

      <li class="<?php if($pLink == 'businessRating'){ echo 'active';} ?> treeview">
        <a href="<?= $this->Url->build('/admin/businessRating', true); ?>">
          <i class="fa fa-envelope"></i> <span>Business Ratings</span>
        </a>
      </li>
	  
	  <li class="<?php if($pLink == 'companyCategoryList'){ echo 'active';} ?> treeview">
        <a href="<?= $this->Url->build('/admin/companyCategoryList', true); ?>">
          <i class="fa fa-list-alt"></i> <span>Company Menu Categories</span>
        </a>
      </li>

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>