<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Users Model
 *
 * @property |\Cake\ORM\Association\HasMany $Companies
 * @property |\Cake\ORM\Association\HasMany $Customers
 * @property |\Cake\ORM\Association\HasMany $Employees
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasOne('Companies', [
            'foreignKey' => 'user_id',
            'dependent'  => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasOne('Customers', [
            'foreignKey' => 'user_id',
            'dependent'  => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasOne('Employees', [
            'foreignKey' => 'user_id',
            'dependent'  => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('PaymentDetails', [
            'foreignKey' => 'user_id',
            'dependent'  => true,
            'cascadeCallbacks' => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 16)
            ->allowEmpty('phone');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->allowEmpty('password');

        $validator
            ->scalar('otp')
            ->maxLength('otp', 4)
            ->allowEmpty('otp');

        $validator
            ->scalar('access_token')
            ->allowEmpty('access_token', 'create');  

        $validator
            ->scalar('type')
            ->allowEmpty('type');

        $validator
            ->scalar('status')
            ->allowEmpty('status');

        return $validator;
    }

     public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['phone']));

        return $rules;
    }

     public function validationPassword(Validator $validator)
    {
        $validator
                ->add('old_password','custom',[
                    'rule' => function($value, $context){
                        $user = $this->get($context['data']['id']);
                        //pr($user);die;
                        if($user)
                        {
                            if((new DefaultPasswordHasher)->check($value, $user->password))
                            {
                                return true;
                            }
                        }
                        return false;
                    },
                    'message' => 'Your old password does not match the entered password!',
                ])
                ->notEmpty('old_password');

        $validator
                ->add('new_password',[
                    'length' => [
                        'rule' => ['minLength',6],
                        'message' => 'Please enter atleast 6 characters in password your password.'
                    ]
                ])
                ->add('new_password',[
                    'match' => [
                        'rule' => ['compareWith','confirm_password'],
                        'message' => 'Sorry! Password dose not match. Please try again!'
                    ]
                ])
                ->notEmpty('new_password');
        
        $validator
                ->add('confirm_password',[
                    'length' => [
                        'rule' => ['minLength',6],
                        'message' => 'Please enter atleast 6 characters in password your password.'
                    ]
                ])
                ->add('confirm_password',[
                    'match' => [
                        'rule' => ['compareWith','new_password'],
                        'message' => 'Sorry! Password dose not match. Please try again!'
                    ]
                ])
                ->notEmpty('confirm_password');
        return $validator;
    }
}
