<!-- Content Header (Page header) -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
<section class="content-header">
    <?php echo $this->Flash->render(); ?>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Companies Menu Categories Listing</h3>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<?php
//pr($compData);die;
?>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-body">
					<table id="categoryLists" class="table table-striped row-border">
						<thead>
							<tr>
								<th>id</th>
								<th>Company Name</th>
								<th>Menu Categories</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($compData as $company): ?>
							<tr>
								<td><?= $this->Number->format($company['id']) ?></td>
								<td><?= h($company['company_name']) ?></td>
								<td>
									<?php foreach($company['company_category'] as $menuCat): ?>
									<div class="menuCls<?= h($menuCat['id']) ?>"><?= h($menuCat['name']) ?><i class="fa fa-trash delMenuCat" style="color:red;padding-left:5px;cursor:pointer;" data-id="<?= h($menuCat['id']) ?>" data-compid="<?= h($menuCat['company_id']) ?>"></i></div>
									<?php endforeach; ?>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</section><!-- /.content -->

<script type="text/javascript">
$(function () {
	//Delete menu category
	$(document).on('click', '.delMenuCat', function (e) {
		e.preventDefault();
		var id = $(this).data('id');
		var compId = $(this).data('compid');
		if (confirm('Are you sure you want to delete this?')) {
			$.ajax({
				type: "POST",
				url: 'deleteMenuCat',
				data: {id: id, compId: compId},
				success: function (jsonResponse) {
					var res = JSON.parse(jsonResponse);
					if (res.status == 'success') {
						$(".menuCls"+id).html('<span style="color:green;">'+res.message+'</span>');
						$(".menuCls"+id).hide('blind', {}, 5000);
					} else {
						$(".menuCls"+id).html('<span style="color:red;">'+res.message+'</span>');
						$(".menuCls"+id).hide('blind', {}, 5000);
					}
				}
			});
		}
	});
});
</script>
