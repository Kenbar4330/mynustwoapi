<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-block alert-success">
    <button type="button" class="close" data-dismiss="alert" onclick="this.classList.add('hidden')">
        <i class="ace-icon fa fa-times"></i>
    </button>
    <p>
       <?= $message ?> &nbsp;&nbsp;<a href="<?php echo WEB_BASE_URL; ?>" >Login here</a>
    </p>
</div>
