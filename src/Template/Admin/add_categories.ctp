<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
   <!-- Content Header (Page header) -->
        <?php echo $this->Flash->render(); ?>    
        <section class="content-header">
        <ul class="side-nav">
          <li><?= $this->Html->link(__('List of Categories'), ['action' => 'allCategories']) ?></li>
      </ul>
        </section>

        <!-- Main content -->
      <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <!-- form start -->
                <?= $this->Form->create('Admin') ?>
                  <div class="box-body">

                     <div class="form-group">
                      <label>Category Name</label>
                      <input type="text" name="cat_name" class="form-control" placeholder="Enter Category Name" required>
                    </div>

                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <?= $this->Form->button(__('Add'),['class'=>'btn btn-primary btn-mg']) ?>
                    <?= $this->Form->end() ?>
                  </div>
                  
                </form>
              </div><!-- /.box -->
            </div><!--/.col (left) -->  
          </div>   <!-- /.row -->

      </section><!-- /.content -->

