<section class="content-header">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3><?= $user['company']['company_name']; ?>: Employees and Rooms</h3>
            </div>
            <div class="pull-right">
                <button class="btn btn-primary btn-xs" onclick="window.history.back();">Back</button>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-body">
          <table id="categoryLists" class="table table-bordered table-striped">
            <thead>
              <tr>
                  <th scope="col">Id</th>
                  <th scope="col">First Name</th>
                  <th scope="col">Last Name</th>
                  <th scope="col">About</th>
                  <th scope="col">Phone</th>
                  <th scope="col">Email</th>
                  <th scope="col">Type</th>
                  <th scope="col">Created</th>
		  <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php if(!empty($user['company'])): ?>
                  <?php foreach ($user['company']['employees'] as  $datas): ?>
                  <tr>
                      <td><?= $this->Number->format($datas['id']) ?></td>
                      <td><?= h($datas['first_name']) ?></td>
                      <td><?= h($datas['last_name']) ?></td>
                      <td><?= h($datas['about']) ?></td>
                      <td><?= h($datas['user']['phone']) ?></td>
                      <td><?= h($datas['email']) ?></td>
                      <td><?php if($datas['type'] == 'other') { echo 'Room'; } else { echo ucfirst($datas['type']); } ?></td>
                      <td><?= h($datas['created']->format('d M Y')) ?></td>
  		      <td class="actions">
                                  <?= $this->Html->link(
                                       __('Delete'),
                                      ['action' => 'deleteEmployee', $datas['id'], $datas['user_id'], $datas['custom'],$id],
                                      ['escape' => false, 'title' => __('Delete'), 'class' => 'btn btn-danger btn-xs',
                                      'confirm' => __('Are you sure you want to Delete ?')]
                                  ) ?>
                        </td>  
                  </tr>
                  <?php endforeach; ?>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
