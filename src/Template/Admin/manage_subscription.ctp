<section class="content-header">
    <?php echo $this->Flash->render(); ?>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Manage Subscription</h3>
            </div>
        </div>
    </div>
</section>
      
<section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box">
          <!-- form start -->
          <?= $this->Form->create('Admin') ?>
            <div class="box-body">
              <div class="form-group" id="staticParent">
                <label>Amount</label>
                <input type="decimal" id="child" min= "1" name="amount" class="form-control" value="<?= $subplan[0]['amount'] ?>" placeholder="Enter Amount" required>
              </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
              <?php if(!empty($subplan[0]['amount'])) {
                  $buttonName = 'Update';
              } else {
                  $buttonName = 'Add';
              }
              ?>
              <?= $this->Form->button(__($buttonName),['class'=>'btn btn-primary btn-mg']) ?>
              <?= $this->Form->end() ?>
            </div>
        </div><!-- /.box -->
      </div><!--/.col (left) -->  
    </div>   <!-- /.row -->
</section><!-- /.content -->

<script type="text/javascript">
$(function() {
    setTimeout(function() {
      $(".alert-success").hide('blind', {}, 500)
    }, 5000);
});
</script>

