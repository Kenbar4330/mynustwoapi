<section class="content-header">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Update Guest Details</h3>
            </div>
            <div class="pull-right">
                <button class="btn btn-primary btn-xs" onclick="window.history.back();">Back</button>
            </div>
        </div>
    </div>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <?= $this->Form->create($user, ['id'=>'AddCatForm']) ?>
        <div class="box-body">
          <div class="form-group">
            <label>Guest Name:</label>
             <?= $this->Form->control('customer.name', ['class' => 'form-control', 'placeholder' => 'Guest Name', 'label' => false ]); ?> 
          </div>

          <div class="form-group">
            <label>Email:</label>
             <?= $this->Form->control('customer.email', ['class' => 'form-control', 'placeholder' => 'email', 'label' => false ]); ?> 
          </div>

        </div>
        <div class="box-footer">
          <?= $this->Form->button(__('Update'),['class'=>'btn btn-primary btn-sm']); ?>
        </div>
        <?= $this->Form->end() ?>
      </div>
    </div>
  </div>
</section>

    