<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$Description = 'Admin Panel';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $Description ?>:
        <?= $this->fetch('title') ?>
    </title>

    <?= $this->Html->css(array('bootstrap/css/bootstrap.min.css','//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css','https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css','dist/css/AdminLTE.min.css', 'dist/css/skins/_all-skins.min.css','plugins/iCheck/flat/blue.css','plugins/morris/morris.css','plugins/jvectormap/jquery-jvectormap-1.2.2.css','plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css', 'plugins/datatables/dataTables.bootstrap.css','custom.css')); ?>
	
    <?= $this->Html->script(array('plugins/jQuery/jQuery-2.1.4.min.js')); ?>
    <?= $this->Html->meta('icon') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?= $this->element('header') ?>

    <div class="content-wrapper">
       
        <?= $this->fetch('content') ?>
                  
    </div>
     
<?= $this->element('footer') ?>

</div>

<?= $this->Html->script(array('https://code.jquery.com/ui/1.11.4/jquery-ui.min.js', 'jquery.validate.min.js','bootstrap/js/bootstrap.min.js','plugins/select2/select2.full.min.js','plugins/input-mask/jquery.inputmask.js','plugins/input-mask/jquery.inputmask.date.extensions.js','plugins/input-mask/jquery.inputmask.extensions.js','plugins/daterangepicker/moment.js','plugins/sparkline/jquery.sparkline.min.js', 'plugins/jvectormap/jquery-jvectormap-1.2.2.min.js', 'plugins/jvectormap/jquery-jvectormap-world-mill-en.js','plugins/knob/jquery.knob.js','plugins/daterangepicker/daterangepicker.js','plugins/slimScroll/jquery.slimscroll.min.js','plugins/fastclick/fastclick.min.js','dist/js/app.min.js','dist/js/pages/dashboard.js','dist/js/demo.js','https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js','plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js','plugins/datatables/jquery.dataTables.min.js', 'plugins/datatables/dataTables.bootstrap.min.js')); ?>
<script>
  $(function () {
	$("#categoryLists").DataTable();
  });
</script>
<script>
/*$(document).ready(function() {
    $("#AddCatForm").validate({
        rules: {
            name: { 
                required: true
            }
        },
        messages: {
          name: { required: "Please enter category name."}
        }
    });
});*/
</script>
</body>
</html>
