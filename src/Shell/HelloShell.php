<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;

class HelloShell extends Shell
{
    //CRON: Send notification to guest when waiting status is pending & bussiness setting is close
    public function deleteAfterClose() {

    	   $this->bookingObj  = TableRegistry::get('Booking');
           $bookingDetails = $this->bookingObj->find()->where(['Booking.status'=>'pending','Booking.type' =>'wait'])->contain(['Companies'=> function ($q) { 
                    return $q->where(['Companies.wait_status' => "close"]);
            }])->order(['Booking.created'=>'ASC'])->toArray();
           //echo date('H:i:s'); die;
           $waitClose = array();
           foreach ($bookingDetails as $key => $value) {
               $waitClose[$key]['wait_id'] = $value['id'];
               $waitClose[$key]['company_id'] = $value['company_id'];
               $waitClose[$key]['customer_id'] = $value['customer_id'];
               $waitClose[$key]['company_name'] = $value['company']['company_name'];
               $waitClose[$key]['owner_name'] = $value['company']['first_name']." ".$value['company']['last_name'];
               $waitClose[$key]['created'] = $value['created']->format('Y-m-d');

               $customer[$key] = $this->customerObj->findById($value['customer_id'])->contain(['Users'=>['fields'=>['Users.id', 'Users.device_type', 'Users.device_token','Users.noti_alert']]])->select(['user_id'])->enableHydration(false)->first();

               $entity = $this->bookingObj->find()->where(['id'=>$value['id']])->first();
               $timing = $this->getBusinessTime($value['company_id'], date('D',strtotime(date('Y-m-d H:i:s')))."day");
               if(!empty($timing)) {
                    $result = date('h:i:s') > $timing['end_time']->format('h:i:s');
               } else {
                    $result = date('Y-m-d') > $value['created']->format('Y-m-d');
               }
               //delete pending status if business setting is closed
               if($result) {
                   $result = $this->bookingObj->delete($entity);
                   $device_type =  $customer[$key]['user']['device_type'];
                   $device_token = $customer[$key]['user']['device_token'];
                    //Send push notification
                   if(!empty($device_token) && !empty($device_type)) {
                        $data = [];
                        $data['message'] = $waitClose[$key]['owner_name'].': Waiting deleted ';
                        $data['type']    = 'delete_waiting';
                        //Get unread notification count(badge_count)
                        $badgeCount = $this->getBadgeCount($customer[$key]['user_id']);
                        //Send notification 
                        if($customer[$key]['user']['noti_alert'] == "on") {   
                            $this->sendPushNotification($device_type, $device_token, $data['message'], $data['type'], $badgeCount,'no');
                        }
                   }
               }
           }
          //pr($waitClose); die;
    }

    public function getBusinessTime($bussId, $day)
    {
        if(!empty($bussId) && !empty($day)) {
            $timing = '';
            $result = $this->businessTimeObj->find('all')->where(['company_id'=>$bussId, 'day'=>$day])->select(['id', 'company_id', 'type', 'day', 'start_time', 'end_time'])->enableHydration(false)->first();
            if(!empty($result)) {
                $timing = $result;
            }
            return $timing;
        }
    }
}

