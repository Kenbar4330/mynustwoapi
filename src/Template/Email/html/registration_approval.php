<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>my Bridge</title>
</head>
<style>
  * {padding: 0px; margin:0px;}
  @import url(http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900);
</style>
<body>
  <table cellpadding="0" cellspacing="0" border="0" width="660" style="background-color:#ffffff; font-family: 'Roboto', sans-serif; border:1px solid #ccc">
    <tr>
      <td style="border-bottom:2px solid #ccc">
          <table cellpadding="0" cellspacing="0" border="0" width="660" style="background-color:#507ca1;">
            <tr>
              <td width="90%">
                <a href="http://soladaves.com" title="MyBridge Logo"><img src="http://soladaves.com/img/logoEm.png"></a>
              </td>
              <td width="10%">
                <img src="http://soladaves.com/img/user.png">
              </td>
            </tr>
          </table>
      </td>
    </tr>
    <tr>
      <td>
          <table style="width:100%; padding:85px 25px">
            <tr>
              <td style="width:100%"><h1 style="color:#36668e; font-weight:normal; font-size:20px; padding:15px 0">Final Step...</h1></td>
            </tr>
            <tr>
               <td style="width:100%">Confirm your email address to complete your Mybridge account. Its easy - just click on the button below</p></td>
            </tr>
            <tr>
              <td>
                <a href="{rootlink}" target="_blank"><input type="submit" value="Confirm now" style=" background:#507ca1; color:#fff; padding:10px 15px; font-size:15px; border:0px; margin:15px 0; cursor: pointer;"></a>
                <a href="{noConfirm}" style="font-size:15px; color:#507ca1; text-decoration:none">Not my account</a>
              </td>
            </tr>
          </table>
      </td>
    </tr>
    <tr>
      <td style="background-color:#36668e;">
        <img src="http://soladaves.com/img/emIcon.png" style="float:left; margin-right:18px;">
        <p style="float:left; padding-top:18px; font-size:15px; color:#fff"> myBridge Corporation, 1054 Tryon Street, Charlotte NC 28202</p>
        <div style="clear:both"></div>
      </td>
    </tr>
  </table>
</body>
</html>
