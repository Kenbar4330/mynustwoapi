<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$Description = 'Admin Panel';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $Description ?>:
        <?= $this->fetch('title') ?>
    </title>

    <?= $this->Html->css(array('bootstrap/css/bootstrap.min.css','https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css','dist/css/AdminLTE.min.css', 'dist/css/skins/_all-skins.min.css','plugins/iCheck/flat/blue.css','plugins/morris/morris.css','plugins/jvectormap/jquery-jvectormap-1.2.2.css',
        'plugins/daterangepicker/daterangepicker-bs3.css', 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')); ?>
    <?= $this->Html->meta('icon') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body class="hold-transition skin-blue sidebar-mini" style="background: grey;">
 <div class="login-box">
      <div class="login-box-body">
      <?php echo $this->Flash->render(); ?>  
        <?= $this->Form->create('user', ['class' => 'form-horizontal', 'id' => 'forgot-password-form']) ?>
          <div class="form-group has-feedback">
           <?= $this->Form->control('email', ['class' => 'form-control', 'placeholder' => 'Email', 'label' => false]); ?>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>

          <div class="row">
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary">Reset Password</button>
            </div><!-- /.col -->
          </div>
        </form>
      <?php echo $this->Form->end(); ?>
      </div><!-- /.login-box-body -->
</div><!-- /.login-box -->
<script>
    $("#forgot-password-form").validate();
</script>


  <?= $this->Html->script(array('plugins/jQuery/jQuery-2.1.4.min.js','bootstrap/js/bootstrap.min.js','https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js','plugins/morris/morris.min.js', 'https://code.jquery.com/ui/1.11.4/jquery-ui.min.js', 'plugins/sparkline/jquery.sparkline.min.js', 'plugins/jvectormap/jquery-jvectormap-1.2.2.min.js', 'plugins/jvectormap/jquery-jvectormap-world-mill-en.js','plugins/knob/jquery.knob.js','https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js','plugins/daterangepicker/daterangepicker.js','plugins/slimScroll/jquery.slimscroll.min.js', 'plugins/fastclick/fastclick.min.js','dist/js/app.min.js','dist/js/pages/dashboard.js','dist/js/demo.js','plugins/ckeditor/pluginsbootstrap3-wysihtml5.all.min.js','plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')); ?>

</body>
</html>