<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\ORM\TableRegistry;
use Cake\ORM\Table;
use Cake\Routing\Router;
use Cake\Filesystem\File;
use Cake\Mailer\Email;

class AdminController extends AppController
{
    private $users;
    private $categoriesObj;
    private $companyObj;
    private $customerObj;
    private $subplanObj;
    private $payDetailObj;
    private $employeeObj;
    private $ratingObj;
	private $compCatObj;

    public $paginate = [
        'limit' => 15
    ];

    public function initialize()
    {
        parent::initialize(); 
        $this->loadComponent('Paginator');    
        
        $this->Auth->allow(['login' ,'forgotPassword' ,'resetPassword']);

        $this->users         = TableRegistry::get('Users');
        $this->categoriesObj = TableRegistry::get('categories');
        $this->companyObj    = TableRegistry::get('Companies');
        $this->customerObj   = TableRegistry::get('Customers');
        $this->subplanObj    = TableRegistry::get('Subscriptions');
        $this->payDetailObj  = TableRegistry::get('PaymentDetails');
        $this->employeeObj   = TableRegistry::get('Employees');
        $this->ratingObj     = TableRegistry::get('Ratings');
		$this->compCatObj = TableRegistry::get('CompanyCategory');
    }
	
	public function beforeFilter(Event $event) {
        $this->viewBuilder()->setLayout('default');
    }

    //Admin dashboard
    public function index()
    {
        $this->redirect(['action' => 'adminDashboard']);
    }

    //Login
    public function login() 
    {
        $this->viewBuilder()->setLayout('');
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Invalid username or password, try again.'));
        }
    }

    //Logout
    public function logout() 
    {
        $this->Flash->error('You have loged out successfully.');
        return $this->redirect($this->Auth->logout());
    }

    //Get Unique id
    public function getUniqId() 
    {
        $uid = substr(md5(uniqid(mt_rand(), true)), 0, 100);
        return $uid;
    }

      //Forgot Password
     public function forgotPassword() {

        $this->viewBuilder()->setLayout('');
        $user = $this->users->newEntity();
        if ($this->request->is('post')) {

            //print_r($this->request->data); die;
            $emailAddress = isset($this->request->data['email']) ? $this->request->data['email'] : '';
            if (empty($emailAddress)) {
                $this->Flash->error(__('Please enter your registered email address.'));
            } else {
                $query = $this->users->find('all', [
                    'conditions' => ['phone' => $emailAddress, 'type' => 'admin']
                ]);
                $user = $query->first();
                if (empty($user)) {
                    $this->Flash->error(__('No user registered with this email address.'));
                } else {
                    $data = [];
                  //  echo $this->getUniqId(); die;
                    $data['access_token'] = $this->getUniqId();
                    $user->access_token = $data['access_token'];
                    //$user = $this->users->patchEntity($user, $data);
                    if ($this->users->save($user)) {
                        $aLink = "http://54.210.15.234/kenbar/admin/resetPassword/".$data['access_token'];
                        $emailBody  = "<div>";
                        $emailBody .= "Dear Admin";
                        $emailBody .= ",<br><br>";
                        $emailBody .= "You recently requested a new password to sign in to Admin Panel. To create a new password, click on the below link.";
                        $emailBody .= "<br><br>";
                        $emailBody .= " <a href=".$aLink."> Click Here </a>";
                        $emailBody .= "<br><br>";
                        $emailBody .= "Thankyou";
                        $emailBody .= "<br>";
                        $emailBody .= 'MynusTwo Team';
                        $emailBody .= "</div>";
                
                        $emailSubject = 'Admin : Recover/Reset Password';
                        //echo $emailAddress; die;
                        //$this->email->send();
                        $this->sendMail($emailAddress, $emailSubject, $emailBody);
                        $this->Flash->success(__('Please check your email. Reset Password link has been sent to your email address.'));
                        return $this->redirect(['action' => 'login']);
                    } else {
                        $this->Flash->error(__('Something went wrong, Please try again.'));
                    }
                }
            }
        }
        $title = 'Forgot Password';
        $this->set(compact('user','title'));
    }

    //Reset Password
    public function resetPassword($access_token = null) 
    {
        $this->viewBuilder()->setLayout('');
        $query = $this->users->find('all', [
            'conditions' => ['access_token' => $access_token],
            'fields' => ['id']
        ]);
        $userData = $query->first();
        if ($this->request->is('post')) {

            if (empty($this->request->data['password'])) {
                $this->Flash->error(__('Please enter new password'));
            } else if ($this->request->data['password'] != $this->request->data['confirm_password']) {
                $this->Flash->error(__('Password does not match the confirm password.'));
            } else {

                $data = [];
                $data['access_token'] = '';
                $data['password'] = $this->request->data['password'];
                $userDetails = $this->users->get($userData['id']);
                $userDetails->password = $data['password'];
                $userDetails->access_token = $data['access_token'];
                //$userData = $this->users->patchEntity($userData, $data);
                if ($this->users->save($userDetails)) {
                    $this->Flash->success(__('You have successfully Changed your password. Please Login with new password'));
                    return $this->redirect(['action' => 'login']);
                } else {
                    $this->Flash->error(__('Something went wrong, Please try again.'));
                }
            }
        }
        if(empty($userData)) {
            $this->Flash->error(__('Invalid account acctivation link'));
        }
        $user = $this->users->newEntity();
        $title = 'Reset Password';
        $this->set(compact('userData', 'user','title'));
    }

    //Update Password
    public function updatePassword()
    {
        $this->viewBuilder()->setLayout('');
        $user = $this->users->get($this->Auth->user('id'));
        if(!empty($this->request->data))
        {
            $user = $this->users->patchEntity($user, [
                'old_password'      => $this->request->data['old_password'],
                'password'          => $this->request->data['new_password'],
                'new_password'      => $this->request->data['new_password'],
                'confirm_password'  => $this->request->data['confirm_password']
                ],['validate' => 'password']   
            );

            if($this->users->save($user))
            {
                $this->Flash->success(__('Your password has been updated successfully.'));

            } else {
                if ($errors = $user->errors()) { 
                    $erorMessage = array(); 
                    $i = 0; 
                    $keys = array_keys($errors); 
                    foreach ($errors as $errors) { 
                        $key = key($errors); 
                        foreach($errors as $error){ 
                            $erorMessage = $error;
                        }
                        $i++;
                    }
                    $this->Flash->error(__($erorMessage));
                } else {
                    $this->Flash->error(__('Error in changing password. Please try again!'));
                }
            }
        }
    }

    //Admin dashboard
    public function adminDashboard() 
    {
        $inactiveCount    = 0;
        $activeCount      = 0;
        $totalCount       = 0;
        $joinedThisMonth  = 0;
        $joinedThisYear   = 0;
        $joinedThisWeek   = 0;

        $inactiveCount1   = 0;
        $activeCount1     = 0;
        $totalCount1      = 0;
        $joinedThisMonth1 = 0;
        $joinedThisYear1  = 0;
        $joinedThisWeek1  = 0;

        //Host(business) results
        $user = $this->users->find('all')->where(['Users.type'=>'host', 'Companies.type IN'=>['company', 'individual']])->contain(['Companies'])->toArray();
        if(!empty($user)) {
            foreach ($user as $key => $value) {
                if($value['status'] == 'inactive') {
                    $inactiveCount++;
                }
                if($value['status'] == 'active') {
                    $activeCount++;
                }
            }
            $totalCount = $inactiveCount + $activeCount;

            $joinedThisMonth = $this->users->find('all')->where(['Users.type'=>'host', 'Companies.type IN'=>['company', 'individual'], 'Users.created >='=> date('Y-m-01'), 'Users.created <='=> date('Y-m-31')])->contain(['Companies'])->count();

            $joinedThisYear = $this->users->find('all')->where(['Users.type'=>'host', 'Companies.type IN'=>['company', 'individual'], 'Users.created >='=> date('Y-01-01'), 'Users.created <='=> date('Y-12-31')])->contain(['Companies'])->count();

            $weekStart = date('Y-m-d', strtotime('monday this week'));
      
            $weekEnd   = date('Y-m-d', strtotime('sunday this week'));

            $joinedThisWeek = $this->users->find('all')->where(['Users.type'=>'host', 'Companies.type IN'=>['company', 'individual'], 'Users.created >='=> $weekStart, 'Users.created <='=> $weekEnd])->contain(['Companies'])->count();
        }

        //Guest results
        $guest = $this->users->find('all')->where(['type' => 'guest'])->toArray();
        if(!empty($guest)) {
            foreach ($guest as $key => $value) {
                if($value['status'] == 'inactive') {
                    $inactiveCount1++;
                }
                if($value['status'] == 'active') {
                    $activeCount1++;
                }
            }
            $totalCount1 = $inactiveCount1 + $activeCount1;

            $joinedThisMonth1 = $this->users->find('all')->where(['type' =>'guest','created >='=> date('Y-m-01'), 'created <='=> date('Y-m-31'),'status !='=>'incomplete'])->count();

            $joinedThisYear1 = $this->users->find('all')->where(['type' =>'guest','created >='=> date('Y-01-01'), 'created <='=> date('Y-12-31')])->count();

            $weekStart = date('Y-m-d', strtotime('monday this week'));
      
            $weekEnd   = date('Y-m-d', strtotime('sunday this week'));

            $joinedThisWeek1 = $this->users->find('all')->where(['type' =>'guest','created >='=> $weekStart,'created <='=> $weekEnd])->count();
        }

        $dashboard['guest'] = array(
            'totalCustomers'    => $totalCount1, 
            'activeCustomers'   => $activeCount1,
            'inactiveCustomers' => $inactiveCount1, 
            'joinedThisWeek'    => $joinedThisWeek1,
            'joinedThisMonth'   => $joinedThisMonth1,
            'joinedThisYear'    => $joinedThisYear1
        );

        $dashboard['host'] = array(
            'totalCompanies'    => $totalCount, 
            'activeCompanies'   => $activeCount,
            'inactiveCompanies' => $inactiveCount, 
            'joinedThisWeek'    => $joinedThisWeek,
            'joinedThisMonth'   => $joinedThisMonth,
            'joinedThisYear'    => $joinedThisYear
        );
        
        $this->set(compact('dashboard'));
    }

    // Add new caterory
    public function addCategory() 
    {   
        $this->viewBuilder()->setLayout('default');
        if($this->request->is('post')) {
            $params = $this->request->data;
            $category = $this->categoriesObj->newEntity();
            $category = $this->categoriesObj->patchEntity($category, $params);
            if($category = $this->categoriesObj->save($category)) {
                $this->Flash->success(__('Category Added successfully.'));
            } else {
                $this->Flash->error(__('Category already exist.'));
            }                     
        }        
    }

    // Update caterory
    public function updateCategory($id = null)
    {
        $categoryData = $this->categoriesObj->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $params = $this->request->data;
            $categoryData = $this->categoriesObj->patchEntity($categoryData, $params);
                if ($this->categoriesObj->save($categoryData)) {
                        $this->Flash->success(__('Updated successfully.'));
                        return $this->redirect(['action' => 'allCategories']);
                    }
                    $this->Flash->error(__('Something went wrong. Please, try again.'));
            }
            $this->set(compact('categoryData'));
    }

    // Get all categories
    public function allCategories() 
    {
		$categories = $this->categoriesObj->find('all')->enableHydration(false)->toArray();
		//pr($categories);die;
        $this->set(compact('categories'));
    }

    //Change status (Category, Host & guest)
    public function changeStatus($id = null, $status = null, $type = null) 
    {
            if ($this->request->is(['patch', 'post', 'put'])) {   
                if($type == 'category') {
                     if ($this->categoriesObj->updateAll(['status'=>$status], ['id'=>$id])) {
                     $this->Flash->success(__('Status updated successfully.'));
                     } else {
                            $this->Flash->error(__('Status not updated. Please try again.'));
                     }
                     return $this->redirect(['action' => 'allCategories']); die;
                }
                /*elseif($type == 'host'){
                     if ($this->userObj->updateAll(['status'=>$status], ['id'=>$id])) {
                     $this->Flash->success(__('Status updated successfully.'));
                     } else {
                            $this->Flash->error(__('Status not updated. Please try again.'));
                     }
                     return $this->redirect(['action' => 'hosts']);
                }
                else{
                     if($this->userObj->updateAll(['status'=>$status], ['id'=>$id])) {
                     $this->Flash->success(__('Status updated successfully.'));
                     } else { echo "Asas"; die;
                            $this->Flash->error(__('Status not updated. Please try again.'));
                     }
                     return $this->redirect(['action' => 'guests']);
                }*/
           }         
    }

    //Get all hosts(business)
    public function hosts() 
    {
        $param = $this->request->query('type');
        $employee = ['Employees' =>function ($q) { 
                        return $q;
                    }, 'Categories'];

        //Search conditions
        $params = $this->request->query;
        $conditions = array();
        $arr0 = $arr1 = $arr2 = $arr3 = $arr4 = array();
        $arr0 = array('Users.type'=>'host');

        if(!empty($params)) {
			$arr1 = !empty($params['company_name']) ? array('Companies.company_name LIKE'=>'%'.$params['company_name'].'%') : $arr1;
            $arr2 = !empty($params['city']) ? array('Companies.city LIKE'=>'%'.$params['city'].'%') : $arr2;
            $arr3 = !empty($params['zipcode']) ? array('Companies.zipcode LIKE'=>'%'.$params['zipcode'].'%') : $arr3;
            $arr4 = !empty($params['category']) ? array('Companies.category_id LIKE'=>'%'.$params['category'].'%') : $arr4;
        }
        //End search conditions
        $conditions = array_merge($arr0, $arr1, $arr2, $arr3, $arr4);

        if($param == 'total-host' || $param == '') {
            $hosts = $this->users->find('all')->where($conditions)->contain(['Companies'=>$employee])->order(['Companies.company_name' => 'ASC']);
        } elseif($param == 'active_host') {
            $arr5 = array('Users.status'=>'active');
            $conditions = array_merge($conditions, $arr5);
            $hosts = $this->users->find('all')->where($conditions)->contain(['Companies'=>$employee])->order(['Companies.company_name' => 'ASC']);
        } elseif($param == 'inactive_host') {
            $arr6 = array('Users.status'=>'inactive');
            $conditions = array_merge($conditions, $arr6);
            $hosts = $this->users->find('all')->where($conditions)->contain(['Companies'=>$employee])->order(['Companies.company_name' => 'ASC']);
        } elseif($param == 'joinedThisMonth') {
            $arr7 = array('Users.created >='=> date('Y-m-01'), 'Users.created <='=> date('Y-m-31'));
            $conditions = array_merge($conditions, $arr7);
            $hosts = $this->users->find('all')->where($conditions)->contain(['Companies'=>$employee])->order(['Companies.company_name' => 'ASC']);
        } elseif($param == 'joinedThisYear') {
            $arr8 = array('Users.created >='=> date('Y-01-01'), 'Users.created  <='=> date('Y-12-31'));
            $conditions = array_merge($conditions, $arr8);
            $hosts = $this->users->find('all')->where($conditions)->contain(['Companies'=>$employee])->order(['Companies.company_name' => 'ASC']);
        } else {
            $weekStart = date('Y-m-d', strtotime('monday this week'));
            $weekEnd   = date('Y-m-d', strtotime('sunday this week'));
            $arr9 = array('Users.created >='=> $weekStart, 'Users.created  <='=> $weekEnd);
            $conditions = array_merge($conditions, $arr9);
            $hosts = $this->users->find('all')->where($conditions)->contain(['Companies'=>$employee])->order(['Companies.company_name' => 'ASC']);
        }

        //All categories
        $categories = $this->categoriesObj->find()->where(['status'=>'active'])->select(['id', 'name']);
        
        $this->set('hosts', $this->paginate($hosts));
        $this->set(compact('categories'));
    }

    //Get all Customers(guest)
    public function guests()
    {
        $param = $this->request->query('type');

        //Search conditions
        $params = $this->request->query;
        $conditions = array();
        $arr1 = $arr2 = $arr3 = $arr4 = array();
        $arr1 = array('Users.type'=>'guest');

        if(!empty($params)) {
            $arr2 = !empty($params['name']) ? array('Customers.name LIKE'=>'%'.$params['name'].'%') : $arr2;
            $arr3 = !empty($params['phone']) ? array('Customers.phone LIKE'=>'%'.$params['phone'].'%') : $arr3;
            $arr4 = !empty($params['email']) ? array('Customers.email LIKE'=>'%'.$params['email'].'%') : $arr4;
        }
        //End search conditions
		
        $conditions = array_merge($arr1, $arr2, $arr3, $arr4);
        
        if($param == 'total-guest' || $param == '') {

            $guests = $this->users->find('all')->where($conditions)->contain(['Customers'])->order(['Customers.name'=>'ASC']);

        } elseif($param == 'active_guest') {
            
            $arr5 = array('Users.status'=>'active');
            $conditions = array_merge($conditions, $arr5);
            $guests = $this->users->find('all')->where($conditions)->contain(['Customers'])->order(['Customers.name'=>'ASC']);

        } elseif($param == 'inactive_guest') {

            $arr6 = array('Users.status'=>'inactive');
            $conditions = array_merge($conditions, $arr6);
            $guests = $this->users->find('all')->where($conditions)->contain(['Customers'])->order(['Customers.name'=>'ASC']);

        } elseif($param == 'joinedThisMonth') {

            $arr7 = array('Users.created >='=> date('Y-m-01'), 'Users.created <='=> date('Y-m-31'));
            $conditions = array_merge($conditions, $arr7);
            $guests = $this->users->find('all')->where($conditions)->contain(['Customers'])->order(['Customers.name'=>'ASC']);

        } elseif($param == 'joinedThisYear') {

            $arr8 = array('Users.created >='=> date('Y-01-01'), 'Users.created  <='=> date('Y-12-31'));
            $conditions = array_merge($conditions, $arr8);
            $guests = $this->users->find('all')->where($conditions)->contain(['Customers'])->order(['Customers.name'=>'ASC']);

        } else {

            $weekStart = date('Y-m-d', strtotime('monday this week'));
            $weekEnd   = date('Y-m-d', strtotime('sunday this week'));
            $arr9 = array('Users.created >='=> $weekStart, 'Users.created  <='=> $weekEnd);
            $conditions = array_merge($conditions, $arr9);
            $guests = $this->users->find('all')->where($conditions)->contain(['Customers'])->order(['Customers.name'=>'ASC']);
        }
        
        $this->set('guests', $this->paginate($guests));
    }

    //Get all hosts(business)
    public function viewHostDetails($id = null) 
    {
        $user = $this->users->findById($id)->contain(['Companies'=>['Categories'=>['fields'=>['name']]]])->enableHydration(false)->first();
        $this->set(compact('user'));
    }

    //Get hosts (Employees and Rooms)
    public function viewHostEmployees($id = null)
    {
        $employee = ['Employees.Users' =>function ($q) { 
                        return $q;
                    }];

        $user = $this->users->findById($id)->contain(['Companies'=>$employee])->enableHydration(false)->first();
        $this->set(compact('user','id'));
    }

    //Update hosts Details
    public function updateHostDetails($id = null) 
    {
        $user = $this->users->get($id,['contain' =>['Companies']]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $params = $this->request->data;
            $user = $this->users->patchEntity($user,$params,['associated'=>['Companies']]);
            if ($this->users->save($user)) {
                $this->Flash->success(__('Updated successfully.'));
                return $this->redirect(['action' => 'hosts']);
            }
            $this->Flash->error(__('Something went wrong. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    //Delete Business with its employees and rooms
    public function deleteHosts($id = null) 
    {
        $entity = $this->users->get($id);
        $result = $this->users->delete($entity);
        if($result) {
            $this->Flash->success(__('Deleted successfully.'));
            return $this->redirect(['action' => 'hosts']);
        } else {
            $this->Flash->error(__('Something went wrong. Please, try again.'));
        }
        return $this->redirect(['action' => 'hosts']);
    }

    //Get all Guests Details
    public function viewGuestDetails($id = null) 
    {
        $user = $this->users->findById($id)->contain(['Customers'])->enableHydration(false)->toArray();
        pr($user); die;
        $this->set(compact('user'));
    }

    //Update guests Details
    public function updateGuestDetails($id = null) 
    {
        $user = $this->users->get($id, ['contain' => ['Customers']]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $params = $this->request->data;
            $user = $this->users->patchEntity($user,$params,['associated'=>['Customers']]);
            if ($this->users->save($user)) {
                $this->Flash->success(__('Updated successfully.'));
                return $this->redirect(['action' => 'guests']);
            }
            $this->Flash->error(__('Something went wrong. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    //Delete guests
    public function deleteGuests($id = null) 
    {
        $entity = $this->users->get($id);
        $result = $this->users->delete($entity);
        if($result) {
            $this->Flash->success(__('Deleted successfully.'));
            return $this->redirect(['action'=>'guests']);
        } else {
            $this->Flash->error(__('Something went wrong. Please, try again.'));
        }
        return $this->redirect(['action'=>'guests']);
    }

    //Delete employee
    public function deleteEmployee($id = null, $userId = null , $custom = null, $rid = null) 
    {
        if($custom == "yes") {
            $entity = $this->employeeObj->get($id);
            $result = $this->employeeObj->delete($entity);
        } else {
            $entity = $this->users ->get($userId);
            $result = $this->users ->delete($entity);
        }   
        if($result) {
            $this->Flash->success(__('Deleted successfully.'));
            return $this->redirect(['action'=>'hosts']);
        } else {
            $this->Flash->error(__('Something went wrong. Please, try again.'));
        }
        return $this->redirect(['action'=>'hosts']);
    }

    // Manage Subscription Plan
    public function manageSubscription()
    {
        $this->viewSubscription();
        if($this->request->is('post')) {
            $params  = $this->request->data;
            $subplan = $this->subplanObj->find('all')->toArray();
            if(empty($subplan)) {
                $subplanData = $this->subplanObj->newEntity();
                $msg = "Added successfully";
            } else {
                $subplanData = $this->subplanObj->get($subplan[0]['id']);
                $msg = "Updated successfully";
            }
            $subplanData->amount = $params['amount'];
            $this->subplanObj->save($subplanData);
            $this->Flash->success(__($msg));
            return $this->redirect(['action' => 'manageSubscription']);
        }
    }

    //View subscription details
    public function viewSubscription()
    {
        $subplan = $this->subplanObj->find('all')->enableHydration(false)->toArray();
        if(empty($subplan)) {
            $subplan = null;
        }
        $this->set(compact('subplan'));
    }

    // Manage Subscription Plan
    public function updateSubscription()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $params = $this->request->data;
            $result = substr($params['payment_status'], 0, 2);
            if($result=='no') {
                $params['payment_status'] = "no";
                $msg = 'Removed Free Subscription from ';
            }
            else{
                $params['payment_status'] = "free";
                $msg = 'Added Free Subscription to ';
            }
       
            $user = $this->users->get($params['id'],['contain' =>['Companies']]);
         
            $user = $this->users->patchEntity($user,$params,['associated'=>['Companies']]);
            if ($this->users->save($user)) {
                $this->Flash->success(__($msg.$user['company']['company_name']));
                return $this->redirect(['action' => 'hosts']);
            }
            $this->Flash->error(__('Something went wrong. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    //Get category name
    public function getCategoryName($catId)
    {
        $category = $this->categoriesObj->find('all')->where(['id'=>$catId])->select('name')->enableHydration(false)->first();
        return $category['name'];
    }

    //Payment details
    public function paymentDetails()
    {
        //Search conditions
        $params = $this->request->query;
        $conditions = array();
        $arr1 = $arr2 = array();
        
        if(!empty($params)) {
            $arr1 = !empty($params['company_name']) ? array('Companies.company_name LIKE'=>'%'.$params['company_name'].'%') : $arr1;

            if(!empty($params['filter']) && $params['filter'] == 'current_week') {

                $weekStart = date('Y-m-d', strtotime('monday this week'));
                $weekEnd   = date('Y-m-d', strtotime('sunday this week'));
                $arr2 = array('PaymentDetails.created >='=> $weekStart, 'PaymentDetails.created <='=> $weekEnd);

            } elseif(!empty($params['filter']) && $params['filter'] == 'current_month') {

                $arr2 = array('PaymentDetails.created >='=> date('Y-m-01'), 'PaymentDetails.created <='=> date('Y-m-31'));
                               
            } elseif (!empty($params['filter']) && $params['filter'] == 'current_year') {

                $arr2 = array('PaymentDetails.created >='=> date('Y-01-01'), 'PaymentDetails.created <='=> date('Y-12-31'));
            }
        }
        //End search conditions
        $conditions = array_merge($arr1, $arr2);
        
        $payDetails = $this->payDetailObj->find('all')->where($conditions)->contain(['Users'=>['fields'=>['Users.id', 'Users.created']], 'Companies'=>['fields'=>['Companies.id', 'Companies.user_id', 'Companies.company_name']]])->select(['PaymentDetails.id', 'PaymentDetails.user_id', 'PaymentDetails.company_id', 'PaymentDetails.amount', 'PaymentDetails.transaction_id', 'PaymentDetails.receipt', 'PaymentDetails.created'])->order(['PaymentDetails.id'=>'DESC']);

        $this->set('payDetails', $this->paginate($payDetails));
    }

    //Delete payment record
    public function deletePayment($id = null) 
    {
        $entity = $this->payDetailObj->get($id);
        $result = $this->payDetailObj->delete($entity);
        if($result) {
            $this->Flash->success(__('Deleted successfully.'));
            return $this->redirect(['action'=>'paymentDetails']);
        } else {
            $this->Flash->error(__('Something went wrong. Please, try again.'));
        }
        return $this->redirect(['action'=>'paymentDetails']);
    }

    //Send Message(Email)
    public function sendMessage()
    {
        $this->viewBuilder()->setLayout('default');
        if($this->request->is('post')) {
            $params = $this->request->data;
			
            if(empty($params['message'])) {
                $this->Flash->error(__('Message can not blank.'));
                return $this->redirect(['action' => 'sendMessage']);
            }
            
            if (!empty($params['filter_type']) && $params['filter_type'] == 'allUsers') {
                    
                //Get all users
                $allUsers = $this->users->find('all')->where(['Users.status'=>'active', 'Users.is_otp_verified'=>'yes', 'Users.type !='=>'admin'])->contain(['Companies'=>['fields'=>['Companies.id', 'Companies.first_name', 'Companies.user_id', 'Companies.email'], 'Employees'=>['fields'=>['Employees.company_id', 'Employees.first_name', 'Employees.email', 'Employees.custom']]], 'Customers'=>['fields'=>['Customers.user_id','Customers.email', 'Customers.name']]])->select(['Users.id', 'Users.phone', 'Users.device_type', 'Users.device_token'])->enableHydration(false)->toArray();
				
                //Send email to all users
                if(!empty($params['msg_type']) && $params['msg_type'] == 'email') {
                    if(!empty($allUsers)) {
                        foreach ($allUsers as $key => $value) {
                            if(!empty($value['customer']['email'])) {

                                $email = $value['customer']['email'];
                                $this->sendEmail($params, $email, $value['customer']['name']);

                            } elseif (!empty($value['company']['email'])) {

                                $email = $value['company']['email'];
                                $this->sendEmail($params, $email, $value['company']['first_name']);
                                
                            } elseif (!empty($value['company']['employees'])) {
                                foreach ($value['company']['employees'] as $key2 => $value2) {
                                    if(!empty($value2['email']) && $value2['custom'] == 'no') {
                                        
                                        $email = $value2['email'];
                                        $this->sendEmail($params, $email, $value2['first_name']);
                                    }
                                }
                            }
                        }
                    }
                }

                $this->Flash->success(__('Email sent successfully.'));

            } elseif (!empty($params['filter_type']) && $params['filter_type'] == 'allHosts') {
                
                $allHosts = $this->users->find('all')->where(['Users.status'=>'active', 'Users.is_otp_verified'=>'yes', 'Users.type'=>'host', 'Users.type !='=>'admin'])->contain(['Companies'=>['fields'=>['Companies.id', 'Companies.first_name', 'Companies.user_id', 'Companies.email'], 'Employees'=>['fields'=>['Employees.company_id', 'Employees.first_name', 'Employees.email', 'Employees.custom']]]])->select(['Users.id', 'Users.phone', 'Users.device_type', 'Users.device_token'])->enableHydration(false)->toArray();

                //Send email to all hosts
                if(!empty($params['msg_type']) && $params['msg_type'] == 'email') {
                    if(!empty($allHosts)) {
                        foreach ($allHosts as $key => $value) {
                            if (!empty($value['company']['email'])) {

                                $email = $value['company']['email'];
                                $this->sendEmail($params, $email, $value['company']['first_name']);
                                
                            } elseif (!empty($value['company']['employees'])) {
                                foreach ($value['company']['employees'] as $key2 => $value2) {
                                    if(!empty($value2['email']) && $value2['custom'] == 'no') {
                                        
                                        $email = $value2['email'];
                                        $this->sendEmail($params, $email, $value2['first_name']);
                                    }
                                }
                            }
                        }
                    }
                }

                $this->Flash->success(__('Email sent successfully.'));

            } elseif (!empty($params['filter_type']) && $params['filter_type'] == 'allGuests') {
                
                $allGuests = $this->users->find('all')->where(['Users.status'=>'active', 'Users.is_otp_verified'=>'yes', 'Users.type'=>'guest', 'Users.type !='=>'admin'])->contain(['Customers'=>['fields'=>['Customers.user_id', 'Customers.email', 'Customers.name']]])->select(['Users.id', 'Users.phone', 'Users.device_type', 'Users.device_token'])->enableHydration(false)->toArray();

                //Send email to all hosts
                if(!empty($params['msg_type']) && $params['msg_type'] == 'email') {
                    if(!empty($allGuests)) {
                        foreach ($allGuests as $key => $value) {
                            if (!empty($value['customer']['email'])) {

                                $email = $value['customer']['email'];
                                $this->sendEmail($params, $email, $value['customer']['name']);
                            }
                        }
                    }
                }

                $this->Flash->success(__('Email sent successfully.'));

            } elseif (!empty($params['filter_type']) && $params['filter_type'] == 'filterCriteria') {
                
                $conditions = array();
                $arr1 = $arr2 = $arr3 = $arr4 = array();

                if(!empty($params['user']) && $params['user'] == 'host') {

                    $arr1 = !empty($params['user']) ? array('Users.type'=>$params['user']) : $arr1;

                    $arr2 = array('Users.created >='=>$params['reg_start_date']." 00:00:00", 'Users.created  <='=> $params['reg_end_date']." 11:59:00");

                    $arr3 = array('Users.status'=>'active', 'Users.is_otp_verified'=>'yes', 'Users.type !='=>'admin');

                    $conditions = array_merge($arr1, $arr2, $arr3);

                    $hosts = $this->users->find('all')->where($conditions)->contain(['Companies'=>['fields'=>['Companies.id', 'Companies.user_id', 'Companies.email', 'Companies.first_name']], 'Employees'=>['fields'=>['Employees.user_id', 'Employees.first_name', 'Employees.email', 'Employees.custom']]])->select(['Users.id', 'Users.phone', 'Users.device_type', 'Users.device_token'])->enableHydration(false)->toArray();
                    
                    //Send email to selected hosts
                    if(!empty($params['msg_type']) && $params['msg_type'] == 'email') {
                        if(!empty($hosts)) {
                            foreach ($hosts as $key => $value) {
                                if (!empty($value['company']['email'])) {

                                    $email = $value['company']['email'];
                                    $this->sendEmail($params, $email, $value['company']['first_name']);
                                    
                                } elseif (!empty($value['employee']['email'])) {
             
                                    $email = $value['employee']['email'];
                                    $this->sendEmail($params, $email, $value['employee']['first_name']);
                                }
                            }
                        }
                    }

                } elseif (!empty($params['user']) && $params['user'] == 'guest') {

                    $arr1 = !empty($params['user']) ? array('Users.type'=>$params['user']) : $arr1;
                    $arr2 = array('Users.created >='=> $params['reg_start_date'], 'Users.created  <='=> $params['reg_end_date']);
                    $arr3 = array('Users.status'=>'active', 'Users.is_otp_verified'=>'yes', 'Users.type !='=>'admin');

                    $conditions = array_merge($arr1, $arr2, $arr3);
                    $guests = $this->users->find('all')->where($conditions)->contain(['Customers'=>['fields'=>['Customers.user_id','Customers.email', 'Customers.name']]])->select(['Users.id', 'Users.phone', 'Users.device_type', 'Users.device_token'])->enableHydration(false)->toArray();
                    
                    //Send email to all hosts
                    if(!empty($params['msg_type']) && $params['msg_type'] == 'email') {
                        if(!empty($guests)) {
                            foreach ($guests as $key => $value) {
                                if (!empty($value['customer']['email'])) {

                                    $email = $value['customer']['email'];
                                    $this->sendEmail($params, $email, $value['customer']['name']);
                                }
                            }
                        }
                    }
                }
                $this->Flash->success(__('Email sent successfully.'));
            }
            //$this->Flash->error(__('Something went wrong. Please, try again.'));
        }
    }

    public function sendEmail($params, $email, $name=null)
    {
		if(!empty($params) && !empty($email)) {
            //echo $email;
			//echo "<pre>";
			//print_r($params);die;				
            $subject = !empty($params['subject']) ? $params['subject'] : '';
            $message = !empty($params['message']) ? $params['message'] : '';
            
            //Email Content Start
            $emailBody  = "<div>";
            $emailBody .= "Dear ".ucfirst($name);
            $emailBody .= ",<br><br>";
            $emailBody .= $message;
            $emailBody .= "<br><br>";
            $emailBody .= "Thankyou";
            $emailBody .= "<br>";
            $emailBody .= 'MynusTwo Team';
            $emailBody .= "</div>";
           
            $this->sendMail($email, $subject, $emailBody);
            //Email End 
        }
    }

    //Business ratings
    public function businessRating()
    {
        $param = $this->request->query('type');
        $employee = ['Employees' =>function ($q) { 
                        return $q->select(['Employees.id', 'Employees.company_id']);
                    }, 'Categories'=>['fields'=>['Categories.id', 'Categories.name']]];

        //Search conditions
        $params = $this->request->query;
        $conditions = array();
        $arr1 = $arr2 = $arr3 = $arr4 = array();
        $arr1 = array('Users.type'=>'host');

        if(!empty($params)) {
            $arr4 = !empty($params['category']) ? array('Companies.category_id LIKE'=>'%'.$params['category'].'%') : $arr4;
        }
        //End search conditions
        $conditions = array_merge($arr1, $arr2, $arr3, $arr4);

        $hosts = $this->users->find('all')->where($conditions)->contain(['Companies'=>$employee])->select(['Users.id', 'Companies.id', 'Companies.company_name', 'Companies.type'])->order(['Users.id'=>'ASC'])->enableHydration(false)->toArray();

        foreach ($hosts as $key => $value) {
            $hosts[$key]['company']['rating'] = $this->getBusinessRating($value['company']['id']);
            $hosts[$key]['company']['userRateCount'] = $this->countRating($value['company']['id']);
        }
        
        //All categories
        $categories = $this->categoriesObj->find()->select(['id', 'name']);
        
        //$this->set('hosts', $this->paginate($hosts));
        $this->set(compact('categories', 'hosts'));
    }


    //Get rating of business
    public function getBusinessRating($businessId)
    {
        if(!empty($businessId)) {
            $employees = $this->employeeObj->find()->where(['company_id'=>$businessId])->select(['id'])->enableHydration(false)->toArray();
            
            $totalRating = 0;
            if(!empty($employees)) {
                $rating = 0;
                $count = 0;
                foreach ($employees as $key => $value) {
                    $rate = $this->employeeRating($value['id']);
                    if($rate>0) {
                        $rating = $rating + $rate;
                        $count++;
                    }
                }

                if(!empty($rating)) {
                    $totalRating = $rating / $count;
                }
            }
            else {
                $cmpRatings = $this->ratingObj->find()->where(['employee_id'=>$businessId,'type'=>'company'])->select(['value'])->enableHydration(false)->toArray();
                if(!empty($cmpRatings)) {
                    $rating = 0;
                    $count = 0;
                    foreach ($cmpRatings as $key => $value) {
                        $rating = $rating + $value['value'];
                        $count++;
                    }
                }
                if(!empty($rating)) {
                    $totalRating = $rating / $count;
                }

            }
            return round($totalRating);  
        }
    }

    //Get rating of business
    public function employeeRating($empId)
    {
        if(!empty($empId)) {
            
            $totalRating = 0;
                            
            //Get rating of all employees
            $empRatings = $this->ratingObj->find()->where(['employee_id'=>$empId])->select(['value'])->enableHydration(false)->toArray();

            if(!empty($empRatings)) {
                $rating = 0;
                $count = 0;
                foreach ($empRatings as $key => $value) {
                    $rating = $rating + $value['value'];
                    $count++;
                }
            }

            if(!empty($rating)) {
                $totalRating = $rating / $count;
            }
            return round($totalRating);  
        }
    }

    //Number of customers who rate business
    public function countRating($businessId)
    {
        if(!empty($businessId)) {
            $employees = $this->employeeObj->find('all')->where(['Employees.company_id'=>$businessId])->contain(['Ratings'=>['fields'=>['Ratings.employee_id', 'Ratings.customer_id'], 'Customers'=>['fields'=>['Customers.id', 'Customers.name']]]])->select(['Employees.id'])->enableHydration(false)->toArray();
            
            $totalCount = 0;
            $uArray = [];
			$cArray = [];
			$res = [];
            if(!empty($employees)) {
                foreach($employees as $emp) {
                    foreach ($emp['ratings'] as $key => $value) {
                        $uArray[] = $value['customer_id'];
                    }
                }
                $unique = array_unique($uArray);
                $res['count'] = count($unique);
				
				foreach($employees as $emp) {
                    foreach ($emp['ratings'] as $key => $value) {
						$cArray[] = $value['customer']['name'];
                    }
                }
				$res['customers'] = array_values(array_unique($cArray));
				
            }
            else {
                $users = $this->ratingObj->find()->where(['employee_id'=>$businessId,'type'=>'company'])->contain('Customers')->select(['employee_id', 'customer_id'])->enableHydration(false)->toArray();
                $totalCount = 0;
                $uArray = [];
                if(!empty($users)) {
                    foreach ($users as $key => $value) {
                        $uArray[] = $value['customer_id'];
                    }
                    // foreach($users as $emp) {
                    //         $cArray[] = $emp['customer']['name'];
                    // }
                    // $res['customers'] = array_values(array_unique($cArray));


                    $unique = array_unique($uArray);
                    $res['count']  = count($unique);
                }

            }
			
            return $res;
        }
    }
    
    //Number of customers who rate employees
    public function countRatingUsers($empId)
    {
        if(!empty($empId)) {
            $users = $this->ratingObj->find()->where(['employee_id'=>$empId])->select(['employee_id', 'customer_id'])->enableHydration(false)->toArray();
            $totalCount = 0;
            $uArray = [];
            if(!empty($users)) {
                foreach ($users as $key => $value) {
                    $uArray[] = $value['customer_id'];
                }
                $unique = array_unique($uArray);
                $totalCount = count($unique);
            }
            return $totalCount;
        }
    }
	
	//View users listing who rate the business
	public function viewRatings($businessId)
	{
		if(!empty($businessId)) {
            $results = $this->employeeObj->find('all')->where(['Employees.company_id'=>$businessId])->contain(['Companies'=>['fields'=>['Companies.id', 'Companies.company_name']], 'Ratings'=>['fields'=>['Ratings.id', 'Ratings.employee_id', 'Ratings.customer_id', 'Ratings.value'], 'Customers'=>['fields'=>['Customers.id', 'Customers.name']]]])->select(['Employees.id', 'Employees.first_name', 'Employees.last_name'])->enableHydration(false)->toArray();
			$this->set(compact('results'));
        }
	}
	
	//Delete rating	
	public function deleteRating($id = null, $compId = null) 
    {
		if ($this->request->is(['patch', 'post', 'put'])) {
			$entity = $this->ratingObj->get($id);
			$result = $this->ratingObj->delete($entity);
			if($result) {
				$this->Flash->success(__('Deleted successfully.'));
				return $this->redirect(['action'=>'viewRatings/'.$compId]);
			} else {
				$this->Flash->error(__('Something went wrong. Please, try again.'));
			}
			return $this->redirect(['action'=>'viewRatings/'.$compId]);
	    }         
    }
	
	// Get all company categories
    public function companyCategoryList() 
    {
		//$companies = $this->companyObj->find('all')->enableHydration(false)->toArray();
		
		$compData = $this->companyObj->find('all')->select(['Companies.id', 'Companies.company_name'])->contain(['CompanyCategory'])->enableHydration(false)->toArray();
		//pr($compData);die('here');
        $this->set(compact('compData'));
    }
	
	public function deleteMenuCat()
	{
		$params = $this->request->data;
		//pr($params);die;
		if(!empty($params['id']) && !empty($params['compId'])) {
			$menuCat = $this->compCatObj->get($params['id']);
			if ($this->compCatObj->delete($menuCat)) {
				$result = array('status'=>'success', 'message'=>'Menu category deleted successfully.');
                echo json_encode($result);die;
            } else {
                $result = array('status'=>'failure', 'message'=>'Something Wrong. Please try again.');
                echo json_encode($result);die;
            }
		} else {
			$result = array('status'=>'failure', 'message'=>'Something Wrong. Please try again.');
			echo json_encode($result);die;
		} 
	}
}
