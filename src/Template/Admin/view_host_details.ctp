<section class="content-header">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Business Details</h3>
            </div>
            <div class="pull-right">
                <button class="btn btn-primary btn-xs" onclick="window.history.back();">Back</button>
            </div>
        </div>
    </div>
</section>

<!-- Host(business) details -->
<section class="content business_details">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
          <div class="box-body">

            <div class="form-group">
              <label>Business Name: </label>
              <?= $user['company']['company_name']; ?>
            </div>

            <div class="form-group">
              <label>First Name: </label>
              <?= $user['company']['first_name']; ?> 
            </div>

            <div class="form-group">
              <label>Last Name: </label>
              <?= $user['company']['last_name']; ?> 
            </div>

            <div class="form-group">
              <label>About: </label>
              <?= $user['company']['about']; ?>
            </div>

            <div class="form-group">
              <label>Phone: </label>
              <?= $user['phone']; ?>
            </div>

            <div class="form-group">
              <label>Email: </label>
              <?= $user['company']['email']; ?>
            </div>

            <div class="form-group">
              <label>Address: </label>
              <?= $user['company']['address']; ?> 
            </div>

            <div class="form-group">
              <label>City: </label>
              <?= $user['company']['city']; ?> 
            </div>
			
			<div class="form-group">
              <label>State: </label>
              <?= $user['company']['state']; ?> 
            </div>
			
			<div class="form-group">
              <label>Country: </label>
              <?= $user['company']['country']; ?> 
            </div>

            <div class="form-group">
              <label>Zipcode: </label>
              <?= $user['company']['zipcode']; ?> 
            </div>

            <div class="form-group">
              <label>Category: </label>
              <?= $user['company']['category']['name']; ?> 
            </div>

            <div class="form-group">
              <label>Business Type: </label>
              <?= $user['company']['type']; ?> 
            </div>

            <div class="form-group">
              <label>Status: </label>
              <?= $user['status']; ?> 
            </div>

            <div class="form-group">
              <label>Created Date: </label>
              <?= date('d F Y', strtotime($user['created'])); ?> 
            </div>

            <div class="form-group">
              <label>Modified Date: </label>
              <?= date('d F Y', strtotime($user['modified'])); ?> 
            </div>

          </div>
      </div>
    </div>
  </div>
</section>
<!-- End Host(business) details -->