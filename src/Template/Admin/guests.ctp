<!-- Content Header (Page header) -->
<?php
$type = $name = $phone = $email = '';

if(isset($_REQUEST['type'])) { 
    $type = trim($_REQUEST['type']); 
}

if(isset($_REQUEST['name'])) { 
    $name = $_REQUEST['name']; 
}

if(isset($_REQUEST['phone'])) { 
    $phone = $_REQUEST['phone']; 
}

if(isset($_REQUEST['email'])) { 
    $email = $_REQUEST['email']; 
}
?>
<section class="content-header">
    <?php echo $this->Flash->render(); ?>
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Guests Listing</h3>
            </div>
        </div>
    </div>
</section>

<!-- Main content -->
<section class="content">
	
	<div class="row well">
	    <div class="col-md-12 col-lg-12 margin-tb searchBoxCls">
	        <!-- Search form -->
	        <div class="row">
	            <form role="form" method="GET" action="">

	                <input type="hidden" name="type" value="<?php if(isset($type)){echo "$type"; } ?>">

	                <div class="col-md-2">
	                    <input type="text" class="form-control" name="name" placeholder="Name" value="<?php if(isset($name)){echo "$name"; } ?>">
	                </div>

	                <div class="col-md-2">
	                    <input type="text" class="form-control" name="phone" placeholder="Phone" value="<?php if(isset($phone)){echo "$phone"; } ?>">
	                </div>

	                <div class="col-md-2">
	                    <input type="text" class="form-control" name="email" placeholder="Email" value="<?php if(isset($email)){echo "$email"; } ?>">
	                </div>  

	                <div class="col-md-1">
	                    <button class="btn btn-default btn-sm form-control" type="submit">
	                        <i class="fa fa-search"></i>
	                    </button>
	                </div>

	                <div class="col-md-1">
	                    <a class="btn btn-default btn-sm form-control" href="guests"><strong>Reset</strong></a>
	                </div>
	                
	            </form>
	        </div>
	        <!-- End search form -->
	    </div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-body">
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><?= $this->Paginator->sort('id', 'Id') ?></th>
								<th>Name</th>
								<th>Phone Number</th>
								<th>Email</th>
								<th>Status</th>
								<th>Created</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($guests as $guest): ?>
							<tr>
								<td><?= $this->Number->format($guest['id']) ?></td>
								<td><?= h($guest['customer']['name']) ?></td>
								<td><?= h($guest['phone']) ?></td>
								<td><?= h($guest['customer']['email']) ?></td>
								<td><?= h($guest['status']) ?></td>
								<td><?= h($guest['created']->format('d F Y')) ?></td>
								<td class="actions">
									<!-- <?= $this->Html->link(
									     __('View'),
									    ['action' => 'viewGuestDetails', $guest['id']],
									    ['escape' => false, 'title' => __('View'), 'class' => 'btn btn-primary btn-xs']
									) ?> -->

									<?= $this->Html->link(
									     __('Edit'),
									    ['action' => 'updateGuestDetails', $guest['id']],
									    ['escape' => false, 'title' => __('Edit'), 'class' => 'btn btn-primary btn-xs']
									) ?>

									<?= $this->Html->link(
									     __('Delete'),
									    ['action' => 'deleteGuests', $guest['id']],
									    ['escape' => false, 'title' => __('Delete'), 'class' => 'btn btn-danger btn-xs',
									    'confirm' => __('Are you sure you want to delete ?')]
									) ?> 
									
                            	</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->

	<div class="row">
		<div class="col-md-6">
	 	<?php
			echo $this->Paginator->counter('Showing {{start}} to {{end}} of {{count}} entries');
		?>
		</div>
		<div class="col-md-6">
			<ul class="pagination" style="float: right; margin: 0!important;">
	 		<?php
				echo $this->Paginator->prev('< ' . __('previous'));
				echo $this->Paginator->numbers();
				echo $this->Paginator->next(__('next') . ' >');
			?>
		 	</ul>
		</div>
	</div>

</section><!-- /.content -->

<script type="text/javascript">
$(function() {
    setTimeout(function() {
      $(".alert-success").hide('blind', {}, 500)
    }, 5000);
});
</script>