<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */

//echo "<pre>"; print_r($dashboard); die;
?>
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Hosts(Business)
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?= $dashboard['host']['totalCompanies']; ?></h3>
                  <p>Total Host</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                 <?= $this->Html->link(__('View Details'), ['action' => 'hosts','type'=>'total-host'],['class'=>'small-box-footer']) ?>
            <!--     <a href="" class="small-box-footer">View Details  <i class="fa fa-arrow-circle-right"></i></a> -->
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?= $dashboard['host']['activeCompanies'] ?></h3>
                  <p>Active Host</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <?= $this->Html->link(__('View Details'), ['action' => 'hosts','type'=>'active_host'],['class'=>'small-box-footer']) ?>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                   <h3><?= $dashboard['host']['inactiveCompanies'] ?></h3>
                  <p>Inactive Host</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <?= $this->Html->link(__('View Details'), ['action' => 'hosts','type'=>'inactive_host'],['class'=>'small-box-footer']) ?>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                   <h3><?= $dashboard['host']['joinedThisWeek'] ?></h3>
                  <p>Joined This Week</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <?= $this->Html->link(__('View Details'), ['action' => 'hosts','type'=>'joinedThisWeek'],['class'=>'small-box-footer']) ?>
              </div>
            </div><!-- ./col -->
             <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                   <h3><?= $dashboard['host']['joinedThisMonth'] ?></h3>
                  <p>Joined This Month</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <?= $this->Html->link(__('View Details'), ['action' => 'hosts','type'=>'joinedThisMonth'],['class'=>'small-box-footer']) ?>
              </div>
            </div><!-- ./col -->

             <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                   <h3><?= $dashboard['host']['joinedThisYear'] ?></h3>
                  <p>Joined This Year</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
              <?= $this->Html->link(__('View Details'), ['action' => 'hosts','type'=>'joinedThisYear'],['class'=>'small-box-footer']) ?>
              </div>
            </div><!-- ./col -->

          </div><!-- /.row -->
        </section><!-- /.content -->


        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Guests
          </h1>
        </section>
        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?= $dashboard['guest']['totalCustomers']; ?></h3>
                  <p>Total Customer</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                 <?= $this->Html->link(__('View Details'), ['action' => 'guests','type'=>'total-guest'],['class'=>'small-box-footer']) ?>
            <!--     <a href="" class="small-box-footer">View Details  <i class="fa fa-arrow-circle-right"></i></a> -->
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?= $dashboard['guest']['activeCustomers'] ?></h3>
                  <p>Active Customer</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <?= $this->Html->link(__('View Details'), ['action' => 'guests','type'=>'active_guest'],['class'=>'small-box-footer']) ?>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                   <h3><?= $dashboard['guest']['inactiveCustomers'] ?></h3>
                  <p>Inactive Customer</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <?= $this->Html->link(__('View Details'), ['action' => 'guests','type'=>'inactive_guest'],['class'=>'small-box-footer']) ?>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                   <h3><?= $dashboard['guest']['joinedThisWeek'] ?></h3>
                  <p>Joined This Week</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <?= $this->Html->link(__('View Details'), ['action' => 'guests','type'=>'joinedThisWeek'],['class'=>'small-box-footer']) ?>
              </div>
            </div><!-- ./col -->
             <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                   <h3><?= $dashboard['guest']['joinedThisMonth'] ?></h3>
                  <p>Joined This Month</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <?= $this->Html->link(__('View Details'), ['action' => 'guests','type'=>'joinedThisMonth'],['class'=>'small-box-footer']) ?>
              </div>
            </div><!-- ./col -->

             <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                   <h3><?= $dashboard['guest']['joinedThisYear'] ?></h3>
                  <p>Joined This Year</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
              <?= $this->Html->link(__('View Details'), ['action' => 'guests','type'=>'joinedThisYear'],['class'=>'small-box-footer']) ?>
              </div>
            </div><!-- ./col -->

          </div><!-- /.row -->
        </section><!-- /.content -->



