 <head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->css(array('bootstrap/css/bootstrap.min.css','https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css','dist/css/AdminLTE.min.css', 'dist/css/skins/_all-skins.min.css','plugins/iCheck/flat/blue.css','plugins/morris/morris.css','plugins/jvectormap/jquery-jvectormap-1.2.2.css',
        'plugins/daterangepicker/daterangepicker-bs3.css', 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')); ?>
    <?= $this->Html->meta('icon') ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body style="background: grey"> 
<div class="login-box">
      <div class="login-box-body">
      <?php echo $this->Flash->render(); ?>  
         <?php if($userData) { ?>
         <?= $this->Form->create($user, ['class' => 'form-horizontal', 'id' => 'reset-password-form']) ?>
          <div class="form-group has-feedback">
            <label>Password</label>
             <?= $this->Form->control('password', ['class' => 'form-control', 'placeholder' => 'Password', 'label' => false]); ?>
          </div>
          <div class="form-group has-feedback">
             <label>Confirm Password</label>
               <?= $this->Form->control('confirm_password', ['type' => 'password','class' => 'form-control', 'placeholder' => 'Confirm Password', 'label' => false]); ?>
          </div>
          <div class="row">
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary ">Reset Password</button>
            </div><!-- /.col -->
          </div>
        </form>
      <?php echo $this->Form->end(); ?>
      <?php } ?>

      </div><!-- /.login-box-body -->
</div><!-- /.login-box -->
</body> 


                    